class ApplicationController < ActionController::Base
  # Prevent CSRF attacks, except for JSON requests (API clients)
  protect_from_forgery unless: -> { request.format.json? }
  
  # protect_from_forgery with: :exception
  # protect_from_forgery with: false # (after rails 5.2.0)  write this to solve: Rails 5 ActionController::InvalidAuthenticityToken error
  
  # Require authentication and do not set a session cookie for JSON requests (API clients)
  # before_action :authenticate_user!, :do_not_set_cookie, if: -> { request.format.json? }

  # This is very good. we are authenticating everything.!
  include Authenticatable
  # binding.pry
  # before_action :authenticate_account!
  before_action :authenticate
  
  
  private

  # Do not generate a session or session ID cookie
  # See https://github.com/rack/rack/blob/master/lib/rack/session/abstract/id.rb#L171
  def do_not_set_cookie
    request.session_options[:skip] = true
  end
end
