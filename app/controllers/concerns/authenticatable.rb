module Authenticatable
  def authenticate

    # In Authenticate we set current user only!
    header_token  = request.headers['Authorization']
    header_email = request.headers["HTTP_AUTHORIZATION_EMAIL"]
    header_main_company = request.headers["HTTP_AUTHORIZATION_MAIN_COMPANY"]
    header_current_company = request.headers["HTTP_AUTHORIZATION_CURRENT_COMPANY"]
    if header_token.present? && header_email.present?
      pattern = /^Bearer /
      token = header_token.gsub(pattern, '') if header_token && header_token.match(pattern)
      main_company = header_main_company.gsub(pattern, '') if header_main_company && header_main_company.match(pattern)
      current_company = header_current_company.gsub(pattern, '') if header_current_company && header_current_company.match(pattern)
      email = header_email.gsub(pattern, '') if header_email && header_email.match(pattern)

      if token.present? && email.present? && main_company.present?
        # 1. Switch To Tenenent if Exist
        customer = User::Customer.find_by_company_slug(main_company)
        if !customer
          return render_errors
        end
        # 3. switch to new tenant
        Apartment::Tenant.switch!(main_company)
        # 4. Find UserName
        # 5. Check Token
        # 6. Set Current User
        # 7. Set Current Company
        resource = User::Account.find_by_email(email)
        token_issuer = Tiddle::TokenIssuer.build
        token = token_issuer.find_token(resource, token)

        if token && unexpired?(token)
          touch_token(token)
          @current_user = resource
          @current_token = token
          @main_company = User::Company.find_by_slug(main_company)
          @current_company = current_company ? User::Company.find_by_slug(current_company) : @main_company
        end

        if @current_user.nil?
          render_errors
        end
      end
    end
  end

  def main_company
    @main_company
  end
  def current_company
    @current_company
  end
  def current_user
    @current_user
  end
  def current_token
    @current_token
  end

  # lib/tiddle/token_issuer.rb
  def find_token(resource, token_from_headers)
    token_class = User::AuthenticationToken
    token_body = Devise.token_generator.digest(token_class, :body, token_from_headers)
    resource.authentication_tokens.find_by(body: token_body)
  end

  def touch_token(token)
    token.update_attribute(:last_used_at, Time.current) if token.last_used_at < 1.hour.ago
  end

  def unexpired?(token)
    return true unless token.respond_to?(:expires_in)
    return true if token.expires_in.blank? || token.expires_in.zero?

    Time.current <= token.last_used_at + token.expires_in
  end


  def render_errors
    if params[:query].present?
      return render json: {data: {field: 'base', messages: "Auth token is invalid"} }
    else
      # Execute multi queries
      # Apollo sends the params in a _json variable when batching is enabled
      # see the Apollo Documentation about query batching: http://dev.apollodata.com/core/network.html#query-batching
      queries_params = params[:_json]
      queries_data = queries_params.map do |param|
        {
          "errors" => [
            { "message" => "Something went wrong" },
          ],
        }
      end
      return render json: queries_data
    end
  end

end

# FieldError.error("Auth token is invalid")