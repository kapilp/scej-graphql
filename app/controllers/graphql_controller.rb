# Read http://graphql-ruby.org/queries/executing_queries
# http://graphql-ruby.org/queries/multiplex.html

# Apollo Query Batching
class GraphqlController < ApplicationController
  def create
    if params[:query].present?
      # Execute one query
      query_string, variables  = params[:query], params[:variables]
      result = execute(query_string, variables)
      # operation_name = params[:operationName]
    else
      # Execute multi queries
      # Apollo sends the params in a _json variable when batching is enabled
      # see the Apollo Documentation about query batching: http://dev.apollodata.com/core/network.html#query-batching
      queries_params = params[:_json]
      result = multiplex(queries_params)
    end

    render json: result
  end

  private
  # Execute one query
  def execute(query_string, variables)
    puts GraphQLFormatter.new(query_string) if Rails.env.development?
    result = ScejGraphqlSchema.execute(
      query_string,
      operation_name: params[:operationName],
      variables: ensure_hash(variables),
      context: {
        request: request,
        main_company: main_company,
        current_company: current_company,
        current_user: current_user,
        current_token: current_token
      })
  end

  # Execute multi queries
  def multiplex(queries_params)
    if Rails.env.development?
      queries_params.each { |query| puts GraphQLFormatter.new(query[:query]) }
    end

    queries = queries_params.map do |param|
      {
        query: param[:query],
        operation_name: param[:operationName],
        variables: ensure_hash(param[:variables]),
        context: {
          request: request,
          main_company: main_company,
          current_company: current_company,
          current_user: current_user,
          current_token: current_token,
          optics_agent: (Rails.env.production? ? request.env[:optics_agent].with_document(param[:query]) : nil)
        }
      }
    end

    ScejGraphqlSchema.multiplex(queries)
  end





















  private

  # Handle form data, JSON body, or a blank value
  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end
end

# ============howtographql==============================
# def execute
# ...
# rescue => e
#   raise e unless Rails.env.development?
#   handle_error_in_development e
# end

# def context
#   {
#       session: session,
#       current_user: AuthToken.user_from_token(session[:token])
#   }
# end

# def handle_error_in_development(e)
#   logger.error e.message
#   logger.error e.backtrace.join("\n")
#
#   render json: { error: { message: e.message, backtrace: e.backtrace }, data: {} }, status: 500
# end
