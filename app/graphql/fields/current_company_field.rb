CurrentCompanyField = GraphQL::Field.define do
  name('currentCompany')
  type(Types::UserCompanyType)
  description 'fetch the current logged in company.'
  resolve ->(object, args, ctx) {
    # binding.pry
    ctx[:current_company]
  }
end