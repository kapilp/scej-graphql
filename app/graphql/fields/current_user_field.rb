CurrentUserField = GraphQL::Field.define do
  name('currentUser')
  type(Types::UserAccountType)
  description 'fetch the current logged in user.'
  resolve ->(object, args, ctx) {
    # binding.pry
    ctx[:current_user]
  }
end