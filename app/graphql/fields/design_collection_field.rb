DesignCollectionField = GraphQL::Field.define do
  name('designCollection')
  type(Types::DesignCollectionType)
  argument :id, types.ID
  description 'fetch a Collection by id'
  resolve ->(object, args, ctx) {
    Design::Collection.find(args[:id])
  }
end