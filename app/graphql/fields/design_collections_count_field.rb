DesignCollectionsCountField = GraphQL::Field.define do
  name('designCollectionsCount')
  type types.Int
  description 'Number of Collections'
  resolve ->(_object, args, ctx) {
    Design::Collection.count
  }
end