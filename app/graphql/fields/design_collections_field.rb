DesignCollectionsField = GraphQL::Field.define do
  name('designCollections')
  type(Types::DesignCollectionType.connection_type)
  description 'Collection connection to fetch paginated Collections collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :all, types.String, default_value: ''

  resolve ->(_obj, args, ctx) {
     # collections = Design::Collection.limit(args[:limit].to_i)
    collections = Design::Collection.all
      collections = collections.(args[:all]) if args[:all] && !args[:all].blank?
    collections
  }
end