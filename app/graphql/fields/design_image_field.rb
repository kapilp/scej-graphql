DesignImageField = GraphQL::Field.define do
  name('designImage')
  type(Types::DesignImageType)
  argument :id, types.ID
  description 'fetch a Image by id'
  resolve ->(object, args, ctx) {
    Design::Image.find(args[:id])
  }
end