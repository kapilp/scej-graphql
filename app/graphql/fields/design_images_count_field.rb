DesignImagesCountField = GraphQL::Field.define do
  name('designImagesCount')
  type types.Int
  description 'Number of Images'
  resolve ->(_object, args, ctx) {
    Design::Image.count
  }
end