DesignImagesField = GraphQL::Field.define do
  name('designImages')
  type(Types::DesignImageType.connection_type)
  description 'Image connection to fetch paginated Images collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # images = Design::Image.limit(args[:limit].to_i)
    images = Design::Image.all
    images
  }
end