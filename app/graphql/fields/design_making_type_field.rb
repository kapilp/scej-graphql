DesignMakingTypeField = GraphQL::Field.define do
  name('designMakingType')
  type(Types::DesignMakingTypeType)
  argument :id, types.ID
  description 'fetch a MakingType by id'
  resolve ->(object, args, ctx) {
    Design::MakingType.find(args[:id])
  }
end