DesignMakingTypesCountField = GraphQL::Field.define do
  name('designMakingTypesCount')
  type types.Int
  description 'Number of MakingTypes'
  resolve ->(_object, args, ctx) {
    Design::MakingType.count
  }
end