DesignMakingTypesField = GraphQL::Field.define do
  name('designMakingTypes')
  type(Types::DesignMakingTypeType.connection_type)
  description 'MakingType connection to fetch paginated MakingTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # makingTypes = Design::MakingType.limit(args[:limit].to_i)
    makingTypes = Design::MakingType.all
    makingTypes
  }
end