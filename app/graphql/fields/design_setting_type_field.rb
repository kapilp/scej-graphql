DesignSettingTypeField = GraphQL::Field.define do
  name('designSettingType')
  type(Types::DesignSettingTypeType)
  argument :id, types.ID
  description 'fetch a SettingType by id'
  resolve ->(object, args, ctx) {
    Design::SettingType.find(args[:id])
  }
end