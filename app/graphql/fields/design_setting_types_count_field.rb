DesignSettingTypesCountField = GraphQL::Field.define do
  name('designSettingTypesCount')
  type types.Int
  description 'Number of SettingTypes'
  resolve ->(_object, args, ctx) {
    Design::SettingType.count
  }
end