DesignSettingTypesField = GraphQL::Field.define do
  name('designSettingTypes')
  type(Types::DesignSettingTypeType.connection_type)
  description 'SettingType connection to fetch paginated SettingTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # settingTypes = Design::SettingType.limit(args[:limit].to_i)
    settingTypes = Design::SettingType.all
    settingTypes
  }
end