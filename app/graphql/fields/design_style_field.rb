DesignStyleField = GraphQL::Field.define do
  name('designStyle')
  type(Types::DesignStyleType)
  argument :id, types.ID
  description 'fetch a Style by id'
  resolve ->(object, args, ctx) {
    Design::StyleView.find(args[:id])
  }
end