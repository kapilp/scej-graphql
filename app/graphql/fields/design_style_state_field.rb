DesignStyleStateField = GraphQL::Field.define do
  name('designStylesState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Design::Style.find(args[:id]).state_machine.current_state
  }
end