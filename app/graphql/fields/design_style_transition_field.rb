DesignStyleTransitionField = GraphQL::Field.define do
  name('designStyleTransition')
  type(Types::DesignStyleTransitionType)
  argument :id, types.ID
  description 'fetch a StyleTransition by id'
  resolve ->(object, args, ctx) {
    Design::StyleTransition.find(args[:id])
  }
end