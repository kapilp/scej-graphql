DesignStyleTransitionsCountField = GraphQL::Field.define do
  name('designStyleTransitionsCount')
  type types.Int
  description 'Number of StyleTransitions'
  resolve ->(_object, args, ctx) {
    Design::StyleTransition.count
  }
end