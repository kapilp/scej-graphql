DesignStyleTransitionsField = GraphQL::Field.define do
  name('designStyleTransitions')
  type(Types::DesignStyleTransitionType.connection_type)
  description 'StyleTransition connection to fetch paginated StyleTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # styleTransitions = Design::StyleTransition.limit(args[:limit].to_i)
    styleTransitions = Design::StyleTransition.all
    styleTransitions
  }
end