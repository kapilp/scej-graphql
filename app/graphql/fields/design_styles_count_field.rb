DesignStylesCountField = GraphQL::Field.define do
  name('designStylesCount')
  type types.Int
  description 'Number of Styles'
  resolve ->(_object, args, ctx) {
    Design::StyleView.count
  }
end