DesignStylesField = GraphQL::Field.define do
  name('designStyles')
  type(Types::DesignStyleType.connection_type)
  description 'Style connection to fetch paginated Styles collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # styles = Design::StyleView.limit(args[:limit].to_i)
    styles = Design::StyleView.all
    styles
  }
end