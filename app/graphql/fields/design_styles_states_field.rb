DesignStylesStatesField = GraphQL::Field.define do
  name('designStylesStates')
  type types[types.String]
  description 'All Styles States.'
    

    resolve ->(_obj, args, ctx) {
      styles = Design::StyleStateMachine.states
      styles
  }
end