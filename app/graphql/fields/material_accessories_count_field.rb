MaterialAccessoriesCountField = GraphQL::Field.define do
  name('materialAccessoriesCount')
  type types.Int
  description 'Number of Accessories'
  resolve ->(_object, args, ctx) {
    Material::Accessory.count
  }
end