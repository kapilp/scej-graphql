MaterialAccessoriesField = GraphQL::Field.define do
  name('materialAccessories')
  type(Types::MaterialAccessoryType.connection_type)
  description 'Accessory connection to fetch paginated Accessories collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # accessories = Material::Accessory.limit(args[:limit].to_i)
    accessories = Material::Accessory.all
    accessories
  }
end