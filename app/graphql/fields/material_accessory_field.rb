MaterialAccessoryField = GraphQL::Field.define do
  name('materialAccessory')
  type(Types::MaterialAccessoryType)
  argument :id, types.ID
  description 'fetch a Accessory by id'
  resolve ->(object, args, ctx) {
    Material::Accessory.find(args[:id])
  }
end