MaterialColorField = GraphQL::Field.define do
  name('materialColor')
  type(Types::MaterialColorType)
  argument :id, types.ID
  description 'fetch a Color by id'
  resolve ->(object, args, ctx) {
    Material::Color.find(args[:id])
  }
end