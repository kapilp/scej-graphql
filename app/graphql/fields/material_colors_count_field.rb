MaterialColorsCountField = GraphQL::Field.define do
  name('materialColorsCount')
  type types.Int
  description 'Number of Colors'
  resolve ->(_object, args, ctx) {
    Material::Color.count
  }
end