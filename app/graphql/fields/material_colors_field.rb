MaterialColorsField = GraphQL::Field.define do
  name('materialColors')
  type(Types::MaterialColorType.connection_type)
  description 'Color connection to fetch paginated Colors collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

  resolve ->(_obj, args, ctx) {
     # colors = Material::Color.limit(args[:limit].to_i)
    colors = Material::Color.all
      colors = colors.by_material(args[:f_material_id]) if args[:f_material_id]
    colors
  }
end