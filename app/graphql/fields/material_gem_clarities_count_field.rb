MaterialGemClaritiesCountField = GraphQL::Field.define do
  name('materialGemClaritiesCount')
  type types.Int
  description 'Number of GemClarities'
  resolve ->(_object, args, ctx) {
    Material::GemClarity.count
  }
end