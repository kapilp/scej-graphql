MaterialGemClaritiesField = GraphQL::Field.define do
  name('materialGemClarities')
  type(Types::MaterialGemClarityType.connection_type)
  description 'GemClarity connection to fetch paginated GemClarities collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

  resolve ->(_obj, args, ctx) {
     # gemClarities = Material::GemClarity.limit(args[:limit].to_i)
    gemClarities = Material::GemClarity.all
      gemClarities = gemClarities.by_material(args[:f_material_id]) if args[:f_material_id]
    gemClarities
  }
end