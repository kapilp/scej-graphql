MaterialGemClarityField = GraphQL::Field.define do
  name('materialGemClarity')
  type(Types::MaterialGemClarityType)
  argument :id, types.ID
  description 'fetch a GemClarity by id'
  resolve ->(object, args, ctx) {
    Material::GemClarity.find(args[:id])
  }
end