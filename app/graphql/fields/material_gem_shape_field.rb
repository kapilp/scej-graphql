MaterialGemShapeField = GraphQL::Field.define do
  name('materialGemShape')
  type(Types::MaterialGemShapeType)
  argument :id, types.ID
  description 'fetch a GemShape by id'
  resolve ->(object, args, ctx) {
    Material::GemShape.find(args[:id])
  }
end