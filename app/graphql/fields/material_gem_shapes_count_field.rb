MaterialGemShapesCountField = GraphQL::Field.define do
  name('materialGemShapesCount')
  type types.Int
  description 'Number of GemShapes'
  resolve ->(_object, args, ctx) {
    Material::GemShape.count
  }
end