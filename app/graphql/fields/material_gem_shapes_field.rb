MaterialGemShapesField = GraphQL::Field.define do
  name('materialGemShapes')
  type(Types::MaterialGemShapeType.connection_type)
  description 'GemShape connection to fetch paginated GemShapes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

  resolve ->(_obj, args, ctx) {
     # gemShapes = Material::GemShape.limit(args[:limit].to_i)
    gemShapes = Material::GemShape.all
      gemShapes = gemShapes.by_material(args[:f_material_id]) if args[:f_material_id]
    gemShapes
  }
end