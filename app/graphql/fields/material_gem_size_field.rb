MaterialGemSizeField = GraphQL::Field.define do
  name('materialGemSize')
  type(Types::MaterialGemSizeType)
  argument :id, types.ID
  description 'fetch a GemSize by id'
  resolve ->(object, args, ctx) {
    Material::GemSize.find(args[:id])
  }
end