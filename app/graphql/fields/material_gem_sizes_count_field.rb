MaterialGemSizesCountField = GraphQL::Field.define do
  name('materialGemSizesCount')
  type types.Int
  description 'Number of GemSizes'
  resolve ->(_object, args, ctx) {
    Material::GemSize.count
  }
end