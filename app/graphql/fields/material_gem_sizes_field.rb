MaterialGemSizesField = GraphQL::Field.define do
  name('materialGemSizes')
  type(Types::MaterialGemSizeType.connection_type)
  description 'GemSize connection to fetch paginated GemSizes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID
    argument :f_gem_shape_id, types.ID
    argument :f_gem_clarity_id, types.ID
    argument :page, types.Int, default_value: 0
    argument :per, types.Int, default_value: 0
    argument :order, types.String, default_value: 'created_at DESC'

  resolve ->(_obj, args, ctx) {
     # gemSizes = Material::GemSize.limit(args[:limit].to_i)
    gemSizes = Material::GemSize.all
      gemSizes = gemSizes.by_filter(args[:f_material_id], args[:f_gem_shape_id]) if args[:f_material_id] && args[:f_gem_shape_id]
      gemSizes = gemSizes.gem_clarity(args[:f_gem_clarity_id]) if args[:f_gem_clarity_id]
      gemSizes = gemSizes.page(args[:page]) if args[:page] && args[:page] > 0
      gemSizes = gemSizes.per(args[:per]) if args[:per] && args[:per] > 0
      gemSizes = gemSizes.order(args[:order]) if args[:order] && !args[:order].blank?
    gemSizes
  }
end