MaterialGemTypeField = GraphQL::Field.define do
  name('materialGemType')
  type(Types::MaterialGemTypeType)
  argument :id, types.ID
  description 'fetch a GemType by id'
  resolve ->(object, args, ctx) {
    Material::GemType.find(args[:id])
  }
end