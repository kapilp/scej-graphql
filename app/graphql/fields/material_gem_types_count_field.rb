MaterialGemTypesCountField = GraphQL::Field.define do
  name('materialGemTypesCount')
  type types.Int
  description 'Number of GemTypes'
  resolve ->(_object, args, ctx) {
    Material::GemType.count
  }
end