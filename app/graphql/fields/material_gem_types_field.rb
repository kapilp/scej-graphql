MaterialGemTypesField = GraphQL::Field.define do
  name('materialGemTypes')
  type(Types::MaterialGemTypeType.connection_type)
  description 'GemType connection to fetch paginated GemTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # gemTypes = Material::GemType.limit(args[:limit].to_i)
    gemTypes = Material::GemType.all
    gemTypes
  }
end