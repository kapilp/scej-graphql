MaterialLockerField = GraphQL::Field.define do
  name('materialLocker')
  type(Types::MaterialLockerType)
  argument :id, types.ID
  description 'fetch a Locker by id'
  resolve ->(object, args, ctx) {
    Material::Locker.find(args[:id])
  }
end