MaterialLockersCountField = GraphQL::Field.define do
  name('materialLockersCount')
  type types.Int
  description 'Number of Lockers'
  resolve ->(_object, args, ctx) {
    Material::Locker.count
  }
end