MaterialLockersField = GraphQL::Field.define do
  name('materialLockers')
  type(Types::MaterialLockerType.connection_type)
  description 'Locker connection to fetch paginated Lockers collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_company_id, types.ID

  resolve ->(_obj, args, ctx) {
     # lockers = Material::Locker.limit(args[:limit].to_i)
    lockers = Material::Locker.all
      lockers = lockers.by_company(args[:f_company_id]) if args[:f_company_id]
    lockers
  }
end