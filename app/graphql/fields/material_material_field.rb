MaterialMaterialField = GraphQL::Field.define do
  name('materialMaterial')
  type(Types::MaterialMaterialType)
  argument :id, types.ID
  description 'fetch a Material by id'
  resolve ->(object, args, ctx) {
    Material::Material.find(args[:id])
  }
end