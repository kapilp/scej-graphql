MaterialMaterialTypeField = GraphQL::Field.define do
  name('materialMaterialType')
  type(Types::MaterialMaterialTypeType)
  argument :id, types.ID
  description 'fetch a MaterialType by id'
  resolve ->(object, args, ctx) {
    Material::MaterialType.find(args[:id])
  }
end