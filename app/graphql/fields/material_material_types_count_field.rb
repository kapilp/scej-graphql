MaterialMaterialTypesCountField = GraphQL::Field.define do
  name('materialMaterialTypesCount')
  type types.Int
  description 'Number of MaterialTypes'
  resolve ->(_object, args, ctx) {
    Material::MaterialType.count
  }
end