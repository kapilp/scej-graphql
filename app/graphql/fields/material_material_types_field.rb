MaterialMaterialTypesField = GraphQL::Field.define do
  name('materialMaterialTypes')
  type(Types::MaterialMaterialTypeType.connection_type)
  description 'MaterialType connection to fetch paginated MaterialTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # materialTypes = Material::MaterialType.limit(args[:limit].to_i)
    materialTypes = Material::MaterialType.all
    materialTypes
  }
end