MaterialMaterialsCountField = GraphQL::Field.define do
  name('materialMaterialsCount')
  type types.Int
  description 'Number of Materials'
  resolve ->(_object, args, ctx) {
    Material::Material.count
  }
end