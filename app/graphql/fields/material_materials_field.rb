MaterialMaterialsField = GraphQL::Field.define do
  name('materialMaterials')
  type(Types::MaterialMaterialType.connection_type)
  description 'Material connection to fetch paginated Materials collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_m_type_id, types.ID
    argument :f_m_type_string, types.String, default_value: ''

  resolve ->(_obj, args, ctx) {
     # materials = Material::Material.limit(args[:limit].to_i)
    materials = Material::Material.all
      materials = materials.by_material_type(args[:f_m_type_id]) if args[:f_m_type_id]
      materials = materials.by_material_type_slug(args[:f_m_type_string]) if args[:f_m_type_string] && !args[:f_m_type_string].blank?
    materials
  }
end