MaterialMetalPuritiesCountField = GraphQL::Field.define do
  name('materialMetalPuritiesCount')
  type types.Int
  description 'Number of MetalPurities'
  resolve ->(_object, args, ctx) {
    Material::MetalPurity.count
  }
end