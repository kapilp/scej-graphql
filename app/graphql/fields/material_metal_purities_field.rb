MaterialMetalPuritiesField = GraphQL::Field.define do
  name('materialMetalPurities')
  type(Types::MaterialMetalPurityType.connection_type)
  description 'MetalPurity connection to fetch paginated MetalPurities collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

  resolve ->(_obj, args, ctx) {
     # metalPurities = Material::MetalPurity.limit(args[:limit].to_i)
    metalPurities = Material::MetalPurity.all
      metalPurities = metalPurities.by_material(args[:f_material_id]) if args[:f_material_id]
    metalPurities
  }
end