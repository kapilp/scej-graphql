MaterialMetalPurityField = GraphQL::Field.define do
  name('materialMetalPurity')
  type(Types::MaterialMetalPurityType)
  argument :id, types.ID
  description 'fetch a MetalPurity by id'
  resolve ->(object, args, ctx) {
    Material::MetalPurity.find(args[:id])
  }
end