MaterialRateOnField = GraphQL::Field.define do
  name('materialRateOn')
  type(Types::MaterialRateOnType)
  argument :id, types.ID
  description 'fetch a RateOn by id'
  resolve ->(object, args, ctx) {
    Material::RateOn.find(args[:id])
  }
end