MaterialRateOnsCountField = GraphQL::Field.define do
  name('materialRateOnsCount')
  type types.Int
  description 'Number of RateOns'
  resolve ->(_object, args, ctx) {
    Material::RateOn.count
  }
end