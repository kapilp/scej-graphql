MaterialRateOnsField = GraphQL::Field.define do
  name('materialRateOns')
  type(Types::MaterialRateOnType.connection_type)
  description 'RateOn connection to fetch paginated RateOns collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_m_type_id, types.ID

  resolve ->(_obj, args, ctx) {
     # rateOns = Material::RateOn.limit(args[:limit].to_i)
    rateOns = Material::RateOn.all
      rateOns = rateOns.by_material_type(args[:f_m_type_id]) if args[:f_m_type_id]
    rateOns
  }
end