MfgCastingField = GraphQL::Field.define do
  name('mfgCasting')
  type(Types::MfgCastingType)
  argument :id, types.ID
  description 'fetch a Casting by id'
  resolve ->(object, args, ctx) {
    Mfg::CastingView.find(args[:id])
  }
end