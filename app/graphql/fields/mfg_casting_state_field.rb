MfgCastingStateField = GraphQL::Field.define do
  name('mfgCastingsState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Mfg::Casting.find(args[:id]).state_machine.current_state
  }
end