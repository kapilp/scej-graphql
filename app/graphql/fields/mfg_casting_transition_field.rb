MfgCastingTransitionField = GraphQL::Field.define do
  name('mfgCastingTransition')
  type(Types::MfgCastingTransitionType)
  argument :id, types.ID
  description 'fetch a CastingTransition by id'
  resolve ->(object, args, ctx) {
    Mfg::CastingTransition.find(args[:id])
  }
end