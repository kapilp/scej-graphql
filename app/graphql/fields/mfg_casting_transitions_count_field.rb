MfgCastingTransitionsCountField = GraphQL::Field.define do
  name('mfgCastingTransitionsCount')
  type types.Int
  description 'Number of CastingTransitions'
  resolve ->(_object, args, ctx) {
    Mfg::CastingTransition.count
  }
end