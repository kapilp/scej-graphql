MfgCastingTransitionsField = GraphQL::Field.define do
  name('mfgCastingTransitions')
  type(Types::MfgCastingTransitionType.connection_type)
  description 'CastingTransition connection to fetch paginated CastingTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # castingTransitions = Mfg::CastingTransition.limit(args[:limit].to_i)
    castingTransitions = Mfg::CastingTransition.all
    castingTransitions
  }
end