MfgCastingsCountField = GraphQL::Field.define do
  name('mfgCastingsCount')
  type types.Int
  description 'Number of Castings'
  resolve ->(_object, args, ctx) {
    Mfg::CastingView.count
  }
end