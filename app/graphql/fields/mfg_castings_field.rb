MfgCastingsField = GraphQL::Field.define do
  name('mfgCastings')
  type(Types::MfgCastingType.connection_type)
  description 'Casting connection to fetch paginated Castings collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # castings = Mfg::CastingView.limit(args[:limit].to_i)
    castings = Mfg::CastingView.all
    castings
  }
end