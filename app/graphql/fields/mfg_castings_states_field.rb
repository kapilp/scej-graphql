MfgCastingsStatesField = GraphQL::Field.define do
  name('mfgCastingsStates')
  type types[types.String]
  description 'All Castings States.'
    

    resolve ->(_obj, args, ctx) {
      castings = Mfg::CastingStateMachine.states
      castings
  }
end