MfgDepartmentField = GraphQL::Field.define do
  name('mfgDepartment')
  type(Types::MfgDepartmentType)
  argument :id, types.ID
  description 'fetch a Department by id'
  resolve ->(object, args, ctx) {
    Mfg::Department.find(args[:id])
  }
end