MfgDepartmentsCountField = GraphQL::Field.define do
  name('mfgDepartmentsCount')
  type types.Int
  description 'Number of Departments'
  resolve ->(_object, args, ctx) {
    Mfg::Department.count
  }
end