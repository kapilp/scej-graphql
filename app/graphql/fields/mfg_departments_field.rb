MfgDepartmentsField = GraphQL::Field.define do
  name('mfgDepartments')
  type(Types::MfgDepartmentType.connection_type)
  description 'Department connection to fetch paginated Departments collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # departments = Mfg::Department.limit(args[:limit].to_i)
    departments = Mfg::Department.all
    departments
  }
end