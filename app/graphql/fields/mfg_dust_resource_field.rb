MfgDustResourceField = GraphQL::Field.define do
  name('mfgDustResource')
  type(Types::MfgDustResourceType)
  argument :id, types.ID
  description 'fetch a DustResource by id'
  resolve ->(object, args, ctx) {
    Mfg::DustResource.find(args[:id])
  }
end