MfgDustResourcesCountField = GraphQL::Field.define do
  name('mfgDustResourcesCount')
  type types.Int
  description 'Number of DustResources'
  resolve ->(_object, args, ctx) {
    Mfg::DustResource.count
  }
end