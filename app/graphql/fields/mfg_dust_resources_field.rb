MfgDustResourcesField = GraphQL::Field.define do
  name('mfgDustResources')
  type(Types::MfgDustResourceType.connection_type)
  description 'DustResource connection to fetch paginated DustResources collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # dustResources = Mfg::DustResource.limit(args[:limit].to_i)
    dustResources = Mfg::DustResource.all
    dustResources
  }
end