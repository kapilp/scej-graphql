MfgMetalIssueField = GraphQL::Field.define do
  name('mfgMetalIssue')
  type(Types::MfgMetalIssueType)
  argument :id, types.ID
  description 'fetch a MetalIssue by id'
  resolve ->(object, args, ctx) {
    Mfg::MetalIssueView.find(args[:id])
  }
end