MfgMetalIssueStateField = GraphQL::Field.define do
  name('mfgMetalIssuesState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Mfg::MetalIssue.find(args[:id]).state_machine.current_state
  }
end