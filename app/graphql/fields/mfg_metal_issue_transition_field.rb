MfgMetalIssueTransitionField = GraphQL::Field.define do
  name('mfgMetalIssueTransition')
  type(Types::MfgMetalIssueTransitionType)
  argument :id, types.ID
  description 'fetch a MetalIssueTransition by id'
  resolve ->(object, args, ctx) {
    Mfg::MetalIssueTransition.find(args[:id])
  }
end