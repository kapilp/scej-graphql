MfgMetalIssueTransitionsCountField = GraphQL::Field.define do
  name('mfgMetalIssueTransitionsCount')
  type types.Int
  description 'Number of MetalIssueTransitions'
  resolve ->(_object, args, ctx) {
    Mfg::MetalIssueTransition.count
  }
end