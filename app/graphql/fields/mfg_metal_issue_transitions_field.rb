MfgMetalIssueTransitionsField = GraphQL::Field.define do
  name('mfgMetalIssueTransitions')
  type(Types::MfgMetalIssueTransitionType.connection_type)
  description 'MetalIssueTransition connection to fetch paginated MetalIssueTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # metalIssueTransitions = Mfg::MetalIssueTransition.limit(args[:limit].to_i)
    metalIssueTransitions = Mfg::MetalIssueTransition.all
    metalIssueTransitions
  }
end