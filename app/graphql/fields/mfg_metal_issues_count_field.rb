MfgMetalIssuesCountField = GraphQL::Field.define do
  name('mfgMetalIssuesCount')
  type types.Int
  description 'Number of MetalIssues'
  resolve ->(_object, args, ctx) {
    Mfg::MetalIssueView.count
  }
end