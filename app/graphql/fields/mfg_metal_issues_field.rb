MfgMetalIssuesField = GraphQL::Field.define do
  name('mfgMetalIssues')
  type(Types::MfgMetalIssueType.connection_type)
  description 'MetalIssue connection to fetch paginated MetalIssues collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # metalIssues = Mfg::MetalIssueView.limit(args[:limit].to_i)
    metalIssues = Mfg::MetalIssueView.all
    metalIssues
  }
end