MfgMetalIssuesStatesField = GraphQL::Field.define do
  name('mfgMetalIssuesStates')
  type types[types.String]
  description 'All MetalIssues States.'
    

    resolve ->(_obj, args, ctx) {
      metalIssues = Mfg::IssueFinishStateMachine.states
      metalIssues
  }
end