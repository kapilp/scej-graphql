MfgMfgTxnField = GraphQL::Field.define do
  name('mfgMfgTxn')
  type(Types::MfgMfgTxnType)
  argument :id, types.ID
  description 'fetch a MfgTxn by id'
  resolve ->(object, args, ctx) {
    Mfg::MfgTxnView.find(args[:id])
  }
end