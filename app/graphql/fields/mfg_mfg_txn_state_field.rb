MfgMfgTxnStateField = GraphQL::Field.define do
  name('mfgMfgTxnsState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Mfg::MfgTxn.find(args[:id]).state_machine.current_state
  }
end