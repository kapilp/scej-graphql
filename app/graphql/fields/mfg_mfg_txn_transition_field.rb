MfgMfgTxnTransitionField = GraphQL::Field.define do
  name('mfgMfgTxnTransition')
  type(Types::MfgMfgTxnTransitionType)
  argument :id, types.ID
  description 'fetch a MfgTxnTransition by id'
  resolve ->(object, args, ctx) {
    Mfg::MfgTxnTransition.find(args[:id])
  }
end