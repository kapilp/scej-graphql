MfgMfgTxnTransitionsCountField = GraphQL::Field.define do
  name('mfgMfgTxnTransitionsCount')
  type types.Int
  description 'Number of MfgTxnTransitions'
  resolve ->(_object, args, ctx) {
    Mfg::MfgTxnTransition.count
  }
end