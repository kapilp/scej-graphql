MfgMfgTxnTransitionsField = GraphQL::Field.define do
  name('mfgMfgTxnTransitions')
  type(Types::MfgMfgTxnTransitionType.connection_type)
  description 'MfgTxnTransition connection to fetch paginated MfgTxnTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # mfgTxnTransitions = Mfg::MfgTxnTransition.limit(args[:limit].to_i)
    mfgTxnTransitions = Mfg::MfgTxnTransition.all
    mfgTxnTransitions
  }
end