MfgMfgTxnsCountField = GraphQL::Field.define do
  name('mfgMfgTxnsCount')
  type types.Int
  description 'Number of MfgTxns'
  resolve ->(_object, args, ctx) {
    Mfg::MfgTxnView.count
  }
end