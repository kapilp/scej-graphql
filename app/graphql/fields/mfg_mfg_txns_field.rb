MfgMfgTxnsField = GraphQL::Field.define do
  name('mfgMfgTxns')
  type(Types::MfgMfgTxnType.connection_type)
  description 'MfgTxn connection to fetch paginated MfgTxns collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # mfgTxns = Mfg::MfgTxnView.limit(args[:limit].to_i)
    mfgTxns = Mfg::MfgTxnView.all
    mfgTxns
  }
end