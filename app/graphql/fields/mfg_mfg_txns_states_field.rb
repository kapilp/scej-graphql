MfgMfgTxnsStatesField = GraphQL::Field.define do
  name('mfgMfgTxnsStates')
  type types[types.String]
  description 'All MfgTxns States.'
    

    resolve ->(_obj, args, ctx) {
      mfgTxns = Mfg::MfgTxnStateMachine.states
      mfgTxns
  }
end