MfgPrioritiesCountField = GraphQL::Field.define do
  name('mfgPrioritiesCount')
  type types.Int
  description 'Number of Priorities'
  resolve ->(_object, args, ctx) {
    Mfg::Priority.count
  }
end