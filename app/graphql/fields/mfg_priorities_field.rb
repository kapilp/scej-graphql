MfgPrioritiesField = GraphQL::Field.define do
  name('mfgPriorities')
  type(Types::MfgPriorityType.connection_type)
  description 'Priority connection to fetch paginated Priorities collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # priorities = Mfg::Priority.limit(args[:limit].to_i)
    priorities = Mfg::Priority.all
    priorities
  }
end