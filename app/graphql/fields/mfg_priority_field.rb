MfgPriorityField = GraphQL::Field.define do
  name('mfgPriority')
  type(Types::MfgPriorityType)
  argument :id, types.ID
  description 'fetch a Priority by id'
  resolve ->(object, args, ctx) {
    Mfg::Priority.find(args[:id])
  }
end