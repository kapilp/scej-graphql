MfgProductTypeField = GraphQL::Field.define do
  name('mfgProductType')
  type(Types::MfgProductTypeType)
  argument :id, types.ID
  description 'fetch a ProductType by id'
  resolve ->(object, args, ctx) {
    Mfg::ProductType.find(args[:id])
  }
end