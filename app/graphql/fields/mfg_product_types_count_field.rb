MfgProductTypesCountField = GraphQL::Field.define do
  name('mfgProductTypesCount')
  type types.Int
  description 'Number of ProductTypes'
  resolve ->(_object, args, ctx) {
    Mfg::ProductType.count
  }
end