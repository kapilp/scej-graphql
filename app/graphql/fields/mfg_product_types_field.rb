MfgProductTypesField = GraphQL::Field.define do
  name('mfgProductTypes')
  type(Types::MfgProductTypeType.connection_type)
  description 'ProductType connection to fetch paginated ProductTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # productTypes = Mfg::ProductType.limit(args[:limit].to_i)
    productTypes = Mfg::ProductType.all
    productTypes
  }
end