MfgRefiningField = GraphQL::Field.define do
  name('mfgRefining')
  type(Types::MfgRefiningType)
  argument :id, types.ID
  description 'fetch a Refining by id'
  resolve ->(object, args, ctx) {
    Mfg::RefiningView.find(args[:id])
  }
end