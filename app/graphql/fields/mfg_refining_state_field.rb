MfgRefiningStateField = GraphQL::Field.define do
  name('mfgRefiningsState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Mfg::Refining.find(args[:id]).state_machine.current_state
  }
end