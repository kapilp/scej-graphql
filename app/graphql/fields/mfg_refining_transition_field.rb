MfgRefiningTransitionField = GraphQL::Field.define do
  name('mfgRefiningTransition')
  type(Types::MfgRefiningTransitionType)
  argument :id, types.ID
  description 'fetch a RefiningTransition by id'
  resolve ->(object, args, ctx) {
    Mfg::RefiningTransition.find(args[:id])
  }
end