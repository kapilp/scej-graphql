MfgRefiningTransitionsCountField = GraphQL::Field.define do
  name('mfgRefiningTransitionsCount')
  type types.Int
  description 'Number of RefiningTransitions'
  resolve ->(_object, args, ctx) {
    Mfg::RefiningTransition.count
  }
end