MfgRefiningTransitionsField = GraphQL::Field.define do
  name('mfgRefiningTransitions')
  type(Types::MfgRefiningTransitionType.connection_type)
  description 'RefiningTransition connection to fetch paginated RefiningTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # refiningTransitions = Mfg::RefiningTransition.limit(args[:limit].to_i)
    refiningTransitions = Mfg::RefiningTransition.all
    refiningTransitions
  }
end