MfgRefiningsCountField = GraphQL::Field.define do
  name('mfgRefiningsCount')
  type types.Int
  description 'Number of Refinings'
  resolve ->(_object, args, ctx) {
    Mfg::RefiningView.count
  }
end