MfgRefiningsField = GraphQL::Field.define do
  name('mfgRefinings')
  type(Types::MfgRefiningType.connection_type)
  description 'Refining connection to fetch paginated Refinings collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # refinings = Mfg::RefiningView.limit(args[:limit].to_i)
    refinings = Mfg::RefiningView.all
    refinings
  }
end