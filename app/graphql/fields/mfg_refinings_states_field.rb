MfgRefiningsStatesField = GraphQL::Field.define do
  name('mfgRefiningsStates')
  type types[types.String]
  description 'All Refinings States.'
    

    resolve ->(_obj, args, ctx) {
      refinings = Mfg::RefiningStateMachine.states
      refinings
  }
end