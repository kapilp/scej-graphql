MfgWaxSettingField = GraphQL::Field.define do
  name('mfgWaxSetting')
  type(Types::MfgWaxSettingType)
  argument :id, types.ID
  description 'fetch a WaxSetting by id'
  resolve ->(object, args, ctx) {
    Mfg::WaxSettingView.find(args[:id])
  }
end