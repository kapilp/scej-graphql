MfgWaxSettingStateField = GraphQL::Field.define do
  name('mfgWaxSettingsState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Mfg::WaxSetting.find(args[:id]).state_machine.current_state
  }
end