MfgWaxSettingTransitionField = GraphQL::Field.define do
  name('mfgWaxSettingTransition')
  type(Types::MfgWaxSettingTransitionType)
  argument :id, types.ID
  description 'fetch a WaxSettingTransition by id'
  resolve ->(object, args, ctx) {
    Mfg::WaxSettingTransition.find(args[:id])
  }
end