MfgWaxSettingTransitionsCountField = GraphQL::Field.define do
  name('mfgWaxSettingTransitionsCount')
  type types.Int
  description 'Number of WaxSettingTransitions'
  resolve ->(_object, args, ctx) {
    Mfg::WaxSettingTransition.count
  }
end