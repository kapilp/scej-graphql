MfgWaxSettingTransitionsField = GraphQL::Field.define do
  name('mfgWaxSettingTransitions')
  type(Types::MfgWaxSettingTransitionType.connection_type)
  description 'WaxSettingTransition connection to fetch paginated WaxSettingTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # waxSettingTransitions = Mfg::WaxSettingTransition.limit(args[:limit].to_i)
    waxSettingTransitions = Mfg::WaxSettingTransition.all
    waxSettingTransitions
  }
end