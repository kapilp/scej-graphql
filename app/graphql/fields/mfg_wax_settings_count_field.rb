MfgWaxSettingsCountField = GraphQL::Field.define do
  name('mfgWaxSettingsCount')
  type types.Int
  description 'Number of WaxSettings'
  resolve ->(_object, args, ctx) {
    Mfg::WaxSettingView.count
  }
end