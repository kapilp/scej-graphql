MfgWaxSettingsField = GraphQL::Field.define do
  name('mfgWaxSettings')
  type(Types::MfgWaxSettingType.connection_type)
  description 'WaxSetting connection to fetch paginated WaxSettings collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # waxSettings = Mfg::WaxSettingView.limit(args[:limit].to_i)
    waxSettings = Mfg::WaxSettingView.all
    waxSettings
  }
end