MfgWaxSettingsStatesField = GraphQL::Field.define do
  name('mfgWaxSettingsStates')
  type types[types.String]
  description 'All WaxSettings States.'
    

    resolve ->(_obj, args, ctx) {
      waxSettings = Mfg::IssueFinishStateMachine.states
      waxSettings
  }
end