SaleJobField = GraphQL::Field.define do
  name('saleJob')
  type(Types::SaleJobType)
  argument :id, types.ID
  description 'fetch a Job by id'
  resolve ->(object, args, ctx) {
    Sale::JobView.find(args[:id])
  }
end