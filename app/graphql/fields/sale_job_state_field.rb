SaleJobStateField = GraphQL::Field.define do
  name('saleJobsState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Sale::Job.find(args[:id]).state_machine.current_state
  }
end