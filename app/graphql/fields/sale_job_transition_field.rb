SaleJobTransitionField = GraphQL::Field.define do
  name('saleJobTransition')
  type(Types::SaleJobTransitionType)
  argument :id, types.ID
  description 'fetch a JobTransition by id'
  resolve ->(object, args, ctx) {
    Sale::JobTransition.find(args[:id])
  }
end