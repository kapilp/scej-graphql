SaleJobTransitionsCountField = GraphQL::Field.define do
  name('saleJobTransitionsCount')
  type types.Int
  description 'Number of JobTransitions'
  resolve ->(_object, args, ctx) {
    Sale::JobTransition.count
  }
end