SaleJobTransitionsField = GraphQL::Field.define do
  name('saleJobTransitions')
  type(Types::SaleJobTransitionType.connection_type)
  description 'JobTransition connection to fetch paginated JobTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # jobTransitions = Sale::JobTransition.limit(args[:limit].to_i)
    jobTransitions = Sale::JobTransition.all
    jobTransitions
  }
end