SaleJobsCountField = GraphQL::Field.define do
  name('saleJobsCount')
  type types.Int
  description 'Number of Jobs'
  resolve ->(_object, args, ctx) {
    Sale::JobView.count
  }
end