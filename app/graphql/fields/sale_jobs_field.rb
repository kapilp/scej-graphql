SaleJobsField = GraphQL::Field.define do
  name('saleJobs')
  type(Types::SaleJobType.connection_type)
  description 'Job connection to fetch paginated Jobs collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # jobs = Sale::JobView.limit(args[:limit].to_i)
    jobs = Sale::JobView.all
    jobs
  }
end