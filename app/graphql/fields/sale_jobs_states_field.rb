SaleJobsStatesField = GraphQL::Field.define do
  name('saleJobsStates')
  type types[types.String]
  description 'All Jobs States.'
    

    resolve ->(_obj, args, ctx) {
      jobs = Sale::JobStateMachine.states
      jobs
  }
end