SaleMStockField = GraphQL::Field.define do
  name('saleMStock')
  type(Types::SaleMStockType)
  argument :id, types.ID
  description 'fetch a MStock by id'
  resolve ->(object, args, ctx) {
    Sale::MStock.find(args[:id])
  }
end