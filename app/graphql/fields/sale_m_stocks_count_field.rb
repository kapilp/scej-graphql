SaleMStocksCountField = GraphQL::Field.define do
  name('saleMStocksCount')
  type types.Int
  description 'Number of MStocks'
  resolve ->(_object, args, ctx) {
    Sale::MStock.count
  }
end