SaleMStocksField = GraphQL::Field.define do
  name('saleMStocks')
  type(Types::SaleMStockType.connection_type)
  description 'MStock connection to fetch paginated MStocks collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_company_id, types.ID
    argument :f_locker_id, types.ID
    argument :f_accessory_id, types.ID
    argument :f_material_id, types.ID
    argument :f_purity_id, types.ID
    argument :f_color_id, types.ID
    argument :f_gem_shape_id, types.ID
    argument :f_gem_clarity_id, types.ID
    argument :f_gem_size_id, types.ID

  resolve ->(_obj, args, ctx) {
     # mStocks = Sale::MStock.limit(args[:limit].to_i)
    mStocks = Sale::MStock.all
      mStocks = mStocks.by_company(args[:f_company_id]) if args[:f_company_id]
      mStocks = mStocks.by_locker(args[:f_locker_id]) if args[:f_locker_id]
      mStocks = mStocks.by_accessory(args[:f_accessory_id]) if args[:f_accessory_id]
      mStocks = mStocks.by_material(args[:f_material_id]) if args[:f_material_id]
      mStocks = mStocks.by_metal_purity(args[:f_purity_id]) if args[:f_purity_id]
      mStocks = mStocks.by_color(args[:f_color_id]) if args[:f_color_id]
      mStocks = mStocks.by_gem_shape(args[:f_gem_shape_id]) if args[:f_gem_shape_id]
      mStocks = mStocks.by_gem_clarity(args[:f_gem_clarity_id]) if args[:f_gem_clarity_id]
      mStocks = mStocks.by_gem_size(args[:f_gem_size_id]) if args[:f_gem_size_id]
    mStocks
  }
end