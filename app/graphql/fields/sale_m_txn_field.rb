SaleMTxnField = GraphQL::Field.define do
  name('saleMTxn')
  type(Types::SaleMTxnType)
  argument :id, types.ID
  description 'fetch a MTxn by id'
  resolve ->(object, args, ctx) {
    Sale::MTxnView.find(args[:id])
  }
end