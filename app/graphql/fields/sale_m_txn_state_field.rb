SaleMTxnStateField = GraphQL::Field.define do
  name('saleMTxnsState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Sale::MTxn.find(args[:id]).state_machine.current_state
  }
end