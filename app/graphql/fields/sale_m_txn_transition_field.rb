SaleMTxnTransitionField = GraphQL::Field.define do
  name('saleMTxnTransition')
  type(Types::SaleMTxnTransitionType)
  argument :id, types.ID
  description 'fetch a MTxnTransition by id'
  resolve ->(object, args, ctx) {
    Sale::MTxnTransition.find(args[:id])
  }
end