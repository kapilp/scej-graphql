SaleMTxnTransitionsCountField = GraphQL::Field.define do
  name('saleMTxnTransitionsCount')
  type types.Int
  description 'Number of MTxnTransitions'
  resolve ->(_object, args, ctx) {
    Sale::MTxnTransition.count
  }
end