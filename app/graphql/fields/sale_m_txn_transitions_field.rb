SaleMTxnTransitionsField = GraphQL::Field.define do
  name('saleMTxnTransitions')
  type(Types::SaleMTxnTransitionType.connection_type)
  description 'MTxnTransition connection to fetch paginated MTxnTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # mTxnTransitions = Sale::MTxnTransition.limit(args[:limit].to_i)
    mTxnTransitions = Sale::MTxnTransition.all
    mTxnTransitions
  }
end