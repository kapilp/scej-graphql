SaleMTxnTypeField = GraphQL::Field.define do
  name('saleMTxnType')
  type(Types::SaleMTxnTypeType)
  argument :id, types.ID
  description 'fetch a MTxnType by id'
  resolve ->(object, args, ctx) {
    Sale::MTxnType.find(args[:id])
  }
end