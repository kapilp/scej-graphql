SaleMTxnTypesCountField = GraphQL::Field.define do
  name('saleMTxnTypesCount')
  type types.Int
  description 'Number of MTxnTypes'
  resolve ->(_object, args, ctx) {
    Sale::MTxnType.count
  }
end