SaleMTxnTypesField = GraphQL::Field.define do
  name('saleMTxnTypes')
  type(Types::SaleMTxnTypeType.connection_type)
  description 'MTxnType connection to fetch paginated MTxnTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # mTxnTypes = Sale::MTxnType.limit(args[:limit].to_i)
    mTxnTypes = Sale::MTxnType.all
    mTxnTypes
  }
end