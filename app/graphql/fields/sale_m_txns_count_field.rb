SaleMTxnsCountField = GraphQL::Field.define do
  name('saleMTxnsCount')
  type types.Int
  description 'Number of MTxns'
  resolve ->(_object, args, ctx) {
    Sale::MTxnView.count
  }
end