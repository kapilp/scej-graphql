SaleMTxnsField = GraphQL::Field.define do
  name('saleMTxns')
  type(Types::SaleMTxnType.connection_type)
  description 'MTxn connection to fetch paginated MTxns collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # mTxns = Sale::MTxnView.limit(args[:limit].to_i)
    mTxns = Sale::MTxnView.all
    mTxns
  }
end