SaleMTxnsStatesField = GraphQL::Field.define do
  name('saleMTxnsStates')
  type types[types.String]
  description 'All MTxns States.'
    

    resolve ->(_obj, args, ctx) {
      mTxns = Sale::MTxnStateMachine.states
      mTxns
  }
end