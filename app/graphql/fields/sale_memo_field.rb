SaleMemoField = GraphQL::Field.define do
  name('saleMemo')
  type(Types::SaleMemoType)
  argument :id, types.ID
  description 'fetch a Memo by id'
  resolve ->(object, args, ctx) {
    Sale::MemoView.find(args[:id])
  }
end