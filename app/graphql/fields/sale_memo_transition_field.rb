SaleMemoTransitionField = GraphQL::Field.define do
  name('saleMemoTransition')
  type(Types::SaleMemoTransitionType)
  argument :id, types.ID
  description 'fetch a MemoTransition by id'
  resolve ->(object, args, ctx) {
    Sale::MemoTransition.find(args[:id])
  }
end