SaleMemoTransitionsCountField = GraphQL::Field.define do
  name('saleMemoTransitionsCount')
  type types.Int
  description 'Number of MemoTransitions'
  resolve ->(_object, args, ctx) {
    Sale::MemoTransition.count
  }
end