SaleMemoTransitionsField = GraphQL::Field.define do
  name('saleMemoTransitions')
  type(Types::SaleMemoTransitionType.connection_type)
  description 'MemoTransition connection to fetch paginated MemoTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # memoTransitions = Sale::MemoTransition.limit(args[:limit].to_i)
    memoTransitions = Sale::MemoTransition.all
    memoTransitions
  }
end