SaleMemosCountField = GraphQL::Field.define do
  name('saleMemosCount')
  type types.Int
  description 'Number of Memos'
  resolve ->(_object, args, ctx) {
    Sale::MemoView.count
  }
end