SaleMemosField = GraphQL::Field.define do
  name('saleMemos')
  type(Types::SaleMemoType.connection_type)
  description 'Memo connection to fetch paginated Memos collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # memos = Sale::MemoView.limit(args[:limit].to_i)
    memos = Sale::MemoView.all
    memos
  }
end