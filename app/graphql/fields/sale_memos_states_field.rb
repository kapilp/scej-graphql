SaleMemosStatesField = GraphQL::Field.define do
  name('saleMemosStates')
  type types[types.String]
  description 'All Memos States.'
    

    resolve ->(_obj, args, ctx) {
      memos = Sale::MemoStateMachine.states
      memos
  }
end