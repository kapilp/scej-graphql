SaleOrderField = GraphQL::Field.define do
  name('saleOrder')
  type(Types::SaleOrderType)
  argument :id, types.ID
  description 'fetch a Order by id'
  resolve ->(object, args, ctx) {
    Sale::OrderView.find(args[:id])
  }
end