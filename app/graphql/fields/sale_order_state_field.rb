SaleOrderStateField = GraphQL::Field.define do
  name('saleOrdersState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Sale::Order.find(args[:id]).state_machine.current_state
  }
end