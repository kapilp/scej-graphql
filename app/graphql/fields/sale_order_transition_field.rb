SaleOrderTransitionField = GraphQL::Field.define do
  name('saleOrderTransition')
  type(Types::SaleOrderTransitionType)
  argument :id, types.ID
  description 'fetch a OrderTransition by id'
  resolve ->(object, args, ctx) {
    Sale::OrderTransition.find(args[:id])
  }
end