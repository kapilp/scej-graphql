SaleOrderTransitionsCountField = GraphQL::Field.define do
  name('saleOrderTransitionsCount')
  type types.Int
  description 'Number of OrderTransitions'
  resolve ->(_object, args, ctx) {
    Sale::OrderTransition.count
  }
end