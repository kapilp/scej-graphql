SaleOrderTransitionsField = GraphQL::Field.define do
  name('saleOrderTransitions')
  type(Types::SaleOrderTransitionType.connection_type)
  description 'OrderTransition connection to fetch paginated OrderTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # orderTransitions = Sale::OrderTransition.limit(args[:limit].to_i)
    orderTransitions = Sale::OrderTransition.all
    orderTransitions
  }
end