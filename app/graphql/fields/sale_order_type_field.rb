SaleOrderTypeField = GraphQL::Field.define do
  name('saleOrderType')
  type(Types::SaleOrderTypeType)
  argument :id, types.ID
  description 'fetch a OrderType by id'
  resolve ->(object, args, ctx) {
    Sale::OrderType.find(args[:id])
  }
end