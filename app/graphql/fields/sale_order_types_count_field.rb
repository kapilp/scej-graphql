SaleOrderTypesCountField = GraphQL::Field.define do
  name('saleOrderTypesCount')
  type types.Int
  description 'Number of OrderTypes'
  resolve ->(_object, args, ctx) {
    Sale::OrderType.count
  }
end