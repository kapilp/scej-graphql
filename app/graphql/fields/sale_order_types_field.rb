SaleOrderTypesField = GraphQL::Field.define do
  name('saleOrderTypes')
  type(Types::SaleOrderTypeType.connection_type)
  description 'OrderType connection to fetch paginated OrderTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # orderTypes = Sale::OrderType.limit(args[:limit].to_i)
    orderTypes = Sale::OrderType.all
    orderTypes
  }
end