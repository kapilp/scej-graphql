SaleOrdersCountField = GraphQL::Field.define do
  name('saleOrdersCount')
  type types.Int
  description 'Number of Orders'
  resolve ->(_object, args, ctx) {
    Sale::OrderView.count
  }
end