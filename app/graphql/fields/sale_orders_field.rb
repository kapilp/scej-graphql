SaleOrdersField = GraphQL::Field.define do
  name('saleOrders')
  type(Types::SaleOrderType.connection_type)
  description 'Order connection to fetch paginated Orders collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # orders = Sale::OrderView.limit(args[:limit].to_i)
    orders = Sale::OrderView.all
    orders
  }
end