SaleOrdersStatesField = GraphQL::Field.define do
  name('saleOrdersStates')
  type types[types.String]
  description 'All Orders States.'
    

    resolve ->(_obj, args, ctx) {
      orders = Sale::OrderStateMachine.states
      orders
  }
end