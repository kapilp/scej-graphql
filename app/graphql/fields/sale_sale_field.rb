SaleSaleField = GraphQL::Field.define do
  name('saleSale')
  type(Types::SaleSaleType)
  argument :id, types.ID
  description 'fetch a Sale by id'
  resolve ->(object, args, ctx) {
    Sale::SaleView.find(args[:id])
  }
end