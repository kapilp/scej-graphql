SaleSaleStateField = GraphQL::Field.define do
  name('saleSalesState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    Sale::Sale.find(args[:id]).state_machine.current_state
  }
end