SaleSaleTransitionField = GraphQL::Field.define do
  name('saleSaleTransition')
  type(Types::SaleSaleTransitionType)
  argument :id, types.ID
  description 'fetch a SaleTransition by id'
  resolve ->(object, args, ctx) {
    Sale::SaleTransition.find(args[:id])
  }
end