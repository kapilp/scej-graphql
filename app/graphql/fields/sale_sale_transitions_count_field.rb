SaleSaleTransitionsCountField = GraphQL::Field.define do
  name('saleSaleTransitionsCount')
  type types.Int
  description 'Number of SaleTransitions'
  resolve ->(_object, args, ctx) {
    Sale::SaleTransition.count
  }
end