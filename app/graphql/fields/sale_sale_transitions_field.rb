SaleSaleTransitionsField = GraphQL::Field.define do
  name('saleSaleTransitions')
  type(Types::SaleSaleTransitionType.connection_type)
  description 'SaleTransition connection to fetch paginated SaleTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # saleTransitions = Sale::SaleTransition.limit(args[:limit].to_i)
    saleTransitions = Sale::SaleTransition.all
    saleTransitions
  }
end