SaleSalesCountField = GraphQL::Field.define do
  name('saleSalesCount')
  type types.Int
  description 'Number of Sales'
  resolve ->(_object, args, ctx) {
    Sale::SaleView.count
  }
end