SaleSalesField = GraphQL::Field.define do
  name('saleSales')
  type(Types::SaleSaleType.connection_type)
  description 'Sale connection to fetch paginated Sales collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # sales = Sale::SaleView.limit(args[:limit].to_i)
    sales = Sale::SaleView.all
    sales
  }
end