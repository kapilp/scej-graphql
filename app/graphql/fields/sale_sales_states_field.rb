SaleSalesStatesField = GraphQL::Field.define do
  name('saleSalesStates')
  type types[types.String]
  description 'All Sales States.'
    

    resolve ->(_obj, args, ctx) {
      sales = Sale::SaleStateMachine.states
      sales
  }
end