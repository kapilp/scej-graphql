UserAccountField = GraphQL::Field.define do
  name('userAccount')
  type(Types::UserAccountType)
  argument :id, types.ID
  description 'fetch a Account by id'
  resolve ->(object, args, ctx) {
    User::AccountView.find(args[:id])
  }
end