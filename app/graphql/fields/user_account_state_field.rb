UserAccountStateField = GraphQL::Field.define do
  name('userAccountsState')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    User::Account.find(args[:id]).state_machine.current_state
  }
end