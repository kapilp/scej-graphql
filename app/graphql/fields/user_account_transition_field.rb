UserAccountTransitionField = GraphQL::Field.define do
  name('userAccountTransition')
  type(Types::UserAccountTransitionType)
  argument :id, types.ID
  description 'fetch a AccountTransition by id'
  resolve ->(object, args, ctx) {
    User::AccountTransition.find(args[:id])
  }
end