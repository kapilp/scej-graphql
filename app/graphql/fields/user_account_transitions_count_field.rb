UserAccountTransitionsCountField = GraphQL::Field.define do
  name('userAccountTransitionsCount')
  type types.Int
  description 'Number of AccountTransitions'
  resolve ->(_object, args, ctx) {
    User::AccountTransition.count
  }
end