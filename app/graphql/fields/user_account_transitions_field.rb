UserAccountTransitionsField = GraphQL::Field.define do
  name('userAccountTransitions')
  type(Types::UserAccountTransitionType.connection_type)
  description 'AccountTransition connection to fetch paginated AccountTransitions collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # accountTransitions = User::AccountTransition.limit(args[:limit].to_i)
    accountTransitions = User::AccountTransition.all
    accountTransitions
  }
end