UserAccountTypeField = GraphQL::Field.define do
  name('userAccountType')
  type(Types::UserAccountTypeType)
  argument :id, types.ID
  description 'fetch a AccountType by id'
  resolve ->(object, args, ctx) {
    User::AccountType.find(args[:id])
  }
end