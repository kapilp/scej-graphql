UserAccountTypesCountField = GraphQL::Field.define do
  name('userAccountTypesCount')
  type types.Int
  description 'Number of AccountTypes'
  resolve ->(_object, args, ctx) {
    User::AccountType.count
  }
end