UserAccountTypesField = GraphQL::Field.define do
  name('userAccountTypes')
  type(Types::UserAccountTypeType.connection_type)
  description 'AccountType connection to fetch paginated AccountTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # accountTypes = User::AccountType.limit(args[:limit].to_i)
    accountTypes = User::AccountType.all
    accountTypes
  }
end