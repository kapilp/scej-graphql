UserAccountsCountField = GraphQL::Field.define do
  name('userAccountsCount')
  type types.Int
  description 'Number of Accounts'
  resolve ->(_object, args, ctx) {
    User::AccountView.count
  }
end