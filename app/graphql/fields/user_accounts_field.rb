UserAccountsField = GraphQL::Field.define do
  name('userAccounts')
  type(Types::UserAccountType.connection_type)
  description 'Account connection to fetch paginated Accounts collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_a_type_id, types.ID
    argument :f_a_type_string, types.String, default_value: ''

  resolve ->(_obj, args, ctx) {
     # accounts = User::AccountView.limit(args[:limit].to_i)
    accounts = User::AccountView.all
      accounts = accounts.by_account_type(args[:f_a_type_id]) if args[:f_a_type_id]
      accounts = accounts.by_type(args[:f_a_type_string]) if args[:f_a_type_string] && !args[:f_a_type_string].blank?
    accounts
  }
end