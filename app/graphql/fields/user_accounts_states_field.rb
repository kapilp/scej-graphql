UserAccountsStatesField = GraphQL::Field.define do
  name('userAccountsStates')
  type types[types.String]
  description 'All Accounts States.'
    
    argument :f_a_type_id, types.ID
    argument :f_a_type_string, types.String, default_value: ''

    resolve ->(_obj, args, ctx) {
      accounts = User::AccountStateMachine.states
      accounts = accounts.by_account_type(args[:f_a_type_id]) if args[:f_a_type_id]
      accounts = accounts.by_type(args[:f_a_type_string]) if args[:f_a_type_string] && !args[:f_a_type_string].blank?
      accounts
  }
end