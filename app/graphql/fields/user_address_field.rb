UserAddressField = GraphQL::Field.define do
  name('userAddress')
  type(Types::UserAddressType)
  argument :id, types.ID
  description 'fetch a Address by id'
  resolve ->(object, args, ctx) {
    User::Address.find(args[:id])
  }
end