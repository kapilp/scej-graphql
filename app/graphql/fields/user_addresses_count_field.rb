UserAddressesCountField = GraphQL::Field.define do
  name('userAddressesCount')
  type types.Int
  description 'Number of Addresses'
  resolve ->(_object, args, ctx) {
    User::Address.count
  }
end