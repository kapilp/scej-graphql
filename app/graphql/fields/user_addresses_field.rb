UserAddressesField = GraphQL::Field.define do
  name('userAddresses')
  type(Types::UserAddressType.connection_type)
  description 'Address connection to fetch paginated Addresses collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # addresses = User::Address.limit(args[:limit].to_i)
    addresses = User::Address.all
    addresses
  }
end