UserAuthenticationTokenField = GraphQL::Field.define do
  name('userAuthenticationToken')
  type(Types::UserAuthenticationTokenType)
  argument :id, types.ID
  description 'fetch a AuthenticationToken by id'
  resolve ->(object, args, ctx) {
    User::AuthenticationToken.find(args[:id])
  }
end