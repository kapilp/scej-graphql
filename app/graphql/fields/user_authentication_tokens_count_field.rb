UserAuthenticationTokensCountField = GraphQL::Field.define do
  name('userAuthenticationTokensCount')
  type types.Int
  description 'Number of AuthenticationTokens'
  resolve ->(_object, args, ctx) {
    User::AuthenticationToken.count
  }
end