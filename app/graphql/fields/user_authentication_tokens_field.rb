UserAuthenticationTokensField = GraphQL::Field.define do
  name('userAuthenticationTokens')
  type(Types::UserAuthenticationTokenType.connection_type)
  description 'AuthenticationToken connection to fetch paginated AuthenticationTokens collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # authenticationTokens = User::AuthenticationToken.limit(args[:limit].to_i)
    authenticationTokens = User::AuthenticationToken.all
    authenticationTokens
  }
end