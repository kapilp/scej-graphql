UserCompaniesCountField = GraphQL::Field.define do
  name('userCompaniesCount')
  type types.Int
  description 'Number of Companies'
  resolve ->(_object, args, ctx) {
    User::Company.count
  }
end