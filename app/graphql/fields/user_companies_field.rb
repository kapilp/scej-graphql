UserCompaniesField = GraphQL::Field.define do
  name('userCompanies')
  type(Types::UserCompanyType.connection_type)
  description 'Company connection to fetch paginated Companies collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # companies = User::Company.limit(args[:limit].to_i)
    companies = User::Company.all
    companies
  }
end