UserCompanyField = GraphQL::Field.define do
  name('userCompany')
  type(Types::UserCompanyType)
  argument :id, types.ID
  description 'fetch a Company by id'
  resolve ->(object, args, ctx) {
    User::Company.find(args[:id])
  }
end