UserContactField = GraphQL::Field.define do
  name('userContact')
  type(Types::UserContactType)
  argument :id, types.ID
  description 'fetch a Contact by id'
  resolve ->(object, args, ctx) {
    User::Contact.find(args[:id])
  }
end