UserContactsCountField = GraphQL::Field.define do
  name('userContactsCount')
  type types.Int
  description 'Number of Contacts'
  resolve ->(_object, args, ctx) {
    User::Contact.count
  }
end