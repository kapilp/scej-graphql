UserContactsField = GraphQL::Field.define do
  name('userContacts')
  type(Types::UserContactType.connection_type)
  description 'Contact connection to fetch paginated Contacts collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # contacts = User::Contact.limit(args[:limit].to_i)
    contacts = User::Contact.all
    contacts
  }
end