UserCountriesCountField = GraphQL::Field.define do
  name('userCountriesCount')
  type types.Int
  description 'Number of Countries'
  resolve ->(_object, args, ctx) {
    User::Country.count
  }
end