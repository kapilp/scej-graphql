UserCountriesField = GraphQL::Field.define do
  name('userCountries')
  type(Types::UserCountryType.connection_type)
  description 'Country connection to fetch paginated Countries collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # countries = User::Country.limit(args[:limit].to_i)
    countries = User::Country.all
    countries
  }
end