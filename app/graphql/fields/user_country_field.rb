UserCountryField = GraphQL::Field.define do
  name('userCountry')
  type(Types::UserCountryType)
  argument :id, types.ID
  description 'fetch a Country by id'
  resolve ->(object, args, ctx) {
    User::Country.find(args[:id])
  }
end