UserRoleField = GraphQL::Field.define do
  name('userRole')
  type(Types::UserRoleType)
  argument :id, types.ID
  description 'fetch a Role by id'
  resolve ->(object, args, ctx) {
    User::Role.find(args[:id])
  }
end