UserRolesCountField = GraphQL::Field.define do
  name('userRolesCount')
  type types.Int
  description 'Number of Roles'
  resolve ->(_object, args, ctx) {
    User::Role.count
  }
end