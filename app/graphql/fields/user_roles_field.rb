UserRolesField = GraphQL::Field.define do
  name('userRoles')
  type(Types::UserRoleType.connection_type)
  description 'Role connection to fetch paginated Roles collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # roles = User::Role.limit(args[:limit].to_i)
    roles = User::Role.all
    roles
  }
end