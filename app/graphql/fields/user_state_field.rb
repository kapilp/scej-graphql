UserStateField = GraphQL::Field.define do
  name('userState')
  type(Types::UserStateType)
  argument :id, types.ID
  description 'fetch a State by id'
  resolve ->(object, args, ctx) {
    User::State.find(args[:id])
  }
end