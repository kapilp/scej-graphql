UserStatesCountField = GraphQL::Field.define do
  name('userStatesCount')
  type types.Int
  description 'Number of States'
  resolve ->(_object, args, ctx) {
    User::State.count
  }
end