UserStatesField = GraphQL::Field.define do
  name('userStates')
  type(Types::UserStateType.connection_type)
  description 'State connection to fetch paginated States collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_country_id, types.ID

  resolve ->(_obj, args, ctx) {
     # states = User::State.limit(args[:limit].to_i)
    states = User::State.all
      states = states.by_country(args[:f_country_id]) if args[:f_country_id]
    states
  }
end