UserTaxRateField = GraphQL::Field.define do
  name('userTaxRate')
  type(Types::UserTaxRateType)
  argument :id, types.ID
  description 'fetch a TaxRate by id'
  resolve ->(object, args, ctx) {
    User::TaxRate.find(args[:id])
  }
end