UserTaxRatesCountField = GraphQL::Field.define do
  name('userTaxRatesCount')
  type types.Int
  description 'Number of TaxRates'
  resolve ->(_object, args, ctx) {
    User::TaxRate.count
  }
end