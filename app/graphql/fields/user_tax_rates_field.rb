UserTaxRatesField = GraphQL::Field.define do
  name('userTaxRates')
  type(Types::UserTaxRateType.connection_type)
  description 'TaxRate connection to fetch paginated TaxRates collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # taxRates = User::TaxRate.limit(args[:limit].to_i)
    taxRates = User::TaxRate.all
    taxRates
  }
end