UserTaxTypeField = GraphQL::Field.define do
  name('userTaxType')
  type(Types::UserTaxTypeType)
  argument :id, types.ID
  description 'fetch a TaxType by id'
  resolve ->(object, args, ctx) {
    User::TaxType.find(args[:id])
  }
end