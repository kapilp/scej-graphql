UserTaxTypesCountField = GraphQL::Field.define do
  name('userTaxTypesCount')
  type types.Int
  description 'Number of TaxTypes'
  resolve ->(_object, args, ctx) {
    User::TaxType.count
  }
end