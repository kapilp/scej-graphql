UserTaxTypesField = GraphQL::Field.define do
  name('userTaxTypes')
  type(Types::UserTaxTypeType.connection_type)
  description 'TaxType connection to fetch paginated TaxTypes collection.'
  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

  resolve ->(_obj, args, ctx) {
     # taxTypes = User::TaxType.limit(args[:limit].to_i)
    taxTypes = User::TaxType.all
    taxTypes
  }
end