ActiveRecordTimestamp = GraphQL::InterfaceType.define do
  name 'ActiveRecordTimestamp'
  # field :createdAt, types.String, property: :created_at
  field :created_at, !DateTimeScalar, 'The time at which this entry was created'
  field :updated_at, !DateTimeScalar, property: :updated_at
end