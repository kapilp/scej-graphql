module CollectionMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createCollection"
    description "create Collection"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :collection, types.String
    input_field :parent_id, types.ID

# Part1A
# Part1A

    return_field :collection, Types::DesignCollectionType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_collection = ctx[:current_user].collections.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_collection = Design::Collection.new(args)

      if new_collection.save
        
        
        { collection: new_collection }
      else
        { messages: new_collection.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateCollection"
    description "Update a Collection and return Collection"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :collection, types.String
    input_field :parent_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :collection, Types::DesignCollectionType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      collection = Design::Collection.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != collection.user
        FieldError.error("You can not modify this collection because you are not the owner")
      elsif collection.update(inputs.to_h)
        
        
        { collection: collection }
      else
        { messages: collection.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyCollection"
    description "Destroy a Collection"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :collection, Types::DesignCollectionType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      collection = Design::Collection.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != collection.user
        FieldError.error("You can not modify this collection because you are not the owner")
# Part3B
      elsif collection.depth > 0
        FieldError.error("You can not modify this collection because it has childrens")
# Part3B

      elsif collection.destroy
        {collection: collection}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createCollection, field: CollectionMutations::Create.field
  field :updateCollection, field: CollectionMutations::Update.field
  field :destroyCollection, field: CollectionMutations::Destroy.field
=end
