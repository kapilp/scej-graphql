module DiamondMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')
    h['material_gem_shape_id'] = h.delete 'gem_shape_id' if h.key?('gem_shape_id')
    h['material_gem_clarity_id'] = h.delete 'gem_clarity_id' if h.key?('gem_clarity_id')
    h['material_color_id'] = h.delete 'color_id' if h.key?('color_id')
    h['material_gem_size_id'] = h.delete 'gem_size_id' if h.key?('gem_size_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createDiamond"
    description "create Diamond"

    input_field :position, types.Int
    input_field :material_id, types.ID
    input_field :gem_shape_id, types.ID
    input_field :gem_clarity_id, types.ID
    input_field :color_id, types.ID
    input_field :gem_size_id, types.ID
    input_field :pcs, types.Int

# Part1A
# Part1A

    return_field :diamond, Types::DesignDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_diamond = ctx[:current_user].diamonds.build(inputs.to_params)
      

      args = DiamondMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_diamond = Design::Diamond.new(args)

      if new_diamond.save
        
        
        { diamond: new_diamond }
      else
        { messages: new_diamond.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateDiamond"
    description "Update a Diamond and return Diamond"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :material_id, types.ID
    input_field :gem_shape_id, types.ID
    input_field :gem_clarity_id, types.ID
    input_field :color_id, types.ID
    input_field :gem_size_id, types.ID
    input_field :pcs, types.Int

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :diamond, Types::DesignDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      diamond = Design::Diamond.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != diamond.user
        FieldError.error("You can not modify this diamond because you are not the owner")
      elsif diamond.update(DiamondMutations.arg_modify(inputs, ctx))
        
        
        { diamond: diamond }
      else
        { messages: diamond.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyDiamond"
    description "Destroy a Diamond"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :diamond, Types::DesignDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      diamond = Design::Diamond.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != diamond.user
        FieldError.error("You can not modify this diamond because you are not the owner")
      elsif diamond.destroy
        {diamond: diamond}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createDiamond, field: DiamondMutations::Create.field
  field :updateDiamond, field: DiamondMutations::Update.field
  field :destroyDiamond, field: DiamondMutations::Destroy.field
=end
