module ImageMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createImage"
    description "create Image"

    input_field :imageable_type, types.String
    input_field :imageable_id, types.ID
    input_field :image_data, types.String

# Part1A
# Part1A

    return_field :image, Types::DesignImageType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_image = ctx[:current_user].images.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_image = Design::Image.new(args)

      if new_image.save
        
        
        { image: new_image }
      else
        { messages: new_image.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateImage"
    description "Update a Image and return Image"

    input_field :id, types.ID
    input_field :imageable_type, types.String
    input_field :imageable_id, types.ID
    input_field :image_data, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :image, Types::DesignImageType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      image = Design::Image.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != image.user
        FieldError.error("You can not modify this image because you are not the owner")
      elsif image.update(inputs.to_h)
        
        
        { image: image }
      else
        { messages: image.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyImage"
    description "Destroy a Image"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :image, Types::DesignImageType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      image = Design::Image.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != image.user
        FieldError.error("You can not modify this image because you are not the owner")
      elsif image.destroy
        {image: image}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createImage, field: ImageMutations::Create.field
  field :updateImage, field: ImageMutations::Update.field
  field :destroyImage, field: ImageMutations::Destroy.field
=end
