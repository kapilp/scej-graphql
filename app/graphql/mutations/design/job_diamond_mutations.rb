module JobDiamondMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['sale_job_id'] = h.delete 'job_id' if h.key?('job_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createJobDiamond"
    description "create JobDiamond"

    input_field :job_id, types.ID
    input_field :design_diamond_id, types.ID

# Part1A
# Part1A

    return_field :jobDiamond, Types::DesignJobDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_jobDiamond = ctx[:current_user].jobDiamonds.build(inputs.to_params)
      

      args = JobDiamondMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_jobDiamond = User::CompanyOwnership.new(args)

      if new_jobDiamond.save
        
        
        { jobDiamond: new_jobDiamond }
      else
        { messages: new_jobDiamond.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateJobDiamond"
    description "Update a JobDiamond and return JobDiamond"

    input_field :id, types.ID
    input_field :job_id, types.ID
    input_field :design_diamond_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :jobDiamond, Types::DesignJobDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      jobDiamond = User::CompanyOwnership.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != jobDiamond.user
        FieldError.error("You can not modify this jobDiamond because you are not the owner")
      elsif jobDiamond.update(JobDiamondMutations.arg_modify(inputs, ctx))
        
        
        { jobDiamond: jobDiamond }
      else
        { messages: jobDiamond.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyJobDiamond"
    description "Destroy a JobDiamond"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :jobDiamond, Types::DesignJobDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      jobDiamond = User::CompanyOwnership.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != jobDiamond.user
        FieldError.error("You can not modify this jobDiamond because you are not the owner")
      elsif jobDiamond.destroy
        {jobDiamond: jobDiamond}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createJobDiamond, field: JobDiamondMutations::Create.field
  field :updateJobDiamond, field: JobDiamondMutations::Update.field
  field :destroyJobDiamond, field: JobDiamondMutations::Destroy.field
=end
