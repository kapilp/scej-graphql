module MakingTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createMakingType"
    description "create MakingType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :making_type, types.String
    input_field :isdefault, types.Boolean

# Part1A
# Part1A

    return_field :makingType, Types::DesignMakingTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_makingType = ctx[:current_user].makingTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_makingType = Design::MakingType.new(args)

      if new_makingType.save
        
        
        { makingType: new_makingType }
      else
        { messages: new_makingType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMakingType"
    description "Update a MakingType and return MakingType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :making_type, types.String
    input_field :isdefault, types.Boolean

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :makingType, Types::DesignMakingTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      makingType = Design::MakingType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != makingType.user
        FieldError.error("You can not modify this makingType because you are not the owner")
      elsif makingType.update(inputs.to_h)
        
        
        { makingType: makingType }
      else
        { messages: makingType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMakingType"
    description "Destroy a MakingType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :makingType, Types::DesignMakingTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      makingType = Design::MakingType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != makingType.user
        FieldError.error("You can not modify this makingType because you are not the owner")
      elsif makingType.destroy
        {makingType: makingType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMakingType, field: MakingTypeMutations::Create.field
  field :updateMakingType, field: MakingTypeMutations::Update.field
  field :destroyMakingType, field: MakingTypeMutations::Destroy.field
=end
