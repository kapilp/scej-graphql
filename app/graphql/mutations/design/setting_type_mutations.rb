module SettingTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createSettingType"
    description "create SettingType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :setting_type, types.String
    input_field :isdefault, types.Boolean

# Part1A
# Part1A

    return_field :settingType, Types::DesignSettingTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_settingType = ctx[:current_user].settingTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_settingType = Design::SettingType.new(args)

      if new_settingType.save
        
        
        { settingType: new_settingType }
      else
        { messages: new_settingType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateSettingType"
    description "Update a SettingType and return SettingType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :setting_type, types.String
    input_field :isdefault, types.Boolean

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :settingType, Types::DesignSettingTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      settingType = Design::SettingType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != settingType.user
        FieldError.error("You can not modify this settingType because you are not the owner")
      elsif settingType.update(inputs.to_h)
        
        
        { settingType: settingType }
      else
        { messages: settingType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroySettingType"
    description "Destroy a SettingType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :settingType, Types::DesignSettingTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      settingType = Design::SettingType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != settingType.user
        FieldError.error("You can not modify this settingType because you are not the owner")
      elsif settingType.destroy
        {settingType: settingType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createSettingType, field: SettingTypeMutations::Create.field
  field :updateSettingType, field: SettingTypeMutations::Update.field
  field :destroySettingType, field: SettingTypeMutations::Destroy.field
=end
