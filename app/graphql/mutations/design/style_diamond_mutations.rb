module StyleDiamondMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['design_style_id'] = h.delete 'style_id' if h.key?('style_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createStyleDiamond"
    description "create StyleDiamond"

    input_field :style_id, types.ID
    input_field :design_diamond_id, types.ID

# Part1A
# Part1A

    return_field :styleDiamond, Types::DesignStyleDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_styleDiamond = ctx[:current_user].styleDiamonds.build(inputs.to_params)
      

      args = StyleDiamondMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_styleDiamond = Design::StyleDiamond.new(args)

      if new_styleDiamond.save
        
        
        { styleDiamond: new_styleDiamond }
      else
        { messages: new_styleDiamond.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateStyleDiamond"
    description "Update a StyleDiamond and return StyleDiamond"

    input_field :id, types.ID
    input_field :style_id, types.ID
    input_field :design_diamond_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :styleDiamond, Types::DesignStyleDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      styleDiamond = Design::StyleDiamond.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != styleDiamond.user
        FieldError.error("You can not modify this styleDiamond because you are not the owner")
      elsif styleDiamond.update(StyleDiamondMutations.arg_modify(inputs, ctx))
        
        
        { styleDiamond: styleDiamond }
      else
        { messages: styleDiamond.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyStyleDiamond"
    description "Destroy a StyleDiamond"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :styleDiamond, Types::DesignStyleDiamondType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      styleDiamond = Design::StyleDiamond.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != styleDiamond.user
        FieldError.error("You can not modify this styleDiamond because you are not the owner")
      elsif styleDiamond.destroy
        {styleDiamond: styleDiamond}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createStyleDiamond, field: StyleDiamondMutations::Create.field
  field :updateStyleDiamond, field: StyleDiamondMutations::Update.field
  field :destroyStyleDiamond, field: StyleDiamondMutations::Destroy.field
=end
