module StyleMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['design_collection_id'] = h.delete 'collection_id' if h.key?('collection_id')
    h['design_making_type_id'] = h.delete 'making_type_id' if h.key?('making_type_id')
    h['design_setting_type_id'] = h.delete 'setting_type_id' if h.key?('setting_type_id')
    h['user_account_id'] = h.delete 'designer_id' if h.key?('designer_id')
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')
    h['material_metal_purity_id'] = h.delete 'metal_purity_id' if h.key?('metal_purity_id')
    h['material_color_id'] = h.delete 'color_id' if h.key?('color_id')
    d = h.delete 'images'
    if d
      d1 = d.map do |a|
        ImageMutations::arg_modify(a, ctx)
      end
      h['design_images_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('image_ids')
      d = h.delete 'image_ids'
      h['design_image_ids'] = d if d # if d is not null
    end
    
    d = h.delete 'diamonds'
    if d
      d1 = d.map do |a|
        DiamondMutations::arg_modify(a, ctx)
      end
      h['design_diamonds_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('diamond_ids')
      d = h.delete 'diamond_ids'
      h['design_diamond_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createStyle"
    description "create Style"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :collection_id, types.ID
    input_field :making_type_id, types.ID
    input_field :setting_type_id, types.ID
    input_field :designer_id, types.ID
    input_field :material_id, types.ID
    input_field :metal_purity_id, types.ID
    input_field :color_id, types.ID
    input_field :net_weight, types.Float
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :active, types.Boolean
    input_field :image_ids, types[types.ID]
    input_field :images, types[ImageMutations::Update.input_type]
    input_field :diamond_ids, types[types.ID]
    input_field :diamonds, types[DiamondMutations::Update.input_type]
# Part1A
# Part1A

    return_field :style, Types::DesignStyleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_style = ctx[:current_user].styles.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = StyleMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_style = Design::Style.new(args)

      if new_style.save
        if new_style.state_machine.can_transition_to?(status_id)
          new_style.state_machine.transition_to!(status_id)
        end
        new_style = Design::StyleView.find(new_style.id)
        { style: new_style }
      else
        { messages: new_style.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateStyle"
    description "Update a Style and return Style"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :collection_id, types.ID
    input_field :making_type_id, types.ID
    input_field :setting_type_id, types.ID
    input_field :designer_id, types.ID
    input_field :material_id, types.ID
    input_field :metal_purity_id, types.ID
    input_field :color_id, types.ID
    input_field :net_weight, types.Float
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :active, types.Boolean
    input_field :image_ids, types[types.ID]
    input_field :images, types[ImageMutations::Update.input_type]
    input_field :diamond_ids, types[types.ID]
    input_field :diamonds, types[DiamondMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :style, Types::DesignStyleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      style = Design::Style.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != style.user
        FieldError.error("You can not modify this style because you are not the owner")
      elsif style.update(StyleMutations.arg_modify(inputs, ctx))
        if style.state_machine.can_transition_to?(status_id)
          style.state_machine.transition_to!(status_id)
        end
        style = Design::StyleView.find(style.id)
        { style: style }
      else
        { messages: style.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyStyle"
    description "Destroy a Style"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :style, Types::DesignStyleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      style = Design::Style.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != style.user
        FieldError.error("You can not modify this style because you are not the owner")
# Part3B
# Part3B

      elsif style.destroy
        {style: style}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createStyle, field: StyleMutations::Create.field
  field :updateStyle, field: StyleMutations::Update.field
  field :destroyStyle, field: StyleMutations::Destroy.field
=end
