module AccessoryMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createAccessory"
    description "create Accessory"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :accessory, types.String

# Part1A
# Part1A

    return_field :accessory, Types::MaterialAccessoryType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_accessory = ctx[:current_user].accessorys.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_accessory = Material::Accessory.new(args)

      if new_accessory.save
        
        
        { accessory: new_accessory }
      else
        { messages: new_accessory.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateAccessory"
    description "Update a Accessory and return Accessory"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :accessory, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :accessory, Types::MaterialAccessoryType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      accessory = Material::Accessory.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != accessory.user
        FieldError.error("You can not modify this accessory because you are not the owner")
      elsif accessory.update(inputs.to_h)
        
        
        { accessory: accessory }
      else
        { messages: accessory.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyAccessory"
    description "Destroy a Accessory"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :accessory, Types::MaterialAccessoryType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      accessory = Material::Accessory.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != accessory.user
        FieldError.error("You can not modify this accessory because you are not the owner")
      elsif accessory.destroy
        {accessory: accessory}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createAccessory, field: AccessoryMutations::Create.field
  field :updateAccessory, field: AccessoryMutations::Update.field
  field :destroyAccessory, field: AccessoryMutations::Destroy.field
=end
