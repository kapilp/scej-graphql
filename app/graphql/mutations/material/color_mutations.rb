module ColorMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createColor"
    description "create Color"

    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :color, types.String

# Part1A
# Part1A

    return_field :color, Types::MaterialColorType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_color = ctx[:current_user].colors.build(inputs.to_params)
      

      args = ColorMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_color = Material::Color.new(args)

      if new_color.save
        
        
        { color: new_color }
      else
        { messages: new_color.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateColor"
    description "Update a Color and return Color"

    input_field :id, types.ID
    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :color, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :color, Types::MaterialColorType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      color = Material::Color.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != color.user
        FieldError.error("You can not modify this color because you are not the owner")
      elsif color.update(ColorMutations.arg_modify(inputs, ctx))
        
        
        { color: color }
      else
        { messages: color.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyColor"
    description "Destroy a Color"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :color, Types::MaterialColorType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      color = Material::Color.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != color.user
        FieldError.error("You can not modify this color because you are not the owner")
      elsif color.destroy
        {color: color}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createColor, field: ColorMutations::Create.field
  field :updateColor, field: ColorMutations::Update.field
  field :destroyColor, field: ColorMutations::Destroy.field
=end
