module GemClarityMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createGemClarity"
    description "create GemClarity"

    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :clarity, types.String

# Part1A
# Part1A

    return_field :gemClarity, Types::MaterialGemClarityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_gemClarity = ctx[:current_user].gemClaritys.build(inputs.to_params)
      

      args = GemClarityMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_gemClarity = Material::GemClarity.new(args)

      if new_gemClarity.save
        
        
        { gemClarity: new_gemClarity }
      else
        { messages: new_gemClarity.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateGemClarity"
    description "Update a GemClarity and return GemClarity"

    input_field :id, types.ID
    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :clarity, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :gemClarity, Types::MaterialGemClarityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemClarity = Material::GemClarity.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != gemClarity.user
        FieldError.error("You can not modify this gemClarity because you are not the owner")
      elsif gemClarity.update(GemClarityMutations.arg_modify(inputs, ctx))
        
        
        { gemClarity: gemClarity }
      else
        { messages: gemClarity.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyGemClarity"
    description "Destroy a GemClarity"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :gemClarity, Types::MaterialGemClarityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemClarity = Material::GemClarity.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != gemClarity.user
        FieldError.error("You can not modify this gemClarity because you are not the owner")
      elsif gemClarity.destroy
        {gemClarity: gemClarity}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createGemClarity, field: GemClarityMutations::Create.field
  field :updateGemClarity, field: GemClarityMutations::Update.field
  field :destroyGemClarity, field: GemClarityMutations::Destroy.field
=end
