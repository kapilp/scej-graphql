module GemClaritySizeMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_gem_clarity_id'] = h.delete 'gem_clarity_id' if h.key?('gem_clarity_id')
    h['material_gem_size_id'] = h.delete 'gem_size_id' if h.key?('gem_size_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createGemClaritySize"
    description "create GemClaritySize"

    input_field :gem_clarity_id, types.ID
    input_field :gem_size_id, types.ID

# Part1A
# Part1A

    return_field :gemClaritySize, Types::MaterialGemClaritySizeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_gemClaritySize = ctx[:current_user].gemClaritySizes.build(inputs.to_params)
      

      args = GemClaritySizeMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_gemClaritySize = Material::GemClaritySize.new(args)

      if new_gemClaritySize.save
        
        
        { gemClaritySize: new_gemClaritySize }
      else
        { messages: new_gemClaritySize.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateGemClaritySize"
    description "Update a GemClaritySize and return GemClaritySize"

    input_field :id, types.ID
    input_field :gem_clarity_id, types.ID
    input_field :gem_size_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :gemClaritySize, Types::MaterialGemClaritySizeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemClaritySize = Material::GemClaritySize.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != gemClaritySize.user
        FieldError.error("You can not modify this gemClaritySize because you are not the owner")
      elsif gemClaritySize.update(GemClaritySizeMutations.arg_modify(inputs, ctx))
        
        
        { gemClaritySize: gemClaritySize }
      else
        { messages: gemClaritySize.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyGemClaritySize"
    description "Destroy a GemClaritySize"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :gemClaritySize, Types::MaterialGemClaritySizeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemClaritySize = Material::GemClaritySize.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != gemClaritySize.user
        FieldError.error("You can not modify this gemClaritySize because you are not the owner")
      elsif gemClaritySize.destroy
        {gemClaritySize: gemClaritySize}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createGemClaritySize, field: GemClaritySizeMutations::Create.field
  field :updateGemClaritySize, field: GemClaritySizeMutations::Update.field
  field :destroyGemClaritySize, field: GemClaritySizeMutations::Destroy.field
=end
