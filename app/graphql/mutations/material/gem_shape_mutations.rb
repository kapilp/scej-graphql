module GemShapeMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createGemShape"
    description "create GemShape"

    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :shape, types.String

# Part1A
# Part1A

    return_field :gemShape, Types::MaterialGemShapeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_gemShape = ctx[:current_user].gemShapes.build(inputs.to_params)
      

      args = GemShapeMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_gemShape = Material::GemShape.new(args)

      if new_gemShape.save
        
        
        { gemShape: new_gemShape }
      else
        { messages: new_gemShape.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateGemShape"
    description "Update a GemShape and return GemShape"

    input_field :id, types.ID
    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :shape, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :gemShape, Types::MaterialGemShapeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemShape = Material::GemShape.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != gemShape.user
        FieldError.error("You can not modify this gemShape because you are not the owner")
      elsif gemShape.update(GemShapeMutations.arg_modify(inputs, ctx))
        
        
        { gemShape: gemShape }
      else
        { messages: gemShape.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyGemShape"
    description "Destroy a GemShape"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :gemShape, Types::MaterialGemShapeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemShape = Material::GemShape.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != gemShape.user
        FieldError.error("You can not modify this gemShape because you are not the owner")
      elsif gemShape.destroy
        {gemShape: gemShape}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createGemShape, field: GemShapeMutations::Create.field
  field :updateGemShape, field: GemShapeMutations::Update.field
  field :destroyGemShape, field: GemShapeMutations::Destroy.field
=end
