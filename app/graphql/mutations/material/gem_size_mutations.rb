module GemSizeMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')
    h['material_gem_shape_id'] = h.delete 'gem_shape_id' if h.key?('gem_shape_id')
    d = h.delete 'gem_clarities'
    if d
      d1 = d.map do |a|
        GemClarityMutations::arg_modify(a, ctx)
      end
      h['material_gem_clarities_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('gem_clarity_ids')
      d = h.delete 'gem_clarity_ids'
      h['material_gem_clarity_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createGemSize"
    description "create GemSize"

    input_field :material_id, types.ID
    input_field :gem_shape_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :mm_size, types.String
    input_field :carat_weight, types.Float
    input_field :price, types.Float
    input_field :gem_clarity_ids, types[types.ID]
    input_field :gem_clarities, types[GemClarityMutations::Update.input_type]
# Part1A
# Part1A

    return_field :gemSize, Types::MaterialGemSizeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_gemSize = ctx[:current_user].gemSizes.build(inputs.to_params)
      

      args = GemSizeMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_gemSize = Material::GemSize.new(args)

      if new_gemSize.save
        
        
        { gemSize: new_gemSize }
      else
        { messages: new_gemSize.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateGemSize"
    description "Update a GemSize and return GemSize"

    input_field :id, types.ID
    input_field :material_id, types.ID
    input_field :gem_shape_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :mm_size, types.String
    input_field :carat_weight, types.Float
    input_field :price, types.Float
    input_field :gem_clarity_ids, types[types.ID]
    input_field :gem_clarities, types[GemClarityMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :gemSize, Types::MaterialGemSizeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemSize = Material::GemSize.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != gemSize.user
        FieldError.error("You can not modify this gemSize because you are not the owner")
      elsif gemSize.update(GemSizeMutations.arg_modify(inputs, ctx))
        
        
        { gemSize: gemSize }
      else
        { messages: gemSize.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyGemSize"
    description "Destroy a GemSize"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :gemSize, Types::MaterialGemSizeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemSize = Material::GemSize.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != gemSize.user
        FieldError.error("You can not modify this gemSize because you are not the owner")
      elsif gemSize.destroy
        {gemSize: gemSize}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createGemSize, field: GemSizeMutations::Create.field
  field :updateGemSize, field: GemSizeMutations::Update.field
  field :destroyGemSize, field: GemSizeMutations::Destroy.field
=end
