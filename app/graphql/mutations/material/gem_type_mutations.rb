module GemTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createGemType"
    description "create GemType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :gem_type, types.String
    input_field :isdefault, types.Boolean

# Part1A
# Part1A

    return_field :gemType, Types::MaterialGemTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_gemType = ctx[:current_user].gemTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_gemType = Material::GemType.new(args)

      if new_gemType.save
        
        
        { gemType: new_gemType }
      else
        { messages: new_gemType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateGemType"
    description "Update a GemType and return GemType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :gem_type, types.String
    input_field :isdefault, types.Boolean

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :gemType, Types::MaterialGemTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemType = Material::GemType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != gemType.user
        FieldError.error("You can not modify this gemType because you are not the owner")
      elsif gemType.update(inputs.to_h)
        
        
        { gemType: gemType }
      else
        { messages: gemType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyGemType"
    description "Destroy a GemType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :gemType, Types::MaterialGemTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      gemType = Material::GemType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != gemType.user
        FieldError.error("You can not modify this gemType because you are not the owner")
      elsif gemType.destroy
        {gemType: gemType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createGemType, field: GemTypeMutations::Create.field
  field :updateGemType, field: GemTypeMutations::Update.field
  field :destroyGemType, field: GemTypeMutations::Destroy.field
=end
