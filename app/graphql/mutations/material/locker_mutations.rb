module LockerMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createLocker"
    description "create Locker"

    input_field :company_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :locker, types.String
    input_field :isdefault, types.Boolean

# Part1A
# Part1A

    return_field :locker, Types::MaterialLockerType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_locker = ctx[:current_user].lockers.build(inputs.to_params)
      

      args = LockerMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_locker = Material::Locker.new(args)

      if new_locker.save
        
        
        { locker: new_locker }
      else
        { messages: new_locker.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateLocker"
    description "Update a Locker and return Locker"

    input_field :id, types.ID
    input_field :company_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :locker, types.String
    input_field :isdefault, types.Boolean

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :locker, Types::MaterialLockerType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      locker = Material::Locker.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != locker.user
        FieldError.error("You can not modify this locker because you are not the owner")
      elsif locker.update(LockerMutations.arg_modify(inputs, ctx))
        
        
        { locker: locker }
      else
        { messages: locker.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyLocker"
    description "Destroy a Locker"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :locker, Types::MaterialLockerType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      locker = Material::Locker.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != locker.user
        FieldError.error("You can not modify this locker because you are not the owner")
      elsif locker.destroy
        {locker: locker}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createLocker, field: LockerMutations::Create.field
  field :updateLocker, field: LockerMutations::Update.field
  field :destroyLocker, field: LockerMutations::Destroy.field
=end
