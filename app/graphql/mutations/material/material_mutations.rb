module MaterialMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_type_id'] = h.delete 'material_type_id' if h.key?('material_type_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createMaterial"
    description "create Material"

    input_field :material_type_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :material_name, types.String

# Part1A
# Part1A

    return_field :material, Types::MaterialMaterialType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_material = ctx[:current_user].materials.build(inputs.to_params)
      

      args = MaterialMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_material = Material::Material.new(args)

      if new_material.save
        
        
        { material: new_material }
      else
        { messages: new_material.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMaterial"
    description "Update a Material and return Material"

    input_field :id, types.ID
    input_field :material_type_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :material_name, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :material, Types::MaterialMaterialType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      material = Material::Material.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != material.user
        FieldError.error("You can not modify this material because you are not the owner")
      elsif material.update(MaterialMutations.arg_modify(inputs, ctx))
        
        
        { material: material }
      else
        { messages: material.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMaterial"
    description "Destroy a Material"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :material, Types::MaterialMaterialType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      material = Material::Material.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != material.user
        FieldError.error("You can not modify this material because you are not the owner")
      elsif material.destroy
        {material: material}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMaterial, field: MaterialMutations::Create.field
  field :updateMaterial, field: MaterialMutations::Update.field
  field :destroyMaterial, field: MaterialMutations::Destroy.field
=end
