module MaterialTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createMaterialType"
    description "create MaterialType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :material_type, types.String

# Part1A
# Part1A

    return_field :materialType, Types::MaterialMaterialTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_materialType = ctx[:current_user].materialTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_materialType = Material::MaterialType.new(args)

      if new_materialType.save
        
        
        { materialType: new_materialType }
      else
        { messages: new_materialType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMaterialType"
    description "Update a MaterialType and return MaterialType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :material_type, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :materialType, Types::MaterialMaterialTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      materialType = Material::MaterialType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != materialType.user
        FieldError.error("You can not modify this materialType because you are not the owner")
      elsif materialType.update(inputs.to_h)
        
        
        { materialType: materialType }
      else
        { messages: materialType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMaterialType"
    description "Destroy a MaterialType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :materialType, Types::MaterialMaterialTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      materialType = Material::MaterialType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != materialType.user
        FieldError.error("You can not modify this materialType because you are not the owner")
      elsif materialType.destroy
        {materialType: materialType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMaterialType, field: MaterialTypeMutations::Create.field
  field :updateMaterialType, field: MaterialTypeMutations::Update.field
  field :destroyMaterialType, field: MaterialTypeMutations::Destroy.field
=end
