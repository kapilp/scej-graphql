module MetalPurityMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createMetalPurity"
    description "create MetalPurity"

    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :name, types.String
    input_field :purity, types.Float
    input_field :specific_density, types.Float
    input_field :price, types.Float
    input_field :description, types.String

# Part1A
# Part1A

    return_field :metalPurity, Types::MaterialMetalPurityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_metalPurity = ctx[:current_user].metalPuritys.build(inputs.to_params)
      

      args = MetalPurityMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_metalPurity = Material::MetalPurity.new(args)

      if new_metalPurity.save
        
        
        { metalPurity: new_metalPurity }
      else
        { messages: new_metalPurity.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMetalPurity"
    description "Update a MetalPurity and return MetalPurity"

    input_field :id, types.ID
    input_field :material_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :name, types.String
    input_field :purity, types.Float
    input_field :specific_density, types.Float
    input_field :price, types.Float
    input_field :description, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :metalPurity, Types::MaterialMetalPurityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      metalPurity = Material::MetalPurity.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != metalPurity.user
        FieldError.error("You can not modify this metalPurity because you are not the owner")
      elsif metalPurity.update(MetalPurityMutations.arg_modify(inputs, ctx))
        
        
        { metalPurity: metalPurity }
      else
        { messages: metalPurity.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMetalPurity"
    description "Destroy a MetalPurity"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :metalPurity, Types::MaterialMetalPurityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      metalPurity = Material::MetalPurity.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != metalPurity.user
        FieldError.error("You can not modify this metalPurity because you are not the owner")
      elsif metalPurity.destroy
        {metalPurity: metalPurity}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMetalPurity, field: MetalPurityMutations::Create.field
  field :updateMetalPurity, field: MetalPurityMutations::Update.field
  field :destroyMetalPurity, field: MetalPurityMutations::Destroy.field
=end
