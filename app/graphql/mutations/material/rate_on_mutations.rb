module RateOnMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_type_id'] = h.delete 'material_type_id' if h.key?('material_type_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createRateOn"
    description "create RateOn"

    input_field :material_type_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :rateon, types.String

# Part1A
# Part1A

    return_field :rateOn, Types::MaterialRateOnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_rateOn = ctx[:current_user].rateOns.build(inputs.to_params)
      

      args = RateOnMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_rateOn = Material::RateOn.new(args)

      if new_rateOn.save
        
        
        { rateOn: new_rateOn }
      else
        { messages: new_rateOn.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateRateOn"
    description "Update a RateOn and return RateOn"

    input_field :id, types.ID
    input_field :material_type_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :rateon, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :rateOn, Types::MaterialRateOnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      rateOn = Material::RateOn.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != rateOn.user
        FieldError.error("You can not modify this rateOn because you are not the owner")
      elsif rateOn.update(RateOnMutations.arg_modify(inputs, ctx))
        
        
        { rateOn: rateOn }
      else
        { messages: rateOn.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyRateOn"
    description "Destroy a RateOn"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :rateOn, Types::MaterialRateOnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      rateOn = Material::RateOn.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != rateOn.user
        FieldError.error("You can not modify this rateOn because you are not the owner")
      elsif rateOn.destroy
        {rateOn: rateOn}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createRateOn, field: RateOnMutations::Create.field
  field :updateRateOn, field: RateOnMutations::Update.field
  field :destroyRateOn, field: RateOnMutations::Destroy.field
=end
