module CastingMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    d = h.delete 'm_txn_details'
    if d
      d1 = d.map do |a|
        MTxnDetailMutations::arg_modify(a, ctx)
      end
      h['sale_m_txn_details_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('m_txn_detail_ids')
      d = h.delete 'm_txn_detail_ids'
      h['sale_m_txn_detail_ids'] = d if d # if d is not null
    end
    
    d = h.delete 'sale_m_txn_details_receives'
    if d
      d1 = d.map do |a|
        MTxnDetailMutations::arg_modify(a, ctx)
      end
      h['sale_m_txn_details_receives_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('sale_m_txn_details_receives_ids')
      d = h.delete 'sale_m_txn_details_receives_ids'
      h['sale_m_txn_details_receives_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createCasting"
    description "create Casting"

    input_field :position, types.Int
    input_field :date, types.String
    input_field :slug, types.String
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
    input_field :sale_m_txn_details_receives_ids, types[types.ID]
    input_field :sale_m_txn_details_receives, types[MTxnDetailMutations::Update.input_type]
# Part1A
# Part1A

    return_field :casting, Types::MfgCastingType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_casting = ctx[:current_user].castings.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = CastingMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_casting = Mfg::Casting.new(args)

      if new_casting.save
        if new_casting.state_machine.can_transition_to?(status_id)
          new_casting.state_machine.transition_to!(status_id)
        end
        new_casting = Mfg::CastingView.find(new_casting.id)
        { casting: new_casting }
      else
        { messages: new_casting.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateCasting"
    description "Update a Casting and return Casting"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :date, types.String
    input_field :slug, types.String
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
    input_field :sale_m_txn_details_receives_ids, types[types.ID]
    input_field :sale_m_txn_details_receives, types[MTxnDetailMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :casting, Types::MfgCastingType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      casting = Mfg::Casting.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != casting.user
        FieldError.error("You can not modify this casting because you are not the owner")
      elsif casting.update(CastingMutations.arg_modify(inputs, ctx))
        if casting.state_machine.can_transition_to?(status_id)
          casting.state_machine.transition_to!(status_id)
        end
        casting = Mfg::CastingView.find(casting.id)
        { casting: casting }
      else
        { messages: casting.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyCasting"
    description "Destroy a Casting"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :casting, Types::MfgCastingType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      casting = Mfg::Casting.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != casting.user
        FieldError.error("You can not modify this casting because you are not the owner")
# Part3B
# Part3B

      elsif casting.destroy
        {casting: casting}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createCasting, field: CastingMutations::Create.field
  field :updateCasting, field: CastingMutations::Update.field
  field :destroyCasting, field: CastingMutations::Destroy.field
=end
