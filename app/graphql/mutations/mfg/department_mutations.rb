module DepartmentMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createDepartment"
    description "create Department"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :department, types.String
    input_field :parent_id, types.ID

# Part1A
# Part1A

    return_field :department, Types::MfgDepartmentType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_department = ctx[:current_user].departments.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_department = Mfg::Department.new(args)

      if new_department.save
        
        
        { department: new_department }
      else
        { messages: new_department.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateDepartment"
    description "Update a Department and return Department"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :department, types.String
    input_field :parent_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :department, Types::MfgDepartmentType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      department = Mfg::Department.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != department.user
        FieldError.error("You can not modify this department because you are not the owner")
      elsif department.update(inputs.to_h)
        
        
        { department: department }
      else
        { messages: department.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyDepartment"
    description "Destroy a Department"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :department, Types::MfgDepartmentType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      department = Mfg::Department.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != department.user
        FieldError.error("You can not modify this department because you are not the owner")
      elsif department.destroy
        {department: department}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createDepartment, field: DepartmentMutations::Create.field
  field :updateDepartment, field: DepartmentMutations::Update.field
  field :destroyDepartment, field: DepartmentMutations::Destroy.field
=end
