module DustResourceMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createDustResource"
    description "create DustResource"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :dust_resource, types.String

# Part1A
# Part1A

    return_field :dustResource, Types::MfgDustResourceType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_dustResource = ctx[:current_user].dustResources.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_dustResource = Mfg::DustResource.new(args)

      if new_dustResource.save
        
        
        { dustResource: new_dustResource }
      else
        { messages: new_dustResource.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateDustResource"
    description "Update a DustResource and return DustResource"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :dust_resource, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :dustResource, Types::MfgDustResourceType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      dustResource = Mfg::DustResource.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != dustResource.user
        FieldError.error("You can not modify this dustResource because you are not the owner")
      elsif dustResource.update(inputs.to_h)
        
        
        { dustResource: dustResource }
      else
        { messages: dustResource.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyDustResource"
    description "Destroy a DustResource"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :dustResource, Types::MfgDustResourceType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      dustResource = Mfg::DustResource.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != dustResource.user
        FieldError.error("You can not modify this dustResource because you are not the owner")
      elsif dustResource.destroy
        {dustResource: dustResource}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createDustResource, field: DustResourceMutations::Create.field
  field :updateDustResource, field: DustResourceMutations::Update.field
  field :destroyDustResource, field: DustResourceMutations::Destroy.field
=end
