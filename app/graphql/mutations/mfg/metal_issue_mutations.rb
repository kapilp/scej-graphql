module MetalIssueMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    d = h.delete 'mfg_txns'
    if d
      d1 = d.map do |a|
        MfgTxnMutations::arg_modify(a, ctx)
      end
      h['mfg_mfg_txns_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('mfg_txn_ids')
      d = h.delete 'mfg_txn_ids'
      h['mfg_mfg_txn_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createMetalIssue"
    description "create MetalIssue"

    input_field :position, types.Int
    input_field :date, types.String
    input_field :slug, types.String
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :mfg_txn_ids, types[types.ID]
    input_field :mfg_txns, types[MfgTxnMutations::Update.input_type]
# Part1A
# Part1A

    return_field :metalIssue, Types::MfgMetalIssueType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_metalIssue = ctx[:current_user].metalIssues.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = MetalIssueMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_metalIssue = Mfg::MetalIssue.new(args)

      if new_metalIssue.save
        if new_metalIssue.state_machine.can_transition_to?(status_id)
          new_metalIssue.state_machine.transition_to!(status_id)
        end
        new_metalIssue = Mfg::MetalIssueView.find(new_metalIssue.id)
        { metalIssue: new_metalIssue }
      else
        { messages: new_metalIssue.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMetalIssue"
    description "Update a MetalIssue and return MetalIssue"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :date, types.String
    input_field :slug, types.String
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :mfg_txn_ids, types[types.ID]
    input_field :mfg_txns, types[MfgTxnMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :metalIssue, Types::MfgMetalIssueType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      metalIssue = Mfg::MetalIssue.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != metalIssue.user
        FieldError.error("You can not modify this metalIssue because you are not the owner")
      elsif metalIssue.update(MetalIssueMutations.arg_modify(inputs, ctx))
        if metalIssue.state_machine.can_transition_to?(status_id)
          metalIssue.state_machine.transition_to!(status_id)
        end
        metalIssue = Mfg::MetalIssueView.find(metalIssue.id)
        { metalIssue: metalIssue }
      else
        { messages: metalIssue.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMetalIssue"
    description "Destroy a MetalIssue"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :metalIssue, Types::MfgMetalIssueType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      metalIssue = Mfg::MetalIssue.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != metalIssue.user
        FieldError.error("You can not modify this metalIssue because you are not the owner")
# Part3B
# Part3B

      elsif metalIssue.destroy
        {metalIssue: metalIssue}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMetalIssue, field: MetalIssueMutations::Create.field
  field :updateMetalIssue, field: MetalIssueMutations::Update.field
  field :destroyMetalIssue, field: MetalIssueMutations::Destroy.field
=end
