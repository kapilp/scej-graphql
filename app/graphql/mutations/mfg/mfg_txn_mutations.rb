module MfgTxnMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['mfg_casting_id'] = h.delete 'casting_id' if h.key?('casting_id')
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['sale_job_id'] = h.delete 'job_id' if h.key?('job_id')
    h['mfg_department_id'] = h.delete 'department_id' if h.key?('department_id')
    d = h.delete 'm_txn_details'
    if d
      d1 = d.map do |a|
        MTxnDetailMutations::arg_modify(a, ctx)
      end
      h['sale_m_txn_details_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('m_txn_detail_ids')
      d = h.delete 'm_txn_detail_ids'
      h['sale_m_txn_detail_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createMfgTxn"
    description "create MfgTxn"

    input_field :position, types.Int
    input_field :mfg_date, types.String
    input_field :casting_id, types.ID
    input_field :mfg_wax_setting_id, types.ID
    input_field :mfg_metal_issue_id, types.ID
    input_field :company_id, types.ID
    input_field :job_id, types.ID
    input_field :department_id, types.ID
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :receive_weight, types.Float
    input_field :status_id, types.String
    input_field :dust_weight, types.Float
    input_field :received, types.Boolean
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
# Part1A
# Part1A

    return_field :mfgTxn, Types::MfgMfgTxnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_mfgTxn = ctx[:current_user].mfgTxns.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = MfgTxnMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_mfgTxn = Mfg::MfgTxn.new(args)

      if new_mfgTxn.save
        if new_mfgTxn.state_machine.can_transition_to?(status_id)
          new_mfgTxn.state_machine.transition_to!(status_id)
        end
        new_mfgTxn = Mfg::MfgTxnView.find(new_mfgTxn.id)
        { mfgTxn: new_mfgTxn }
      else
        { messages: new_mfgTxn.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMfgTxn"
    description "Update a MfgTxn and return MfgTxn"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :mfg_date, types.String
    input_field :casting_id, types.ID
    input_field :mfg_wax_setting_id, types.ID
    input_field :mfg_metal_issue_id, types.ID
    input_field :company_id, types.ID
    input_field :job_id, types.ID
    input_field :department_id, types.ID
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :receive_weight, types.Float
    input_field :status_id, types.String
    input_field :dust_weight, types.Float
    input_field :received, types.Boolean
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :mfgTxn, Types::MfgMfgTxnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mfgTxn = Mfg::MfgTxn.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != mfgTxn.user
        FieldError.error("You can not modify this mfgTxn because you are not the owner")
      elsif mfgTxn.update(MfgTxnMutations.arg_modify(inputs, ctx))
        if mfgTxn.state_machine.can_transition_to?(status_id)
          mfgTxn.state_machine.transition_to!(status_id)
        end
        mfgTxn = Mfg::MfgTxnView.find(mfgTxn.id)
        { mfgTxn: mfgTxn }
      else
        { messages: mfgTxn.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMfgTxn"
    description "Destroy a MfgTxn"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :mfgTxn, Types::MfgMfgTxnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mfgTxn = Mfg::MfgTxn.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != mfgTxn.user
        FieldError.error("You can not modify this mfgTxn because you are not the owner")
# Part3B
# Part3B

      elsif mfgTxn.destroy
        {mfgTxn: mfgTxn}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMfgTxn, field: MfgTxnMutations::Create.field
  field :updateMfgTxn, field: MfgTxnMutations::Update.field
  field :destroyMfgTxn, field: MfgTxnMutations::Destroy.field
=end
