module PriorityMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createPriority"
    description "create Priority"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :priority, types.String
    input_field :isdefault, types.Boolean

# Part1A
# Part1A

    return_field :priority, Types::MfgPriorityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_priority = ctx[:current_user].prioritys.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_priority = Mfg::Priority.new(args)

      if new_priority.save
        
        
        { priority: new_priority }
      else
        { messages: new_priority.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updatePriority"
    description "Update a Priority and return Priority"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :priority, types.String
    input_field :isdefault, types.Boolean

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :priority, Types::MfgPriorityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      priority = Mfg::Priority.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != priority.user
        FieldError.error("You can not modify this priority because you are not the owner")
      elsif priority.update(inputs.to_h)
        
        
        { priority: priority }
      else
        { messages: priority.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyPriority"
    description "Destroy a Priority"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :priority, Types::MfgPriorityType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      priority = Mfg::Priority.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != priority.user
        FieldError.error("You can not modify this priority because you are not the owner")
      elsif priority.destroy
        {priority: priority}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createPriority, field: PriorityMutations::Create.field
  field :updatePriority, field: PriorityMutations::Update.field
  field :destroyPriority, field: PriorityMutations::Destroy.field
=end
