module ProductTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createProductType"
    description "create ProductType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :product_type, types.String
    input_field :isdefault, types.Boolean

# Part1A
# Part1A

    return_field :productType, Types::MfgProductTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_productType = ctx[:current_user].productTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_productType = Mfg::ProductType.new(args)

      if new_productType.save
        
        
        { productType: new_productType }
      else
        { messages: new_productType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateProductType"
    description "Update a ProductType and return ProductType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :product_type, types.String
    input_field :isdefault, types.Boolean

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :productType, Types::MfgProductTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      productType = Mfg::ProductType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != productType.user
        FieldError.error("You can not modify this productType because you are not the owner")
      elsif productType.update(inputs.to_h)
        
        
        { productType: productType }
      else
        { messages: productType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyProductType"
    description "Destroy a ProductType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :productType, Types::MfgProductTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      productType = Mfg::ProductType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != productType.user
        FieldError.error("You can not modify this productType because you are not the owner")
      elsif productType.destroy
        {productType: productType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createProductType, field: ProductTypeMutations::Create.field
  field :updateProductType, field: ProductTypeMutations::Update.field
  field :destroyProductType, field: ProductTypeMutations::Destroy.field
=end
