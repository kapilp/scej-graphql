module RefiningMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_account_id'] = h.delete 'entry_by_id' if h.key?('entry_by_id')
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['mfg_department_id'] = h.delete 'department_id' if h.key?('department_id')
    d = h.delete 'm_txn_details'
    if d
      d1 = d.map do |a|
        MTxnDetailMutations::arg_modify(a, ctx)
      end
      h['sale_m_txn_details_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('m_txn_detail_ids')
      d = h.delete 'm_txn_detail_ids'
      h['sale_m_txn_detail_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createRefining"
    description "create Refining"

    input_field :position, types.Int
    input_field :entry_by_id, types.ID
    input_field :date_from, types.String
    input_field :date_to, types.String
    input_field :company_id, types.ID
    input_field :department_id, types.ID
    input_field :dust_weight, types.Float
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
# Part1A
# Part1A

    return_field :refining, Types::MfgRefiningType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_refining = ctx[:current_user].refinings.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = RefiningMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_refining = Mfg::Refining.new(args)

      if new_refining.save
        if new_refining.state_machine.can_transition_to?(status_id)
          new_refining.state_machine.transition_to!(status_id)
        end
        new_refining = Mfg::RefiningView.find(new_refining.id)
        { refining: new_refining }
      else
        { messages: new_refining.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateRefining"
    description "Update a Refining and return Refining"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :entry_by_id, types.ID
    input_field :date_from, types.String
    input_field :date_to, types.String
    input_field :company_id, types.ID
    input_field :department_id, types.ID
    input_field :dust_weight, types.Float
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :refining, Types::MfgRefiningType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      refining = Mfg::Refining.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != refining.user
        FieldError.error("You can not modify this refining because you are not the owner")
      elsif refining.update(RefiningMutations.arg_modify(inputs, ctx))
        if refining.state_machine.can_transition_to?(status_id)
          refining.state_machine.transition_to!(status_id)
        end
        refining = Mfg::RefiningView.find(refining.id)
        { refining: refining }
      else
        { messages: refining.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyRefining"
    description "Destroy a Refining"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :refining, Types::MfgRefiningType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      refining = Mfg::Refining.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != refining.user
        FieldError.error("You can not modify this refining because you are not the owner")
# Part3B
# Part3B

      elsif refining.destroy
        {refining: refining}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createRefining, field: RefiningMutations::Create.field
  field :updateRefining, field: RefiningMutations::Update.field
  field :destroyRefining, field: RefiningMutations::Destroy.field
=end
