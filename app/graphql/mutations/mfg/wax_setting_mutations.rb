module WaxSettingMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    d = h.delete 'mfg_txns'
    if d
      d1 = d.map do |a|
        MfgTxnMutations::arg_modify(a, ctx)
      end
      h['mfg_mfg_txns_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('mfg_txn_ids')
      d = h.delete 'mfg_txn_ids'
      h['mfg_mfg_txn_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createWaxSetting"
    description "create WaxSetting"

    input_field :position, types.Int
    input_field :date, types.String
    input_field :slug, types.String
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :mfg_txn_ids, types[types.ID]
    input_field :mfg_txns, types[MfgTxnMutations::Update.input_type]
# Part1A
# Part1A

    return_field :waxSetting, Types::MfgWaxSettingType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_waxSetting = ctx[:current_user].waxSettings.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = WaxSettingMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_waxSetting = Mfg::WaxSetting.new(args)

      if new_waxSetting.save
        if new_waxSetting.state_machine.can_transition_to?(status_id)
          new_waxSetting.state_machine.transition_to!(status_id)
        end
        new_waxSetting = Mfg::WaxSettingView.find(new_waxSetting.id)
        { waxSetting: new_waxSetting }
      else
        { messages: new_waxSetting.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateWaxSetting"
    description "Update a WaxSetting and return WaxSetting"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :date, types.String
    input_field :slug, types.String
    input_field :employee_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :mfg_txn_ids, types[types.ID]
    input_field :mfg_txns, types[MfgTxnMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :waxSetting, Types::MfgWaxSettingType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      waxSetting = Mfg::WaxSetting.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != waxSetting.user
        FieldError.error("You can not modify this waxSetting because you are not the owner")
      elsif waxSetting.update(WaxSettingMutations.arg_modify(inputs, ctx))
        if waxSetting.state_machine.can_transition_to?(status_id)
          waxSetting.state_machine.transition_to!(status_id)
        end
        waxSetting = Mfg::WaxSettingView.find(waxSetting.id)
        { waxSetting: waxSetting }
      else
        { messages: waxSetting.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyWaxSetting"
    description "Destroy a WaxSetting"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :waxSetting, Types::MfgWaxSettingType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      waxSetting = Mfg::WaxSetting.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != waxSetting.user
        FieldError.error("You can not modify this waxSetting because you are not the owner")
# Part3B
# Part3B

      elsif waxSetting.destroy
        {waxSetting: waxSetting}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createWaxSetting, field: WaxSettingMutations::Create.field
  field :updateWaxSetting, field: WaxSettingMutations::Update.field
  field :destroyWaxSetting, field: WaxSettingMutations::Destroy.field
=end
