module JobMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['design_style_id'] = h.delete 'style_id' if h.key?('style_id')
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')
    h['mfg_priority_id'] = h.delete 'priority_id' if h.key?('priority_id')
    h['mfg_department_id'] = h.delete 'department_id' if h.key?('department_id')
    d = h.delete 'diamonds'
    if d
      d1 = d.map do |a|
        DiamondMutations::arg_modify(a, ctx)
      end
      h['design_diamonds_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('diamond_ids')
      d = h.delete 'diamond_ids'
      h['design_diamond_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateJob"
    description "Update a Job and return Job"

    input_field :id, types.ID
    input_field :processable_type, types.String
    input_field :processable_id, types.ID
    input_field :salable_type, types.String
    input_field :salable_id, types.ID
    input_field :final, types.Boolean
    input_field :position, types.Int
    input_field :style_id, types.ID
    input_field :company_id, types.ID
    input_field :material_id, types.ID
    input_field :metal_purity_id, types.ID
    input_field :metal_color_id, types.ID
    input_field :qty, types.Int
    input_field :diamond_clarity_id, types.ID
    input_field :diamond_color_id, types.ID
    input_field :cs_clarity_id, types.ID
    input_field :cs_color_id, types.ID
    input_field :instruction, types.String
    input_field :item_size, types.String
    input_field :priority_id, types.ID
    input_field :department_id, types.ID
    input_field :diamond_ids, types[types.ID]
    input_field :diamonds, types[DiamondMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :job, Types::SaleJobType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      job = Sale::Job.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != job.user
        FieldError.error("You can not modify this job because you are not the owner")
      elsif job.update(JobMutations.arg_modify(inputs, ctx))
        
        job = Sale::JobView.find(job.id)
        { job: job }
      else
        { messages: job.fields_errors }
      end
# Part2B
# Part2B

    })
  end

# Part5A
# Part5A

end

=begin
  field :createJob, field: JobMutations::Create.field
  field :updateJob, field: JobMutations::Update.field
  field :destroyJob, field: JobMutations::Destroy.field
=end
