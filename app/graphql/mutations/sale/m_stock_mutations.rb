module MStockMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')
    h['material_metal_purity_id'] = h.delete 'metal_purity_id' if h.key?('metal_purity_id')
    h['material_gem_shape_id'] = h.delete 'gem_shape_id' if h.key?('gem_shape_id')
    h['material_gem_clarity_id'] = h.delete 'gem_clarity_id' if h.key?('gem_clarity_id')
    h['material_color_id'] = h.delete 'color_id' if h.key?('color_id')
    h['material_gem_size_id'] = h.delete 'gem_size_id' if h.key?('gem_size_id')
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['material_locker_id'] = h.delete 'locker_id' if h.key?('locker_id')
    h['material_accessory_id'] = h.delete 'accessory_id' if h.key?('accessory_id')
    h['material_gem_type_id'] = h.delete 'gem_type_id' if h.key?('gem_type_id')

# Part6A
# Part6A

    h
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMStock"
    description "Update a MStock and return MStock"

    input_field :id, types.ID
    input_field :countable_type, types.String
    input_field :countable_id, types.ID
    input_field :countable_r_type, types.String
    input_field :countable_r_id, types.ID
    input_field :position, types.Int
    input_field :material_id, types.ID
    input_field :metal_purity_id, types.ID
    input_field :gem_shape_id, types.ID
    input_field :gem_clarity_id, types.ID
    input_field :color_id, types.ID
    input_field :gem_size_id, types.ID
    input_field :company_id, types.ID
    input_field :locker_id, types.ID
    input_field :accessory_id, types.ID
    input_field :isissue, types.Boolean
    input_field :pcs, types.Int
    input_field :calc_pcs, types.Int
    input_field :weight1, types.Float
    input_field :calc_weight, types.Float
    input_field :note, types.String
    input_field :gem_type_id, types.ID
    input_field :rate_on_pc, types.Boolean
    input_field :rate1, types.Float

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :mStock, Types::SaleMStockType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mStock = Sale::MStock.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != mStock.user
        FieldError.error("You can not modify this mStock because you are not the owner")
      elsif mStock.update(MStockMutations.arg_modify(inputs, ctx))
        
        
        { mStock: mStock }
      else
        { messages: mStock.fields_errors }
      end
# Part2B
# Part2B

    })
  end

# Part5A
# Part5A

end

=begin
  field :createMStock, field: MStockMutations::Create.field
  field :updateMStock, field: MStockMutations::Update.field
  field :destroyMStock, field: MStockMutations::Destroy.field
=end
