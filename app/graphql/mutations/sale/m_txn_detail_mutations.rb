module MTxnDetailMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['material_material_id'] = h.delete 'material_id' if h.key?('material_id')
    h['material_metal_purity_id'] = h.delete 'metal_purity_id' if h.key?('metal_purity_id')
    h['material_gem_shape_id'] = h.delete 'gem_shape_id' if h.key?('gem_shape_id')
    h['material_gem_clarity_id'] = h.delete 'gem_clarity_id' if h.key?('gem_clarity_id')
    h['material_color_id'] = h.delete 'color_id' if h.key?('color_id')
    h['material_gem_size_id'] = h.delete 'gem_size_id' if h.key?('gem_size_id')
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['material_locker_id'] = h.delete 'locker_id' if h.key?('locker_id')
    h['material_accessory_id'] = h.delete 'accessory_id' if h.key?('accessory_id')
    h['material_gem_type_id'] = h.delete 'gem_type_id' if h.key?('gem_type_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createMTxnDetail"
    description "create MTxnDetail"

    input_field :countable_type, types.String
    input_field :countable_id, types.ID
    input_field :countable_r_type, types.String
    input_field :countable_r_id, types.ID
    input_field :position, types.Int
    input_field :material_id, types.ID
    input_field :metal_purity_id, types.ID
    input_field :gem_shape_id, types.ID
    input_field :gem_clarity_id, types.ID
    input_field :color_id, types.ID
    input_field :gem_size_id, types.ID
    input_field :company_id, types.ID
    input_field :locker_id, types.ID
    input_field :accessory_id, types.ID
    input_field :customer_stock, types.Boolean
    input_field :isissue, types.Boolean
    input_field :pcs, types.Int
    input_field :weight1, types.Float
    input_field :note, types.String
    input_field :gem_type_id, types.ID
    input_field :rate1, types.Float
    input_field :rate_on_pc, types.Boolean

# Part1A
# Part1A

    return_field :mTxnDetail, Types::SaleMTxnDetailType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_mTxnDetail = ctx[:current_user].mTxnDetails.build(inputs.to_params)
      

      args = MTxnDetailMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_mTxnDetail = Sale::MTxnDetail.new(args)

      if new_mTxnDetail.save
        
        
        { mTxnDetail: new_mTxnDetail }
      else
        { messages: new_mTxnDetail.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMTxnDetail"
    description "Update a MTxnDetail and return MTxnDetail"

    input_field :id, types.ID
    input_field :countable_type, types.String
    input_field :countable_id, types.ID
    input_field :countable_r_type, types.String
    input_field :countable_r_id, types.ID
    input_field :position, types.Int
    input_field :material_id, types.ID
    input_field :metal_purity_id, types.ID
    input_field :gem_shape_id, types.ID
    input_field :gem_clarity_id, types.ID
    input_field :color_id, types.ID
    input_field :gem_size_id, types.ID
    input_field :company_id, types.ID
    input_field :locker_id, types.ID
    input_field :accessory_id, types.ID
    input_field :customer_stock, types.Boolean
    input_field :isissue, types.Boolean
    input_field :pcs, types.Int
    input_field :weight1, types.Float
    input_field :note, types.String
    input_field :gem_type_id, types.ID
    input_field :rate1, types.Float
    input_field :rate_on_pc, types.Boolean

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :mTxnDetail, Types::SaleMTxnDetailType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mTxnDetail = Sale::MTxnDetail.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != mTxnDetail.user
        FieldError.error("You can not modify this mTxnDetail because you are not the owner")
      elsif mTxnDetail.update(MTxnDetailMutations.arg_modify(inputs, ctx))
        
        
        { mTxnDetail: mTxnDetail }
      else
        { messages: mTxnDetail.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMTxnDetail"
    description "Destroy a MTxnDetail"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :mTxnDetail, Types::SaleMTxnDetailType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mTxnDetail = Sale::MTxnDetail.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != mTxnDetail.user
        FieldError.error("You can not modify this mTxnDetail because you are not the owner")
      elsif mTxnDetail.destroy
        {mTxnDetail: mTxnDetail}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMTxnDetail, field: MTxnDetailMutations::Create.field
  field :updateMTxnDetail, field: MTxnDetailMutations::Update.field
  field :destroyMTxnDetail, field: MTxnDetailMutations::Destroy.field
=end
