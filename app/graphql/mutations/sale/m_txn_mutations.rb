module MTxnMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['sale_m_txn_type_id'] = h.delete 'm_txn_type_id' if h.key?('m_txn_type_id')
    h['user_account_id'] = h.delete 'party_id' if h.key?('party_id')
    d = h.delete 'm_txn_details'
    if d
      d1 = d.map do |a|
        MTxnDetailMutations::arg_modify(a, ctx)
      end
      h['sale_m_txn_details_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('m_txn_detail_ids')
      d = h.delete 'm_txn_detail_ids'
      h['sale_m_txn_detail_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createMTxn"
    description "create MTxn"

    input_field :position, types.Int
    input_field :m_txn_type_id, types.ID
    input_field :party_id, types.ID
    input_field :taken_by_id, types.ID
    input_field :date, types.String
    input_field :due_date, types.String
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
# Part1A
# Part1A

    return_field :mTxn, Types::SaleMTxnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_mTxn = ctx[:current_user].mTxns.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = MTxnMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_mTxn = Sale::MTxn.new(args)

      if new_mTxn.save
        if new_mTxn.state_machine.can_transition_to?(status_id)
          new_mTxn.state_machine.transition_to!(status_id)
        end
        new_mTxn = Sale::MTxnView.find(new_mTxn.id)
        { mTxn: new_mTxn }
      else
        { messages: new_mTxn.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMTxn"
    description "Update a MTxn and return MTxn"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :m_txn_type_id, types.ID
    input_field :party_id, types.ID
    input_field :taken_by_id, types.ID
    input_field :date, types.String
    input_field :due_date, types.String
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :m_txn_detail_ids, types[types.ID]
    input_field :m_txn_details, types[MTxnDetailMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :mTxn, Types::SaleMTxnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mTxn = Sale::MTxn.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != mTxn.user
        FieldError.error("You can not modify this mTxn because you are not the owner")
      elsif mTxn.update(MTxnMutations.arg_modify(inputs, ctx))
        if mTxn.state_machine.can_transition_to?(status_id)
          mTxn.state_machine.transition_to!(status_id)
        end
        mTxn = Sale::MTxnView.find(mTxn.id)
        { mTxn: mTxn }
      else
        { messages: mTxn.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMTxn"
    description "Destroy a MTxn"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :mTxn, Types::SaleMTxnType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mTxn = Sale::MTxn.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != mTxn.user
        FieldError.error("You can not modify this mTxn because you are not the owner")
# Part3B
# Part3B

      elsif mTxn.destroy
        {mTxn: mTxn}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMTxn, field: MTxnMutations::Create.field
  field :updateMTxn, field: MTxnMutations::Update.field
  field :destroyMTxn, field: MTxnMutations::Destroy.field
=end
