module MTxnTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createMTxnType"
    description "create MTxnType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :txn_type, types.String

# Part1A
# Part1A

    return_field :mTxnType, Types::SaleMTxnTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_mTxnType = ctx[:current_user].mTxnTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_mTxnType = Sale::MTxnType.new(args)

      if new_mTxnType.save
        
        
        { mTxnType: new_mTxnType }
      else
        { messages: new_mTxnType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMTxnType"
    description "Update a MTxnType and return MTxnType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :txn_type, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :mTxnType, Types::SaleMTxnTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mTxnType = Sale::MTxnType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != mTxnType.user
        FieldError.error("You can not modify this mTxnType because you are not the owner")
      elsif mTxnType.update(inputs.to_h)
        
        
        { mTxnType: mTxnType }
      else
        { messages: mTxnType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMTxnType"
    description "Destroy a MTxnType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :mTxnType, Types::SaleMTxnTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      mTxnType = Sale::MTxnType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != mTxnType.user
        FieldError.error("You can not modify this mTxnType because you are not the owner")
      elsif mTxnType.destroy
        {mTxnType: mTxnType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMTxnType, field: MTxnTypeMutations::Create.field
  field :updateMTxnType, field: MTxnTypeMutations::Update.field
  field :destroyMTxnType, field: MTxnTypeMutations::Destroy.field
=end
