module MemoMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createMemo"
    description "create Memo"

    input_field :position, types.Int
    input_field :date, types.String
    input_field :description, types.String
    input_field :status_id, types.String

# Part1A
# Part1A

    return_field :memo, Types::SaleMemoType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_memo = ctx[:current_user].memos.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = inputs.to_h
# Part1C
# Part1C

      new_memo = Sale::Memo.new(args)

      if new_memo.save
        if new_memo.state_machine.can_transition_to?(status_id)
          new_memo.state_machine.transition_to!(status_id)
        end
        new_memo = Sale::MemoView.find(new_memo.id)
        { memo: new_memo }
      else
        { messages: new_memo.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateMemo"
    description "Update a Memo and return Memo"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :date, types.String
    input_field :description, types.String
    input_field :status_id, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :memo, Types::SaleMemoType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      memo = Sale::Memo.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != memo.user
        FieldError.error("You can not modify this memo because you are not the owner")
      elsif memo.update(inputs.to_h)
        if memo.state_machine.can_transition_to?(status_id)
          memo.state_machine.transition_to!(status_id)
        end
        memo = Sale::MemoView.find(memo.id)
        { memo: memo }
      else
        { messages: memo.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyMemo"
    description "Destroy a Memo"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :memo, Types::SaleMemoType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      memo = Sale::Memo.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != memo.user
        FieldError.error("You can not modify this memo because you are not the owner")
# Part3B
# Part3B

      elsif memo.destroy
        {memo: memo}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createMemo, field: MemoMutations::Create.field
  field :updateMemo, field: MemoMutations::Update.field
  field :destroyMemo, field: MemoMutations::Destroy.field
=end
