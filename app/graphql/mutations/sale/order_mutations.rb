module OrderMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['mfg_product_type_id'] = h.delete 'product_type_id' if h.key?('product_type_id')
    d = h.delete 'jobs'
    if d
      d1 = d.map do |a|
        JobMutations::arg_modify(a, ctx)
      end
      h['sale_jobs_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('job_ids')
      d = h.delete 'job_ids'
      h['sale_job_ids'] = d if d # if d is not null
    end
    
# Part6A
h['sale_jobs_attributes'].map do |j|
  j['user_company_id'] = ctx[:main_company].id unless j['id']
end
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createOrder"
    description "create Order"

    input_field :position, types.Int
    input_field :company_id, types.ID
    input_field :customer_id, types.ID
    input_field :taken_by_id, types.ID
    input_field :order_date, types.String
    input_field :delivery_date, types.String
    input_field :product_type_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :job_ids, types[types.ID]
    input_field :jobs, types[JobMutations::Update.input_type]
# Part1A
# Part1A

    return_field :order, Types::SaleOrderType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_order = ctx[:current_user].orders.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = OrderMutations.arg_modify(inputs, ctx)
# Part1C
      args['user_company_id'] = ctx[:main_company].id
# Part1C

      new_order = Sale::Order.new(args)

      if new_order.save
        if new_order.state_machine.can_transition_to?(status_id)
          new_order.state_machine.transition_to!(status_id)
        end
        new_order = Sale::OrderView.find(new_order.id)
        { order: new_order }
      else
        { messages: new_order.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateOrder"
    description "Update a Order and return Order"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :company_id, types.ID
    input_field :customer_id, types.ID
    input_field :taken_by_id, types.ID
    input_field :order_date, types.String
    input_field :delivery_date, types.String
    input_field :product_type_id, types.ID
    input_field :description, types.String
    input_field :status_id, types.String
    input_field :job_ids, types[types.ID]
    input_field :jobs, types[JobMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :order, Types::SaleOrderType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      order = Sale::Order.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != order.user
        FieldError.error("You can not modify this order because you are not the owner")
      elsif order.update(OrderMutations.arg_modify(inputs, ctx))
        if order.state_machine.can_transition_to?(status_id)
          order.state_machine.transition_to!(status_id)
        end
        order = Sale::OrderView.find(order.id)
        { order: order }
      else
        { messages: order.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyOrder"
    description "Destroy a Order"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :order, Types::SaleOrderType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      order = Sale::Order.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != order.user
        FieldError.error("You can not modify this order because you are not the owner")
# Part3B
# Part3B

      elsif order.destroy
        {order: order}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createOrder, field: OrderMutations::Create.field
  field :updateOrder, field: OrderMutations::Update.field
  field :destroyOrder, field: OrderMutations::Destroy.field
=end
