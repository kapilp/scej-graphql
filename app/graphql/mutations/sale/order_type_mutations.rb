module OrderTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createOrderType"
    description "create OrderType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :order_type, types.String

# Part1A
# Part1A

    return_field :orderType, Types::SaleOrderTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_orderType = ctx[:current_user].orderTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_orderType = Sale::OrderType.new(args)

      if new_orderType.save
        
        
        { orderType: new_orderType }
      else
        { messages: new_orderType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateOrderType"
    description "Update a OrderType and return OrderType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :order_type, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :orderType, Types::SaleOrderTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      orderType = Sale::OrderType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != orderType.user
        FieldError.error("You can not modify this orderType because you are not the owner")
      elsif orderType.update(inputs.to_h)
        
        
        { orderType: orderType }
      else
        { messages: orderType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyOrderType"
    description "Destroy a OrderType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :orderType, Types::SaleOrderTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      orderType = Sale::OrderType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != orderType.user
        FieldError.error("You can not modify this orderType because you are not the owner")
      elsif orderType.destroy
        {orderType: orderType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createOrderType, field: OrderTypeMutations::Create.field
  field :updateOrderType, field: OrderTypeMutations::Update.field
  field :destroyOrderType, field: OrderTypeMutations::Destroy.field
=end
