module SaleDetailMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createSaleDetail"
    description "create SaleDetail"

    input_field :saleable_type, types.String
    input_field :saleable_id, types.ID
    input_field :position, types.Int
    input_field :tounch, types.Float
    input_field :m_rate_on_id, types.ID
    input_field :m_rate, types.Float
    input_field :d_rate_on_id, types.ID
    input_field :d_rate, types.Float
    input_field :cs_rate_on_id, types.ID
    input_field :cs_rate, types.Float

# Part1A
# Part1A

    return_field :saleDetail, Types::SaleSaleDetailType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_saleDetail = ctx[:current_user].saleDetails.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_saleDetail = Sale::SaleDetail.new(args)

      if new_saleDetail.save
        
        
        { saleDetail: new_saleDetail }
      else
        { messages: new_saleDetail.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateSaleDetail"
    description "Update a SaleDetail and return SaleDetail"

    input_field :id, types.ID
    input_field :saleable_type, types.String
    input_field :saleable_id, types.ID
    input_field :position, types.Int
    input_field :tounch, types.Float
    input_field :m_rate_on_id, types.ID
    input_field :m_rate, types.Float
    input_field :d_rate_on_id, types.ID
    input_field :d_rate, types.Float
    input_field :cs_rate_on_id, types.ID
    input_field :cs_rate, types.Float

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :saleDetail, Types::SaleSaleDetailType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      saleDetail = Sale::SaleDetail.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != saleDetail.user
        FieldError.error("You can not modify this saleDetail because you are not the owner")
      elsif saleDetail.update(inputs.to_h)
        
        
        { saleDetail: saleDetail }
      else
        { messages: saleDetail.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroySaleDetail"
    description "Destroy a SaleDetail"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :saleDetail, Types::SaleSaleDetailType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      saleDetail = Sale::SaleDetail.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != saleDetail.user
        FieldError.error("You can not modify this saleDetail because you are not the owner")
      elsif saleDetail.destroy
        {saleDetail: saleDetail}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createSaleDetail, field: SaleDetailMutations::Create.field
  field :updateSaleDetail, field: SaleDetailMutations::Update.field
  field :destroySaleDetail, field: SaleDetailMutations::Destroy.field
=end
