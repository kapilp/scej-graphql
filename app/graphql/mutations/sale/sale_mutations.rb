module SaleMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createSale"
    description "create Sale"

    input_field :position, types.Int
    input_field :customer_id, types.ID
    input_field :sold_by_id, types.ID
    input_field :date, types.String
    input_field :due_date, types.String
    input_field :description, types.String
    input_field :status_id, types.String

# Part1A
# Part1A

    return_field :sale, Types::SaleSaleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_sale = ctx[:current_user].sales.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = inputs.to_h
# Part1C
# Part1C

      new_sale = Sale::Sale.new(args)

      if new_sale.save
        if new_sale.state_machine.can_transition_to?(status_id)
          new_sale.state_machine.transition_to!(status_id)
        end
        new_sale = Sale::SaleView.find(new_sale.id)
        { sale: new_sale }
      else
        { messages: new_sale.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateSale"
    description "Update a Sale and return Sale"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :customer_id, types.ID
    input_field :sold_by_id, types.ID
    input_field :date, types.String
    input_field :due_date, types.String
    input_field :description, types.String
    input_field :status_id, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :sale, Types::SaleSaleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      sale = Sale::Sale.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != sale.user
        FieldError.error("You can not modify this sale because you are not the owner")
      elsif sale.update(inputs.to_h)
        if sale.state_machine.can_transition_to?(status_id)
          sale.state_machine.transition_to!(status_id)
        end
        sale = Sale::SaleView.find(sale.id)
        { sale: sale }
      else
        { messages: sale.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroySale"
    description "Destroy a Sale"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :sale, Types::SaleSaleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      sale = Sale::Sale.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != sale.user
        FieldError.error("You can not modify this sale because you are not the owner")
# Part3B
# Part3B

      elsif sale.destroy
        {sale: sale}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createSale, field: SaleMutations::Create.field
  field :updateSale, field: SaleMutations::Update.field
  field :destroySale, field: SaleMutations::Destroy.field
=end
