module AccountMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_account_type_id'] = h.delete 'account_type_id' if h.key?('account_type_id')
    h['user_role_id'] = h.delete 'role_id' if h.key?('role_id')
    d = h.delete 'addresses'
    if d
      d1 = d.map do |a|
        AddressMutations::arg_modify(a, ctx)
      end
      h['user_addresses_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('address_ids')
      d = h.delete 'address_ids'
      h['user_address_ids'] = d if d # if d is not null
    end
    
    d = h.delete 'contacts'
    if d
      d1 = d.map do |a|
        ContactMutations::arg_modify(a, ctx)
      end
      h['user_contacts_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('contact_ids')
      d = h.delete 'contact_ids'
      h['user_contact_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createAccount"
    description "create Account"

    input_field :account_type_id, types.ID
    input_field :position, types.Int
    input_field :join_date, types.String
    input_field :slug, types.String
    input_field :account_name, types.String
    input_field :salary, types.Int
    input_field :role_id, types.ID
    input_field :parent_id, types.ID
    input_field :email, types.String
    input_field :password, types.String
    input_field :status_id, types.String
    input_field :encrypted_password, types.String
    input_field :reset_password_token, types.String
    input_field :reset_password_sent_at, types.String
    input_field :remember_created_at, types.String
    input_field :sign_in_count, types.Int
    input_field :current_sign_in_at, types.String
    input_field :last_sign_in_at, types.String
    input_field :current_sign_in_ip, types.String
    input_field :last_sign_in_ip, types.String
    input_field :address_ids, types[types.ID]
    input_field :addresses, types[AddressMutations::Update.input_type]
    input_field :contact_ids, types[types.ID]
    input_field :contacts, types[ContactMutations::Update.input_type]
# Part1A
# Part1A

    return_field :account, Types::UserAccountType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_account = ctx[:current_user].accounts.build(inputs.to_params)
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      args = AccountMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_account = User::Account.new(args)

      if new_account.save
        if new_account.state_machine.can_transition_to?(status_id)
          new_account.state_machine.transition_to!(status_id)
        end
        new_account = User::AccountView.find(new_account.id)
        { account: new_account }
      else
        { messages: new_account.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateAccount"
    description "Update a Account and return Account"

    input_field :id, types.ID
    input_field :account_type_id, types.ID
    input_field :position, types.Int
    input_field :join_date, types.String
    input_field :slug, types.String
    input_field :account_name, types.String
    input_field :salary, types.Int
    input_field :role_id, types.ID
    input_field :parent_id, types.ID
    input_field :email, types.String
    input_field :password, types.String
    input_field :status_id, types.String
    input_field :encrypted_password, types.String
    input_field :reset_password_token, types.String
    input_field :reset_password_sent_at, types.String
    input_field :remember_created_at, types.String
    input_field :sign_in_count, types.Int
    input_field :current_sign_in_at, types.String
    input_field :last_sign_in_at, types.String
    input_field :current_sign_in_ip, types.String
    input_field :last_sign_in_ip, types.String
    input_field :address_ids, types[types.ID]
    input_field :addresses, types[AddressMutations::Update.input_type]
    input_field :contact_ids, types[types.ID]
    input_field :contacts, types[ContactMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :account, Types::UserAccountType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      account = User::Account.find(inputs[:id])
      inputs = inputs.to_h
      status_id = inputs.delete 'status_id'

      if !ctx[:current_user] # if ctx[:current_user] != account.user
        FieldError.error("You can not modify this account because you are not the owner")
      elsif account.update(AccountMutations.arg_modify(inputs, ctx))
        if account.state_machine.can_transition_to?(status_id)
          account.state_machine.transition_to!(status_id)
        end
        account = User::AccountView.find(account.id)
        { account: account }
      else
        { messages: account.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyAccount"
    description "Destroy a Account"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :account, Types::UserAccountType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      account = User::Account.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != account.user
        FieldError.error("You can not modify this account because you are not the owner")
# Part3B
# Part3B

      elsif account.destroy
        {account: account}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createAccount, field: AccountMutations::Create.field
  field :updateAccount, field: AccountMutations::Update.field
  field :destroyAccount, field: AccountMutations::Destroy.field
=end
