module AccountSignInMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_account_type_id'] = h.delete 'account_type_id' if h.key?('account_type_id')
    h['user_role_id'] = h.delete 'role_id' if h.key?('role_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createAccountSignIn"
    description "create AccountSignIn"

    input_field :account_type_id, types.ID
    input_field :position, types.Int
    input_field :join_date, types.String
    input_field :slug, types.String
    input_field :account_name, types.String
    input_field :salary, types.Int
    input_field :role_id, types.ID
    input_field :parent_id, types.ID
    input_field :company, types.String
    input_field :email, types.String
    input_field :password, types.String
    input_field :encrypted_password, types.String
    input_field :reset_password_token, types.String
    input_field :reset_password_sent_at, types.String
    input_field :remember_created_at, types.String
    input_field :sign_in_count, types.Int
    input_field :current_sign_in_at, types.String
    input_field :last_sign_in_at, types.String
    input_field :current_sign_in_ip, types.String
    input_field :last_sign_in_ip, types.String

# Part1A
    return_field :token, types.String
    return_field :company, Types::UserCompanyType
# Part1A

    return_field :accountSignIn, Types::UserAccountType
    return_field :messages, types[Types::FieldErrorType]

    resolve ->(obj, inputs, ctx) {
# Part1B
      # 1. Switch To Tenenent if Exist
      customer = User::Customer.find_by_company_slug(inputs[:company])
      if !customer
        return FieldError.error("Invalid company, email or password")
      end
      Apartment::Tenant.switch!(inputs[:company])
      # 3. switch to new tenant
      # 4. Find UserName
      # 5. Check Token
      # 6. Set Current User
      # 7. Set Current Company

      company = User::Company.find_by(slug: inputs[:company])
      user = User::Account.find_by(email: inputs[:email])
      if user.present? && user.valid_password?(inputs[:password])
        request = ActionDispatch::Request.new({})
        token = Tiddle.create_and_return_token(user, request, expires_in: 12.hours)
        { company: company, accountSignIn: user, token: token }
      else
        FieldError.error("Invalid email or password")
      end
# Part1B

    }
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateAccountSignIn"
    description "Update a AccountSignIn and return AccountSignIn"

    input_field :id, types.ID
    input_field :account_type_id, types.ID
    input_field :position, types.Int
    input_field :join_date, types.String
    input_field :slug, types.String
    input_field :account_name, types.String
    input_field :salary, types.Int
    input_field :role_id, types.ID
    input_field :parent_id, types.ID
    input_field :company, types.String
    input_field :email, types.String
    input_field :password, types.String
    input_field :encrypted_password, types.String
    input_field :reset_password_token, types.String
    input_field :reset_password_sent_at, types.String
    input_field :remember_created_at, types.String
    input_field :sign_in_count, types.Int
    input_field :current_sign_in_at, types.String
    input_field :last_sign_in_at, types.String
    input_field :current_sign_in_ip, types.String
    input_field :last_sign_in_ip, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :accountSignIn, Types::UserAccountType
    return_field :messages, types[Types::FieldErrorType]

    resolve ->(obj, inputs, ctx) {
      accountSignIn = User::Account.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != accountSignIn.user
        FieldError.error("You can not modify this accountSignIn because you are not the owner")
      elsif accountSignIn.update(AccountSignInMutations.arg_modify(inputs, ctx))
        
        accountSignIn = User::AccountView.find(accountSignIn.id)
        { accountSignIn: accountSignIn }
      else
        { messages: accountSignIn.fields_errors }
      end
# Part2B
# Part2B

    }
  end

# Part5A
  RevokeToken = GraphQL::Relay::Mutation.define do
    name "revokeToken"
    description "revoke token"
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # ctx[:current_user].update(access_token: nil)
      # render json: {}

      # Tenent is switched by authenticate method.
      # Tiddle.expire_token(ctx[:current_user], request) if ctx[:current_user] # this use find_token mehtod.
      ctx[:current_token].try(:destroy) if ctx[:current_token]
      {}
    })
  end

# Part5A

end

=begin
  field :createAccountSignIn, field: AccountSignInMutations::Create.field
  field :updateAccountSignIn, field: AccountSignInMutations::Update.field
  field :destroyAccountSignIn, field: AccountSignInMutations::Destroy.field
=end
