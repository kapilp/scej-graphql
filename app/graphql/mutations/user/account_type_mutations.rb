module AccountTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createAccountType"
    description "create AccountType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :account_type, types.String

# Part1A
# Part1A

    return_field :accountType, Types::UserAccountTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_accountType = ctx[:current_user].accountTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_accountType = User::AccountType.new(args)

      if new_accountType.save
        
        
        { accountType: new_accountType }
      else
        { messages: new_accountType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateAccountType"
    description "Update a AccountType and return AccountType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :account_type, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :accountType, Types::UserAccountTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      accountType = User::AccountType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != accountType.user
        FieldError.error("You can not modify this accountType because you are not the owner")
      elsif accountType.update(inputs.to_h)
        
        
        { accountType: accountType }
      else
        { messages: accountType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyAccountType"
    description "Destroy a AccountType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :accountType, Types::UserAccountTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      accountType = User::AccountType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != accountType.user
        FieldError.error("You can not modify this accountType because you are not the owner")
      elsif accountType.destroy
        {accountType: accountType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createAccountType, field: AccountTypeMutations::Create.field
  field :updateAccountType, field: AccountTypeMutations::Update.field
  field :destroyAccountType, field: AccountTypeMutations::Destroy.field
=end
