module AddressMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_account_id'] = h.delete 'account_id' if h.key?('account_id')
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['user_state_id'] = h.delete 'state_id' if h.key?('state_id')
    h['user_country_id'] = h.delete 'country_id' if h.key?('country_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createAddress"
    description "create Address"

    input_field :account_id, types.ID
    input_field :company_id, types.ID
    input_field :position, types.Int
    input_field :firstname, types.String
    input_field :lastname, types.String
    input_field :address1, types.String
    input_field :address2, types.String
    input_field :city, types.String
    input_field :zipcode, types.String
    input_field :state_id, types.ID
    input_field :country_id, types.ID
    input_field :phone, types.String
    input_field :alternative_phone, types.String
    input_field :company, types.String

# Part1A
# Part1A

    return_field :address, Types::UserAddressType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_address = ctx[:current_user].addresss.build(inputs.to_params)
      

      args = AddressMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_address = User::Address.new(args)

      if new_address.save
        
        
        { address: new_address }
      else
        { messages: new_address.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateAddress"
    description "Update a Address and return Address"

    input_field :id, types.ID
    input_field :account_id, types.ID
    input_field :company_id, types.ID
    input_field :position, types.Int
    input_field :firstname, types.String
    input_field :lastname, types.String
    input_field :address1, types.String
    input_field :address2, types.String
    input_field :city, types.String
    input_field :zipcode, types.String
    input_field :state_id, types.ID
    input_field :country_id, types.ID
    input_field :phone, types.String
    input_field :alternative_phone, types.String
    input_field :company, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :address, Types::UserAddressType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      address = User::Address.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != address.user
        FieldError.error("You can not modify this address because you are not the owner")
      elsif address.update(AddressMutations.arg_modify(inputs, ctx))
        
        
        { address: address }
      else
        { messages: address.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyAddress"
    description "Destroy a Address"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :address, Types::UserAddressType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      address = User::Address.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != address.user
        FieldError.error("You can not modify this address because you are not the owner")
      elsif address.destroy
        {address: address}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createAddress, field: AddressMutations::Create.field
  field :updateAddress, field: AddressMutations::Update.field
  field :destroyAddress, field: AddressMutations::Destroy.field
=end
