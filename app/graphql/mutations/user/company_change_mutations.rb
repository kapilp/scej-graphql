module CompanyChangeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createCompanyChange"
    description "create CompanyChange"

    input_field :position, types.Int
    input_field :current_company, types.Int
    input_field :next_company, types.Int
    input_field :slug, types.String
    input_field :company, types.String
    input_field :gst_no, types.String
    input_field :parent_id, types.ID

# Part1A
# Part1A

    return_field :companyChange, Types::UserCompanyType
    return_field :messages, types[Types::FieldErrorType]

    resolve ->(obj, inputs, ctx) {
# Part1B
      previous_company = User::Company.find(inputs['current_company'])
      next_company = User::Company.find(inputs['next_company'])
      if(previous_company && next_company)
        {companyChange: next_company}
      end
# Part1B

    }
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateCompanyChange"
    description "Update a CompanyChange and return CompanyChange"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :current_company, types.Int
    input_field :next_company, types.Int
    input_field :slug, types.String
    input_field :company, types.String
    input_field :gst_no, types.String
    input_field :parent_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :companyChange, Types::UserCompanyType
    return_field :messages, types[Types::FieldErrorType]

    resolve ->(obj, inputs, ctx) {
      companyChange = User::Company.find(inputs[:id])


      if !ctx[:current_user] # if ctx[:current_user] != companyChange.user
        FieldError.error("You can not modify this companyChange because you are not the owner")
      elsif companyChange.update(inputs.to_h)


        { companyChange: companyChange }
      else
        { messages: companyChange.fields_errors }
      end
# Part2B
# Part2B

    }
  end

# Part5A
# Part5A

end

=begin
  field :createCompanyChange, field: CompanyChangeMutations::Create.field
  field :updateCompanyChange, field: CompanyChangeMutations::Update.field
  field :destroyCompanyChange, field: CompanyChangeMutations::Destroy.field
=end
