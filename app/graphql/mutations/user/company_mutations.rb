module CompanyMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    d = h.delete 'contacts'
    if d
      d1 = d.map do |a|
        ContactMutations::arg_modify(a, ctx)
      end
      h['user_contacts_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('contact_ids')
      d = h.delete 'contact_ids'
      h['user_contact_ids'] = d if d # if d is not null
    end
    
    d = h.delete 'addresses'
    if d
      d1 = d.map do |a|
        AddressMutations::arg_modify(a, ctx)
      end
      h['user_addresses_attributes'] = d1
    end
    # when this key exist it doesnt allowing to create new attributes.
    # only add this array when selecting existing attributes.
    if h.key?('address_ids')
      d = h.delete 'address_ids'
      h['user_address_ids'] = d if d # if d is not null
    end
    
# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createCompany"
    description "create Company"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :company, types.String
    input_field :gst_no, types.String
    input_field :parent_id, types.ID
    input_field :contact_ids, types[types.ID]
    input_field :contacts, types[ContactMutations::Update.input_type]
    input_field :address_ids, types[types.ID]
    input_field :addresses, types[AddressMutations::Update.input_type]
# Part1A
# Part1A

    return_field :company, Types::UserCompanyType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_company = ctx[:current_user].companys.build(inputs.to_params)
      

      args = CompanyMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_company = User::Company.new(args)

      if new_company.save
        
        
        { company: new_company }
      else
        { messages: new_company.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateCompany"
    description "Update a Company and return Company"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :company, types.String
    input_field :gst_no, types.String
    input_field :parent_id, types.ID
    input_field :contact_ids, types[types.ID]
    input_field :contacts, types[ContactMutations::Update.input_type]
    input_field :address_ids, types[types.ID]
    input_field :addresses, types[AddressMutations::Update.input_type]
    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :company, Types::UserCompanyType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      company = User::Company.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != company.user
        FieldError.error("You can not modify this company because you are not the owner")
      elsif company.update(CompanyMutations.arg_modify(inputs, ctx))
        
        
        { company: company }
      else
        { messages: company.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyCompany"
    description "Destroy a Company"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :company, Types::UserCompanyType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      company = User::Company.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != company.user
        FieldError.error("You can not modify this company because you are not the owner")
# Part3B
# Part3B

      elsif company.destroy
        {company: company}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createCompany, field: CompanyMutations::Create.field
  field :updateCompany, field: CompanyMutations::Update.field
  field :destroyCompany, field: CompanyMutations::Destroy.field
=end
