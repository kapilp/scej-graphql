module CompanyOwnershipMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['user_account_id'] = h.delete 'account_id' if h.key?('account_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createCompanyOwnership"
    description "create CompanyOwnership"

    input_field :company_id, types.ID
    input_field :account_id, types.ID

# Part1A
# Part1A

    return_field :companyOwnership, Types::UserCompanyOwnershipType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_companyOwnership = ctx[:current_user].companyOwnerships.build(inputs.to_params)
      

      args = CompanyOwnershipMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_companyOwnership = User::CompanyOwnership.new(args)

      if new_companyOwnership.save
        
        
        { companyOwnership: new_companyOwnership }
      else
        { messages: new_companyOwnership.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateCompanyOwnership"
    description "Update a CompanyOwnership and return CompanyOwnership"

    input_field :id, types.ID
    input_field :company_id, types.ID
    input_field :account_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :companyOwnership, Types::UserCompanyOwnershipType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      companyOwnership = User::CompanyOwnership.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != companyOwnership.user
        FieldError.error("You can not modify this companyOwnership because you are not the owner")
      elsif companyOwnership.update(CompanyOwnershipMutations.arg_modify(inputs, ctx))
        
        
        { companyOwnership: companyOwnership }
      else
        { messages: companyOwnership.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyCompanyOwnership"
    description "Destroy a CompanyOwnership"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :companyOwnership, Types::UserCompanyOwnershipType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      companyOwnership = User::CompanyOwnership.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != companyOwnership.user
        FieldError.error("You can not modify this companyOwnership because you are not the owner")
      elsif companyOwnership.destroy
        {companyOwnership: companyOwnership}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createCompanyOwnership, field: CompanyOwnershipMutations::Create.field
  field :updateCompanyOwnership, field: CompanyOwnershipMutations::Update.field
  field :destroyCompanyOwnership, field: CompanyOwnershipMutations::Destroy.field
=end
