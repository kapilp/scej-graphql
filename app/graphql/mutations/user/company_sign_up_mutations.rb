module CompanySignUpMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createCompanySignUp"
    description "create CompanySignUp"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :company, types.String
    input_field :account_name, types.String
    input_field :email, types.String
    input_field :password, types.String
    input_field :password_confirmation, types.String
    input_field :gst_no, types.String
    input_field :parent_id, types.ID

# Part1A
    return_field :token, types.String
    return_field :new_account, Types::UserAccountType
# Part1A

    return_field :companySignUp, Types::UserCompanyType
    return_field :messages, types[Types::FieldErrorType]

    resolve ->(obj, inputs, ctx) {
# Part1B
  # 1. Create Entery in customers table
  customer = User::Customer.new(company_slug: inputs['slug'])
  if !customer.save
    return { messages: [FieldError.new('slug', 'Already Exists')] }
  end
  # 2. Create new tenant
  # # 2. Create tenant, comapny and account
  # HardWorker.perform_async(inputs.to_h)
  begin
    Apartment::Tenant.create(inputs['slug'])
  rescue ::Apartment::TenantExists => e
    # Rollbar.error(e, "ERROR: Apartment Tenant Already Exists: #{Apartment::Tenant.current.inspect}")
    return { messages:  [FieldError.new('slug', 'Already Exists')] }
  end

  # 3. switch to new tenant
  Apartment::Tenant.switch(inputs['slug']) do
    # 4. Create new Company in new tenant
    company = User::Company.create!(slug: inputs['slug'], company: inputs['company'])
    # 5. Create new Account Below Company in new tenant
    account = User::Account.create!(user_account_type_id: 1, slug: inputs['account_name'], join_date: DateTime.now ,account_name: inputs['account_name'], email: inputs['email'], password: inputs['password'], user_role_id: 1)
    company.user_accounts << account
    # 6. Generate Token For the user and return token:
    if account
      request = ActionDispatch::Request.new({})
      token = Tiddle.create_and_return_token(account, request, expires_in: 12.hours)
      { companySignUp: company, new_account: account, token: token }
    else
      { messages: account.fields_errors }
    end

  end
# Part1B

    }
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateCompanySignUp"
    description "Update a CompanySignUp and return CompanySignUp"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :company, types.String
    input_field :account_name, types.String
    input_field :email, types.String
    input_field :password, types.String
    input_field :password_confirmation, types.String
    input_field :gst_no, types.String
    input_field :parent_id, types.ID

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :companySignUp, Types::UserCompanyType
    return_field :messages, types[Types::FieldErrorType]

    resolve ->(obj, inputs, ctx) {
      companySignUp = User::Company.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != companySignUp.user
        FieldError.error("You can not modify this companySignUp because you are not the owner")
      elsif companySignUp.update(inputs.to_h)
        
        
        { companySignUp: companySignUp }
      else
        { messages: companySignUp.fields_errors }
      end
# Part2B
# Part2B

    }
  end

# Part5A
# Part5A

end

=begin
  field :createCompanySignUp, field: CompanySignUpMutations::Create.field
  field :updateCompanySignUp, field: CompanySignUpMutations::Update.field
  field :destroyCompanySignUp, field: CompanySignUpMutations::Destroy.field
=end
