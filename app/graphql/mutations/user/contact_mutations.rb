module ContactMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_company_id'] = h.delete 'company_id' if h.key?('company_id')
    h['user_account_id'] = h.delete 'account_id' if h.key?('account_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createContact"
    description "create Contact"

    input_field :company_id, types.ID
    input_field :account_id, types.ID
    input_field :position, types.Int
    input_field :company_name, types.String
    input_field :first_name, types.String
    input_field :last_name, types.String
    input_field :email, types.String
    input_field :phone, types.String
    input_field :mobile, types.String

# Part1A
# Part1A

    return_field :contact, Types::UserContactType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_contact = ctx[:current_user].contacts.build(inputs.to_params)
      

      args = ContactMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_contact = User::Contact.new(args)

      if new_contact.save
        
        
        { contact: new_contact }
      else
        { messages: new_contact.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateContact"
    description "Update a Contact and return Contact"

    input_field :id, types.ID
    input_field :company_id, types.ID
    input_field :account_id, types.ID
    input_field :position, types.Int
    input_field :company_name, types.String
    input_field :first_name, types.String
    input_field :last_name, types.String
    input_field :email, types.String
    input_field :phone, types.String
    input_field :mobile, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :contact, Types::UserContactType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      contact = User::Contact.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != contact.user
        FieldError.error("You can not modify this contact because you are not the owner")
      elsif contact.update(ContactMutations.arg_modify(inputs, ctx))
        
        
        { contact: contact }
      else
        { messages: contact.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyContact"
    description "Destroy a Contact"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :contact, Types::UserContactType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      contact = User::Contact.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != contact.user
        FieldError.error("You can not modify this contact because you are not the owner")
      elsif contact.destroy
        {contact: contact}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createContact, field: ContactMutations::Create.field
  field :updateContact, field: ContactMutations::Update.field
  field :destroyContact, field: ContactMutations::Destroy.field
=end
