module CountryMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createCountry"
    description "create Country"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :country, types.String

# Part1A
# Part1A

    return_field :country, Types::UserCountryType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_country = ctx[:current_user].countrys.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_country = User::Country.new(args)

      if new_country.save
        
        
        { country: new_country }
      else
        { messages: new_country.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateCountry"
    description "Update a Country and return Country"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :country, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :country, Types::UserCountryType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      country = User::Country.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != country.user
        FieldError.error("You can not modify this country because you are not the owner")
      elsif country.update(inputs.to_h)
        
        
        { country: country }
      else
        { messages: country.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyCountry"
    description "Destroy a Country"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :country, Types::UserCountryType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      country = User::Country.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != country.user
        FieldError.error("You can not modify this country because you are not the owner")
      elsif country.destroy
        {country: country}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createCountry, field: CountryMutations::Create.field
  field :updateCountry, field: CountryMutations::Update.field
  field :destroyCountry, field: CountryMutations::Destroy.field
=end
