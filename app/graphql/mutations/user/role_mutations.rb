module RoleMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createRole"
    description "create Role"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :role, types.String

# Part1A
# Part1A

    return_field :role, Types::UserRoleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_role = ctx[:current_user].roles.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_role = User::Role.new(args)

      if new_role.save
        
        
        { role: new_role }
      else
        { messages: new_role.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateRole"
    description "Update a Role and return Role"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :role, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :role, Types::UserRoleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      role = User::Role.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != role.user
        FieldError.error("You can not modify this role because you are not the owner")
      elsif role.update(inputs.to_h)
        
        
        { role: role }
      else
        { messages: role.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyRole"
    description "Destroy a Role"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :role, Types::UserRoleType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      role = User::Role.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != role.user
        FieldError.error("You can not modify this role because you are not the owner")
# Part3B
# Part3B

      elsif role.destroy
        {role: role}
      end
    })

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createRole, field: RoleMutations::Create.field
  field :updateRole, field: RoleMutations::Update.field
  field :destroyRole, field: RoleMutations::Destroy.field
=end
