module StateMutations
  def self.arg_modify(inputs, ctx)
    h = inputs.to_h
    h['user_country_id'] = h.delete 'country_id' if h.key?('country_id')

# Part6A
# Part6A

    h
  end
  Create = GraphQL::Relay::Mutation.define do
    name "createState"
    description "create State"

    input_field :position, types.Int
    input_field :country_id, types.ID
    input_field :slug, types.String
    input_field :state, types.String

# Part1A
# Part1A

    return_field :state, Types::UserStateType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_state = ctx[:current_user].states.build(inputs.to_params)
      

      args = StateMutations.arg_modify(inputs, ctx)
# Part1C
# Part1C

      new_state = User::State.new(args)

      if new_state.save
        
        
        { state: new_state }
      else
        { messages: new_state.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateState"
    description "Update a State and return State"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :country_id, types.ID
    input_field :slug, types.String
    input_field :state, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :state, Types::UserStateType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      state = User::State.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != state.user
        FieldError.error("You can not modify this state because you are not the owner")
      elsif state.update(StateMutations.arg_modify(inputs, ctx))
        
        
        { state: state }
      else
        { messages: state.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyState"
    description "Destroy a State"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :state, Types::UserStateType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      state = User::State.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != state.user
        FieldError.error("You can not modify this state because you are not the owner")
      elsif state.destroy
        {state: state}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createState, field: StateMutations::Create.field
  field :updateState, field: StateMutations::Update.field
  field :destroyState, field: StateMutations::Destroy.field
=end
