module TaxRateMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createTaxRate"
    description "create TaxRate"

    input_field :user_tax_type_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :name, types.String
    input_field :rate, types.Float

# Part1A
# Part1A

    return_field :taxRate, Types::UserTaxRateType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_taxRate = ctx[:current_user].taxRates.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_taxRate = User::TaxRate.new(args)

      if new_taxRate.save
        
        
        { taxRate: new_taxRate }
      else
        { messages: new_taxRate.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateTaxRate"
    description "Update a TaxRate and return TaxRate"

    input_field :id, types.ID
    input_field :user_tax_type_id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :name, types.String
    input_field :rate, types.Float

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :taxRate, Types::UserTaxRateType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      taxRate = User::TaxRate.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != taxRate.user
        FieldError.error("You can not modify this taxRate because you are not the owner")
      elsif taxRate.update(inputs.to_h)
        
        
        { taxRate: taxRate }
      else
        { messages: taxRate.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyTaxRate"
    description "Destroy a TaxRate"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :taxRate, Types::UserTaxRateType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      taxRate = User::TaxRate.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != taxRate.user
        FieldError.error("You can not modify this taxRate because you are not the owner")
      elsif taxRate.destroy
        {taxRate: taxRate}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createTaxRate, field: TaxRateMutations::Create.field
  field :updateTaxRate, field: TaxRateMutations::Update.field
  field :destroyTaxRate, field: TaxRateMutations::Destroy.field
=end
