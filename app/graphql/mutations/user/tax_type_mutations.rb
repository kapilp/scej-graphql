module TaxTypeMutations
  Create = GraphQL::Relay::Mutation.define do
    name "createTaxType"
    description "create TaxType"

    input_field :position, types.Int
    input_field :slug, types.String
    input_field :tax_type, types.String

# Part1A
# Part1A

    return_field :taxType, Types::UserTaxTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      # new_taxType = ctx[:current_user].taxTypes.build(inputs.to_params)
      

      args = inputs.to_h
# Part1C
# Part1C

      new_taxType = User::TaxType.new(args)

      if new_taxType.save
        
        
        { taxType: new_taxType }
      else
        { messages: new_taxType.fields_errors }
      end
# Part1B
# Part1B

    })
  end

  Update = GraphQL::Relay::Mutation.define do
    name "updateTaxType"
    description "Update a TaxType and return TaxType"

    input_field :id, types.ID
    input_field :position, types.Int
    input_field :slug, types.String
    input_field :tax_type, types.String

    input_field :_destroy, types.Boolean
# Part2A
# Part2A

    return_field :taxType, Types::UserTaxTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      taxType = User::TaxType.find(inputs[:id])
      

      if !ctx[:current_user] # if ctx[:current_user] != taxType.user
        FieldError.error("You can not modify this taxType because you are not the owner")
      elsif taxType.update(inputs.to_h)
        
        
        { taxType: taxType }
      else
        { messages: taxType.fields_errors }
      end
# Part2B
# Part2B

    })
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "destroyTaxType"
    description "Destroy a TaxType"
    input_field :id, types.ID
# Part3A
# Part3A

    return_field :taxType, Types::UserTaxTypeType
    return_field :messages, types[Types::FieldErrorType]

    resolve(Auth.protect ->(obj, inputs, ctx) {
      taxType = User::TaxType.find(inputs[:id])

      if !ctx[:current_user] # if ctx[:current_user] != taxType.user
        FieldError.error("You can not modify this taxType because you are not the owner")
      elsif taxType.destroy
        {taxType: taxType}
      end
    })
# Part3B
# Part3B

  end
# Part4A
# Part4A

# Part5A
# Part5A

end

=begin
  field :createTaxType, field: TaxTypeMutations::Create.field
  field :updateTaxType, field: TaxTypeMutations::Update.field
  field :destroyTaxType, field: TaxTypeMutations::Destroy.field
=end
