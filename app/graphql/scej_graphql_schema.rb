# Multiplex Instrumentation : http://graphql-ruby.org/queries/multiplex.html#multiplex-instrumentation
# You can add hooks for each multiplex run with multiplex instrumentation.
# An instrumenter must implement .before_multiplex(multiplex) and .after_multiplex(multiplex). 
# Then, it can be mounted with instrument(:multiplex, MyMultiplexAnalyzer).
# Count how many queries are in the multiplex run:
module MultiplexCounter
  def self.before_multiplex(multiplex)
    Rails.logger.info("Multiplex size: #{multiplex.queries.length}")
  end

  def self.after_multiplex(multiplex)
  end
end

ScejGraphqlSchema = GraphQL::Schema.define do
  max_depth 9
  max_complexity 200
  use GraphQL::Batch
  use GraphQL::Subscriptions::ActionCableSubscriptions

  query(Types::QueryType)
  mutation(Types::MutationType)
  subscription(Types::SubscriptionType)

  instrument(:multiplex, MultiplexCounter)

  # schema contains Interfaces or Unions, 
  # so you must define a `resolve_type -> (obj, ctx) { ... }` function
  # resolve_type ->(type, obj, ctx) {
  #   case obj
  #     when Material::Material
  #       Types::MaterialMaterialType
  #     else
  #       raise("Unexpected object: #{obj}")
  #   end
  # }
  # 1. Defining UUIDs
  object_from_id ->(id, _ctx) { decode_object(id) }
  id_from_object ->(obj, type, _ctx) { encode_object(obj, type) }
  
  # 2. Node interface: To tell GraphQL how to resolve members of the "Node" interface, you must also define Schema#resolve_type:
  resolve_type ->(type, obj, _ctx) { RelaySchema.types[type_name(obj)] }
  
  # Use Query Analysis http://graphql-ruby.org/queries/analysis
  # and Multiplex-Level Analysis
end

def type_name(object)
  object.class.name
end

# Create UUIDs by joining the type name & ID, then base64-encoding it
def encode_object(object, type)
  GraphQL::Schema::UniqueWithinType.encode(
    type.name,
    object.id,
    separator: '---'
  )
end

def decode_object(id)
  type_name, object_id = GraphQL::Schema::UniqueWithinType.decode(
    id,
    separator: '---'
  )
  # Now, based on `type_name` and `id`
  # find an object in your application
  Object.const_get(type_name).find(object_id)
end

# Responsible for dumping Schema.json to app/assets/javascripts/relay/
module RelaySchemaHelpers
  # Schema.json location
  SCHEMA_DIR  = Rails.root.join('app/assets/javascripts/relay/')
  SCHEMA_PATH = File.join(SCHEMA_DIR, 'schema.json')

  def execute_introspection_query
    # Cache the query result
    Rails.cache.fetch checksum do
      RelaySchema.execute GraphQL::Introspection::INTROSPECTION_QUERY
    end
  end

  def checksum
    files   = Dir['app/api/**/*.rb'].reject { |f| File.directory?(f) }
    content = files.map { |f| File.read(f) }.join
    Digest::SHA256.hexdigest(content).to_s
  end

  def dump_schema
    # Generate the schema on start/reload
    FileUtils.mkdir_p SCHEMA_DIR
    result = JSON.pretty_generate(RelaySchema.execute_introspection_query)
    unless File.exist?(SCHEMA_PATH) && File.read(SCHEMA_PATH) == result
      File.write(SCHEMA_PATH, result)
    end
  end
end

ScejGraphqlSchema.extend RelaySchemaHelpers