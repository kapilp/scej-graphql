Types::DesignCollectionType = GraphQL::ObjectType.define do
  name 'DesignCollection'
  description 'DesignCollection'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :collection, types.String
  field :parent_id, -> { Types::DesignCollectionType }, property: :parent
  # ObjectType :
  field :childs, !types[Types::DesignCollectionType] do
    # argument :limit, types.Int
   resolve ->(object, args, ctx) {
     # stars = object.cast.stars
     object.leaves
     # stars
   }
 end
end
