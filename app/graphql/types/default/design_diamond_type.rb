Types::DesignDiamondType = GraphQL::ObjectType.define do
  name 'DesignDiamond'
  description 'DesignDiamond'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :gem_shape_id, -> { Types::MaterialGemShapeType }, property: :material_gem_shape do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemShape).load(obj.material_gem_shape_id) } end
  field :gem_clarity_id, -> { Types::MaterialGemClarityType }, property: :material_gem_clarity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemClarity).load(obj.material_gem_clarity_id) } end
  field :color_id, -> { Types::MaterialColorType }, property: :material_color do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Color).load(obj.material_color_id) } end
  field :gem_size_id, -> { Types::MaterialGemSizeType }, property: :material_gem_size do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemSize).load(obj.material_gem_size_id) } end
  field :pcs, types.Int
  field :style_diamonds, !types[Types::DesignStyleDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_style_diamonds
    }
  end
  field :styles, !types[Types::DesignStyleType] do
    resolve ->(obj, args, ctx) {
      obj.design_styles
    }
  end
  field :job_diamonds, !types[Types::DesignJobDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_job_diamonds
    }
  end
  field :jobs, !types[Types::SaleJobType] do
    resolve ->(obj, args, ctx) {
      obj.sale_jobs
    }
  end
# Part1A
# Part1A

end
