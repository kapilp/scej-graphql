Types::DesignImageType = GraphQL::ObjectType.define do
  name 'DesignImage'
  description 'DesignImage'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :imageable_type, types.String
  field :image_data, types.String

# Part1A
  field :image_data_url, types.String do
    resolve ->(image, _args, _ctx) {
      image.image_url
    }
  end
# Part1A

end
