Types::DesignJobDiamondType = GraphQL::ObjectType.define do
  name 'DesignJobDiamond'
  description 'DesignJobDiamond'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :job_id, -> { Types::SaleJobType }, property: :sale_job do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::Job).load(obj.sale_job_id) } end
  field :design_diamond_id, -> { Types::DesignDiamondType }, property: :design_diamond do resolve ->(obj, args, ctx) { RecordLoader.for(Design::Diamond).load(obj.design_diamond_id) } end

# Part1A
# Part1A

end
