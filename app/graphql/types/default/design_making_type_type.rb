Types::DesignMakingTypeType = GraphQL::ObjectType.define do
  name 'DesignMakingType'
  description 'DesignMakingType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :making_type, types.String
  field :isdefault, types.Boolean

# Part1A
# Part1A

end
