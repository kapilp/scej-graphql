Types::DesignSettingTypeType = GraphQL::ObjectType.define do
  name 'DesignSettingType'
  description 'DesignSettingType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :setting_type, types.String
  field :isdefault, types.Boolean

# Part1A
# Part1A

end
