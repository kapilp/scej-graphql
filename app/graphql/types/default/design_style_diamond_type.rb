Types::DesignStyleDiamondType = GraphQL::ObjectType.define do
  name 'DesignStyleDiamond'
  description 'DesignStyleDiamond'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :style_id, -> { Types::DesignStyleType }, property: :design_style do resolve ->(obj, args, ctx) { RecordLoader.for(Design::Style).load(obj.design_style_id) } end
  field :design_diamond_id, -> { Types::DesignDiamondType }, property: :design_diamond do resolve ->(obj, args, ctx) { RecordLoader.for(Design::Diamond).load(obj.design_diamond_id) } end

# Part1A
# Part1A

end
