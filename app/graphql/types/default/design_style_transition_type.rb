Types::DesignStyleTransitionType = GraphQL::ObjectType.define do
  name 'DesignStyleTransition'
  description 'DesignStyleTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :style_id, -> { Types::DesignStyleType }, property: :design_style do resolve ->(obj, args, ctx) { RecordLoader.for(Design::Style).load(obj.design_style_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
