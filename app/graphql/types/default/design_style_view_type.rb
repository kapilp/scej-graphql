Types::DesignStyleViewType = GraphQL::ObjectType.define do
  name 'DesignStyleView'
  description 'DesignStyleView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :collection_id, -> { Types::DesignCollectionType }, property: :design_collection do resolve ->(obj, args, ctx) { RecordLoader.for(Design::Collection).load(obj.design_collection_id) } end
  field :position, types.Int
  field :slug, types.String
  field :making_type_id, -> { Types::DesignMakingTypeType }, property: :design_making_type do resolve ->(obj, args, ctx) { RecordLoader.for(Design::MakingType).load(obj.design_making_type_id) } end
  field :setting_type_id, -> { Types::DesignSettingTypeType }, property: :design_setting_type do resolve ->(obj, args, ctx) { RecordLoader.for(Design::SettingType).load(obj.design_setting_type_id) } end
  field :designer_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :metal_purity_id, -> { Types::MaterialMetalPurityType }, property: :material_metal_purity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::MetalPurity).load(obj.material_metal_purity_id) } end
  field :color_id, -> { Types::MaterialColorType }, property: :material_color do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Color).load(obj.material_color_id) } end
  field :net_weight, types.Float
  field :volume, types.Float
  field :pure_weight, types.Float
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :active, types.Boolean
  field :diamond_pcs, types.Int
  field :diamond_weight, types.Float
  field :cs_pcs, types.Int
  field :cs_weight, types.Float
  field :gross_weight, types.Float
  field :style_diamonds, !types[Types::DesignStyleDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_style_diamonds
    }
  end
  field :diamonds, !types[Types::DesignDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_diamonds
    }
  end
  field :diamond_views, !types[Types::DesignDiamondViewType] do
    resolve ->(obj, args, ctx) {
      obj.design_diamond_views
    }
  end
  field :images, !types[Types::DesignImageType] do
    resolve ->(obj, args, ctx) {
      obj.design_images
    }
  end
  field :style_transitions, !types[Types::DesignStyleTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.design_style_transitions
    }
  end
# Part1A
# Part1A

end
