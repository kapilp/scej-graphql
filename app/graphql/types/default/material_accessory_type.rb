Types::MaterialAccessoryType = GraphQL::ObjectType.define do
  name 'MaterialAccessory'
  description 'MaterialAccessory'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :accessory, types.String
  field :m_txn_details, !types[Types::SaleMTxnDetailType] do
    resolve ->(obj, args, ctx) {
      obj.sale_m_txn_details
    }
  end
# Part1A
# Part1A

end
