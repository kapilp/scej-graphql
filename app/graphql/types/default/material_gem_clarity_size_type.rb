Types::MaterialGemClaritySizeType = GraphQL::ObjectType.define do
  name 'MaterialGemClaritySize'
  description 'MaterialGemClaritySize'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :gem_clarity_id, -> { Types::MaterialGemClarityType }, property: :material_gem_clarity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemClarity).load(obj.material_gem_clarity_id) } end
  field :gem_size_id, -> { Types::MaterialGemSizeType }, property: :material_gem_size do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemSize).load(obj.material_gem_size_id) } end

# Part1A
# Part1A

end
