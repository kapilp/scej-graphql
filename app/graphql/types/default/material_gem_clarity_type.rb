Types::MaterialGemClarityType = GraphQL::ObjectType.define do
  name 'MaterialGemClarity'
  description 'MaterialGemClarity'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :position, types.Int
  field :slug, types.String
  field :clarity, types.String
  field :gem_clarity_sizes, !types[Types::MaterialGemClaritySizeType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_clarity_sizes
    }
  end
  field :gem_sizes, !types[Types::MaterialGemSizeType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_sizes
    }
  end
  field :diamonds, !types[Types::DesignDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_diamonds
    }
  end
# Part1A
# Part1A

end
