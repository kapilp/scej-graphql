Types::MaterialGemShapeType = GraphQL::ObjectType.define do
  name 'MaterialGemShape'
  description 'MaterialGemShape'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :position, types.Int
  field :slug, types.String
  field :shape, types.String
  field :gem_sizes, !types[Types::MaterialGemSizeType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_sizes
    }
  end
  field :diamonds, !types[Types::DesignDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_diamonds
    }
  end
# Part1A
# Part1A

end
