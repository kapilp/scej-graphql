Types::MaterialGemSizeType = GraphQL::ObjectType.define do
  name 'MaterialGemSize'
  description 'MaterialGemSize'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :gem_shape_id, -> { Types::MaterialGemShapeType }, property: :material_gem_shape do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemShape).load(obj.material_gem_shape_id) } end
  field :gem_clarity_ids, types.Int do
    resolve ->(obj, args, ctx) {
      0
    }
  end
  field :position, types.Int
  field :slug, types.String
  field :mm_size, types.String
  field :carat_weight, types.Float
  field :price, types.Float
  field :gem_clarity_sizes, !types[Types::MaterialGemClaritySizeType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_clarity_sizes
    }
  end
  field :gem_clarities, !types[Types::MaterialGemClarityType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_clarities
    }
  end
# Part1A
# Part1A

end
