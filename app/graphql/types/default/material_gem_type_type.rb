Types::MaterialGemTypeType = GraphQL::ObjectType.define do
  name 'MaterialGemType'
  description 'MaterialGemType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :gem_type, types.String
  field :isdefault, types.Boolean

# Part1A
# Part1A

end
