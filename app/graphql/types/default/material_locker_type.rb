Types::MaterialLockerType = GraphQL::ObjectType.define do
  name 'MaterialLocker'
  description 'MaterialLocker'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :position, types.Int
  field :slug, types.String
  field :locker, types.String
  field :isdefault, types.Boolean

# Part1A
# Part1A

end
