Types::MaterialMaterialType = GraphQL::ObjectType.define do
  name 'MaterialMaterial'
  description 'MaterialMaterial'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :material_type_id, -> { Types::MaterialMaterialTypeType }, property: :material_material_type do resolve ->(obj, args, ctx) { RecordLoader.for(Material::MaterialType).load(obj.material_material_type_id) } end
  field :position, types.Int
  field :slug, types.String
  field :material_name, types.String
  field :metal_purities, !types[Types::MaterialMetalPurityType] do
    resolve ->(obj, args, ctx) {
      obj.material_metal_purities
    }
  end
  field :gem_shapes, !types[Types::MaterialGemShapeType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_shapes
    }
  end
  field :gem_clarities, !types[Types::MaterialGemClarityType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_clarities
    }
  end
  field :gem_sizes, !types[Types::MaterialGemSizeType] do
    resolve ->(obj, args, ctx) {
      obj.material_gem_sizes
    }
  end
  field :colors, !types[Types::MaterialColorType] do
    resolve ->(obj, args, ctx) {
      obj.material_colors
    }
  end
  field :lockers, !types[Types::MaterialLockerType] do
    resolve ->(obj, args, ctx) {
      obj.material_lockers
    }
  end
  field :diamonds, !types[Types::DesignDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_diamonds
    }
  end
  field :m_stocks, !types[Types::SaleMStockType] do
    resolve ->(obj, args, ctx) {
      obj.sale_m_stocks
    }
  end
# Part1A
# Part1A

end
