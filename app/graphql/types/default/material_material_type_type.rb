Types::MaterialMaterialTypeType = GraphQL::ObjectType.define do
  name 'MaterialMaterialType'
  description 'MaterialMaterialType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :material_type, types.String
  field :materials, !types[Types::MaterialMaterialType] do
    resolve ->(obj, args, ctx) {
      obj.material_materials
    }
  end
  field :rate_ons, !types[Types::MaterialRateOnType] do
    resolve ->(obj, args, ctx) {
      obj.material_rate_ons
    }
  end
# Part1A
# Part1A

end
