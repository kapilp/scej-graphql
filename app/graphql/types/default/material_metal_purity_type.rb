Types::MaterialMetalPurityType = GraphQL::ObjectType.define do
  name 'MaterialMetalPurity'
  description 'MaterialMetalPurity'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :position, types.Int
  field :slug, types.String
  field :name, types.String
  field :purity, types.Float
  field :specific_density, types.Float
  field :price, types.Float
  field :description, types.String

# Part1A
# Part1A

end
