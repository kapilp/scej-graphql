Types::MaterialRateOnType = GraphQL::ObjectType.define do
  name 'MaterialRateOn'
  description 'MaterialRateOn'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :material_type_id, -> { Types::MaterialMaterialTypeType }, property: :material_material_type do resolve ->(obj, args, ctx) { RecordLoader.for(Material::MaterialType).load(obj.material_material_type_id) } end
  field :position, types.Int
  field :slug, types.String
  field :rateon, types.String

# Part1A
# Part1A

end
