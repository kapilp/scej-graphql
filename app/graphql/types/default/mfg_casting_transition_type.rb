Types::MfgCastingTransitionType = GraphQL::ObjectType.define do
  name 'MfgCastingTransition'
  description 'MfgCastingTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :casting_id, -> { Types::MfgCastingType }, property: :mfg_casting do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::Casting).load(obj.mfg_casting_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
