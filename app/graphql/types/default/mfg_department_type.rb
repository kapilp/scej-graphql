Types::MfgDepartmentType = GraphQL::ObjectType.define do
  name 'MfgDepartment'
  description 'MfgDepartment'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :department, types.String
  field :parent_id, -> { Types::MfgDepartmentType }, property: :parent
end
