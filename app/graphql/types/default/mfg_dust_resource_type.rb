Types::MfgDustResourceType = GraphQL::ObjectType.define do
  name 'MfgDustResource'
  description 'MfgDustResource'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :dust_resource, types.String

# Part1A
# Part1A

end
