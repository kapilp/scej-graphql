Types::MfgMetalIssueTransitionType = GraphQL::ObjectType.define do
  name 'MfgMetalIssueTransition'
  description 'MfgMetalIssueTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :mfg_metal_issue_id, -> { Types::MfgMetalIssueType }, property: :mfg_metal_issue do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::MetalIssue).load(obj.mfg_metal_issue_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
