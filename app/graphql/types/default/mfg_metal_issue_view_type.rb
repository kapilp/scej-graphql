Types::MfgMetalIssueViewType = GraphQL::ObjectType.define do
  name 'MfgMetalIssueView'
  description 'MfgMetalIssueView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :date, types.String
  field :slug, types.String
  field :employee_id, -> { Types::UserAccountType }, property: :employee do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.employee_id) } end
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :mfg_txns, !types[Types::MfgMfgTxnType] do
    resolve ->(obj, args, ctx) {
      obj.mfg_mfg_txns
    }
  end
  field :metal_issue_transitions, !types[Types::MfgMetalIssueTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.mfg_metal_issue_transitions
    }
  end
# Part1A
# Part1A

end
