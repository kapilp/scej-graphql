Types::MfgMfgTxnTransitionType = GraphQL::ObjectType.define do
  name 'MfgMfgTxnTransition'
  description 'MfgMfgTxnTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :mfg_mfg_txn_id, -> { Types::MfgMfgTxnType }, property: :mfg_mfg_txn do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::MfgTxn).load(obj.mfg_mfg_txn_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
