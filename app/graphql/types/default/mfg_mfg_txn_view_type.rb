Types::MfgMfgTxnViewType = GraphQL::ObjectType.define do
  name 'MfgMfgTxnView'
  description 'MfgMfgTxnView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :mfg_date, types.String
  field :casting_id, -> { Types::MfgCastingType }, property: :mfg_casting do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::Casting).load(obj.mfg_casting_id) } end
  field :mfg_wax_setting_id, -> { Types::MfgWaxSettingType }, property: :mfg_wax_setting do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::WaxSetting).load(obj.mfg_wax_setting_id) } end
  field :mfg_metal_issue_id, -> { Types::MfgMetalIssueType }, property: :mfg_metal_issue do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::MetalIssue).load(obj.mfg_metal_issue_id) } end
  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :job_id, -> { Types::SaleJobType }, property: :sale_job do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::Job).load(obj.sale_job_id) } end
  field :department_id, -> { Types::MfgDepartmentType }, property: :mfg_department do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::Department).load(obj.mfg_department_id) } end
  field :employee_id, -> { Types::UserAccountType }, property: :employee do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.employee_id) } end
  field :description, types.String
  field :issue_weight, types.Float
  field :diamond_pcs, types.Int
  field :cs_pcs, types.Int
  field :diamond_weight, types.Float
  field :cs_weight, types.Float
  field :d_cs_weight, types.Float
  field :total_d_cs_weight, types.Float
  field :receive_weight, types.Float
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :dust_weight, types.Float
  field :received, types.Boolean
  field :purity, types.Float
  field :net_weight, types.Float
  field :pure_weight, types.Float
  field :m_txn_details, !types[Types::SaleMTxnDetailType] do
    resolve ->(obj, args, ctx) {
      obj.sale_m_txn_details
    }
  end
  field :mfg_txn_transitions, !types[Types::MfgMfgTxnTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.mfg_mfg_txn_transitions
    }
  end
# Part1A
# Part1A

end
