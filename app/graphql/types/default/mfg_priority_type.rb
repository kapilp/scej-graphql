Types::MfgPriorityType = GraphQL::ObjectType.define do
  name 'MfgPriority'
  description 'MfgPriority'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :priority, types.String
  field :isdefault, types.Boolean

# Part1A
# Part1A

end
