Types::MfgProductTypeType = GraphQL::ObjectType.define do
  name 'MfgProductType'
  description 'MfgProductType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :product_type, types.String
  field :isdefault, types.Boolean
  field :orders, !types[Types::SaleOrderType] do
    resolve ->(obj, args, ctx) {
      obj.sale_orders
    }
  end
# Part1A
# Part1A

end
