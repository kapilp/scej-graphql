Types::MfgRefiningTransitionType = GraphQL::ObjectType.define do
  name 'MfgRefiningTransition'
  description 'MfgRefiningTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :mfg_refining_id, -> { Types::MfgRefiningType }, property: :mfg_refining do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::Refining).load(obj.mfg_refining_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
