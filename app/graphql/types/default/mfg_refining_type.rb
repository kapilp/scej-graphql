Types::MfgRefiningType = GraphQL::ObjectType.define do
  name 'MfgRefining'
  description 'MfgRefining'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :entry_by_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :date_from, types.String
  field :date_to, types.String
  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :department_id, -> { Types::MfgDepartmentType }, property: :mfg_department do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::Department).load(obj.mfg_department_id) } end
  field :dust_weight, types.Float
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :m_txn_details, !types[Types::SaleMTxnDetailType] do
    resolve ->(obj, args, ctx) {
      obj.sale_m_txn_details_receive
    }
  end
  field :refining_transitions, !types[Types::MfgRefiningTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.mfg_refining_transitions
    }
  end
# Part1A
# Part1A

end
