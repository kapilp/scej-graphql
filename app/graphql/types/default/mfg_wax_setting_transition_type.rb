Types::MfgWaxSettingTransitionType = GraphQL::ObjectType.define do
  name 'MfgWaxSettingTransition'
  description 'MfgWaxSettingTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :mfg_wax_setting_id, -> { Types::MfgWaxSettingType }, property: :mfg_wax_setting do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::WaxSetting).load(obj.mfg_wax_setting_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
