Types::SaleJobTransitionType = GraphQL::ObjectType.define do
  name 'SaleJobTransition'
  description 'SaleJobTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :job_id, -> { Types::SaleJobType }, property: :sale_job do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::Job).load(obj.sale_job_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
