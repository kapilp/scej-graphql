Types::SaleJobViewType = GraphQL::ObjectType.define do
  name 'SaleJobView'
  description 'SaleJobView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :id01, types.Int
  field :position, types.String
  field :position_order, types.Int
  field :customer_id, -> { Types::UserAccountType }, property: :customer do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.customer_id) } end
  field :taken_by_id, -> { Types::UserAccountType }, property: :taken_by do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.taken_by_id) } end
  field :order_date, types.String
  field :delivery_date, types.String
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :final, types.Boolean
  field :position_job, types.Int
  field :style_id, -> { Types::DesignStyleType }, property: :design_style do resolve ->(obj, args, ctx) { RecordLoader.for(Design::Style).load(obj.design_style_id) } end
  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :metal_purity_id, -> { Types::MaterialMetalPurityType }, property: :metal_purity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::MetalPurity).load(obj.metal_purity_id) } end
  field :metal_color_id, -> { Types::MaterialColorType }, property: :metal_color do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Color).load(obj.metal_color_id) } end
  field :qty, types.Int
  field :instruction, types.String
  field :item_size, types.String
  field :priority_id, -> { Types::MfgPriorityType }, property: :mfg_priority do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::Priority).load(obj.mfg_priority_id) } end
  field :created_at01, types.String
  field :updated_at01, types.String
  field :gross_weight, types.Float
  field :net_weight, types.Float
  field :pure_weight, types.Float
  field :job_diamonds, !types[Types::DesignJobDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_job_diamonds
    }
  end
  field :diamonds, !types[Types::DesignDiamondType] do
    resolve ->(obj, args, ctx) {
      obj.design_diamonds
    }
  end
  field :job_transitions, !types[Types::SaleJobTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.sale_job_transitions
    }
  end
# Part1A
# Part1A

end
