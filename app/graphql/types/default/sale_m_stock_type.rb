Types::SaleMStockType = GraphQL::ObjectType.define do
  name 'SaleMStock'
  description 'SaleMStock'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :metal_purity_id, -> { Types::MaterialMetalPurityType }, property: :material_metal_purity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::MetalPurity).load(obj.material_metal_purity_id) } end
  field :gem_shape_id, -> { Types::MaterialGemShapeType }, property: :material_gem_shape do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemShape).load(obj.material_gem_shape_id) } end
  field :gem_clarity_id, -> { Types::MaterialGemClarityType }, property: :material_gem_clarity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemClarity).load(obj.material_gem_clarity_id) } end
  field :color_id, -> { Types::MaterialColorType }, property: :material_color do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Color).load(obj.material_color_id) } end
  field :gem_size_id, -> { Types::MaterialGemSizeType }, property: :material_gem_size do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemSize).load(obj.material_gem_size_id) } end
  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :locker_id, -> { Types::MaterialLockerType }, property: :material_locker do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Locker).load(obj.material_locker_id) } end
  field :accessory_id, -> { Types::MaterialAccessoryType }, property: :material_accessory do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Accessory).load(obj.material_accessory_id) } end
  field :isissue, types.Boolean
  field :pcs, types.Int
  field :calc_pcs, types.Int
  field :weight1, types.Float
  field :calc_weight, types.Float
  field :note, types.String
  field :gem_type_id, -> { Types::MaterialGemTypeType }, property: :material_gem_type do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemType).load(obj.material_gem_type_id) } end
  field :rate_on_pc, types.Boolean
  field :rate1, types.Float

# Part1A
# Part1A

end
