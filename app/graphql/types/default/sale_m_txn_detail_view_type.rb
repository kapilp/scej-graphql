Types::SaleMTxnDetailViewType = GraphQL::ObjectType.define do
  name 'SaleMTxnDetailView'
  description 'SaleMTxnDetailView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :account, types.Int
  field :calc_pcs, types.Int
  field :calc_weight, types.Float
  field :position, types.Int
  field :material_id, -> { Types::MaterialMaterialType }, property: :material_material do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Material).load(obj.material_material_id) } end
  field :metal_purity_id, -> { Types::MaterialMetalPurityType }, property: :material_metal_purity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::MetalPurity).load(obj.material_metal_purity_id) } end
  field :gem_shape_id, -> { Types::MaterialGemShapeType }, property: :material_gem_shape do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemShape).load(obj.material_gem_shape_id) } end
  field :gem_clarity_id, -> { Types::MaterialGemClarityType }, property: :material_gem_clarity do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemClarity).load(obj.material_gem_clarity_id) } end
  field :color_id, -> { Types::MaterialColorType }, property: :material_color do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Color).load(obj.material_color_id) } end
  field :gem_size_id, -> { Types::MaterialGemSizeType }, property: :material_gem_size do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemSize).load(obj.material_gem_size_id) } end
  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :locker_id, -> { Types::MaterialLockerType }, property: :material_locker do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Locker).load(obj.material_locker_id) } end
  field :accessory_id, -> { Types::MaterialAccessoryType }, property: :material_accessory do resolve ->(obj, args, ctx) { RecordLoader.for(Material::Accessory).load(obj.material_accessory_id) } end
  field :isissue, types.Boolean
  field :customer_stock, types.Boolean
  field :pcs, types.Int
  field :weight1, types.Float
  field :note, types.String
  field :gem_type_id, -> { Types::MaterialGemTypeType }, property: :material_gem_type do resolve ->(obj, args, ctx) { RecordLoader.for(Material::GemType).load(obj.material_gem_type_id) } end
  field :rate_on_pc, types.Boolean
  field :id01, types.Int
  field :position01, types.Int
  field :m_txn_type_id, -> { Types::IntType }, property: :sale_m_txn_type 
  field :account_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :taken_by_id, -> { Types::IntType }, property: :taken_by 
  field :date, types.String
  field :due_date, types.String
  field :description, types.String
  field :created_at01, types.String
  field :updated_at01, types.String
  field :id02, types.Int
  field :position02, types.Int
  field :date01, types.String
  field :slug, types.String
  field :casting_employee_id, -> { Types::IntType }, property: :casting_employee 
  field :created_at02, types.String
  field :updated_at02, types.String
  field :id03, types.Int
  field :position03, types.Int
  field :mfg_date, types.String
  field :casting_id, -> { Types::IntType }, property: :mfg_casting 
  field :job_id, -> { Types::IntType }, property: :sale_job 
  field :user_company_id01, types.Int
  field :department_id, -> { Types::IntType }, property: :mfg_department 
  field :mfg_employee_id, -> { Types::IntType }, property: :mfg_employee 
  field :receive_weight, types.Float
  field :received, types.Boolean
  field :dust_weight, types.Float
  field :created_at03, types.String
  field :updated_at03, types.String
  field :processable_type, types.String
  field :jobno, types.Int
  field :sale_order_id, -> { Types::IntType }, property: :sale_order 
  field :customer_id, -> { Types::IntType }, property: :customer 

# Part1A
# Part1A

end
