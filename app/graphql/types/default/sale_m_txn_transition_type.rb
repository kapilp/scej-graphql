Types::SaleMTxnTransitionType = GraphQL::ObjectType.define do
  name 'SaleMTxnTransition'
  description 'SaleMTxnTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :sale_m_txn_id, -> { Types::SaleMTxnType }, property: :sale_m_txn do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::MTxn).load(obj.sale_m_txn_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
