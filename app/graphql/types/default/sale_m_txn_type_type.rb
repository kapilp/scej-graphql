Types::SaleMTxnTypeType = GraphQL::ObjectType.define do
  name 'SaleMTxnType'
  description 'SaleMTxnType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :txn_type, types.String

# Part1A
# Part1A

end
