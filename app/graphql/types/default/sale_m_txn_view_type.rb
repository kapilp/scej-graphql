Types::SaleMTxnViewType = GraphQL::ObjectType.define do
  name 'SaleMTxnView'
  description 'SaleMTxnView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :m_txn_type_id, -> { Types::SaleMTxnTypeType }, property: :sale_m_txn_type do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::MTxnType).load(obj.sale_m_txn_type_id) } end
  field :party_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :taken_by_id, -> { Types::UserAccountType }, property: :taken_by do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.taken_by_id) } end
  field :date, types.String
  field :due_date, types.String
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :m_txn_details, !types[Types::SaleMTxnDetailType] do
    resolve ->(obj, args, ctx) {
      obj.sale_m_txn_details
    }
  end
  field :m_txn_transitions, !types[Types::SaleMTxnTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.sale_m_txn_transitions
    }
  end
# Part1A
# Part1A

end
