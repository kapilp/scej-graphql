Types::SaleMemoTransitionType = GraphQL::ObjectType.define do
  name 'SaleMemoTransition'
  description 'SaleMemoTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :sale_memo_id, -> { Types::SaleMemoType }, property: :sale_memo do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::Memo).load(obj.sale_memo_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
