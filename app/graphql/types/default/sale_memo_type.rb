Types::SaleMemoType = GraphQL::ObjectType.define do
  name 'SaleMemo'
  description 'SaleMemo'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :date, types.String
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :jobs, !types[Types::SaleJobType] do
    resolve ->(obj, args, ctx) {
      obj.sale_jobs
    }
  end
  field :memo_transitions, !types[Types::SaleMemoTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.sale_memo_transitions
    }
  end
# Part1A
# Part1A

end
