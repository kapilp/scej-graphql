Types::SaleOrderTransitionType = GraphQL::ObjectType.define do
  name 'SaleOrderTransition'
  description 'SaleOrderTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :sale_order_id, -> { Types::SaleOrderType }, property: :sale_order do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::Order).load(obj.sale_order_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
