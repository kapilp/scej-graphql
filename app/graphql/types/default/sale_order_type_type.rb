Types::SaleOrderTypeType = GraphQL::ObjectType.define do
  name 'SaleOrderType'
  description 'SaleOrderType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :order_type, types.String

# Part1A
# Part1A

end
