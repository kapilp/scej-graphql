Types::SaleOrderViewType = GraphQL::ObjectType.define do
  name 'SaleOrderView'
  description 'SaleOrderView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :customer_id, -> { Types::UserAccountType }, property: :customer do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.customer_id) } end
  field :taken_by_id, -> { Types::UserAccountType }, property: :taken_by do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.taken_by_id) } end
  field :order_date, types.String
  field :delivery_date, types.String
  field :product_type_id, -> { Types::MfgProductTypeType }, property: :mfg_product_type do resolve ->(obj, args, ctx) { RecordLoader.for(Mfg::ProductType).load(obj.mfg_product_type_id) } end
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :num_jobs, types.Int
  field :total_qty, types.Int
  field :jobs, !types[Types::SaleJobType] do
    resolve ->(obj, args, ctx) {
      obj.sale_jobs
    }
  end
  field :order_transitions, !types[Types::SaleOrderTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.sale_order_transitions
    }
  end
# Part1A
# Part1A

end
