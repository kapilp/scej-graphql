Types::SaleSaleDetailType = GraphQL::ObjectType.define do
  name 'SaleSaleDetail'
  description 'SaleSaleDetail'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :saleable_type, types.String
  field :saleable_id, -> { Types::IntType }, property: :saleable 
  field :position, types.Int
  field :tounch, types.Float
  field :m_rate_on_id, -> { Types::MaterialRateOnType }, property: :m_rate_on do resolve ->(obj, args, ctx) { RecordLoader.for(Material::RateOn).load(obj.m_rate_on_id) } end
  field :m_rate, types.Float
  field :d_rate_on_id, -> { Types::MaterialRateOnType }, property: :d_rate_on do resolve ->(obj, args, ctx) { RecordLoader.for(Material::RateOn).load(obj.d_rate_on_id) } end
  field :d_rate, types.Float
  field :cs_rate_on_id, -> { Types::MaterialRateOnType }, property: :cs_rate_on do resolve ->(obj, args, ctx) { RecordLoader.for(Material::RateOn).load(obj.cs_rate_on_id) } end
  field :cs_rate, types.Float

# Part1A
# Part1A

end
