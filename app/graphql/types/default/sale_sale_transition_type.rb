Types::SaleSaleTransitionType = GraphQL::ObjectType.define do
  name 'SaleSaleTransition'
  description 'SaleSaleTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :sale_sale_id, -> { Types::SaleSaleType }, property: :sale_sale do resolve ->(obj, args, ctx) { RecordLoader.for(Sale::Sale).load(obj.sale_sale_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
