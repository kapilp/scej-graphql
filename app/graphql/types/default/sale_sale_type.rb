Types::SaleSaleType = GraphQL::ObjectType.define do
  name 'SaleSale'
  description 'SaleSale'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :customer_id, -> { Types::UserAccountType }, property: :customer do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.customer_id) } end
  field :sold_by_id, -> { Types::UserAccountType }, property: :sold_by do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.sold_by_id) } end
  field :date, types.String
  field :due_date, types.String
  field :description, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :jobs, !types[Types::SaleJobType] do
    resolve ->(obj, args, ctx) {
      obj.sale_jobs
    }
  end
  field :sale_transitions, !types[Types::SaleSaleTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.sale_sale_transitions
    }
  end
# Part1A
# Part1A

end
