Types::UserAccountTransitionType = GraphQL::ObjectType.define do
  name 'UserAccountTransition'
  description 'UserAccountTransition'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :to_state, types.String
  field :metadata, types.String
  field :sort_key, types.Int
  field :account_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :most_recent, types.Boolean

# Part1A
# Part1A

end
