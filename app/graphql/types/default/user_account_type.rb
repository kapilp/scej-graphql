Types::UserAccountType = GraphQL::ObjectType.define do
  name 'UserAccount'
  description 'UserAccount'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :account_type_id, -> { Types::UserAccountTypeType }, property: :user_account_type do resolve ->(obj, args, ctx) { RecordLoader.for(User::AccountType).load(obj.user_account_type_id) } end
  field :position, types.Int
  field :join_date, types.String
  field :slug, types.String
  field :account_name, types.String
  field :email, types.String
  field :password, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :salary, types.Int
  field :role_id, -> { Types::UserRoleType }, property: :user_role do resolve ->(obj, args, ctx) { RecordLoader.for(User::Role).load(obj.user_role_id) } end
  field :parent_id, -> { Types::UserAccountType }, property: :parent do resolve ->(obj, args, ctx) { RecordLoader.for(User::AccountView).load(obj.parent_id) } end
  field :accounts, !types[Types::UserAccountType] do
    resolve ->(obj, args, ctx) {
      obj.children
    }
  end
  field :accounts, !types[Types::UserAccountType] do
    resolve ->(obj, args, ctx) {
      obj.self_and_ancestors
    }
  end
  field :accounts, !types[Types::UserAccountType] do
    resolve ->(obj, args, ctx) {
      obj.self_and_descendants
    }
  end
  field :authentication_tokens, !types[Types::UserAuthenticationTokenType] do
    resolve ->(obj, args, ctx) {
      obj.authentication_tokens
    }
  end
  field :addresses, !types[Types::UserAddressType] do
    resolve ->(obj, args, ctx) {
      obj.user_addresses
    }
  end
  field :company_ownerships, !types[Types::UserCompanyOwnershipType] do
    resolve ->(obj, args, ctx) {
      obj.user_company_ownerships
    }
  end
  field :companies, !types[Types::UserCompanyType] do
    resolve ->(obj, args, ctx) {
      obj.user_companies
    }
  end
  field :contacts, !types[Types::UserContactType] do
    resolve ->(obj, args, ctx) {
      obj.user_contacts
    }
  end
  field :account_transitions, !types[Types::UserAccountTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.user_account_transitions
    }
  end
# Part1A
# Part1A

end
