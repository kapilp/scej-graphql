Types::UserAccountTypeType = GraphQL::ObjectType.define do
  name 'UserAccountType'
  description 'UserAccountType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :account_type, types.String
  field :accounts, !types[Types::UserAccountType] do
    resolve ->(obj, args, ctx) {
      obj.user_accounts
    }
  end
# Part1A
# Part1A

end
