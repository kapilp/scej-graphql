Types::UserAccountViewType = GraphQL::ObjectType.define do
  name 'UserAccountView'
  description 'UserAccountView'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :account_type_id, -> { Types::UserAccountTypeType }, property: :user_account_type do resolve ->(obj, args, ctx) { RecordLoader.for(User::AccountType).load(obj.user_account_type_id) } end
  field :position, types.Int
  field :join_date, types.String
  field :slug, types.String
  field :account_name, types.String
  field :email, types.String
  field :password, types.String
  field :status_id, types.String do
    resolve ->(obj, args, ctx) {
      obj.state_machine.current_state
    }
  end
  field :salary, types.Int
  field :role_id, -> { Types::UserRoleType }, property: :user_role do resolve ->(obj, args, ctx) { RecordLoader.for(User::Role).load(obj.user_role_id) } end
  field :encrypted_password, types.String
  field :reset_password_token, types.String
  field :reset_password_sent_at, types.String
  field :remember_created_at, types.String
  field :sign_in_count, types.Int
  field :current_sign_in_at, types.String
  field :last_sign_in_at, types.String
  field :current_sign_in_ip, types.String
  field :last_sign_in_ip, types.String
  field :addresses, !types[Types::UserAddressType] do
    resolve ->(obj, args, ctx) {
      obj.user_addresses
    }
  end
  field :company_ownerships, !types[Types::UserCompanyOwnershipType] do
    resolve ->(obj, args, ctx) {
      obj.user_company_ownerships
    }
  end
  field :companies, !types[Types::UserCompanyType] do
    resolve ->(obj, args, ctx) {
      obj.user_companies
    }
  end
  field :account_transitions, !types[Types::UserAccountTransitionType] do
    resolve ->(obj, args, ctx) {
      obj.user_account_transitions
    }
  end
# Part1A
# Part1A

end
