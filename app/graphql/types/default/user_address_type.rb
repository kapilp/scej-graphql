Types::UserAddressType = GraphQL::ObjectType.define do
  name 'UserAddress'
  description 'UserAddress'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :account_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :company_id, -> { Types::UserAccountType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_company_id) } end
  field :position, types.Int
  field :firstname, types.String
  field :lastname, types.String
  field :address1, types.String
  field :address2, types.String
  field :city, types.String
  field :zipcode, types.String
  field :state_id, -> { Types::UserStateType }, property: :user_state do resolve ->(obj, args, ctx) { RecordLoader.for(User::State).load(obj.user_state_id) } end
  field :country_id, -> { Types::UserCountryType }, property: :user_country do resolve ->(obj, args, ctx) { RecordLoader.for(User::Country).load(obj.user_country_id) } end
  field :phone, types.String
  field :alternative_phone, types.String
  field :company, types.String

# Part1A
# Part1A

end
