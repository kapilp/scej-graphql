Types::UserAuthenticationTokenType = GraphQL::ObjectType.define do
  name 'UserAuthenticationToken'
  description 'UserAuthenticationToken'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :body, types.String
  field :account_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :last_used_at, types.String
  field :expires_in, types.Int
  field :ip_address, types.String
  field :user_agent, types.String

# Part1A
# Part1A

end
