Types::UserCompanyOwnershipType = GraphQL::ObjectType.define do
  name 'UserCompanyOwnership'
  description 'UserCompanyOwnership'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :account_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end

# Part1A
# Part1A

end
