Types::UserCompanyType = GraphQL::ObjectType.define do
  name 'UserCompany'
  description 'UserCompany'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :company, types.String
  field :gst_no, types.String
  field :parent_id, -> { Types::UserCompanyType }, property: :parent do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.parent_id) } end
  field :companies, !types[Types::UserCompanyType] do
    resolve ->(obj, args, ctx) {
      obj.children
    }
  end
  field :companies, !types[Types::UserCompanyType] do
    resolve ->(obj, args, ctx) {
      obj.self_and_ancestors
    }
  end
  field :companies, !types[Types::UserCompanyType] do
    resolve ->(obj, args, ctx) {
      obj.self_and_descendants
    }
  end
  field :company_ownerships, !types[Types::UserCompanyOwnershipType] do
    resolve ->(obj, args, ctx) {
      obj.user_company_ownerships
    }
  end
  field :accounts, !types[Types::UserAccountType] do
    resolve ->(obj, args, ctx) {
      obj.user_accounts
    }
  end
  field :addresses, !types[Types::UserAddressType] do
    resolve ->(obj, args, ctx) {
      obj.user_addresses
    }
  end
  field :contacts, !types[Types::UserContactType] do
    resolve ->(obj, args, ctx) {
      obj.user_contacts
    }
  end
  field :orders, !types[Types::SaleOrderType] do
    resolve ->(obj, args, ctx) {
      obj.sale_orders
    }
  end
# Part1A
# Part1A

end
