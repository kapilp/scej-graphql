Types::UserContactType = GraphQL::ObjectType.define do
  name 'UserContact'
  description 'UserContact'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :company_id, -> { Types::UserCompanyType }, property: :user_company do resolve ->(obj, args, ctx) { RecordLoader.for(User::Company).load(obj.user_company_id) } end
  field :account_id, -> { Types::UserAccountType }, property: :user_account do resolve ->(obj, args, ctx) { RecordLoader.for(User::Account).load(obj.user_account_id) } end
  field :position, types.Int
  field :company_name, types.String
  field :first_name, types.String
  field :last_name, types.String
  field :email, types.String
  field :phone, types.String
  field :mobile, types.String

# Part1A
# Part1A

end
