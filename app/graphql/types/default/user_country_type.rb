Types::UserCountryType = GraphQL::ObjectType.define do
  name 'UserCountry'
  description 'UserCountry'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :country, types.String
  field :states, !types[Types::UserStateType] do
    resolve ->(obj, args, ctx) {
      obj.user_states
    }
  end
  field :addresses, !types[Types::UserAddressType] do
    resolve ->(obj, args, ctx) {
      obj.user_addresses
    }
  end
# Part1A
# Part1A

end
