Types::UserRoleType = GraphQL::ObjectType.define do
  name 'UserRole'
  description 'UserRole'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :role, types.String
  field :accounts, !types[Types::UserAccountType] do
    resolve ->(obj, args, ctx) {
      obj.user_accounts
    }
  end
# Part1A
# Part1A

end
