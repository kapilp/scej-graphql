Types::UserStateType = GraphQL::ObjectType.define do
  name 'UserState'
  description 'UserState'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :country_id, -> { Types::UserCountryType }, property: :user_country do resolve ->(obj, args, ctx) { RecordLoader.for(User::Country).load(obj.user_country_id) } end
  field :slug, types.String
  field :state, types.String
  field :addresses, !types[Types::UserAddressType] do
    resolve ->(obj, args, ctx) {
      obj.user_addresses
    }
  end
# Part1A
# Part1A

end
