Types::UserTaxRateType = GraphQL::ObjectType.define do
  name 'UserTaxRate'
  description 'UserTaxRate'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :user_tax_type_id, -> { Types::UserTaxTypeType }, property: :user_tax_type do resolve ->(obj, args, ctx) { RecordLoader.for(User::TaxType).load(obj.user_tax_type_id) } end
  field :position, types.Int
  field :slug, types.String
  field :name, types.String
  field :rate, types.Float

# Part1A
# Part1A

end
