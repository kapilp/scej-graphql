Types::UserTaxTypeType = GraphQL::ObjectType.define do
  name 'UserTaxType'
  description 'UserTaxType'

  interfaces [ActiveRecordTimestamp]
  field :id, !types.ID

  field :position, types.Int
  field :slug, types.String
  field :tax_type, types.String
  field :tax_rates, !types[Types::UserTaxRateType] do
    resolve ->(obj, args, ctx) {
      obj.user_tax_rates
    }
  end
# Part1A
# Part1A

end
