Types::MutationType = GraphQL::ObjectType.define do
  name "Mutation"

  # Add root-level Mutation fields here.

  # All Generated mutations are below:
  # ================All Active Record Mutations============================ 
  
  field :createContact, field: ContactMutations::Create.field
  field :updateContact, field: ContactMutations::Update.field
  field :destroyContact, field: ContactMutations::Destroy.field


  field :createCompany, field: CompanyMutations::Create.field
  field :updateCompany, field: CompanyMutations::Update.field
  field :destroyCompany, field: CompanyMutations::Destroy.field


  field :createCompanySignUp, field: CompanySignUpMutations::Create.field



  field :createAccountType, field: AccountTypeMutations::Create.field
  field :updateAccountType, field: AccountTypeMutations::Update.field
  field :destroyAccountType, field: AccountTypeMutations::Destroy.field


  field :createAccount, field: AccountMutations::Create.field
  field :updateAccount, field: AccountMutations::Update.field
  field :destroyAccount, field: AccountMutations::Destroy.field


  field :createAccountSignIn, field: AccountSignInMutations::Create.field




  field :createRole, field: RoleMutations::Create.field
  field :updateRole, field: RoleMutations::Update.field
  field :destroyRole, field: RoleMutations::Destroy.field


  field :createAddress, field: AddressMutations::Create.field
  field :updateAddress, field: AddressMutations::Update.field
  field :destroyAddress, field: AddressMutations::Destroy.field


  field :createCountry, field: CountryMutations::Create.field
  field :updateCountry, field: CountryMutations::Update.field
  field :destroyCountry, field: CountryMutations::Destroy.field


  field :createState, field: StateMutations::Create.field
  field :updateState, field: StateMutations::Update.field
  field :destroyState, field: StateMutations::Destroy.field


  field :createTaxType, field: TaxTypeMutations::Create.field
  field :updateTaxType, field: TaxTypeMutations::Update.field
  field :destroyTaxType, field: TaxTypeMutations::Destroy.field


  field :createTaxRate, field: TaxRateMutations::Create.field
  field :updateTaxRate, field: TaxRateMutations::Update.field
  field :destroyTaxRate, field: TaxRateMutations::Destroy.field


  field :createMaterialType, field: MaterialTypeMutations::Create.field
  field :updateMaterialType, field: MaterialTypeMutations::Update.field
  field :destroyMaterialType, field: MaterialTypeMutations::Destroy.field


  field :createMaterial, field: MaterialMutations::Create.field
  field :updateMaterial, field: MaterialMutations::Update.field
  field :destroyMaterial, field: MaterialMutations::Destroy.field


  field :createMetalPurity, field: MetalPurityMutations::Create.field
  field :updateMetalPurity, field: MetalPurityMutations::Update.field
  field :destroyMetalPurity, field: MetalPurityMutations::Destroy.field


  field :createGemShape, field: GemShapeMutations::Create.field
  field :updateGemShape, field: GemShapeMutations::Update.field
  field :destroyGemShape, field: GemShapeMutations::Destroy.field


  field :createGemClarity, field: GemClarityMutations::Create.field
  field :updateGemClarity, field: GemClarityMutations::Update.field
  field :destroyGemClarity, field: GemClarityMutations::Destroy.field


  field :createColor, field: ColorMutations::Create.field
  field :updateColor, field: ColorMutations::Update.field
  field :destroyColor, field: ColorMutations::Destroy.field


  field :createGemSize, field: GemSizeMutations::Create.field
  field :updateGemSize, field: GemSizeMutations::Update.field
  field :destroyGemSize, field: GemSizeMutations::Destroy.field


  field :createAccessory, field: AccessoryMutations::Create.field
  field :updateAccessory, field: AccessoryMutations::Update.field
  field :destroyAccessory, field: AccessoryMutations::Destroy.field



  field :createLocker, field: LockerMutations::Create.field
  field :updateLocker, field: LockerMutations::Update.field
  field :destroyLocker, field: LockerMutations::Destroy.field


  field :createRateOn, field: RateOnMutations::Create.field
  field :updateRateOn, field: RateOnMutations::Update.field
  field :destroyRateOn, field: RateOnMutations::Destroy.field


  field :createGemType, field: GemTypeMutations::Create.field
  field :updateGemType, field: GemTypeMutations::Update.field
  field :destroyGemType, field: GemTypeMutations::Destroy.field


  field :createCollection, field: CollectionMutations::Create.field
  field :updateCollection, field: CollectionMutations::Update.field
  field :destroyCollection, field: CollectionMutations::Destroy.field


  field :createMakingType, field: MakingTypeMutations::Create.field
  field :updateMakingType, field: MakingTypeMutations::Update.field
  field :destroyMakingType, field: MakingTypeMutations::Destroy.field


  field :createSettingType, field: SettingTypeMutations::Create.field
  field :updateSettingType, field: SettingTypeMutations::Update.field
  field :destroySettingType, field: SettingTypeMutations::Destroy.field


  field :createStyle, field: StyleMutations::Create.field
  field :updateStyle, field: StyleMutations::Update.field
  field :destroyStyle, field: StyleMutations::Destroy.field







  field :createImage, field: ImageMutations::Create.field
  field :updateImage, field: ImageMutations::Update.field
  field :destroyImage, field: ImageMutations::Destroy.field


  field :createMTxnType, field: MTxnTypeMutations::Create.field
  field :updateMTxnType, field: MTxnTypeMutations::Update.field
  field :destroyMTxnType, field: MTxnTypeMutations::Destroy.field


  field :createMTxn, field: MTxnMutations::Create.field
  field :updateMTxn, field: MTxnMutations::Update.field
  field :destroyMTxn, field: MTxnMutations::Destroy.field


  field :createOrderType, field: OrderTypeMutations::Create.field
  field :updateOrderType, field: OrderTypeMutations::Update.field
  field :destroyOrderType, field: OrderTypeMutations::Destroy.field


  field :createOrder, field: OrderMutations::Create.field
  field :updateOrder, field: OrderMutations::Update.field
  field :destroyOrder, field: OrderMutations::Destroy.field






  field :createSale, field: SaleMutations::Create.field
  field :updateSale, field: SaleMutations::Update.field
  field :destroySale, field: SaleMutations::Destroy.field




  field :createMemo, field: MemoMutations::Create.field
  field :updateMemo, field: MemoMutations::Update.field
  field :destroyMemo, field: MemoMutations::Destroy.field








  field :createPriority, field: PriorityMutations::Create.field
  field :updatePriority, field: PriorityMutations::Update.field
  field :destroyPriority, field: PriorityMutations::Destroy.field


  field :createProductType, field: ProductTypeMutations::Create.field
  field :updateProductType, field: ProductTypeMutations::Update.field
  field :destroyProductType, field: ProductTypeMutations::Destroy.field


  field :createDepartment, field: DepartmentMutations::Create.field
  field :updateDepartment, field: DepartmentMutations::Update.field
  field :destroyDepartment, field: DepartmentMutations::Destroy.field


  field :createDustResource, field: DustResourceMutations::Create.field
  field :updateDustResource, field: DustResourceMutations::Update.field
  field :destroyDustResource, field: DustResourceMutations::Destroy.field


  field :createWaxSetting, field: WaxSettingMutations::Create.field
  field :updateWaxSetting, field: WaxSettingMutations::Update.field
  field :destroyWaxSetting, field: WaxSettingMutations::Destroy.field



  field :createCasting, field: CastingMutations::Create.field
  field :updateCasting, field: CastingMutations::Update.field
  field :destroyCasting, field: CastingMutations::Destroy.field



  field :createMetalIssue, field: MetalIssueMutations::Create.field
  field :updateMetalIssue, field: MetalIssueMutations::Update.field
  field :destroyMetalIssue, field: MetalIssueMutations::Destroy.field



  field :createMfgTxn, field: MfgTxnMutations::Create.field
  field :updateMfgTxn, field: MfgTxnMutations::Update.field
  field :destroyMfgTxn, field: MfgTxnMutations::Destroy.field



  field :createRefining, field: RefiningMutations::Create.field
  field :updateRefining, field: RefiningMutations::Update.field
  field :destroyRefining, field: RefiningMutations::Destroy.field
















# ============================================
# Part1A
  # field :signIn, field: AuthMutations::SignIn.field
  field :revokeToken, field: AccountSignInMutations::RevokeToken.field
  field :createCompanyChange, field: CompanyChangeMutations::Create.field

  # field :signUp, field: UserMutations::SignUp.field
  # field :updateUser, field: UserMutations::Update.field
  # field :changePassword, field: UserMutations::ChangePassword.field
  # field :cancelAccount, field: UserMutations::CancelAccount.field
# Part1A

# Extra Mutation:
  # TODO: Remove me
  field :testField, types.String do
    description "An example field added by the generator"
    resolve ->(obj, args, ctx) {
      "Hello World!"
    }
  end
end
