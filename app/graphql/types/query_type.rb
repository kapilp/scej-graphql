Types::QueryType = GraphQL::ObjectType.define do
  name 'Query'
  description 'The query root of this schema. See available queries.'

  # Used by Relay to lookup objects by UUID:
  # node field (find-by-UUID)
  # http://graphql-ruby.org/relay/object_identification.html#node-field-find-by-uuid
  field :node, GraphQL::Relay::Node.field
  # Fetches a list of objects given a list of IDs
  field :nodes, GraphQL::Relay::Node.plural_field

  # Hack until relay has lookup for root fields
  field :root, Types::ViewerType do
    description 'Root object to get viewer related collections'
    resolve ->(_obj, _args, _ctx) { Viewer::STATIC }
  end
end
