# https://github.com/stankec/lectures/blob/master/14-graphql/demo/app/graphql/graph/scalars/date_time_scalar.rb
# https://github.com/howtographql/graphql-ruby/blob/master/app/graphql/types/date_time_type.rb
DateTimeScalar = GraphQL::ScalarType.define do
  name 'DateTime'
  description 'ISO 8601 date-time representation'
  coerce_input ->(value, ctx) do
    Time.parse(value)
  end
  coerce_result ->(value, ctx) do
    value.iso8601
  end
end
