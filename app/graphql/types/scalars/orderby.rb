=begin
https://github.com/graphql/graphql-relay-js/issues/20
We actually rarely run into this case at FB, and when we do we just define another ordering, so I don't necessarily have a suggestion based on experience here.

Your suggestion seems good, though; specifically, I would have orderBy take a [Ordering!], where Ordering is an InputObject:

input Ordering {
  sort: Sort!
  direction: Direction! = ASC
}

enum Sort { AGE, POSTCOUNT }
enum Direction { ASC, DESC }
seems right to me. You'd then do:

{
  friends(orderBy: [{sort: AGE}, {sort: POSTCOUNT, direction: DESC}])
}
=end