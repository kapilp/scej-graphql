Dir[File.dirname(__FILE__) + '/scalars/*.rb'].each {|file| require file}
Dir[File.dirname(__FILE__) + '/default/*.rb'].each {|file| require file}

# app/graphql/types/subscription_type.rb
Types::SubscriptionType = GraphQL::ObjectType.define do
  name "Subscription"

  # Add root-level fields here.
  # They will be entry points for queries on your schema.

  # queries are just represented as fields
  # All Generated Queries are below:
  # ================All Active Record Queries============================


  connection :allUserContacts, Types::UserContactType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # contacts = User::Contact.limit(args[:limit].to_i)
      contacts = User::Contact.all
      contacts
    }
  end
  field :contactsCount do
    type types.Int
    description 'Number of Contacts'
    resolve ->(_object, args, ctx) {
      User::Contact.count
    }
  end
  field :contact, Types::UserContactType do
    argument :id, types.ID
    description 'fetch a Contact by id'
    resolve ->(object, args, ctx) {
      User::Contact.find(args[:id])
    }
  end

  connection :allUserCompanies, Types::UserCompanyType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # companies = User::Company.limit(args[:limit].to_i)
      companies = User::Company.all
      companies
    }
  end
  field :companiesCount do
    type types.Int
    description 'Number of Companies'
    resolve ->(_object, args, ctx) {
      User::Company.count
    }
  end
  field :company, Types::UserCompanyType do
    argument :id, types.ID
    description 'fetch a Company by id'
    resolve ->(object, args, ctx) {
      User::Company.find(args[:id])
    }
  end



  connection :allUserAccountTypes, Types::UserAccountTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # accountTypes = User::AccountType.limit(args[:limit].to_i)
      accountTypes = User::AccountType.all
      accountTypes
    }
  end
  field :accountTypesCount do
    type types.Int
    description 'Number of AccountTypes'
    resolve ->(_object, args, ctx) {
      User::AccountType.count
    }
  end
  field :accountType, Types::UserAccountTypeType do
    argument :id, types.ID
    description 'fetch a AccountType by id'
    resolve ->(object, args, ctx) {
      User::AccountType.find(args[:id])
    }
  end

  connection :allUserAccounts, Types::UserAccountType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_a_type_id, types.ID
    argument :f_a_type_string, types.String, default_value: ''

    resolve ->(_obj, args, ctx) {
      # accounts = User::AccountView.limit(args[:limit].to_i)
      accounts = User::AccountView.all
      accounts = accounts.by_account_type(args[:f_a_type_id]) if args[:f_a_type_id]
      accounts = accounts.by_type(args[:f_a_type_string]) if args[:f_a_type_string] && !args[:f_a_type_string].blank?
      accounts
    }
  end
  field :accountsCount do
    type types.Int
    description 'Number of Accounts'
    resolve ->(_object, args, ctx) {
      User::AccountView.count
    }
  end
  field :account, Types::UserAccountType do
    argument :id, types.ID
    description 'fetch a Account by id'
    resolve ->(object, args, ctx) {
      User::AccountView.find(args[:id])
    }
  end



  connection :allUserAuthenticationTokens, Types::UserAuthenticationTokenType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # authenticationTokens = User::AuthenticationToken.limit(args[:limit].to_i)
      authenticationTokens = User::AuthenticationToken.all
      authenticationTokens
    }
  end
  field :authenticationTokensCount do
    type types.Int
    description 'Number of AuthenticationTokens'
    resolve ->(_object, args, ctx) {
      User::AuthenticationToken.count
    }
  end
  field :authenticationToken, Types::UserAuthenticationTokenType do
    argument :id, types.ID
    description 'fetch a AuthenticationToken by id'
    resolve ->(object, args, ctx) {
      User::AuthenticationToken.find(args[:id])
    }
  end

  connection :allUserRoles, Types::UserRoleType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # roles = User::Role.limit(args[:limit].to_i)
      roles = User::Role.all
      roles
    }
  end
  field :rolesCount do
    type types.Int
    description 'Number of Roles'
    resolve ->(_object, args, ctx) {
      User::Role.count
    }
  end
  field :role, Types::UserRoleType do
    argument :id, types.ID
    description 'fetch a Role by id'
    resolve ->(object, args, ctx) {
      User::Role.find(args[:id])
    }
  end

  connection :allUserAddresses, Types::UserAddressType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # addresses = User::Address.limit(args[:limit].to_i)
      addresses = User::Address.all
      addresses
    }
  end
  field :addressesCount do
    type types.Int
    description 'Number of Addresses'
    resolve ->(_object, args, ctx) {
      User::Address.count
    }
  end
  field :address, Types::UserAddressType do
    argument :id, types.ID
    description 'fetch a Address by id'
    resolve ->(object, args, ctx) {
      User::Address.find(args[:id])
    }
  end

  connection :allUserCountries, Types::UserCountryType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # countries = User::Country.limit(args[:limit].to_i)
      countries = User::Country.all
      countries
    }
  end
  field :countriesCount do
    type types.Int
    description 'Number of Countries'
    resolve ->(_object, args, ctx) {
      User::Country.count
    }
  end
  field :country, Types::UserCountryType do
    argument :id, types.ID
    description 'fetch a Country by id'
    resolve ->(object, args, ctx) {
      User::Country.find(args[:id])
    }
  end

  connection :allUserStates, Types::UserStateType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_country_id, types.ID

    resolve ->(_obj, args, ctx) {
      # states = User::State.limit(args[:limit].to_i)
      states = User::State.all
      states = states.by_country(args[:f_country_id]) if args[:f_country_id]
      states
    }
  end
  field :statesCount do
    type types.Int
    description 'Number of States'
    resolve ->(_object, args, ctx) {
      User::State.count
    }
  end
  field :state, Types::UserStateType do
    argument :id, types.ID
    description 'fetch a State by id'
    resolve ->(object, args, ctx) {
      User::State.find(args[:id])
    }
  end

  connection :allUserTaxTypes, Types::UserTaxTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # taxTypes = User::TaxType.limit(args[:limit].to_i)
      taxTypes = User::TaxType.all
      taxTypes
    }
  end
  field :taxTypesCount do
    type types.Int
    description 'Number of TaxTypes'
    resolve ->(_object, args, ctx) {
      User::TaxType.count
    }
  end
  field :taxType, Types::UserTaxTypeType do
    argument :id, types.ID
    description 'fetch a TaxType by id'
    resolve ->(object, args, ctx) {
      User::TaxType.find(args[:id])
    }
  end

  connection :allUserTaxRates, Types::UserTaxRateType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # taxRates = User::TaxRate.limit(args[:limit].to_i)
      taxRates = User::TaxRate.all
      taxRates
    }
  end
  field :taxRatesCount do
    type types.Int
    description 'Number of TaxRates'
    resolve ->(_object, args, ctx) {
      User::TaxRate.count
    }
  end
  field :taxRate, Types::UserTaxRateType do
    argument :id, types.ID
    description 'fetch a TaxRate by id'
    resolve ->(object, args, ctx) {
      User::TaxRate.find(args[:id])
    }
  end

  connection :allMaterialMaterialTypes, Types::MaterialMaterialTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # materialTypes = Material::MaterialType.limit(args[:limit].to_i)
      materialTypes = Material::MaterialType.all
      materialTypes
    }
  end
  field :materialTypesCount do
    type types.Int
    description 'Number of MaterialTypes'
    resolve ->(_object, args, ctx) {
      Material::MaterialType.count
    }
  end
  field :materialType, Types::MaterialMaterialTypeType do
    argument :id, types.ID
    description 'fetch a MaterialType by id'
    resolve ->(object, args, ctx) {
      Material::MaterialType.find(args[:id])
    }
  end

  connection :allMaterialMaterials, Types::MaterialMaterialType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_m_type_id, types.ID
    argument :f_m_type_string, types.String, default_value: ''

    resolve ->(_obj, args, ctx) {
      # materials = Material::Material.limit(args[:limit].to_i)
      materials = Material::Material.all
      materials = materials.by_material_type(args[:f_m_type_id]) if args[:f_m_type_id]
      materials = materials.by_material_type_slug(args[:f_m_type_string]) if args[:f_m_type_string] && !args[:f_m_type_string].blank?
      materials
    }
  end
  field :materialsCount do
    type types.Int
    description 'Number of Materials'
    resolve ->(_object, args, ctx) {
      Material::Material.count
    }
  end
  field :material, Types::MaterialMaterialType do
    argument :id, types.ID
    description 'fetch a Material by id'
    resolve ->(object, args, ctx) {
      Material::Material.find(args[:id])
    }
  end

  connection :allMaterialMetalPurities, Types::MaterialMetalPurityType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

    resolve ->(_obj, args, ctx) {
      # metalPurities = Material::MetalPurity.limit(args[:limit].to_i)
      metalPurities = Material::MetalPurity.all
      metalPurities = metalPurities.by_material(args[:f_material_id]) if args[:f_material_id]
      metalPurities
    }
  end
  field :metalPuritiesCount do
    type types.Int
    description 'Number of MetalPurities'
    resolve ->(_object, args, ctx) {
      Material::MetalPurity.count
    }
  end
  field :metalPurity, Types::MaterialMetalPurityType do
    argument :id, types.ID
    description 'fetch a MetalPurity by id'
    resolve ->(object, args, ctx) {
      Material::MetalPurity.find(args[:id])
    }
  end

  connection :allMaterialGemShapes, Types::MaterialGemShapeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

    resolve ->(_obj, args, ctx) {
      # gemShapes = Material::GemShape.limit(args[:limit].to_i)
      gemShapes = Material::GemShape.all
      gemShapes = gemShapes.by_material(args[:f_material_id]) if args[:f_material_id]
      gemShapes
    }
  end
  field :gemShapesCount do
    type types.Int
    description 'Number of GemShapes'
    resolve ->(_object, args, ctx) {
      Material::GemShape.count
    }
  end
  field :gemShape, Types::MaterialGemShapeType do
    argument :id, types.ID
    description 'fetch a GemShape by id'
    resolve ->(object, args, ctx) {
      Material::GemShape.find(args[:id])
    }
  end

  connection :allMaterialGemClarities, Types::MaterialGemClarityType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

    resolve ->(_obj, args, ctx) {
      # gemClarities = Material::GemClarity.limit(args[:limit].to_i)
      gemClarities = Material::GemClarity.all
      gemClarities = gemClarities.by_material(args[:f_material_id]) if args[:f_material_id]
      gemClarities
    }
  end
  field :gemClaritiesCount do
    type types.Int
    description 'Number of GemClarities'
    resolve ->(_object, args, ctx) {
      Material::GemClarity.count
    }
  end
  field :gemClarity, Types::MaterialGemClarityType do
    argument :id, types.ID
    description 'fetch a GemClarity by id'
    resolve ->(object, args, ctx) {
      Material::GemClarity.find(args[:id])
    }
  end

  connection :allMaterialColors, Types::MaterialColorType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID

    resolve ->(_obj, args, ctx) {
      # colors = Material::Color.limit(args[:limit].to_i)
      colors = Material::Color.all
      colors = colors.by_material(args[:f_material_id]) if args[:f_material_id]
      colors
    }
  end
  field :colorsCount do
    type types.Int
    description 'Number of Colors'
    resolve ->(_object, args, ctx) {
      Material::Color.count
    }
  end
  field :color, Types::MaterialColorType do
    argument :id, types.ID
    description 'fetch a Color by id'
    resolve ->(object, args, ctx) {
      Material::Color.find(args[:id])
    }
  end

  connection :allMaterialGemSizes, Types::MaterialGemSizeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_material_id, types.ID
    argument :f_gem_shape_id, types.ID
    argument :f_gem_clarity_id, types.ID
    argument :page, types.Int, default_value: 0
    argument :per, types.Int, default_value: 0
    argument :order, types.String, default_value: 'created_at DESC'

    resolve ->(_obj, args, ctx) {
      # gemSizes = Material::GemSize.limit(args[:limit].to_i)
      gemSizes = Material::GemSize.all
      gemSizes = gemSizes.by_filter(args[:f_material_id], args[:f_gem_shape_id]) if args[:f_material_id] && args[:f_gem_shape_id]
      gemSizes = gemSizes.gem_clarity(args[:f_gem_clarity_id]) if args[:f_gem_clarity_id]
      gemSizes = gemSizes.page(args[:page]) if args[:page] && args[:page] > 0
      gemSizes = gemSizes.per(args[:per]) if args[:per] && args[:per] > 0
      gemSizes = gemSizes.order(args[:order]) if args[:order] && !args[:order].blank?
      gemSizes
    }
  end
  field :gemSizesCount do
    type types.Int
    description 'Number of GemSizes'
    resolve ->(_object, args, ctx) {
      Material::GemSize.count
    }
  end
  field :gemSize, Types::MaterialGemSizeType do
    argument :id, types.ID
    description 'fetch a GemSize by id'
    resolve ->(object, args, ctx) {
      Material::GemSize.find(args[:id])
    }
  end

  connection :allMaterialAccessories, Types::MaterialAccessoryType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # accessories = Material::Accessory.limit(args[:limit].to_i)
      accessories = Material::Accessory.all
      accessories
    }
  end
  field :accessoriesCount do
    type types.Int
    description 'Number of Accessories'
    resolve ->(_object, args, ctx) {
      Material::Accessory.count
    }
  end
  field :accessory, Types::MaterialAccessoryType do
    argument :id, types.ID
    description 'fetch a Accessory by id'
    resolve ->(object, args, ctx) {
      Material::Accessory.find(args[:id])
    }
  end


  connection :allMaterialLockers, Types::MaterialLockerType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_company_id, types.ID

    resolve ->(_obj, args, ctx) {
      # lockers = Material::Locker.limit(args[:limit].to_i)
      lockers = Material::Locker.all
      lockers = lockers.by_company(args[:f_company_id]) if args[:f_company_id]
      lockers
    }
  end
  field :lockersCount do
    type types.Int
    description 'Number of Lockers'
    resolve ->(_object, args, ctx) {
      Material::Locker.count
    }
  end
  field :locker, Types::MaterialLockerType do
    argument :id, types.ID
    description 'fetch a Locker by id'
    resolve ->(object, args, ctx) {
      Material::Locker.find(args[:id])
    }
  end

  connection :allMaterialRateOns, Types::MaterialRateOnType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_m_type_id, types.ID

    resolve ->(_obj, args, ctx) {
      # rateOns = Material::RateOn.limit(args[:limit].to_i)
      rateOns = Material::RateOn.all
      rateOns = rateOns.by_material_type(args[:f_m_type_id]) if args[:f_m_type_id]
      rateOns
    }
  end
  field :rateOnsCount do
    type types.Int
    description 'Number of RateOns'
    resolve ->(_object, args, ctx) {
      Material::RateOn.count
    }
  end
  field :rateOn, Types::MaterialRateOnType do
    argument :id, types.ID
    description 'fetch a RateOn by id'
    resolve ->(object, args, ctx) {
      Material::RateOn.find(args[:id])
    }
  end

  connection :allMaterialGemTypes, Types::MaterialGemTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # gemTypes = Material::GemType.limit(args[:limit].to_i)
      gemTypes = Material::GemType.all
      gemTypes
    }
  end
  field :gemTypesCount do
    type types.Int
    description 'Number of GemTypes'
    resolve ->(_object, args, ctx) {
      Material::GemType.count
    }
  end
  field :gemType, Types::MaterialGemTypeType do
    argument :id, types.ID
    description 'fetch a GemType by id'
    resolve ->(object, args, ctx) {
      Material::GemType.find(args[:id])
    }
  end

  connection :allDesignCollections, Types::DesignCollectionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :all, types.String, default_value: ''

    resolve ->(_obj, args, ctx) {
      # collections = Design::Collection.limit(args[:limit].to_i)
      collections = Design::Collection.all
      collections = collections.(args[:all]) if args[:all] && !args[:all].blank?
      collections
    }
  end
  field :collectionsCount do
    type types.Int
    description 'Number of Collections'
    resolve ->(_object, args, ctx) {
      Design::Collection.count
    }
  end
  field :collection, Types::DesignCollectionType do
    argument :id, types.ID
    description 'fetch a Collection by id'
    resolve ->(object, args, ctx) {
      Design::Collection.find(args[:id])
    }
  end

  connection :allDesignMakingTypes, Types::DesignMakingTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # makingTypes = Design::MakingType.limit(args[:limit].to_i)
      makingTypes = Design::MakingType.all
      makingTypes
    }
  end
  field :makingTypesCount do
    type types.Int
    description 'Number of MakingTypes'
    resolve ->(_object, args, ctx) {
      Design::MakingType.count
    }
  end
  field :makingType, Types::DesignMakingTypeType do
    argument :id, types.ID
    description 'fetch a MakingType by id'
    resolve ->(object, args, ctx) {
      Design::MakingType.find(args[:id])
    }
  end

  connection :allDesignSettingTypes, Types::DesignSettingTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # settingTypes = Design::SettingType.limit(args[:limit].to_i)
      settingTypes = Design::SettingType.all
      settingTypes
    }
  end
  field :settingTypesCount do
    type types.Int
    description 'Number of SettingTypes'
    resolve ->(_object, args, ctx) {
      Design::SettingType.count
    }
  end
  field :settingType, Types::DesignSettingTypeType do
    argument :id, types.ID
    description 'fetch a SettingType by id'
    resolve ->(object, args, ctx) {
      Design::SettingType.find(args[:id])
    }
  end

  connection :allDesignStyles, Types::DesignStyleType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # styles = Design::StyleView.limit(args[:limit].to_i)
      styles = Design::StyleView.all
      styles
    }
  end
  field :stylesCount do
    type types.Int
    description 'Number of Styles'
    resolve ->(_object, args, ctx) {
      Design::StyleView.count
    }
  end
  field :style, Types::DesignStyleType do
    argument :id, types.ID
    description 'fetch a Style by id'
    resolve ->(object, args, ctx) {
      Design::StyleView.find(args[:id])
    }
  end






  connection :allDesignImages, Types::DesignImageType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # images = Design::Image.limit(args[:limit].to_i)
      images = Design::Image.all
      images
    }
  end
  field :imagesCount do
    type types.Int
    description 'Number of Images'
    resolve ->(_object, args, ctx) {
      Design::Image.count
    }
  end
  field :image, Types::DesignImageType do
    argument :id, types.ID
    description 'fetch a Image by id'
    resolve ->(object, args, ctx) {
      Design::Image.find(args[:id])
    }
  end

  connection :allSaleMTxnTypes, Types::SaleMTxnTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # mTxnTypes = Sale::MTxnType.limit(args[:limit].to_i)
      mTxnTypes = Sale::MTxnType.all
      mTxnTypes
    }
  end
  field :mTxnTypesCount do
    type types.Int
    description 'Number of MTxnTypes'
    resolve ->(_object, args, ctx) {
      Sale::MTxnType.count
    }
  end
  field :mTxnType, Types::SaleMTxnTypeType do
    argument :id, types.ID
    description 'fetch a MTxnType by id'
    resolve ->(object, args, ctx) {
      Sale::MTxnType.find(args[:id])
    }
  end

  connection :allSaleMTxns, Types::SaleMTxnType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # mTxns = Sale::MTxnView.limit(args[:limit].to_i)
      mTxns = Sale::MTxnView.all
      mTxns
    }
  end
  field :mTxnsCount do
    type types.Int
    description 'Number of MTxns'
    resolve ->(_object, args, ctx) {
      Sale::MTxnView.count
    }
  end
  field :mTxn, Types::SaleMTxnType do
    argument :id, types.ID
    description 'fetch a MTxn by id'
    resolve ->(object, args, ctx) {
      Sale::MTxnView.find(args[:id])
    }
  end

  connection :allSaleOrderTypes, Types::SaleOrderTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # orderTypes = Sale::OrderType.limit(args[:limit].to_i)
      orderTypes = Sale::OrderType.all
      orderTypes
    }
  end
  field :orderTypesCount do
    type types.Int
    description 'Number of OrderTypes'
    resolve ->(_object, args, ctx) {
      Sale::OrderType.count
    }
  end
  field :orderType, Types::SaleOrderTypeType do
    argument :id, types.ID
    description 'fetch a OrderType by id'
    resolve ->(object, args, ctx) {
      Sale::OrderType.find(args[:id])
    }
  end

  connection :allSaleOrders, Types::SaleOrderType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # orders = Sale::OrderView.limit(args[:limit].to_i)
      orders = Sale::OrderView.all
      orders
    }
  end
  field :ordersCount do
    type types.Int
    description 'Number of Orders'
    resolve ->(_object, args, ctx) {
      Sale::OrderView.count
    }
  end
  field :order, Types::SaleOrderType do
    argument :id, types.ID
    description 'fetch a Order by id'
    resolve ->(object, args, ctx) {
      Sale::OrderView.find(args[:id])
    }
  end


  connection :allSaleJobs, Types::SaleJobType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # jobs = Sale::JobView.limit(args[:limit].to_i)
      jobs = Sale::JobView.all
      jobs
    }
  end
  field :jobsCount do
    type types.Int
    description 'Number of Jobs'
    resolve ->(_object, args, ctx) {
      Sale::JobView.count
    }
  end
  field :job, Types::SaleJobType do
    argument :id, types.ID
    description 'fetch a Job by id'
    resolve ->(object, args, ctx) {
      Sale::JobView.find(args[:id])
    }
  end


  connection :allSaleSales, Types::SaleSaleType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # sales = Sale::SaleView.limit(args[:limit].to_i)
      sales = Sale::SaleView.all
      sales
    }
  end
  field :salesCount do
    type types.Int
    description 'Number of Sales'
    resolve ->(_object, args, ctx) {
      Sale::SaleView.count
    }
  end
  field :sale, Types::SaleSaleType do
    argument :id, types.ID
    description 'fetch a Sale by id'
    resolve ->(object, args, ctx) {
      Sale::SaleView.find(args[:id])
    }
  end



  connection :allSaleMemos, Types::SaleMemoType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # memos = Sale::MemoView.limit(args[:limit].to_i)
      memos = Sale::MemoView.all
      memos
    }
  end
  field :memosCount do
    type types.Int
    description 'Number of Memos'
    resolve ->(_object, args, ctx) {
      Sale::MemoView.count
    }
  end
  field :memo, Types::SaleMemoType do
    argument :id, types.ID
    description 'fetch a Memo by id'
    resolve ->(object, args, ctx) {
      Sale::MemoView.find(args[:id])
    }
  end





  connection :allSaleMStocks, Types::SaleMStockType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}
    argument :f_company_id, types.ID
    argument :f_locker_id, types.ID
    argument :f_accessory_id, types.ID
    argument :f_material_id, types.ID
    argument :f_purity_id, types.ID
    argument :f_color_id, types.ID
    argument :f_gem_shape_id, types.ID
    argument :f_gem_clarity_id, types.ID
    argument :f_gem_size_id, types.ID

    resolve ->(_obj, args, ctx) {
      # mStocks = Sale::MStock.limit(args[:limit].to_i)
      mStocks = Sale::MStock.all
      mStocks = mStocks.by_company(args[:f_company_id]) if args[:f_company_id]
      mStocks = mStocks.by_locker(args[:f_locker_id]) if args[:f_locker_id]
      mStocks = mStocks.by_accessory(args[:f_accessory_id]) if args[:f_accessory_id]
      mStocks = mStocks.by_material(args[:f_material_id]) if args[:f_material_id]
      mStocks = mStocks.by_metal_purity(args[:f_purity_id]) if args[:f_purity_id]
      mStocks = mStocks.by_color(args[:f_color_id]) if args[:f_color_id]
      mStocks = mStocks.by_gem_shape(args[:f_gem_shape_id]) if args[:f_gem_shape_id]
      mStocks = mStocks.by_gem_clarity(args[:f_gem_clarity_id]) if args[:f_gem_clarity_id]
      mStocks = mStocks.by_gem_size(args[:f_gem_size_id]) if args[:f_gem_size_id]
      mStocks
    }
  end
  field :mStocksCount do
    type types.Int
    description 'Number of MStocks'
    resolve ->(_object, args, ctx) {
      Sale::MStock.count
    }
  end
  field :mStock, Types::SaleMStockType do
    argument :id, types.ID
    description 'fetch a MStock by id'
    resolve ->(object, args, ctx) {
      Sale::MStock.find(args[:id])
    }
  end

  connection :allMfgPriorities, Types::MfgPriorityType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # priorities = Mfg::Priority.limit(args[:limit].to_i)
      priorities = Mfg::Priority.all
      priorities
    }
  end
  field :prioritiesCount do
    type types.Int
    description 'Number of Priorities'
    resolve ->(_object, args, ctx) {
      Mfg::Priority.count
    }
  end
  field :priority, Types::MfgPriorityType do
    argument :id, types.ID
    description 'fetch a Priority by id'
    resolve ->(object, args, ctx) {
      Mfg::Priority.find(args[:id])
    }
  end

  connection :allMfgProductTypes, Types::MfgProductTypeType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # productTypes = Mfg::ProductType.limit(args[:limit].to_i)
      productTypes = Mfg::ProductType.all
      productTypes
    }
  end
  field :productTypesCount do
    type types.Int
    description 'Number of ProductTypes'
    resolve ->(_object, args, ctx) {
      Mfg::ProductType.count
    }
  end
  field :productType, Types::MfgProductTypeType do
    argument :id, types.ID
    description 'fetch a ProductType by id'
    resolve ->(object, args, ctx) {
      Mfg::ProductType.find(args[:id])
    }
  end

  connection :allMfgDepartments, Types::MfgDepartmentType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # departments = Mfg::Department.limit(args[:limit].to_i)
      departments = Mfg::Department.all
      departments
    }
  end
  field :departmentsCount do
    type types.Int
    description 'Number of Departments'
    resolve ->(_object, args, ctx) {
      Mfg::Department.count
    }
  end
  field :department, Types::MfgDepartmentType do
    argument :id, types.ID
    description 'fetch a Department by id'
    resolve ->(object, args, ctx) {
      Mfg::Department.find(args[:id])
    }
  end

  connection :allMfgDustResources, Types::MfgDustResourceType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # dustResources = Mfg::DustResource.limit(args[:limit].to_i)
      dustResources = Mfg::DustResource.all
      dustResources
    }
  end
  field :dustResourcesCount do
    type types.Int
    description 'Number of DustResources'
    resolve ->(_object, args, ctx) {
      Mfg::DustResource.count
    }
  end
  field :dustResource, Types::MfgDustResourceType do
    argument :id, types.ID
    description 'fetch a DustResource by id'
    resolve ->(object, args, ctx) {
      Mfg::DustResource.find(args[:id])
    }
  end

  connection :allMfgWaxSettings, Types::MfgWaxSettingType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # waxSettings = Mfg::WaxSettingView.limit(args[:limit].to_i)
      waxSettings = Mfg::WaxSettingView.all
      waxSettings
    }
  end
  field :waxSettingsCount do
    type types.Int
    description 'Number of WaxSettings'
    resolve ->(_object, args, ctx) {
      Mfg::WaxSettingView.count
    }
  end
  field :waxSetting, Types::MfgWaxSettingType do
    argument :id, types.ID
    description 'fetch a WaxSetting by id'
    resolve ->(object, args, ctx) {
      Mfg::WaxSettingView.find(args[:id])
    }
  end


  connection :allMfgCastings, Types::MfgCastingType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # castings = Mfg::CastingView.limit(args[:limit].to_i)
      castings = Mfg::CastingView.all
      castings
    }
  end
  field :castingsCount do
    type types.Int
    description 'Number of Castings'
    resolve ->(_object, args, ctx) {
      Mfg::CastingView.count
    }
  end
  field :casting, Types::MfgCastingType do
    argument :id, types.ID
    description 'fetch a Casting by id'
    resolve ->(object, args, ctx) {
      Mfg::CastingView.find(args[:id])
    }
  end


  connection :allMfgMetalIssues, Types::MfgMetalIssueType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # metalIssues = Mfg::MetalIssueView.limit(args[:limit].to_i)
      metalIssues = Mfg::MetalIssueView.all
      metalIssues
    }
  end
  field :metalIssuesCount do
    type types.Int
    description 'Number of MetalIssues'
    resolve ->(_object, args, ctx) {
      Mfg::MetalIssueView.count
    }
  end
  field :metalIssue, Types::MfgMetalIssueType do
    argument :id, types.ID
    description 'fetch a MetalIssue by id'
    resolve ->(object, args, ctx) {
      Mfg::MetalIssueView.find(args[:id])
    }
  end


  connection :allMfgMfgTxns, Types::MfgMfgTxnType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # mfgTxns = Mfg::MfgTxnView.limit(args[:limit].to_i)
      mfgTxns = Mfg::MfgTxnView.all
      mfgTxns
    }
  end
  field :mfgTxnsCount do
    type types.Int
    description 'Number of MfgTxns'
    resolve ->(_object, args, ctx) {
      Mfg::MfgTxnView.count
    }
  end
  field :mfgTxn, Types::MfgMfgTxnType do
    argument :id, types.ID
    description 'fetch a MfgTxn by id'
    resolve ->(object, args, ctx) {
      Mfg::MfgTxnView.find(args[:id])
    }
  end


  connection :allMfgRefinings, Types::MfgRefiningType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # refinings = Mfg::RefiningView.limit(args[:limit].to_i)
      refinings = Mfg::RefiningView.all
      refinings
    }
  end
  field :refiningsCount do
    type types.Int
    description 'Number of Refinings'
    resolve ->(_object, args, ctx) {
      Mfg::RefiningView.count
    }
  end
  field :refining, Types::MfgRefiningType do
    argument :id, types.ID
    description 'fetch a Refining by id'
    resolve ->(object, args, ctx) {
      Mfg::RefiningView.find(args[:id])
    }
  end


  connection :allDesignStyleTransitions, Types::DesignStyleTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # styleTransitions = Design::StyleTransition.limit(args[:limit].to_i)
      styleTransitions = Design::StyleTransition.all
      styleTransitions
    }
  end
  field :styleTransitionsCount do
    type types.Int
    description 'Number of StyleTransitions'
    resolve ->(_object, args, ctx) {
      Design::StyleTransition.count
    }
  end
  field :styleTransition, Types::DesignStyleTransitionType do
    argument :id, types.ID
    description 'fetch a StyleTransition by id'
    resolve ->(object, args, ctx) {
      Design::StyleTransition.find(args[:id])
    }
  end

  connection :allMfgCastingTransitions, Types::MfgCastingTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # castingTransitions = Mfg::CastingTransition.limit(args[:limit].to_i)
      castingTransitions = Mfg::CastingTransition.all
      castingTransitions
    }
  end
  field :castingTransitionsCount do
    type types.Int
    description 'Number of CastingTransitions'
    resolve ->(_object, args, ctx) {
      Mfg::CastingTransition.count
    }
  end
  field :castingTransition, Types::MfgCastingTransitionType do
    argument :id, types.ID
    description 'fetch a CastingTransition by id'
    resolve ->(object, args, ctx) {
      Mfg::CastingTransition.find(args[:id])
    }
  end

  connection :allSaleJobTransitions, Types::SaleJobTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # jobTransitions = Sale::JobTransition.limit(args[:limit].to_i)
      jobTransitions = Sale::JobTransition.all
      jobTransitions
    }
  end
  field :jobTransitionsCount do
    type types.Int
    description 'Number of JobTransitions'
    resolve ->(_object, args, ctx) {
      Sale::JobTransition.count
    }
  end
  field :jobTransition, Types::SaleJobTransitionType do
    argument :id, types.ID
    description 'fetch a JobTransition by id'
    resolve ->(object, args, ctx) {
      Sale::JobTransition.find(args[:id])
    }
  end

  connection :allSaleMTxnTransitions, Types::SaleMTxnTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # mTxnTransitions = Sale::MTxnTransition.limit(args[:limit].to_i)
      mTxnTransitions = Sale::MTxnTransition.all
      mTxnTransitions
    }
  end
  field :mTxnTransitionsCount do
    type types.Int
    description 'Number of MTxnTransitions'
    resolve ->(_object, args, ctx) {
      Sale::MTxnTransition.count
    }
  end
  field :mTxnTransition, Types::SaleMTxnTransitionType do
    argument :id, types.ID
    description 'fetch a MTxnTransition by id'
    resolve ->(object, args, ctx) {
      Sale::MTxnTransition.find(args[:id])
    }
  end

  connection :allSaleOrderTransitions, Types::SaleOrderTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # orderTransitions = Sale::OrderTransition.limit(args[:limit].to_i)
      orderTransitions = Sale::OrderTransition.all
      orderTransitions
    }
  end
  field :orderTransitionsCount do
    type types.Int
    description 'Number of OrderTransitions'
    resolve ->(_object, args, ctx) {
      Sale::OrderTransition.count
    }
  end
  field :orderTransition, Types::SaleOrderTransitionType do
    argument :id, types.ID
    description 'fetch a OrderTransition by id'
    resolve ->(object, args, ctx) {
      Sale::OrderTransition.find(args[:id])
    }
  end

  connection :allUserAccountTransitions, Types::UserAccountTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # accountTransitions = User::AccountTransition.limit(args[:limit].to_i)
      accountTransitions = User::AccountTransition.all
      accountTransitions
    }
  end
  field :accountTransitionsCount do
    type types.Int
    description 'Number of AccountTransitions'
    resolve ->(_object, args, ctx) {
      User::AccountTransition.count
    }
  end
  field :accountTransition, Types::UserAccountTransitionType do
    argument :id, types.ID
    description 'fetch a AccountTransition by id'
    resolve ->(object, args, ctx) {
      User::AccountTransition.find(args[:id])
    }
  end

  connection :allMfgWaxSettingTransitions, Types::MfgWaxSettingTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # waxSettingTransitions = Mfg::WaxSettingTransition.limit(args[:limit].to_i)
      waxSettingTransitions = Mfg::WaxSettingTransition.all
      waxSettingTransitions
    }
  end
  field :waxSettingTransitionsCount do
    type types.Int
    description 'Number of WaxSettingTransitions'
    resolve ->(_object, args, ctx) {
      Mfg::WaxSettingTransition.count
    }
  end
  field :waxSettingTransition, Types::MfgWaxSettingTransitionType do
    argument :id, types.ID
    description 'fetch a WaxSettingTransition by id'
    resolve ->(object, args, ctx) {
      Mfg::WaxSettingTransition.find(args[:id])
    }
  end

  connection :allMfgMetalIssueTransitions, Types::MfgMetalIssueTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # metalIssueTransitions = Mfg::MetalIssueTransition.limit(args[:limit].to_i)
      metalIssueTransitions = Mfg::MetalIssueTransition.all
      metalIssueTransitions
    }
  end
  field :metalIssueTransitionsCount do
    type types.Int
    description 'Number of MetalIssueTransitions'
    resolve ->(_object, args, ctx) {
      Mfg::MetalIssueTransition.count
    }
  end
  field :metalIssueTransition, Types::MfgMetalIssueTransitionType do
    argument :id, types.ID
    description 'fetch a MetalIssueTransition by id'
    resolve ->(object, args, ctx) {
      Mfg::MetalIssueTransition.find(args[:id])
    }
  end

  connection :allMfgMfgTxnTransitions, Types::MfgMfgTxnTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # mfgTxnTransitions = Mfg::MfgTxnTransition.limit(args[:limit].to_i)
      mfgTxnTransitions = Mfg::MfgTxnTransition.all
      mfgTxnTransitions
    }
  end
  field :mfgTxnTransitionsCount do
    type types.Int
    description 'Number of MfgTxnTransitions'
    resolve ->(_object, args, ctx) {
      Mfg::MfgTxnTransition.count
    }
  end
  field :mfgTxnTransition, Types::MfgMfgTxnTransitionType do
    argument :id, types.ID
    description 'fetch a MfgTxnTransition by id'
    resolve ->(object, args, ctx) {
      Mfg::MfgTxnTransition.find(args[:id])
    }
  end

  connection :allMfgRefiningTransitions, Types::MfgRefiningTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # refiningTransitions = Mfg::RefiningTransition.limit(args[:limit].to_i)
      refiningTransitions = Mfg::RefiningTransition.all
      refiningTransitions
    }
  end
  field :refiningTransitionsCount do
    type types.Int
    description 'Number of RefiningTransitions'
    resolve ->(_object, args, ctx) {
      Mfg::RefiningTransition.count
    }
  end
  field :refiningTransition, Types::MfgRefiningTransitionType do
    argument :id, types.ID
    description 'fetch a RefiningTransition by id'
    resolve ->(object, args, ctx) {
      Mfg::RefiningTransition.find(args[:id])
    }
  end

  connection :allSaleMemoTransitions, Types::SaleMemoTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # memoTransitions = Sale::MemoTransition.limit(args[:limit].to_i)
      memoTransitions = Sale::MemoTransition.all
      memoTransitions
    }
  end
  field :memoTransitionsCount do
    type types.Int
    description 'Number of MemoTransitions'
    resolve ->(_object, args, ctx) {
      Sale::MemoTransition.count
    }
  end
  field :memoTransition, Types::SaleMemoTransitionType do
    argument :id, types.ID
    description 'fetch a MemoTransition by id'
    resolve ->(object, args, ctx) {
      Sale::MemoTransition.find(args[:id])
    }
  end

  connection :allSaleSaleTransitions, Types::SaleSaleTransitionType.connection_type do
    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}

    resolve ->(_obj, args, ctx) {
      # saleTransitions = Sale::SaleTransition.limit(args[:limit].to_i)
      saleTransitions = Sale::SaleTransition.all
      saleTransitions
    }
  end
  field :saleTransitionsCount do
    type types.Int
    description 'Number of SaleTransitions'
    resolve ->(_object, args, ctx) {
      Sale::SaleTransition.count
    }
  end
  field :saleTransition, Types::SaleSaleTransitionType do
    argument :id, types.ID
    description 'fetch a SaleTransition by id'
    resolve ->(object, args, ctx) {
      Sale::SaleTransition.find(args[:id])
    }
  end

# ================All State Machine Queries============================

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Design::StyleStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Design::Style.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Mfg::CastingStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Mfg::Casting.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Sale::JobStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Sale::Job.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Sale::MTxnStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Sale::MTxn.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Sale::OrderStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Sale::Order.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = User::AccountStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      User::Account.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Mfg::IssueFinishStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Mfg::WaxSetting.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Mfg::IssueFinishStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Mfg::MetalIssue.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Mfg::MfgTxnStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Mfg::MfgTxn.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Mfg::RefiningStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Mfg::Refining.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Sale::MemoStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Sale::Memo.find(args[:id]).state_machine.current_state
    }
  end

  field :allSaleSaleTransitions do
    type types[types.String]
    

    resolve ->(_obj, args, ctx) {
      saleTransitions = Sale::SaleStateMachine.states
      saleTransitions
    }
  end
  field :saleTransition, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      Sale::Sale.find(args[:id]).state_machine.current_state
    }
  end
# Part1A
# Part1A

# ============================================
# Extra Queries:
  field :currentUser, Types::UserAccountType do
    description 'fetch the current user.'
    resolve ->(object, args, ctx) {
      # ctx[:current_user]
      User::Account.first
    }
  end
end