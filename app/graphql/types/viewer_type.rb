Dir[File.dirname(__FILE__) + '/scalars/*.rb'].each {|file| require file}
Dir[File.dirname(__FILE__) + '/default/*.rb'].each {|file| require file}

Types::ViewerType = GraphQL::ObjectType.define do
  # Hack to support root queries
  name 'Viewer'
  description 'Support unassociated root queries that fetches collections.'
  interfaces [GraphQL::Relay::Node.interface]

  # `id` exposes the UUID
  global_id_field :id

# All Generated Queries are below:
# ================All Active Record Queries============================

  field :userContactsCount, UserContactsCountField
  connection :userContacts, UserContactsField
  field :userContact, UserContactField
  field :userCompaniesCount, UserCompaniesCountField
  connection :userCompanies, UserCompaniesField
  field :userCompany, UserCompanyField


  field :userAccountTypesCount, UserAccountTypesCountField
  connection :userAccountTypes, UserAccountTypesField
  field :userAccountType, UserAccountTypeField
  field :userAccountsCount, UserAccountsCountField
  connection :userAccounts, UserAccountsField
  field :userAccount, UserAccountField


  field :userAuthenticationTokensCount, UserAuthenticationTokensCountField
  connection :userAuthenticationTokens, UserAuthenticationTokensField
  field :userAuthenticationToken, UserAuthenticationTokenField
  field :userRolesCount, UserRolesCountField
  connection :userRoles, UserRolesField
  field :userRole, UserRoleField
  field :userAddressesCount, UserAddressesCountField
  connection :userAddresses, UserAddressesField
  field :userAddress, UserAddressField
  field :userCountriesCount, UserCountriesCountField
  connection :userCountries, UserCountriesField
  field :userCountry, UserCountryField
  field :userStatesCount, UserStatesCountField
  connection :userStates, UserStatesField
  field :userState, UserStateField
  field :userTaxTypesCount, UserTaxTypesCountField
  connection :userTaxTypes, UserTaxTypesField
  field :userTaxType, UserTaxTypeField
  field :userTaxRatesCount, UserTaxRatesCountField
  connection :userTaxRates, UserTaxRatesField
  field :userTaxRate, UserTaxRateField
  field :materialMaterialTypesCount, MaterialMaterialTypesCountField
  connection :materialMaterialTypes, MaterialMaterialTypesField
  field :materialMaterialType, MaterialMaterialTypeField
  field :materialMaterialsCount, MaterialMaterialsCountField
  connection :materialMaterials, MaterialMaterialsField
  field :materialMaterial, MaterialMaterialField
  field :materialMetalPuritiesCount, MaterialMetalPuritiesCountField
  connection :materialMetalPurities, MaterialMetalPuritiesField
  field :materialMetalPurity, MaterialMetalPurityField
  field :materialGemShapesCount, MaterialGemShapesCountField
  connection :materialGemShapes, MaterialGemShapesField
  field :materialGemShape, MaterialGemShapeField
  field :materialGemClaritiesCount, MaterialGemClaritiesCountField
  connection :materialGemClarities, MaterialGemClaritiesField
  field :materialGemClarity, MaterialGemClarityField
  field :materialColorsCount, MaterialColorsCountField
  connection :materialColors, MaterialColorsField
  field :materialColor, MaterialColorField
  field :materialGemSizesCount, MaterialGemSizesCountField
  connection :materialGemSizes, MaterialGemSizesField
  field :materialGemSize, MaterialGemSizeField
  field :materialAccessoriesCount, MaterialAccessoriesCountField
  connection :materialAccessories, MaterialAccessoriesField
  field :materialAccessory, MaterialAccessoryField

  field :materialLockersCount, MaterialLockersCountField
  connection :materialLockers, MaterialLockersField
  field :materialLocker, MaterialLockerField
  field :materialRateOnsCount, MaterialRateOnsCountField
  connection :materialRateOns, MaterialRateOnsField
  field :materialRateOn, MaterialRateOnField
  field :materialGemTypesCount, MaterialGemTypesCountField
  connection :materialGemTypes, MaterialGemTypesField
  field :materialGemType, MaterialGemTypeField
  field :designCollectionsCount, DesignCollectionsCountField
  connection :designCollections, DesignCollectionsField
  field :designCollection, DesignCollectionField
  field :designMakingTypesCount, DesignMakingTypesCountField
  connection :designMakingTypes, DesignMakingTypesField
  field :designMakingType, DesignMakingTypeField
  field :designSettingTypesCount, DesignSettingTypesCountField
  connection :designSettingTypes, DesignSettingTypesField
  field :designSettingType, DesignSettingTypeField
  field :designStylesCount, DesignStylesCountField
  connection :designStyles, DesignStylesField
  field :designStyle, DesignStyleField





  field :designImagesCount, DesignImagesCountField
  connection :designImages, DesignImagesField
  field :designImage, DesignImageField
  field :saleMTxnTypesCount, SaleMTxnTypesCountField
  connection :saleMTxnTypes, SaleMTxnTypesField
  field :saleMTxnType, SaleMTxnTypeField
  field :saleMTxnsCount, SaleMTxnsCountField
  connection :saleMTxns, SaleMTxnsField
  field :saleMTxn, SaleMTxnField
  field :saleOrderTypesCount, SaleOrderTypesCountField
  connection :saleOrderTypes, SaleOrderTypesField
  field :saleOrderType, SaleOrderTypeField
  field :saleOrdersCount, SaleOrdersCountField
  connection :saleOrders, SaleOrdersField
  field :saleOrder, SaleOrderField

  field :saleJobsCount, SaleJobsCountField
  connection :saleJobs, SaleJobsField
  field :saleJob, SaleJobField

  field :saleSalesCount, SaleSalesCountField
  connection :saleSales, SaleSalesField
  field :saleSale, SaleSaleField


  field :saleMemosCount, SaleMemosCountField
  connection :saleMemos, SaleMemosField
  field :saleMemo, SaleMemoField




  field :saleMStocksCount, SaleMStocksCountField
  connection :saleMStocks, SaleMStocksField
  field :saleMStock, SaleMStockField
  field :mfgPrioritiesCount, MfgPrioritiesCountField
  connection :mfgPriorities, MfgPrioritiesField
  field :mfgPriority, MfgPriorityField
  field :mfgProductTypesCount, MfgProductTypesCountField
  connection :mfgProductTypes, MfgProductTypesField
  field :mfgProductType, MfgProductTypeField
  field :mfgDepartmentsCount, MfgDepartmentsCountField
  connection :mfgDepartments, MfgDepartmentsField
  field :mfgDepartment, MfgDepartmentField
  field :mfgDustResourcesCount, MfgDustResourcesCountField
  connection :mfgDustResources, MfgDustResourcesField
  field :mfgDustResource, MfgDustResourceField
  field :mfgWaxSettingsCount, MfgWaxSettingsCountField
  connection :mfgWaxSettings, MfgWaxSettingsField
  field :mfgWaxSetting, MfgWaxSettingField

  field :mfgCastingsCount, MfgCastingsCountField
  connection :mfgCastings, MfgCastingsField
  field :mfgCasting, MfgCastingField

  field :mfgMetalIssuesCount, MfgMetalIssuesCountField
  connection :mfgMetalIssues, MfgMetalIssuesField
  field :mfgMetalIssue, MfgMetalIssueField

  field :mfgMfgTxnsCount, MfgMfgTxnsCountField
  connection :mfgMfgTxns, MfgMfgTxnsField
  field :mfgMfgTxn, MfgMfgTxnField

  field :mfgRefiningsCount, MfgRefiningsCountField
  connection :mfgRefinings, MfgRefiningsField
  field :mfgRefining, MfgRefiningField

  field :designStyleTransitionsCount, DesignStyleTransitionsCountField
  connection :designStyleTransitions, DesignStyleTransitionsField
  field :designStyleTransition, DesignStyleTransitionField
  field :mfgCastingTransitionsCount, MfgCastingTransitionsCountField
  connection :mfgCastingTransitions, MfgCastingTransitionsField
  field :mfgCastingTransition, MfgCastingTransitionField
  field :saleJobTransitionsCount, SaleJobTransitionsCountField
  connection :saleJobTransitions, SaleJobTransitionsField
  field :saleJobTransition, SaleJobTransitionField
  field :saleMTxnTransitionsCount, SaleMTxnTransitionsCountField
  connection :saleMTxnTransitions, SaleMTxnTransitionsField
  field :saleMTxnTransition, SaleMTxnTransitionField
  field :saleOrderTransitionsCount, SaleOrderTransitionsCountField
  connection :saleOrderTransitions, SaleOrderTransitionsField
  field :saleOrderTransition, SaleOrderTransitionField
  field :userAccountTransitionsCount, UserAccountTransitionsCountField
  connection :userAccountTransitions, UserAccountTransitionsField
  field :userAccountTransition, UserAccountTransitionField
  field :mfgWaxSettingTransitionsCount, MfgWaxSettingTransitionsCountField
  connection :mfgWaxSettingTransitions, MfgWaxSettingTransitionsField
  field :mfgWaxSettingTransition, MfgWaxSettingTransitionField
  field :mfgMetalIssueTransitionsCount, MfgMetalIssueTransitionsCountField
  connection :mfgMetalIssueTransitions, MfgMetalIssueTransitionsField
  field :mfgMetalIssueTransition, MfgMetalIssueTransitionField
  field :mfgMfgTxnTransitionsCount, MfgMfgTxnTransitionsCountField
  connection :mfgMfgTxnTransitions, MfgMfgTxnTransitionsField
  field :mfgMfgTxnTransition, MfgMfgTxnTransitionField
  field :mfgRefiningTransitionsCount, MfgRefiningTransitionsCountField
  connection :mfgRefiningTransitions, MfgRefiningTransitionsField
  field :mfgRefiningTransition, MfgRefiningTransitionField
  field :saleMemoTransitionsCount, SaleMemoTransitionsCountField
  connection :saleMemoTransitions, SaleMemoTransitionsField
  field :saleMemoTransition, SaleMemoTransitionField
  field :saleSaleTransitionsCount, SaleSaleTransitionsCountField
  connection :saleSaleTransitions, SaleSaleTransitionsField
  field :saleSaleTransition, SaleSaleTransitionField

# Part2A
  field :currentUser, CurrentUserField
  field :currentCompany, CurrentCompanyField
# Part2A



# ================All State Machine Queries============================
# Temparary disable all_state_machines_queries_generate
  field :designStylesStates, DesignStylesStatesField
  field :designStyleState, DesignStyleStateField
  field :mfgCastingsStates, MfgCastingsStatesField
  field :mfgCastingState, MfgCastingStateField
  field :saleJobsStates, SaleJobsStatesField
  field :saleJobState, SaleJobStateField
  field :saleMTxnsStates, SaleMTxnsStatesField
  field :saleMTxnState, SaleMTxnStateField
  field :saleOrdersStates, SaleOrdersStatesField
  field :saleOrderState, SaleOrderStateField
  field :userAccountsStates, UserAccountsStatesField
  field :userAccountState, UserAccountStateField
  field :mfgWaxSettingsStates, MfgWaxSettingsStatesField
  field :mfgWaxSettingState, MfgWaxSettingStateField
  field :mfgMetalIssuesStates, MfgMetalIssuesStatesField
  field :mfgMetalIssueState, MfgMetalIssueStateField
  field :mfgMfgTxnsStates, MfgMfgTxnsStatesField
  field :mfgMfgTxnState, MfgMfgTxnStateField
  field :mfgRefiningsStates, MfgRefiningsStatesField
  field :mfgRefiningState, MfgRefiningStateField
  field :saleMemosStates, SaleMemosStatesField
  field :saleMemoState, SaleMemoStateField
  field :saleSalesStates, SaleSalesStatesField
  field :saleSaleState, SaleSaleStateField
end
# Part3A
# Part3A

# ============================================
