# == Schema Information
#
# Table name: design_collections
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string           not null
#  collection :string           not null
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Design::Collection < ApplicationRecord

  acts_as_list
  has_closure_tree

  validates :slug, presence: true, length: {minimum: 2}, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :collection, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  has_many :design_categories, :class_name => 'Design::Category'
  has_many :design_sub_categories, :class_name => 'Design::SubCategory'

  default_scope {order(position: :asc)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end
end
