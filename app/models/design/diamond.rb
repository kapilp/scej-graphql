# == Schema Information
#
# Table name: design_diamonds
#
#  id                      :integer          not null, primary key
#  position                :integer
#  material_material_id    :integer          not null
#  material_gem_shape_id   :integer          not null
#  material_gem_clarity_id :integer          not null
#  material_color_id       :integer
#  material_gem_size_id    :integer          not null
#  pcs                     :integer          default(0), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class Design::Diamond < ApplicationRecord
  acts_as_list

  belongs_to :material_material, :class_name => 'Material::Material'
  validates_presence_of :material_material
  belongs_to :material_gem_shape, :class_name => 'Material::GemShape'
  validates_presence_of :material_gem_shape
  belongs_to :material_gem_clarity, :class_name => 'Material::GemClarity'
  validates_presence_of :material_gem_clarity
  belongs_to :material_color, :class_name => 'Material::Color'
  validates_presence_of :material_color
  belongs_to :material_gem_size, :class_name => 'Material::GemSize'
  validates_presence_of :material_gem_size

  has_many :design_style_diamonds, :class_name => 'Design::StyleDiamond', foreign_key: :design_diamond_id, dependent: :destroy, inverse_of: :design_diamond
  has_many :design_styles, :class_name => 'Design::Style', through: :design_style_diamonds

  has_many :design_job_diamonds, :class_name => 'Design::JobDiamond', foreign_key: :design_diamond_id, dependent: :destroy, inverse_of: :design_diamond
  has_many :sale_jobs, :class_name => 'Sale::Job', through: :design_job_diamonds

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end
end
