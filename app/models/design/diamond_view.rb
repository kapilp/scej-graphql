# == Schema Information
#
# Table name: design_diamond_views
#
#  id                      :integer          primary key
#  position                :integer
#  material_material_id    :integer
#  material_gem_shape_id   :integer
#  material_gem_clarity_id :integer
#  material_color_id       :integer
#  material_gem_size_id    :integer
#  pcs                     :integer
#  weight                  :decimal(, )
#  created_at              :datetime
#  updated_at              :datetime
#  id01                    :integer
#  material_material_id01  :integer
#  material_gem_shape_id01 :integer
#  position01              :integer
#  slug                    :string
#  mm_size                 :string
#  carat_weight            :decimal(10, 3)
#  price                   :decimal(10, 2)
#  created_at01            :datetime
#  updated_at01            :datetime
#

class Design::DiamondView < ApplicationRecord
  acts_as_list

  belongs_to :material_material, :class_name => 'Material::Material'
  validates_presence_of :material_material
  belongs_to :material_gem_shape, :class_name => 'Material::GemShape'
  validates_presence_of :material_gem_shape
  belongs_to :material_gem_clarity, :class_name => 'Material::GemClarity'
  validates_presence_of :material_gem_clarity
  belongs_to :material_color, :class_name => 'Material::Color'
  validates_presence_of :material_color
  belongs_to :material_gem_size, :class_name => 'Material::GemSize'
  validates_presence_of :material_gem_size

  has_many :design_style_diamonds, :class_name => 'Design::StyleDiamond', foreign_key: :design_diamond_id, dependent: :destroy, inverse_of: :design_diamond
  has_many :design_styles, :class_name => 'Design::Style', through: :design_style_diamonds

  has_many :design_job_diamonds, :class_name => 'Design::JobDiamond', foreign_key: :design_diamond_id, dependent: :destroy, inverse_of: :design_diamond
  has_many :sale_jobs, :class_name => 'Sale::Job', through: :design_job_diamonds

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end
  
  
  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
  
end
