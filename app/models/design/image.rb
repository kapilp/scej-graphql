# == Schema Information
#
# Table name: design_images
#
#  id             :integer          not null, primary key
#  imageable_type :string
#  imageable_id   :integer
#  image_data     :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Design::Image < ApplicationRecord
  belongs_to :imageable, polymorphic: true, optional: true
  # mount_uploader :image, StyleUploader
  include ImageUploader::Attachment.new(:image) # adds an `image` virtual attribute
  
end
