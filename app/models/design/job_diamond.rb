# == Schema Information
#
# Table name: design_job_diamonds
#
#  id                :integer          not null, primary key
#  sale_job_id       :integer
#  design_diamond_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Design::JobDiamond < ApplicationRecord
  belongs_to :sale_job, :class_name => 'Sale::Job', inverse_of: :design_job_diamonds
  belongs_to :design_diamond, :class_name => 'Design::Diamond', inverse_of: :design_job_diamonds
end
