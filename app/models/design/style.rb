# == Schema Information
#
# Table name: design_styles
#
#  id                       :integer          not null, primary key
#  position                 :integer
#  slug                     :string           not null
#  design_collection_id     :integer
#  design_making_type_id    :integer
#  design_setting_type_id   :integer
#  user_account_id          :integer
#  material_material_id     :integer
#  material_metal_purity_id :integer
#  material_color_id        :integer
#  net_weight               :decimal(10, 3)   not null
#  description              :text
#  active                   :boolean          default(FALSE), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

#

class Design::Style < ApplicationRecord
  acts_as_list scope: [:design_collection_id]

  # validates :slug, :design_collection_id,
  #           presence: true
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  cattr_accessor(:paginates_per) {10}

  has_many :design_style_diamonds, :class_name => 'Design::StyleDiamond', foreign_key: :design_style_id, inverse_of: :design_style
  has_many :design_diamonds, :class_name => 'Design::Diamond', through: :design_style_diamonds, dependent: :destroy

  has_many :design_diamond_views, :class_name => 'Design::DiamondView', through: :design_style_diamonds, :source => :design_diamond
  # dependent: :destroy is necessary otherwise its gives error.

  belongs_to :design_collection, :class_name => 'Design::Collection'
  validates_presence_of :design_collection
  belongs_to :design_making_type, :class_name => 'Design::MakingType'
  validates_presence_of :design_making_type
  belongs_to :design_setting_type, :class_name => 'Design::SettingType'
  validates_presence_of :design_setting_type
  belongs_to :user_account, :class_name => 'User::Account'
  validates_presence_of :user_account
  belongs_to :material_material, :class_name => 'Material::Material'
  validates_presence_of :material_material
  belongs_to :material_metal_purity, :class_name => 'Material::MetalPurity'
  validates_presence_of :material_metal_purity
  belongs_to :material_color, :class_name => 'Material::Color'
  validates_presence_of :material_color

  has_many :design_images, :class_name => 'Design::Image', as: :imageable, inverse_of: :imageable

  accepts_nested_attributes_for :design_diamonds,
                                reject_if: :all_blank,
                                allow_destroy: true
  accepts_nested_attributes_for :design_images,
                                reject_if: :all_blank,
                                allow_destroy: true
  accepts_nested_attributes_for :design_style_diamonds,
                                reject_if: :all_blank,
                                allow_destroy: true

  default_scope {order(design_collection_id: :asc, position: :asc)}
  scope :id, ->(a) {where(id: a)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :design_style_transitions, :class_name => 'Design::StyleTransition', autosave: false, :foreign_key => 'design_style_id'

  def state_machine
    @state_machine ||= Design::StyleStateMachine.new(self, transition_class: Design::StyleTransition)
  end

  def self.transition_class
    Design::StyleTransition
  end

  def self.initial_state
    :pending
  end

  private_class_method :initial_state

  # -------------------------------Statesman End-------------------------------


end
