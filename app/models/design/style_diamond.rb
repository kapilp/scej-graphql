# == Schema Information
#
# Table name: design_style_diamonds
#
#  id                :integer          not null, primary key
#  design_style_id   :integer
#  design_diamond_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Design::StyleDiamond < ApplicationRecord
  belongs_to :design_style, :class_name => 'Design::Style', inverse_of: :design_style_diamonds
  belongs_to :design_diamond, :class_name => 'Design::Diamond', inverse_of: :design_style_diamonds
end
