class Design::StyleStateMachine
  include Statesman::Machine

  state :'Manual Running', initial: :true
  state :'Manual Finished'
  state :'CAD Running'
  state :'CAD Finished'
  state :'Approved'

  transition from: :'Manual Running', to: [:'Manual Finished', :'CAD Running']
  transition from: :'CAD Running', to: [:'CAD Finished']
  transition from: :'CAD Finished', to: [:'Approved']
end


# to do dynamic denine------
=begin
# First define state..
Design::StyleStateMachine.state(:another_state55)
# Then Define Rules..
Design::StyleStateMachine.transition(from: 'Manual Running', to: :another_state55)

# Now You can transition it too...
kk = Design::Style.first
kk.state_machine.transition_to!(:another_state55)




# Useful Mehods
Design::StyleStateMachine.states
# Returns an array of all possible state names as strings.

Design::StyleStateMachine.successors
# Returns a hash of states and the states it is valid for them to transition to.

kk.state_machine.current_state
# Returns the current state based on existing transition objects.

kk.state_machine.in_state?(:another_state55)
# Returns true if the machine is in any of the given states.

kk.state_machine.history
# Returns a sorted array of all transition objects.

kk.state_machine.last_transition
# Returns the most recent transition object.

kk.state_machine.allowed_transitions
# Returns an array of states you can transition_to from current state.

kk.state_machine.can_transition_to?(:another_state55)
# Returns true if the current state can transition to the passed state and all applicable guards pass.

kk.state_machine.transition_to!(:another_state55)
# Transition to the passed state, returning true on success. Raises Statesman::GuardFailedError or Statesman::TransitionFailedError on failure.

kk.state_machine.transition_to(:another_state55)
# Transition to the passed state, returning true on success. Swallows all Statesman exceptions and returns false on failure. (NB. if your guard or callback code throws an exception, it will not be caught.)

=end