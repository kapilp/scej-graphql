# == Schema Information
#
# Table name: design_style_transitions
#
#  id              :integer          not null, primary key
#  to_state        :string           not null
#  metadata        :json
#  sort_key        :integer          not null
#  design_style_id :integer          not null
#  most_recent     :boolean          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

# Transition Model is for Persistence.
class Design::StyleTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition # Instead of using serialize to store the metadata in JSON format, Using PostgreSQL JSON column # Instead of using serialize to store the metadata in JSON format, Using PostgreSQL JSON column

  belongs_to :design_style, :class_name => 'Design::Style', inverse_of: :design_style_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = design_style.style_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
