# == Schema Information
#
# Table name: design_style_views
#
#  id                       :integer          primary key
#  position                 :integer
#  slug                     :string
#  design_collection_id     :integer
#  design_making_type_id    :integer
#  design_setting_type_id   :integer
#  user_account_id          :integer
#  material_material_id     :integer
#  material_metal_purity_id :integer
#  material_color_id        :integer
#  net_weight               :decimal(10, 3)
#  volume                   :decimal(, )
#  pure_weight              :decimal(, )
#  description              :text
#  active                   :boolean
#  created_at               :datetime
#  updated_at               :datetime
#  diamond_pcs              :integer
#  cs_pcs                   :integer
#  diamond_weight           :decimal(, )
#  cs_weight                :decimal(, )
#  gross_weight             :decimal(, )
#

class Design::StyleView < ApplicationRecord
  acts_as_list scope: [:design_collection_id]

  cattr_accessor(:paginates_per) {10}

  has_many :design_style_diamonds, :class_name => 'Design::StyleDiamond', foreign_key: :design_style_id, inverse_of: :design_style
  has_many :design_diamonds, :class_name => 'Design::Diamond', through: :design_style_diamonds, dependent: :destroy
  has_many :design_diamond_views, :class_name => 'Design::DiamondView', through: :design_style_diamonds, :source => :design_diamond
  # dependent: :destroy is necessary otherwise its gives error.

  belongs_to :design_collection, :class_name => 'Design::Collection'
  belongs_to :design_making_type, :class_name => 'Design::MakingType'
  belongs_to :design_setting_type, :class_name => 'Design::SettingType'
  belongs_to :user_account, :class_name => 'User::Account'
  belongs_to :material_material, :class_name => 'Material::Material'
  belongs_to :material_metal_purity, :class_name => 'Material::MetalPurity'
  belongs_to :material_color, :class_name => 'Material::Color'

  has_many :design_images, :class_name => 'Design::Image', as: :imageable, inverse_of: :imageable

  default_scope {order(design_collection_id: :asc, position: :asc)}
  scope :id, ->(a) {where(id: a)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end


  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :design_style_transitions, :class_name => 'Design::StyleTransition', autosave: false, :foreign_key => 'design_style_id'

  def state_machine
    @state_machine ||= Design::StyleStateMachine.new(self, transition_class: Design::StyleTransition)
  end

  def self.transition_class
    Design::StyleTransition
  end

  def self.initial_state
    :pending
  end

  private_class_method :initial_state

  # -------------------------------Statesman End-------------------------------

  
  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
  
end
