# == Schema Information
#
# Table name: material_accessories
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string           not null
#  accessory  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Material::Accessory < ApplicationRecord
  audited
  acts_as_list

  validates :slug, presence: true, uniqueness: true,
            format: { with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers' }
  validates :accessory, presence: true, length: { minimum: 2 }, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  has_many :sale_m_txn_details, :class_name => 'Sale::MTxnDetail'


  default_scope { order(position: :asc) }

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end
end
