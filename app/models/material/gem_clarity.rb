# == Schema Information
#
# Table name: material_gem_clarities
#
#  id                   :integer          not null, primary key
#  material_material_id :integer          not null
#  position             :integer
#  slug                 :string           not null
#  clarity              :string           not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Material::GemClarity < ApplicationRecord

  acts_as_list scope: [:material_material_id]
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :clarity, presence: true, length: {minimum: 2}, uniqueness: true
  validates :material_material_id, presence: true, numericality: { only_integer: true }
  cattr_accessor(:paginates_per) {10}

  has_many :material_gem_clarity_sizes, :class_name => 'Material::GemClaritySize', foreign_key: :material_gem_clarity_id, inverse_of: :material_gem_clarity
  # No, inverse_of doesn't work with has_many :through
  #
  has_many :material_gem_sizes, :class_name => 'Material::GemSize', through: :material_gem_clarity_sizes

  has_many :design_diamonds, :class_name => 'Design::Diamond'
  has_associated_audits

  belongs_to :material_material, :class_name => 'Material::Material'
  validates_presence_of :material_material
  validate :validate_material_id
  audited associated_with: :material_material

  default_scope {order(material_material_id: :asc, position: :asc)}
  scope :by_material, ->(material_material_id) {where(:material_material_id => material_material_id)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_material_id
    errors.add(:material_material_id, "is invalid") unless Material::Material.exists?(self.material_material_id)
  end
end
