# == Schema Information
#
# Table name: material_gem_clarity_sizes
#
#  id                      :integer          not null, primary key
#  material_gem_clarity_id :integer
#  material_gem_size_id    :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class Material::GemClaritySize < ApplicationRecord
  belongs_to :material_gem_clarity, :class_name => 'Material::GemClarity', inverse_of: :material_gem_clarity_sizes
  belongs_to :material_gem_size, :class_name => 'Material::GemSize', inverse_of: :material_gem_clarity_sizes
end
