# == Schema Information
#
# Table name: material_gem_sizes
#
#  id                    :integer          not null, primary key
#  material_material_id  :integer          not null
#  material_gem_shape_id :integer          not null
#  position              :integer
#  slug                  :string           not null
#  mm_size               :string
#  carat_weight          :decimal(10, 3)   not null
#  price                 :decimal(10, 2)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class Material::GemSize < ApplicationRecord
  acts_as_list scope: [:material_material_id, :material_gem_shape_id]
  validates :slug, presence: true, uniqueness: { scope: [:material_material_id, :material_gem_shape_id] }
  # , format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :mm_size, presence: true, length: {minimum: 2}, uniqueness: { scope: [:material_material_id, :material_gem_shape_id] }
  validates :material_material_id, presence: true, numericality: { only_integer: true }
  validates :material_gem_shape_id, presence: true, numericality: { only_integer: true }
  cattr_accessor(:paginates_per) {10}

  has_many :material_gem_clarity_sizes, :class_name => 'Material::GemClaritySize', foreign_key: :material_gem_size_id, dependent: :destroy, inverse_of: :material_gem_size
  has_many :material_gem_clarities, :class_name => 'Material::GemClarity', through: :material_gem_clarity_sizes
  accepts_nested_attributes_for :material_gem_clarity_sizes,
                                reject_if: :all_blank,
                                allow_destroy: true
  accepts_nested_attributes_for :material_gem_clarities
  belongs_to :material_material, :class_name => 'Material::Material'
  validates_presence_of :material_material
  validate :validate_material_id
  belongs_to :material_gem_shape, :class_name => 'Material::GemShape'
  validates_presence_of :material_gem_shape
  validate :validate_gem_shape_id

  audited associated_with: :material_material
  audited associated_with: :material_gem_shape
  # audited associated_with: :material_gem_clarity

  default_scope {order(material_material_id: :asc, material_gem_shape_id: :asc, position: :asc)}
  # Active Record Query Interface
  # 14 Scopes
  # scope :by_filter, ->(material_material_id, material_gem_shape_id, material_gem_clarity_id) { where(:material_material_id => material_material_id, :material_gem_shape_id => material_gem_shape_id, :material_gem_clarities =>  material_gem_clarity_id) }
  scope :by_filter, ->(material_material_id, material_gem_shape_id) {where(:material_material_id => material_material_id, :material_gem_shape_id => material_gem_shape_id)}
  # Thanks : https://stackoverflow.com/questions/35119720/creating-a-list-of-users-using-scope-based-on-join-table-in-ror
  scope :gem_clarity, ->(a) {joins(:material_gem_clarity_sizes).where(material_gem_clarity_sizes: {material_gem_clarity_id: a})}
  scope :id, ->(a) {where(id: a)}
  # User.joins(:roles).where(roles: { name: "Admin" })

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_material_id
    errors.add(:material_material_id, "is invalid") unless Material::Material.exists?(self.material_material_id)
  end

  def validate_gem_shape_id
    errors.add(:material_gem_shape_id, "is invalid") unless Material::GemShape.exists?(self.material_gem_shape_id)
  end
end
