# == Schema Information
#
# Table name: material_lockers
#
#  id              :integer          not null, primary key
#  user_company_id :integer          not null
#  position        :integer
#  slug            :string           not null
#  locker          :string           not null
#  isdefault       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Material::Locker < ApplicationRecord

  acts_as_list scope: [:user_company_id]
  audited associated_with: :user_company

  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :locker, presence: true, length: {minimum: 2}, uniqueness: true
  validates :user_company_id, presence: true, numericality: { only_integer: true }
  cattr_accessor(:paginates_per) {10}

  belongs_to :user_company, :class_name => 'User::Company'
  validates_presence_of :user_company
  validate :validate_user_company_id

  default_scope {order(user_company_id: :asc, position: :asc)}
  scope :by_company, ->(id) { where(:user_company_id => id) }

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_user_company_id
    errors.add(:user_company_id, "is invalid") unless User::Company.exists?(self.user_company_id)
  end
end
