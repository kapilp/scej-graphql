# == Schema Information
#
# Table name: material_materials
#
#  id                        :integer          not null, primary key
#  material_material_type_id :integer          not null
#  position                  :integer
#  slug                      :string           not null
#  material_name             :string           not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class Material::Material < ApplicationRecord

  acts_as_list scope: [:material_material_type_id]

  validates :slug, presence: true, uniqueness: true,
            format: { with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers' }
  validates :material_name, presence: true, length: { minimum: 2 }, uniqueness: true
  validates :material_material_type_id, presence: true, numericality: { only_integer: true }
  cattr_accessor(:paginates_per) {10}

  has_many :material_metal_purities, :class_name => 'Material::MetalPurity'
  has_many :material_gem_shapes, :class_name => 'Material::GemShape'
  has_many :material_gem_clarities, :class_name => 'Material::GemClarity'
  has_many :material_gem_sizes, :class_name => 'Material::GemSize'
  has_many :material_colors, :class_name => 'Material::Color'
  has_many :material_lockers, :class_name => 'Material::Locker'

  has_many :design_diamonds, :class_name => 'Design::Diamond'
  has_associated_audits

  has_many :sale_m_stocks, :class_name => 'Sale::MStock', foreign_key: 'material_material_id'

  belongs_to :material_material_type, :class_name => 'Material::MaterialType'
  validates_presence_of :material_material_type
  validate :validate_material_type_id
  audited associated_with: :material_material_type

  scope :by_material_type_slug, ->(query) {joins(:material_material_type).where(material_material_types: {slug: query})}
  scope :sorted, -> {order('position ASC')}
  scope :newest_first, -> {order('created_at DESC')}
  # scope :search, lambda{|query| where(["name LIKE ?", "%#{query}%"])}

  default_scope {order(material_material_type_id: :asc, position: :asc)}
  scope :by_material_type, ->(id) {where(material_material_type_id: id)}


  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_material_type_id
    errors.add(:material_material_type_id, "is invalid") unless Material::MaterialType.exists?(self.material_material_type_id)
  end
end
