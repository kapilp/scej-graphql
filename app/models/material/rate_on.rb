# == Schema Information
#
# Table name: material_rate_ons
#
#  id                        :integer          not null, primary key
#  material_material_type_id :integer
#  position                  :integer
#  slug                      :string
#  rateon                    :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class Material::RateOn < ApplicationRecord

  validates :slug, presence: true, uniqueness: true,
            format: { with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers' }
  validates :rateon, presence: true, length: { minimum: 2 }, uniqueness: true
  validates :material_material_type_id, presence: true, numericality: { only_integer: true }
  cattr_accessor(:paginates_per) {10}

  belongs_to :material_material_type, :class_name => 'Material::MaterialType'
  validates_presence_of :material_material_type
  validate :validate_material_type_id

  acts_as_list scope: :material_material_type_id
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :rateon, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  audited associated_with: :material_material_type
  default_scope {order(material_material_type_id: :asc, position: :asc)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_material_type_id
    errors.add(:material_material_type_id, "is invalid") unless Material::MaterialType.exists?(self.material_material_type_id)
  end
end
