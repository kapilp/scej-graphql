# == Schema Information
#
# Table name: mfg_castings
#
#  id          :integer          not null, primary key
#  position    :integer
#  date        :datetime         not null
#  slug        :string           not null
#  employee_id :integer
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Mfg::Casting < ApplicationRecord
  # acts_as_list

  belongs_to :employee,  :class_name => 'User::Account',:foreign_key => 'employee_id'
  validates_presence_of :employee

  has_many :sale_m_txn_details, :class_name => 'Sale::MTxnDetail', as: :countable, inverse_of: :countable
  # has_many :purchase_transactions, -> { where(transaction_type: 'purchase'), class_name: 'Transaction'
  # to do that you have to define a new field 'countable_r_type' in your transaction model
  has_many :sale_m_txn_details_receives, -> { where countable_r_type: "Mfg::Casting"},class_name: 'Sale::MTxnDetail',  as: :countable_r, inverse_of: :countable_r, dependent: :destroy

  has_many :mfg_mfg_txns, :class_name => 'Mfg::MfgTxn',dependent: :destroy, foreign_key: 'mfg_casting_id'

  accepts_nested_attributes_for :sale_m_txn_details,
                                reject_if: :all_blank,
                                allow_destroy: true
  accepts_nested_attributes_for :sale_m_txn_details_receives,
                                reject_if: :all_blank,
                                allow_destroy: true

  accepts_nested_attributes_for :mfg_mfg_txns,
                                reject_if: :all_blank,
                                allow_destroy: true



  # validates_presence_of :name

  # default_scope { order(position: :asc) }



  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end
  
  # Statesman----------
  # example on github page, use autosave: false.
  has_many :mfg_casting_transitions, :class_name => 'Mfg::CastingTransition' , autosave: false, :foreign_key => 'mfg_casting_id'

  def state_machine
    @state_machine ||= Mfg::CastingStateMachine.new(self, transition_class: Mfg::CastingTransition)
  end
  #-------------------
end
