# == Schema Information
#
# Table name: mfg_casting_transitions
#
#  id             :integer          not null, primary key
#  to_state       :string           not null
#  metadata       :json
#  sort_key       :integer          not null
#  mfg_casting_id :integer          not null
#  most_recent    :boolean          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Mfg::CastingTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition # Instead of using serialize to store the metadata in JSON format, Using PostgreSQL JSON column

  belongs_to :mfg_casting, class_name: 'Mfg::Casting', inverse_of: :mfg_casting_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = mfg_casting.casting_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
