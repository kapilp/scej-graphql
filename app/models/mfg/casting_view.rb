# == Schema Information
#
# Table name: mfg_casting_views
#
#  id          :integer          primary key
#  position    :integer
#  date        :datetime
#  slug        :string
#  employee_id :integer
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Mfg::CastingView < ApplicationRecord
  # acts_as_list

  belongs_to :employee,  :class_name => 'User::Account',:foreign_key => 'employee_id'
  validates_presence_of :employee

  has_many :sale_m_txn_details, :class_name => 'Sale::MTxnDetail', as: :countable, inverse_of: :countable
  has_many :sale_m_txn_details_receive, -> { where countable_r_type: "Mfg::Casting"},class_name: 'Sale::MTxnDetail',  as: :countable_r, inverse_of: :countable_r, dependent: :destroy

  has_many :mfg_mfg_txns, :class_name => 'Mfg::MfgTxn',dependent: :destroy, foreign_key: 'mfg_casting_id'

  # validates_presence_of :name

  # default_scope { order(position: :asc) }

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end
  
  # Statesman----------
  # example on github page, use autosave: false.
  has_many :mfg_casting_transitions, :class_name => 'Mfg::CastingTransition' , autosave: false, :foreign_key => 'mfg_casting_id'

  def state_machine
    @state_machine ||= Mfg::CastingStateMachine.new(self, transition_class: Mfg::CastingTransition)
  end
  #-------------------
  
  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
  
end
