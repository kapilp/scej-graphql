# == Schema Information
#
# Table name: mfg_dust_resources
#
#  id            :integer          not null, primary key
#  position      :integer
#  slug          :string           not null
#  dust_resource :string           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Mfg::DustResource < ApplicationRecord

  acts_as_list
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :dust_resource, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  default_scope {order(position: :asc)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end
end
