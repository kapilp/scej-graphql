class Mfg::IssueFinishStateMachine
  include Statesman::Machine

  state :Issuing, initial: :true
  state :Finish

  transition from: :Issuing, to: [:Finish]


  # 1.Guards should return either true or false. If the latter is returned, transition will not succeed.
#  guard_transition(from: :Casting, to: :Finish) do |account|
#    account.valid_credit_card?
#  end

  # 2.define callbacks that will be executed either before or after the transition.
  # The syntax is very similar to guards:
#  before_transition(from: :Casting, to: :Finish) do |account, account_transition|
#    account.generate_verification_code!
#  end

#  after_transition(to: :Finish) do |account, account_transition|
#    AccountMailer.verification_code(account).deliver
#  end
end