# == Schema Information
#
# Table name: mfg_metal_issue_transitions
#
#  id                 :integer          not null, primary key
#  to_state           :string           not null
#  metadata           :json
#  sort_key           :integer          not null
#  mfg_metal_issue_id :integer          not null
#  most_recent        :boolean          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Mfg::MetalIssueTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition # Instead of using serialize to store the metadata in JSON format, Using PostgreSQL JSON column

  belongs_to :mfg_metal_issue, class_name: 'Mfg::MetalIssue', inverse_of: :mfg_metal_issue_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = mfg_metal_issue.metal_issue_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
