# == Schema Information
#
# Table name: mfg_mfg_txns
#
#  id                 :integer          not null, primary key
#  position           :integer
#  mfg_date           :datetime         not null
#  mfg_casting_id     :integer
#  mfg_wax_setting_id :integer
#  mfg_metal_issue_id :integer
#  user_company_id    :integer          not null
#  sale_job_id        :integer          not null
#  mfg_department_id  :integer          not null
#  employee_id        :integer          not null
#  description        :text
#  receive_weight     :decimal(10, 3)   default(0.0), not null
#  dust_weight        :decimal(10, 3)   default(0.0)
#  received           :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Mfg::MfgTxn < ApplicationRecord
  before_validation :default_values, on: :create

  # acts_as_list scope: [:sale_job_id]

  # has_closure_tree

  belongs_to :mfg_casting, :class_name => 'Mfg::Casting', optional: true
  belongs_to :mfg_wax_setting, :class_name => 'Mfg::WaxSetting', optional: true
  belongs_to :mfg_metal_issue, :class_name => 'Mfg::MetalIssue', optional: true
  # validates_presence_of :mfg_casting
  belongs_to :sale_job, :class_name => 'Sale::Job'
  # validates_presence_of :sale_job
  belongs_to :user_company, :class_name => 'User::Company'
  validates_presence_of :user_company
  belongs_to :mfg_department, :class_name => 'Mfg::Department', optional: true
  # validates_presence_of :mfg_department

  # belongs_to :user_account, :class_name => 'User::Account'
  # validates_presence_of :user_account
  belongs_to :employee,  :class_name => 'User::Account',:foreign_key => 'employee_id'
  validates_presence_of :employee

  has_many :sale_m_txn_details, :class_name => 'Sale::MTxnDetail', as: :countable, inverse_of: :countable, dependent: :destroy


  accepts_nested_attributes_for :sale_m_txn_details,
                                reject_if: :all_blank,
                                allow_destroy: true


  def default_values
    # self.mfg_date ||= 'P' # note self.status = 'P' if self.status.nil? might be safer (per @frontendbeauty)
    self.mfg_date = mfg_date.presence || Time.now
    self.mfg_department_id = mfg_department_id.presence || 1
  end

  # default_scope { order(sale_job_id: :asc, position: :asc) }

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :mfg_mfg_txn_transitions, :class_name => 'Mfg::MfgTxnTransition', autosave: false, :foreign_key => 'mfg_mfg_txn_id'

  def state_machine
    @state_machine ||= Mfg::MfgTxnStateMachine.new(self, transition_class: Mfg::MfgTxnTransition)
  end

  # -------------------------------Statesman End-------------------------------

end
