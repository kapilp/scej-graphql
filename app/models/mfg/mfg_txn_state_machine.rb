class Mfg::MfgTxnStateMachine
  include Statesman::Machine
# Part1A
  state :'Issued', initial: :true
  state :'Received'
  state :'Received Unfinished'

  transition from: :'Issued', to: [:'Received', :'Received Unfinished']
  transition from: :'Received Unfinished', to: [:'Issued']
# Part1A
end