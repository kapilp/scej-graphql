# == Schema Information
#
# Table name: mfg_mfg_txn_transitions
#
#  id             :integer          not null, primary key
#  to_state       :string           not null
#  metadata       :json
#  sort_key       :integer          not null
#  mfg_mfg_txn_id :integer          not null
#  most_recent    :boolean          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Mfg::MfgTxnTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition

  belongs_to :mfg_mfg_txn, class_name: 'Mfg::MfgTxn', inverse_of: :mfg_mfg_txn_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = mfg_mfg_txn.mfg_txn_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
