# == Schema Information
#
# Table name: mfg_mfg_txn_views
#
#  id                 :integer          primary key
#  position           :integer
#  mfg_date           :datetime
#  mfg_casting_id     :integer
#  mfg_wax_setting_id :integer
#  mfg_metal_issue_id :integer
#  user_company_id    :integer
#  sale_job_id        :integer
#  mfg_department_id  :integer
#  employee_id        :integer
#  description        :text
#  issue_weight       :decimal(, )
#  diamond_pcs        :integer
#  cs_pcs             :integer
#  diamond_weight     :decimal(, )
#  cs_weight          :decimal(, )
#  d_cs_weight        :decimal(, )
#  total_d_cs_weight  :decimal(, )
#  receive_weight     :decimal(10, 3)
#  dust_weight        :decimal(10, 3)
#  received           :boolean
#  purity             :decimal(10, 3)
#  net_weight         :decimal(, )
#  pure_weight        :decimal(, )
#  created_at         :datetime
#  updated_at         :datetime
#

class Mfg::MfgTxnView < ApplicationRecord
  # acts_as_list scope: [:sale_job_id]

  # has_closure_tree

  belongs_to :mfg_casting, :class_name => 'Mfg::Casting', optional: true
  belongs_to :mfg_wax_setting, :class_name => 'Mfg::WaxSetting', optional: true
  belongs_to :mfg_metal_issue, :class_name => 'Mfg::MetalIssue', optional: true
  # validates_presence_of :mfg_casting
  belongs_to :sale_job, :class_name => 'Sale::Job'
  # validates_presence_of :sale_job
  belongs_to :user_company, :class_name => 'User::Company'
  validates_presence_of :user_company
  belongs_to :mfg_department, :class_name => 'Mfg::Department', optional: true
  # validates_presence_of :mfg_department

  # belongs_to :user_account, :class_name => 'User::Account'
  # validates_presence_of :user_account
  belongs_to :employee,  :class_name => 'User::Account',:foreign_key => 'employee_id'
  validates_presence_of :employee

  has_many :sale_m_txn_details, :class_name => 'Sale::MTxnDetail', as: :countable, inverse_of: :countable, dependent: :destroy


  # default_scope { order(sale_job_id: :asc, position: :asc) }
  default_scope { order(id: :asc) }

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end


  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :mfg_mfg_txn_transitions, :class_name => 'Mfg::MfgTxnTransition', autosave: false, :foreign_key => 'mfg_mfg_txn_id'

  def state_machine
    @state_machine ||= Mfg::MfgTxnStateMachine.new(self, transition_class: Mfg::MfgTxnTransition)
  end

  def self.transition_class
    Mfg::MfgTxnTransition
  end

  def self.initial_state
    :pending
  end

  private_class_method :initial_state

  # -------------------------------Statesman End-------------------------------

  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end

end
