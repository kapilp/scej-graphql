# == Schema Information
#
# Table name: mfg_product_types
#
#  id           :integer          not null, primary key
#  position     :integer
#  slug         :string           not null
#  product_type :string           not null
#  isdefault    :boolean          default(FALSE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Mfg::ProductType < ApplicationRecord

  acts_as_list
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :product_type, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  has_many :sale_orders, :class_name => 'Sale::Order'

  default_scope {order(position: :asc)}

end
