# == Schema Information
#
# Table name: mfg_refinings
#
#  id                :integer          not null, primary key
#  position          :integer
#  user_account_id   :integer          not null
#  date_from         :datetime         not null
#  date_to           :datetime         not null
#  user_company_id   :integer          not null
#  mfg_department_id :integer          not null
#  dust_weight       :decimal(10, 3)   default(0.0), not null
#  description       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Mfg::Refining < ApplicationRecord
  # acts_as_list

  belongs_to :user_account, :class_name => 'User::Account'
  validates_presence_of :user_account
  belongs_to :user_company, :class_name => 'User::Company'
  validates_presence_of :user_company
  belongs_to :mfg_department, :class_name => 'Mfg::Department'
  validates_presence_of :mfg_department

  has_many :sale_m_txn_details_receive, -> { where countable_r_type: "Mfg::Refining"},class_name: 'Sale::MTxnDetail',  as: :countable_r, inverse_of: :countable_r, dependent: :destroy


  accepts_nested_attributes_for :sale_m_txn_details_receive,
                                reject_if: :all_blank,
                                allow_destroy: true

  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :mfg_refining_transitions, :class_name => 'Mfg::RefiningTransition', autosave: false, :foreign_key => 'mfg_refining_id'

  def state_machine
    @state_machine ||= Mfg::RefiningStateMachine.new(self, transition_class: Mfg::RefiningTransition)
  end

  def self.transition_class
    Mfg::RefiningTransition
  end

  def self.initial_state
    :pending
  end

  private_class_method :initial_state

  # -------------------------------Statesman End-------------------------------

end
