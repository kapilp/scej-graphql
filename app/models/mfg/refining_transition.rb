# == Schema Information
#
# Table name: mfg_refining_transitions
#
#  id              :integer          not null, primary key
#  to_state        :string           not null
#  metadata        :json
#  sort_key        :integer          not null
#  mfg_refining_id :integer          not null
#  most_recent     :boolean          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Mfg::RefiningTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition

  belongs_to :mfg_refining, class_name: 'Mfg::Refining', inverse_of: :mfg_refining_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = mfg_refining.refining_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
