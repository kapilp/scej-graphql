class Sale::JobStateMachine
  include Statesman::Machine
  include Statesman::Events

  # Sale
  state :Begin, initial: :true

  state :Unverified
  state :Verified
  state :Suspended
  state :Deleted

  # MFG
  state :Center
  state :Casting
  state :'Metal Issue'
  state :'Diamond Issue'
  state :Filling
  state :'Pre Polish'
  state :Setting
  state :'Final Polish'
  state :QC
  state :Finished

  state :Memo
  state :Sale

  event :Start do
    transition from: :Begin,  to: :Center
  end

  event :Finished do
    transition from: :QC, to: :Finished
  end

  transition from: :Begin, to: [:Center]
  transition from: :Center, to: [:Unverified, :Verified, :Suspended, :Deleted, :Casting, :'Metal Issue', :'Diamond Issue', :Filling, :'Pre Polish', :Setting, :'Final Polish', :QC]
  transition from: :Begin, to: [:Unverified, :Deleted]
  transition from: :Unverified, to: [:Verified, :Deleted]
  transition from: :Verified, to: [:Suspended, :Deleted]
  transition from: :Suspended, to: [:Unverified, :Deleted]
  transition from: :'Metal Issue', to: [:Center]
  transition from: :Casting, to: [:Center]
  transition from: :'Diamond Issue', to: [:Center]
  transition from: :Filling, to: [:Center]
  transition from: :'Pre Polish', to: [:Center]
  transition from: :Setting, to: [:Center]
  transition from: :'Final Polish', to: [:Center]
  transition from: :QC, to: [:Center]

  transition from: :QC, to: [:Finished]

  transition from: :Finished, to: [:Sale]
  transition from: :Finished, to: [:Memo]

  transition from: :Suspended, to: [:Unverified, :Deleted]

  # 1.Guards should return either true or false. If the latter is returned, transition will not succeed.
  guard_transition(from: :Unverified, to: :Verified) do |account|
    account.valid_credit_card?
  end

  # 2.define callbacks that will be executed either before or after the transition.
  # The syntax is very similar to guards:
  before_transition(from: :Unverified, to: :Verified) do |account, account_transition|
    account.generate_verification_code!
  end

  after_transition(to: :Unverified) do |account, account_transition|
    AccountMailer.verification_code(account).deliver
  end
end