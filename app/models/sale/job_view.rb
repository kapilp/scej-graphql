# == Schema Information
#
# Table name: sale_job_views
#
#  id01                 :integer
#  position             :text
#  position_order       :integer
#  customer_id          :integer
#  taken_by_id          :integer
#  order_date           :datetime
#  delivery_date        :datetime
#  mfg_product_type_id  :integer
#  description          :text
#  created_at           :datetime
#  updated_at           :datetime
#  id                   :integer          primary key
#  processable_type     :string
#  processable_id       :integer
#  final                :boolean
#  position_job         :integer
#  design_style_id      :integer
#  user_company_id      :integer
#  material_material_id :integer
#  metal_purity_id      :integer
#  metal_color_id       :integer
#  qty                  :integer
#  diamond_clarity_id   :integer
#  diamond_color_id     :integer
#  cs_clarity_id        :integer
#  cs_color_id          :integer
#  instruction          :string
#  item_size            :string
#  mfg_priority_id      :integer
#  mfg_department_id    :integer
#  created_at01         :datetime
#  updated_at01         :datetime
#  gross_weight         :decimal(10, 3)
#  net_weight           :decimal(, )
#  pure_weight          :decimal(, )
#

class Sale::JobView < ApplicationRecord
  include Statesman::Adapters::ActiveRecordQueries
  delegate :current_state, :trigger, :available_events, to: :state_machine

  acts_as_list scope: [:final, :processable_id]

  # Set One Optional True Otherwise validation fails.
  belongs_to :processable, polymorphic: true, optional: true
  belongs_to :salable, polymorphic: true, optional: true


  belongs_to :design_style, :class_name => 'Design::Style'
  belongs_to :mfg_product_type, :class_name => 'Mfg::ProductType', optional: true
  belongs_to :user_company, :class_name => 'User::Company'
  belongs_to :material_material, :class_name => 'Material::Material'
  belongs_to :metal_purity, :class_name => 'Material::MetalPurity'
  belongs_to :metal_color, :class_name => 'Material::Color', :foreign_key => 'metal_color_id'
  belongs_to :diamond_clarity, :class_name => 'Material::GemClarity', :foreign_key => 'diamond_clarity_id', optional: true
  belongs_to :diamond_color, :class_name => 'Material::Color', :foreign_key => 'diamond_color_id', optional: true
  belongs_to :cs_clarity, :class_name => 'Material::GemClarity', :foreign_key => 'cs_clarity_id', optional: true
  belongs_to :cs_color, :class_name => 'Material::Color', :foreign_key => 'cs_color_id', optional: true
  belongs_to :mfg_priority, :class_name => 'Mfg::Priority'
  belongs_to :mfg_department, :class_name => 'Mfg::Department', optional: true

  has_many :design_job_diamonds, :class_name => 'Design::JobDiamond', foreign_key: :sale_job_id, inverse_of: :sale_job
  has_many :design_diamonds, :class_name => 'Design::Diamond', through: :design_job_diamonds


  # TODO
  # default_scope { order(material_material_id: :asc, position: :asc) }
  scope :by_company, ->(id) { where(:user_company_id => id) }

  # StateMan
  has_many :sale_job_transitions, :class_name => 'Sale::JobTransition', :foreign_key => 'sale_job_id'

  def state_machine
    @state_machine ||= Sale::JobStateMachine.new(self, transition_class: Sale::JobTransition)
  end

  private

  def self.transition_class
    Sale::JobTransition
  end

  def self.initial_state
    :new
  end

  #  after_create do
  #    state_machine.transition_to! "center"
  #  end

  default_scope {order(final: :asc, position: :asc)}
  scope :id, ->(a) {where(id: a)}
  scope :in_state_scope, ->(a) {in_state(a)}
  scope :finish, ->(a) {in_state("finish")}
  scope :sale, ->(a) {in_state("sale")}


# ADDED RELATIONSHIPS:=================
  belongs_to :customer, :class_name => 'User::Account',:foreign_key => 'customer_id'
  belongs_to :taken_by, :class_name => 'User::Account',:foreign_key => 'taken_by_id'







  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end


end
