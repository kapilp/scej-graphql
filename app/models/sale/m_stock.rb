# == Schema Information
#
# Table name: sale_m_stocks
#
#  id                       :integer
#  countable_type           :string
#  countable_id             :integer
#  countable_r_type         :string
#  countable_r_id           :integer
#  position                 :integer
#  material_material_id     :integer
#  material_metal_purity_id :integer
#  material_gem_shape_id    :integer
#  material_gem_clarity_id  :integer
#  material_color_id        :integer
#  material_gem_size_id     :integer
#  user_company_id          :integer
#  material_locker_id       :integer
#  material_accessory_id    :integer
#  isissue                  :boolean
#  pcs                      :integer
#  calc_pcs                 :integer
#  weight1                  :decimal(10, 3)
#  calc_weight              :decimal(, )
#  note                     :string
#  material_gem_type_id     :integer
#  rate_on_pc               :boolean
#  rate1                    :decimal(10, 3)
#  created_at               :datetime
#  updated_at               :datetime
#

class Sale::MStock < ApplicationRecord
  # belongs_to :user_account
  # belongs_to :sale_order
  # belongs_to :sale_job
  belongs_to :material_material, :class_name => 'Material::Material'
  belongs_to :material_metal_purity, :class_name => 'Material::MetalPurity', optional: true
  belongs_to :material_gem_shape, :class_name => 'Material::GemShape', optional: true
  belongs_to :material_gem_clarity, :class_name => 'Material::GemClarity', optional: true
  belongs_to :material_color, :class_name => 'Material::Color'
  belongs_to :material_gem_size, :class_name => 'Material::GemSize', optional: true
  belongs_to :user_company, :class_name => 'User::Company'
  belongs_to :material_locker, :class_name => 'Material::Locker'
  belongs_to :material_accessory, :class_name => 'Material::Accessory'
  belongs_to :material_gem_type, :class_name => 'Material::GemType', optional: true

  scope :by_company, ->(a) { where(:user_company_id => a) }
  scope :by_locker, ->(a) { where(:material_locker_id => a) }
  scope :by_accessory, ->(a) { where(:material_accessory_id => a) }

  scope :by_material, ->(a) { where(:material_material_id => a) }
  scope :by_metal_purity, ->(a) { where(:material_metal_purity_id => a) }
  scope :by_color, ->(a) { where(:material_color_id => a) }

  scope :by_gem_shape, ->(a) { where(:material_gem_shape_id => a) }
  scope :by_gem_clarity, ->(a) { where(:material_gem_clarity_id => a) }
  scope :by_gem_size, ->(a) { where(:material_gem_size_id => a) }
  scope :id, ->(a) {where(id: a)}

  def readonly?
    true
  end

end
