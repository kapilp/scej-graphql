# == Schema Information
#
# Table name: sale_m_txns
#
#  id                 :integer          not null, primary key
#  position           :integer
#  sale_m_txn_type_id :integer          not null
#  user_account_id    :integer          not null
#  taken_by_id        :integer          not null
#  date               :datetime         not null
#  due_date           :datetime         not null
#  description        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Sale::MTxn < ApplicationRecord
  acts_as_list

  belongs_to :sale_m_txn_type, :class_name => 'Sale::MTxnType'
  validates_presence_of :sale_m_txn_type
  belongs_to :user_account, :class_name => 'User::Account'
  validates_presence_of :user_account
  belongs_to :taken_by, :class_name => 'User::Account',:foreign_key => 'taken_by_id'
  validates_presence_of :taken_by


  # M_TXN_DETAIL
  has_many :sale_m_txn_details, :class_name => 'Sale::MTxnDetail', as: :countable, inverse_of: :countable, dependent: :destroy


  accepts_nested_attributes_for :sale_m_txn_details,
                                reject_if: :all_blank,
                                allow_destroy: true

  default_scope { order(position: :asc) }


  # StateMan
  has_many :sale_m_txn_transitions, :class_name => 'Sale::MTxnTransition', :foreign_key => 'sale_m_txn_id'

  def state_machine
    @state_machine ||= Sale::MTxnStateMachine.new(self, transition_class: Sale::MTxnTransition)
  end

  private

  def self.transition_class
    Sale::MTxnTransition
  end

  def self.initial_state
    :pending
  end





end
