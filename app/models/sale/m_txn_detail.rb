# == Schema Information
#
# Table name: sale_m_txn_details
#
#  id                       :integer          not null, primary key
#  countable_type           :string
#  countable_id             :integer
#  countable_r_type         :string
#  countable_r_id           :integer
#  position                 :integer
#  material_material_id     :integer
#  material_metal_purity_id :integer
#  material_gem_shape_id    :integer
#  material_gem_clarity_id  :integer
#  material_color_id        :integer
#  material_gem_size_id     :integer
#  user_company_id          :integer
#  material_locker_id       :integer
#  material_accessory_id    :integer
#  customer_stock           :boolean          default(FALSE), not null
#  isissue                  :boolean          default(TRUE), not null
#  pcs                      :integer          default(0)
#  weight1                  :decimal(10, 3)   default(0.0)
#  note                     :string
#  material_gem_type_id     :integer
#  rate1                    :decimal(10, 3)   default(0.0)
#  rate_on_pc               :boolean          default(FALSE), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class Sale::MTxnDetail < ApplicationRecord
  acts_as_list
  belongs_to :countable, polymorphic: true, optional: true
  belongs_to :countable_r, polymorphic: true, optional: true
  # belongs_to :user_account, :class_name => 'User::Account'
  # belongs_to :sale_order, :class_name => 'Sale::Order'
  # belongs_to :sale_job, :class_name => 'Sale::Job'
  belongs_to :material_material, :class_name => 'Material::Material'
  belongs_to :material_metal_purity, :class_name => 'Material::MetalPurity', optional: true
  belongs_to :material_gem_shape, :class_name => 'Material::GemShape', optional: true
  belongs_to :material_gem_clarity, :class_name => 'Material::GemClarity', optional: true
  belongs_to :material_color, :class_name => 'Material::Color'
  belongs_to :material_gem_size, :class_name => 'Material::GemSize', optional: true
  belongs_to :user_company, :class_name => 'User::Company'
  belongs_to :material_locker, :class_name => 'Material::Locker'
  belongs_to :material_accessory, :class_name => 'Material::Accessory', optional: true
  belongs_to :material_gem_type, :class_name => 'Material::GemType', optional: true

  # default_scope { order(position: :asc) }
end
