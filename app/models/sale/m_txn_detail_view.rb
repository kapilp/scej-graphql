# == Schema Information
#
# Table name: sale_m_txn_detail_views
#
#  id                       :integer          primary key
#  account                  :integer
#  calc_pcs                 :integer
#  calc_weight              :decimal(, )
#  countable_type           :string
#  countable_id             :integer
#  countable_r_type         :string
#  countable_r_id           :integer
#  position                 :integer
#  material_material_id     :integer
#  material_metal_purity_id :integer
#  material_gem_shape_id    :integer
#  material_gem_clarity_id  :integer
#  material_color_id        :integer
#  material_gem_size_id     :integer
#  user_company_id          :integer
#  material_locker_id       :integer
#  material_accessory_id    :integer
#  isissue                  :boolean
#  customer_stock           :boolean
#  pcs                      :integer
#  weight1                  :decimal(10, 3)
#  note                     :string
#  material_gem_type_id     :integer
#  rate_on_pc               :boolean
#  created_at               :datetime
#  updated_at               :datetime
#  id01                     :integer
#  position01               :integer
#  sale_m_txn_type_id       :integer
#  user_account_id          :integer
#  taken_by_id              :integer
#  date                     :datetime
#  due_date                 :datetime
#  description              :text
#  created_at01             :datetime
#  updated_at01             :datetime
#  id02                     :integer
#  position02               :integer
#  date01                   :datetime
#  slug                     :string
#  casting_employee_id      :integer
#  created_at02             :datetime
#  updated_at02             :datetime
#  id03                     :integer
#  position03               :integer
#  mfg_date                 :datetime
#  mfg_casting_id           :integer
#  sale_job_id              :integer
#  user_company_id01        :integer
#  mfg_department_id        :integer
#  mfg_employee_id          :integer
#  receive_weight           :decimal(10, 3)
#  received                 :boolean
#  dust_weight              :decimal(10, 3)
#  created_at03             :datetime
#  updated_at03             :datetime
#  processable_type         :string
#  jobno                    :integer
#  sale_order_id            :integer
#  customer_id              :integer
#

class Sale::MTxnDetailView < ApplicationRecord
  belongs_to :user_account, :class_name => 'User::Account'
  # belongs_to :sale_order
  # belongs_to :sale_job
  belongs_to :material_material, :class_name => 'Material::Material'
  belongs_to :material_metal_purity, :class_name => 'Material::MetalPurity', optional: true
  belongs_to :material_gem_shape, :class_name => 'Material::GemShape', optional: true
  belongs_to :material_gem_clarity, :class_name => 'Material::GemClarity', optional: true
  belongs_to :material_color, :class_name => 'Material::Color'
  belongs_to :material_gem_size, :class_name => 'Material::GemSize', optional: true
  belongs_to :user_company, :class_name => 'User::Company'
  belongs_to :material_locker, :class_name => 'Material::Locker'
  belongs_to :material_accessory, :class_name => 'Material::Accessory'
  belongs_to :material_gem_type, :class_name => 'Material::GemType', optional: true

  scope :by_company, ->(a) { where(:user_company_id => a) }
  scope :by_locker, ->(a) { where(:material_locker_id => a) }
  scope :by_accessory, ->(a) { where(:material_accessory_id => a) }

  scope :by_material, ->(a) { where(:material_material_id => a) }
  scope :by_metal_purity, ->(a) { where(:material_metal_purity_id => a) }
  scope :by_color, ->(a) { where(:material_color_id => a) }

  scope :by_gem_shape, ->(a) { where(:material_gem_shape_id => a) }
  scope :by_gem_clarity, ->(a) { where(:material_gem_clarity_id => a) }
  scope :by_gem_size, ->(a) { where(:material_gem_size_id => a) }
  scope :id, ->(a) {where(id: a)}

  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end

end
