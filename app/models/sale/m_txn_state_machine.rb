class Sale::MTxnStateMachine
  include Statesman::Machine

  state :Pending, initial: true
  state :'Checking Out'
  state :Shipped
  state :Cancelled
  state :Received

  transition from: :Pending,      to: [:'Checking Out', :Cancelled]
  transition from: :'Checking Out', to: [:Shipped, :Cancelled]
  transition from: :Shipped,      to: :Received

  guard_transition(to: :'Checking Out') do |order|
    # order.products_in_stock?
  end

  before_transition(from: :'Checking Out', to: :Cancelled) do |order, transition|
    order.reallocate_stock
  end

end