# == Schema Information
#
# Table name: sale_m_txn_transitions
#
#  id            :integer          not null, primary key
#  to_state      :string           not null
#  metadata      :json
#  sort_key      :integer          not null
#  sale_m_txn_id :integer          not null
#  most_recent   :boolean          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Sale::MTxnTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition # Instead of using serialize to store the metadata in JSON format, Using PostgreSQL JSON column

  belongs_to :sale_m_txn, class_name: 'Sale::MTxn', inverse_of: :sale_m_txn_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = sale_m_txn.sale_m_txn_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
