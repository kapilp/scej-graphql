# == Schema Information
#
# Table name: sale_m_txn_types
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string           not null
#  txn_type   :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Sale::MTxnType < ApplicationRecord

  acts_as_list

  default_scope { order(position: :asc) }
end
