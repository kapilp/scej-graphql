# == Schema Information
#
# Table name: sale_m_txn_views
#
#  id                 :integer          primary key
#  position           :integer
#  sale_m_txn_type_id :integer
#  user_account_id    :integer
#  taken_by_id        :integer
#  date               :datetime
#  due_date           :datetime
#  description        :text
#  created_at         :datetime
#  updated_at         :datetime
#

class Sale::MTxnView < ApplicationRecord
  belongs_to :sale_m_txn_type, :class_name => 'Sale::MTxnType'
  belongs_to :user_account, :class_name => 'User::Account'
  belongs_to :taken_by, :class_name => 'User::Account',:foreign_key => 'taken_by_id'
  
  # M_TXN_DETAIL
  has_many :sale_m_txn_details, :class_name => 'Sale::MTxnDetail', as: :countable, inverse_of: :countable, dependent: :destroy

  default_scope { order(position: :asc) }

  # StateMan
  has_many :sale_m_txn_transitions, :class_name => 'Sale::MTxnTransition', :foreign_key => 'sale_m_txn_id'

  def state_machine
    @state_machine ||= Sale::MTxnStateMachine.new(self, transition_class: Sale::MTxnTransition)
  end

  private

  def self.transition_class
    Sale::MTxnTransition
  end

  def self.initial_state
    :pending
  end


  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
  
end
