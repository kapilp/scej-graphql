# == Schema Information
#
# Table name: sale_memos
#
#  id          :integer          not null, primary key
#  position    :integer
#  date        :datetime         not null
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Sale::Memo < ApplicationRecord

  has_many  :sale_jobs, :class_name => 'Sale::Job', as: :processable, inverse_of: :processable


  accepts_nested_attributes_for :sale_jobs,
                                reject_if: :all_blank,
                                allow_destroy: true


  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :sale_memo_transitions, :class_name => 'Sale::MemoTransition', autosave: false, :foreign_key => 'sale_memo_id'

  def state_machine
    @state_machine ||= Sale::MemoStateMachine.new(self, transition_class: Sale::MemoTransition)
  end

  def self.transition_class
    Sale::MemoTransition
  end

  def self.initial_state
    :pending
  end

  private_class_method :initial_state

  # -------------------------------Statesman End-------------------------------

end
