class Sale::MemoStateMachine
  include Statesman::Machine
# Part1A
  state :'Issued', initial: :true
  state :'Received'
  
  transition from: :'Issued', to: [:'Received']
# Part1A
end