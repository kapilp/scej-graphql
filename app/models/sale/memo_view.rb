# == Schema Information
#
# Table name: sale_memo_views
#
#  id          :integer          primary key
#  position    :integer
#  date        :datetime
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Sale::MemoView < ApplicationRecord
  has_many  :sale_jobs, :class_name => 'Sale::Job', as: :processable, inverse_of: :processable
  
  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :sale_memo_transitions, :class_name => 'Sale::MemoTransition', autosave: false, :foreign_key => 'sale_memo_id'

  def state_machine
    @state_machine ||= Sale::MemoStateMachine.new(self, transition_class: Sale::MemoTransition)
  end

  def self.transition_class
    Sale::MemoTransition
  end

  def self.initial_state
    :pending
  end

  private_class_method :initial_state

  # -------------------------------Statesman End-------------------------------
  
  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
  
end
