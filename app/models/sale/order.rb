# == Schema Information
#
# Table name: sale_orders
#
#  id                  :integer          not null, primary key
#  position            :integer
#  customer_id         :integer          not null
#  taken_by_id         :integer          not null
#  order_date          :datetime         not null
#  delivery_date       :datetime         not null
#  mfg_product_type_id :integer
#  description         :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Sale::Order < ApplicationRecord
  acts_as_list

  belongs_to :customer, :class_name => 'User::Account',:foreign_key => 'customer_id'
  validates_presence_of :customer
  belongs_to :taken_by, :class_name => 'User::Account',:foreign_key => 'taken_by_id'
  validates_presence_of :taken_by
  belongs_to :mfg_product_type, :class_name => 'Mfg::ProductType'
  validates_presence_of :mfg_product_type
  belongs_to :user_company, :class_name => 'User::Company'
  validates_presence_of :user_company

  has_many :sale_jobs, :class_name => 'Sale::Job', as: :processable, inverse_of: :processable, dependent: :destroy


  accepts_nested_attributes_for :sale_jobs,
                                reject_if: :all_blank,
                                allow_destroy: true
  default_scope { order(position: :asc) }


  # Statesman----------
  # example on github page, use autosave: false.
  has_many :sale_order_transitions, :class_name => 'Sale::OrderTransition', autosave: false, :foreign_key => 'sale_order_id'

  def state_machine
    @state_machine ||= Sale::OrderStateMachine.new(self, transition_class: Sale::OrderTransition)
  end

  def self.transition_class
    Sale::OrderTransition
  end

  def self.initial_state
    :pending
  end
  private_class_method :initial_state

  #-------------------

end
