class Sale::OrderStateMachine
  include Statesman::Machine

  state :Pending, initial: true
  state :'Checking Out'
  state :Purchased
  state :Shipped
  state :Cancelled
  state :Failed
  state :Refunded

  transition from: :Pending,      to: [:'Checking Out', :Cancelled]
  transition from: :'Checking Out', to: [:Purchased, :Cancelled]
  transition from: :Purchased,    to: [:Shipped, :Failed]
  transition from: :Shipped,      to: :Refunded

  guard_transition(to: :'Checking Out') do |order|
    # order.products_in_stock?
  end

  before_transition(from: :'Checking Out', to: :Cancelled) do |order, transition|
    order.reallocate_stock
  end

  before_transition(to: :Purchased) do |order, transition|
    PaymentService.new(order).submit
  end

  after_transition(to: :Purchased) do |order, transition|
    MailerService.order_confirmation(order).deliver
  end
end