# == Schema Information
#
# Table name: sale_order_transitions
#
#  id            :integer          not null, primary key
#  to_state      :string           not null
#  metadata      :json
#  sort_key      :integer          not null
#  sale_order_id :integer          not null
#  most_recent   :boolean          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Sale::OrderTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition # Instead of using serialize to store the metadata in JSON format, Using PostgreSQL JSON column

  belongs_to :sale_order, class_name: 'Sale::Order', inverse_of: :sale_order_transitions, :foreign_key => 'sale_order_id'

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = order.order_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
