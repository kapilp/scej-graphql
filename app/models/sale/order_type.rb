# == Schema Information
#
# Table name: sale_order_types
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string           not null
#  order_type :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Sale::OrderType < ApplicationRecord
  acts_as_list
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :order_type, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  default_scope {order(position: :asc)}
end
