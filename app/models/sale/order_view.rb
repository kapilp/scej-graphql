# == Schema Information
#
# Table name: sale_order_views
#
#  id                  :integer          primary key
#  position            :integer
#  customer_id         :integer
#  taken_by_id         :integer
#  order_date          :datetime
#  delivery_date       :datetime
#  mfg_product_type_id :integer
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#  num_jobs            :integer
#  processable_id      :integer
#  total_qty           :integer
#

class Sale::OrderView < ApplicationRecord
  belongs_to :customer, :class_name => 'User::Account',:foreign_key => 'customer_id'
  belongs_to :taken_by, :class_name => 'User::Account',:foreign_key => 'taken_by_id'
  belongs_to :sale_order_type, :class_name => 'Sale::OrderType'
  belongs_to :mfg_product_type, :class_name => 'Mfg::ProductType'

  has_many  :sale_jobs, :class_name => 'Sale::Job', as: :processable, inverse_of: :processable, dependent: :destroy


  accepts_nested_attributes_for :sale_jobs,
                                reject_if: :all_blank,
                                allow_destroy: true
  default_scope { order(position: :asc) }
  
  

  # Statesman----------
  # example on github page, use autosave: false.
  has_many :sale_order_transitions, :class_name => 'Sale::OrderTransition', autosave: false, :foreign_key => 'sale_order_id'

  def state_machine
    @state_machine ||= Sale::OrderStateMachine.new(self, transition_class: Sale::OrderTransition)
  end

  def self.transition_class
    Sale::OrderTransition
  end

  def self.initial_state
    :pending
  end
  private_class_method :initial_state

  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
  
end
