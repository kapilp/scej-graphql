# == Schema Information
#
# Table name: sale_sale_details
#
#  id            :integer          not null, primary key
#  saleable_type :string
#  saleable_id   :integer
#  position      :integer
#  tounch        :decimal(10, 3)   default(0.0)
#  m_rate_on_id  :integer
#  m_rate        :decimal(10, 3)   default(0.0)
#  d_rate_on_id  :integer
#  d_rate        :decimal(10, 3)   default(0.0)
#  cs_rate_on_id :integer
#  cs_rate       :decimal(10, 3)   default(0.0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Sale::SaleDetail < ApplicationRecord
  acts_as_list
  # belongs_to :saleable, polymorphic: true
  belongs_to :m_rate_on, :class_name => 'Material::RateOn', :foreign_key => 'm_rate_on_id'
  validates_presence_of :m_rate_on
  belongs_to :d_rate_on, :class_name => 'Material::RateOn', :foreign_key => 'd_rate_on_id'
  validates_presence_of :d_rate_on
  belongs_to :cs_rate_on, :class_name => 'Material::RateOn', :foreign_key => 'cs_rate_on_id'
  validates_presence_of :cs_rate_on

  default_scope { order(position: :asc) }
end
