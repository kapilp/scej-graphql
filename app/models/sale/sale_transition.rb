# == Schema Information
#
# Table name: sale_sale_transitions
#
#  id           :integer          not null, primary key
#  to_state     :string           not null
#  metadata     :json
#  sort_key     :integer          not null
#  sale_sale_id :integer          not null
#  most_recent  :boolean          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Sale::SaleTransition < ApplicationRecord
  # include Statesman::Adapters::ActiveRecordTransition

  belongs_to :sale_sale, class_name: 'Sale::Sale', inverse_of: :sale_sale_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = sale_sale.sale_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
