# == Schema Information
#
# Table name: sale_sale_views
#
#  id          :integer          primary key
#  position    :integer
#  customer_id :integer
#  sold_by_id  :integer
#  date        :datetime
#  due_date    :datetime
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Sale::SaleView < ApplicationRecord
  acts_as_list

  belongs_to :customer, :class_name => 'User::Account',:foreign_key => 'customer_id'
  validates_presence_of :customer
  belongs_to :sold_by, :class_name => 'User::Account',:foreign_key => 'sold_by_id'
  validates_presence_of :sold_by


  # TODO Where condition not needed.
  has_many  :sale_jobs,  class_name: 'Sale::Job', as: :salable, inverse_of: :salable, dependent: :nullify
  # has_many  :sale_jobs, -> { where salable_type: "Sale::Sale"}, class_name: 'Sale::Job', as: :salable, inverse_of: :salable, dependent: :nullify
  # when you set inverse of set on Sale::Sale
  # has_many :admin_scholarships, :class_name => 'Admin::Scholarship',inverse_of: :group, foreign_key: 'group_id'

  # on delete, you could have a method called
  # that gets the product and updates the foreign key attribute to be nil
  # add a callback, probably before_destroy
  
  # ---------------------------------Statesman---------------------------------
  # example on github page, use autosave: false.
  # you can add association_name (It is useful when, say, you have model wrapped in namespace.)
  has_many :sale_sale_transitions, :class_name => 'Sale::SaleTransition', autosave: false, :foreign_key => 'sale_sale_id'

  def state_machine
    @state_machine ||= Sale::SaleStateMachine.new(self, transition_class: Sale::SaleTransition)
  end

  def self.transition_class
    Sale::SaleTransition
  end

  def self.initial_state
    :pending
  end

  private_class_method :initial_state

  # -------------------------------Statesman End-------------------------------

  
  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
  
end
