# == Schema Information
#
# Table name: user_accounts
#
#  id                     :integer          not null, primary key
#  user_account_type_id   :integer          not null
#  position               :integer
#  join_date              :datetime
#  slug                   :string           not null
#  account_name           :string           not null
#  salary                 :integer          default(0)
#  user_role_id           :integer          not null
#  parent_id              :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

class User::Account < ApplicationRecord
  #--------------------------------Structures--------------------------------
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  acts_as_list
  has_closure_tree
  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable,
         :timeoutable,
         :token_authenticatable
  has_many :authentication_tokens, :class_name => 'User::AuthenticationToken', foreign_key: :user_account_id

  cattr_accessor(:paginates_per) {10}

  belongs_to :user_account_type, :class_name => 'User::AccountType'
  validates_presence_of :user_account_type
  validate :validate_account_type_id
  belongs_to :user_role, :class_name => 'User::Role'
  validates_presence_of :user_role
  validate :validate_role_id

  has_many :user_addresses, :class_name => 'User::Address', foreign_key: :user_account_id, inverse_of: :user_account

  has_many :user_company_ownerships, :class_name => 'User::CompanyOwnership', foreign_key: :user_account_id, inverse_of: :user_account
  has_many :user_companies, :class_name => 'User::Company', through: :user_company_ownerships

  has_many :user_contacts, :class_name => 'User::Contact', foreign_key: :user_account_id, inverse_of: :user_account

  scope :by_account_type, ->(user_account_type_id) {where(:user_account_type_id => user_account_type_id)}
  scope :by_type, ->(query) {joins(:user_account_type).where(user_account_types: {slug: query})}
  default_scope {order(user_account_type_id: :asc, position: :asc)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  # Statesman----------
  has_many :user_account_transitions, :class_name => 'User::AccountTransition', :foreign_key => 'user_account_id'

  def state_machine
    @state_machine ||= User::AccountStateMachine.new(self, transition_class: User::AccountTransition)
  end

  def validate_account_type_id
    errors.add(:user_account_type_id, "is invalid") unless User::AccountType.exists?(self.user_account_type_id)
  end

  def validate_role_id
    errors.add(:user_role_id, "is invalid") unless User::Role.exists?(self.user_role_id)
  end

  #-------------------

  def admin?
    user_role.role == "admin"
  end

  def regular?
    user_role.role == "regular"
  end

  def guest?
    user_role.role == "guest"
  end

  # ----------
  # after_create :send_admin_mail
  def send_admin_mail
    User::AccountMailer.send_signup_email(self).deliver
  end

  #--------------------------------behavior--------------------------------
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :account_name, presence: true, length: {minimum: 2}, uniqueness: true
  validates :user_account_type_id, presence: true, numericality: { only_integer: true }
  validates :user_role_id, presence: true, numericality: { only_integer: true }
  validates :email, uniqueness: true, format: /@/

  accepts_nested_attributes_for :user_addresses,
                              # reject_if: :all_blank,
                              allow_destroy: true
  accepts_nested_attributes_for :user_contacts,
                              # reject_if: :all_blank,
                              allow_destroy: true

  # ===============Tokens====================
  # include Trackable # gives update_tracked_fields(request)
  # has_secure_password

  # def generate_access_token!
  #   self.access_token = Digest::SHA1.hexdigest("#{Time.now}-#{self.id}-#{SecureRandom.hex}")
  #   self.save!
  #   self.access_token
  # end

  # def update_with_password(password: nil, password_confirmation: nil, current_password: nil)
  #   if self.authenticate(current_password)
  #     self.errors.add(:base, "Password can't be blank") && (return false) if password.blank?
  #     self.update(password: password, password_confirmation: password_confirmation)
  #   else
  #     self.errors.add(:current_password, "Invalid password")
  #     return false
  #   end
  # end

end
