class User::AccountStateMachine
  include Statesman::Machine

  state :New, initial: :true
  state :Unverified
  state :Verified
  state :Suspended
  state :Deleted

  transition from: :New, to: [:Unverified, :Deleted]
  transition from: :Unverified, to: [:Verified, :Deleted]
  transition from: :Verified, to: [:Suspended, :Deleted]
  transition from: :Suspended, to: [:Unverified, :Deleted]
end