# == Schema Information
#
# Table name: user_account_types
#
#  id           :integer          not null, primary key
#  position     :integer
#  slug         :string
#  account_type :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class User::AccountType < ApplicationRecord
  acts_as_list
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :account_type, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  default_scope {order(position: :asc)}
  has_many :user_accounts, :class_name => 'User::Account'
end
