# == Schema Information
#
# Table name: user_account_views
#
#  id                     :integer          primary key
#  user_account_type_id   :integer
#  position               :integer
#  join_date              :datetime
#  slug                   :string
#  account_name           :string
#  email                  :string
#  salary                 :integer
#  user_role_id           :integer
#  created_at             :datetime
#  updated_at             :datetime
#  encrypted_password     :string
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

class User::AccountView < ApplicationRecord
  #--------------------------------Structures--------------------------------
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  acts_as_list
  has_closure_tree
  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable,
         :timeoutable,
         :token_authenticatable
  has_many :authentication_tokens, :class_name => 'User::AuthenticationToken', foreign_key: :user_account_id

  cattr_accessor(:paginates_per) {10}

  belongs_to :user_account_type, :class_name => 'User::AccountType'
  belongs_to :user_role, :class_name => 'User::Role'
  has_many :user_addresses, :class_name => 'User::Address', foreign_key: :user_account_id, inverse_of: :user_account

  # ------------------
  has_many :user_company_ownerships, :class_name => 'User::CompanyOwnership', foreign_key: :user_account_id, inverse_of: :user_account
  has_many :user_companies, :class_name => 'User::Company', through: :user_company_ownerships

  has_many :user_contacts, :class_name => 'User::Contact', foreign_key: :user_account_id, inverse_of: :user_account

  scope :by_account_type, ->(user_account_type_id) {where(:user_account_type_id => user_account_type_id)}
  scope :by_type, ->(query) {joins(:user_account_type).where(user_account_types: {slug: query})}
  default_scope {order(user_account_type_id: :asc, position: :asc)}
  # --------------

  # Statesman----------
  has_many :user_account_transitions, :class_name => 'User::AccountTransition', :foreign_key => 'user_account_id'

  def state_machine
    @state_machine ||= User::AccountStateMachine.new(self, transition_class: User::AccountTransition)
  end

  #-------------------

  def admin?
    user_role.role == "admin"
  end

  def regular?
    user_role.role == "regular"
  end

  def guest?
    user_role.role == "guest"
  end

  # ----------
  # after_create :send_admin_mail
  def send_admin_mail
    User::AccountMailer.send_signup_email(self).deliver
  end

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_account_type_id
    errors.add(:user_account_type_id, "is invalid") unless User::AccountType.exists?(self.user_account_type_id)
  end

  def validate_role_id
    errors.add(:user_role_id, "is invalid") unless User::Role.exists?(self.user_role_id)
  end

  # Must add in each view:
  # you probably have to declare the PK in the Rails model
  # Postgres won’t care, but Rails will use that for all its magic
  self.primary_key = 'id'
  def readonly?
    true
  end
end
