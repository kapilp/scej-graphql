# == Schema Information
#
# Table name: user_addresses
#
#  id                :integer          not null, primary key
#  user_account_id   :integer
#  user_company_id   :integer
#  position          :integer
#  firstname         :string
#  lastname          :string
#  address1          :string
#  address2          :string
#  city              :string
#  zipcode           :string
#  user_state_id     :integer
#  user_country_id   :integer
#  phone             :string
#  alternative_phone :string
#  company           :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class User::Address < ApplicationRecord
  acts_as_list

  validates :firstname, presence: true, length: { minimum: 3 }
  validates :lastname, presence: true, length: { minimum: 3 }
  validates :user_state_id, presence: true, numericality: { only_integer: true }
  validates :user_country_id, presence: true, numericality: { only_integer: true }
  # If you are using a belongs_to on the join model, it is a good idea to set the :inverse_of option
  belongs_to :user_account, :class_name => 'User::Account', inverse_of: :user_addresses, optional: true
  belongs_to :user_company, :class_name => 'User::Account', inverse_of: :user_addresses, optional: true
  # This Validations are casing problems when saving account.....
  # validates_presence_of :user_account
  # validate :validate_account_id
  belongs_to :user_state, :class_name => 'User::State'
  validates_presence_of :user_state
  validate :validate_state_id
  belongs_to :user_country, :class_name => 'User::Country'
  validates_presence_of :user_country
  validate :validate_country_id

  default_scope { order(user_account_id: :asc, position: :asc) }

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_account_id
    errors.add(:user_account_id, "is invalid") unless User::Account.exists?(self.user_account_id)
  end

  def validate_state_id
    errors.add(:user_state_id, "is invalid") unless User::State.exists?(self.user_state_id)
  end

  def validate_country_id
    errors.add(:user_country_id, "is invalid") unless User::Country.exists?(self.user_country_id)
  end
end
