# == Schema Information
#
# Table name: user_authentication_tokens
#
#  id              :integer          not null, primary key
#  body            :string
#  user_account_id :integer
#  last_used_at    :datetime
#  expires_in      :integer
#  ip_address      :string
#  user_agent      :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User::AuthenticationToken < ApplicationRecord
  belongs_to :user_account, :class_name => 'User::Account'
end
