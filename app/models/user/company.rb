# == Schema Information
#
# Table name: user_companies
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string
#  company    :string
#  gst_no     :string
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User::Company < ApplicationRecord

  acts_as_list
  has_closure_tree
  audited

  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :company, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  has_many :user_company_ownerships, :class_name => 'User::CompanyOwnership', foreign_key: :user_company_id, inverse_of: :user_company
  has_many :user_accounts, :class_name => 'User::Account', through: :user_company_ownerships

  has_many :user_addresses, :class_name => 'User::Address', foreign_key: :user_company_id, inverse_of: :user_company, dependent: :destroy

  has_many :user_contacts, :class_name => 'User::Contact', foreign_key: :user_company_id, inverse_of: :user_company, dependent: :destroy

  has_many :sale_orders, :class_name => 'Sale::Order', foreign_key: :user_company_id, inverse_of: :user_company

  accepts_nested_attributes_for :user_addresses,
                            # reject_if: :all_blank,
                            allow_destroy: true
  accepts_nested_attributes_for :user_contacts,
                            # reject_if: :all_blank,
                            allow_destroy: true

  default_scope {order(position: :asc)}

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

end
#:join_table => "schools_students",
