# == Schema Information
#
# Table name: user_company_ownerships
#
#  id              :integer          not null, primary key
#  user_company_id :integer
#  user_account_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User::CompanyOwnership < ApplicationRecord
  belongs_to :user_company, :class_name => 'User::Company', inverse_of: :user_company_ownerships
  belongs_to :user_account, :class_name => 'User::Account', inverse_of: :user_company_ownerships
end
# What is the difference between t.belongs_to and t.references in rails?
# https://stackoverflow.com/questions/7788188/what-is-the-difference-between-t-belongs-to-and-t-references-in-rails