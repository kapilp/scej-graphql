# == Schema Information
#
# Table name: user_contacts
#
#  id              :integer          not null, primary key
#  user_company_id :integer
#  user_account_id :integer
#  position        :integer
#  company_name    :string
#  first_name      :string
#  last_name       :string
#  email           :string
#  phone           :string
#  mobile          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User::Contact < ApplicationRecord

  acts_as_list scope: [:user_company_id, :user_account_id]
  validates :first_name, presence: true, length: {minimum: 2}
  cattr_accessor(:paginates_per) {10}

  belongs_to :user_company, :class_name => 'User::Company', inverse_of: :user_contacts, optional: true
  belongs_to :user_account, :class_name => 'User::Account', inverse_of: :user_contacts, optional: true

  #--------------------------------behavior--------------------------------
  validates :email, uniqueness: true, format: /@/

  default_scope {order(position: :asc)}
end
