# == Schema Information
#
# Table name: user_countries
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string
#  country    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User::Country < ApplicationRecord

  acts_as_list
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :country, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  has_many :user_states, :class_name => 'User::State'
  has_many :user_addresses, :class_name => 'User::Address'

  default_scope {order(position: :asc)}
end
