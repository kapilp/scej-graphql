# == Schema Information
#
# Table name: public.user_customers
#
#  id           :integer          not null, primary key
#  company_slug :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class User::Customer < ApplicationRecord
  validates :company_slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}

end
