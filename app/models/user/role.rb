# == Schema Information
#
# Table name: user_roles
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string
#  role       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User::Role < ApplicationRecord

  acts_as_list
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :role, presence: true, length: {minimum: 2}, uniqueness: true
  cattr_accessor(:paginates_per) {10}

  has_many :user_accounts, :class_name => 'User::Account'

  default_scope {order(position: :asc)}
end
