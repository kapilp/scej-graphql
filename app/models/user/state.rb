# == Schema Information
#
# Table name: user_states
#
#  id              :integer          not null, primary key
#  position        :integer
#  user_country_id :integer
#  slug            :string
#  state           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User::State < ApplicationRecord

  acts_as_list scope: [:user_country_id]
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :state, presence: true, length: {minimum: 2}, uniqueness: true
  validates :user_country_id, presence: true, numericality: { only_integer: true }
  cattr_accessor(:paginates_per) {10}

  belongs_to :user_country, :class_name => 'User::Country'
  validates_presence_of :user_country
  validate :validate_country_id
  has_many :user_addresses, :class_name => 'User::Address'

  default_scope {order(user_country_id: :asc, position: :asc)}
  scope :by_country, ->(id) { where(:user_country_id => id) }

  def self.paginate(offset)
    offset(offset).limit(self.paginates_per)
  end

  def self.search(keywords)
    where('lower(title) like :keywords OR lower(content) like :keywords', :keywords => "%#{keywords.downcase}%")
  end

  def validate_country_id
    errors.add(:user_country_id, "is invalid") unless User::Country.exists?(self.user_country_id)
  end
end
