# == Schema Information
#
# Table name: user_tax_rates
#
#  id               :integer          not null, primary key
#  user_tax_type_id :integer
#  position         :integer
#  slug             :string           not null
#  name             :string           not null
#  rate             :decimal(10, 2)   not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class User::TaxRate < ApplicationRecord
  acts_as_list
  belongs_to :user_tax_type, :class_name => 'User::TaxType'
  
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :user_tax_type_id, presence: true, numericality: { only_integer: true }
  validates :name, presence: true, length: {minimum: 2}, uniqueness: true
  validates :rate, presence: true
end
