# == Schema Information
#
# Table name: user_tax_types
#
#  id         :integer          not null, primary key
#  position   :integer
#  slug       :string           not null
#  tax_type   :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User::TaxType < ApplicationRecord
  acts_as_list

  has_many :user_tax_rates, :class_name => 'User::TaxRate'
  validates :slug, presence: true, uniqueness: true,
            format: {with: /\A[a-zA-Z0-9]+\z/, message: 'only allows letters and numbers'}
  validates :tax_type, presence: true, length: {minimum: 2}, uniqueness: true
end
