class Viewer < Struct.new :id
  # Below is our simple Struct class to create temporary data structure (root)
  # https://medium.com/@gauravtiwari/graphql-and-relay-on-rails-first-relay-powered-react-component-cb3f9ee95eca
  # HACK: // For relay root queries
  STATIC = new(id: 'root').freeze

  def self.find(_)
    STATIC
  end
end
