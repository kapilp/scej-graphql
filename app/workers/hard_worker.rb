class HardWorker
  include Sidekiq::Worker

  def perform(inputs)
    # 2. Create new tenant
    begin
      Apartment::Tenant.create(inputs['slug'])
    rescue ::Apartment::TenantExists => e
      # Rollbar.error(e, "ERROR: Apartment Tenant Already Exists: #{Apartment::Tenant.current.inspect}")
      return { messages:  [FieldError.new('slug', 'Already Exists')] }
    end

    # 2. switch to new tenant
    Apartment::Tenant.switch(inputs['slug']) do
      # 4. Create new Company in new tenant
      company = User::Company.create!(slug: inputs['slug'], company: inputs['company'])
      # 5. Create new Account Below Company in new tenant
      account = User::Account.create!(user_account_type_id: 1, slug: inputs['account_name'], join_date: DateTime.now ,account_name: inputs['account_name'], email: inputs['email'], password: inputs['password'], user_role_id: 1)
      company.user_accounts << account
      # 6. Generate Token For the user and return token:
      if account
        request = ActionDispatch::Request.new({})
        token = Tiddle.create_and_return_token(account, request, expires_in: 12.hours)
        { new_account: account, token: token }
      else
        { messages: account.fields_errors }
      end
    end
  end
end
