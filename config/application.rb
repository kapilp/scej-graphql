$LOAD_PATH << File.expand_path('../lib', __dir__)
require_relative 'boot'

require 'rails/all'
# require "rails/test_unit/railtie"
require 'graphql_reloader'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ScejGraphql
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # the sorting effect on speed of booting.
    config.autoload_paths << Rails.root.join('app/graphql')
    config.autoload_paths << Rails.root.join('app/graphql/fields')
    config.autoload_paths << Rails.root.join('app/graphql/types')
    config.autoload_paths << Rails.root.join('app/graphql/utils')
    config.autoload_paths << Rails.root.join('app/graphql/mutations')
    config.autoload_paths << Rails.root.join('app/graphql/interfaces')
    config.autoload_paths << Rails.root.join('app/graphql/mutations/design')
    config.autoload_paths << Rails.root.join('app/graphql/mutations/material')
    config.autoload_paths << Rails.root.join('app/graphql/mutations/mfg')
    config.autoload_paths << Rails.root.join('app/graphql/mutations/sale')
    config.autoload_paths << Rails.root.join('app/graphql/mutations/user')


    # Do not swallow errors in after_commit/after_rollback callbacks.
    # config.active_record.schema_format = :sql

    # Configure rails g to skip helper/assets files
    config.generators do |g|
      g.assets = false
      g.helper = false
      g.view_specs      false
      g.helper_specs    false
    end

    # Tiddle # disable Rails session
    config.middleware.delete ActionDispatch::Session::CookieStore

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        # origins 'localhost:3000'
        # resource '*', headers: :any, credentials: true, methods: [:get, :post, :put, :patch, :delete, :head, :options], expose: ['X-Requested-With', 'Content-Type', 'Accept']
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options]
      end
    end
    # https://github.com/rails/rails/tree/master/actioncable#allowed-request-origins
    Rails.application.config.action_cable.disable_request_forgery_protection = true

    # https://github.com/rails/rails/issues/22965
    config.action_controller.forgery_protection_origin_check = false


  end
end
