Rails.application.routes.draw do

  # devise settings:
  devise_for :accounts, class_name: "User::Account", controllers: { sessions: 'accounts/sessions' }
  # root to: "home#index"
  as :user do
    get   '/signup' => 'users/registrations#new',    as: 'new_user_registration'
    post  '/signup' => 'users/registrations#create', as: 'user_registration'
  end
  
  mount ActionCable.server => '/subscriptions'
  
  if Rails.env.production?
    mount Shrine.presign_endpoint(:cache) => "/presign"
  else
    # In development and test environment we're using filesystem storage
    # for speed, so on the client side we'll upload files to our app.
    mount Shrine.upload_endpoint(:cache) => "/upload"
  end


  scope '/graphql' do
    post '/', to: 'graphql#create'
  end
  
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '*all', to: 'application#index'
  root "application#index"
end
