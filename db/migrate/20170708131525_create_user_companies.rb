class CreateUserCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :user_companies do |t|

      t.integer :position
      t.string :slug
      t.string :company

      t.string :gst_no
      t.integer :parent_id, comment: 'self joining table'

      t.timestamps
    end
    add_index :user_companies, :position
    add_index :user_companies, :slug, unique: true
    add_index :user_companies, :company, unique: true
  end
end
