class CreateUserCompanyHierarchies < ActiveRecord::Migration[5.2]
  def change
    create_table :user_company_hierarchies, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :generations, null: false
    end

    add_index :user_company_hierarchies, [:ancestor_id, :descendant_id, :generations],
      unique: true,
      name: "company_anc_desc_idx"

    add_index :user_company_hierarchies, [:descendant_id],
      name: "company_desc_idx"
  end
end
