class CreateUserRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :user_roles do |t|
      t.integer :position
      t.string :slug
      t.string :role

      t.timestamps
    end
    add_index :user_roles, :position
    add_index :user_roles, :slug, unique: true
    add_index :user_roles, :role, unique: true
  end
end
