class CreateUserAccountTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :user_account_types do |t|
      t.integer :position
      t.string :slug
      t.string :account_type, null: false

      t.timestamps
    end
    add_index :user_account_types, :position
    add_index :user_account_types, :slug, unique: true
    add_index :user_account_types, :account_type, unique: true
  end
end
