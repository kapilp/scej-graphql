class CreateUserAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :user_accounts do |t|
      t.belongs_to :user_account_type, foreign_key: true, null: false
      t.integer :position
      t.datetime :join_date
      t.string :slug, null: false
      t.string :account_name, null: false
      # t.string :email, null: false # added by devise.
      t.integer :salary, :default => 0

      # No need this when join table: t.belongs_to :user_company, foreign_key: true, null: false
      t.belongs_to :user_role, foreign_key: true, null: false
      t.integer :parent_id, comment: 'self joining table'

      t.timestamps
    end
    add_index :user_accounts, :position
    add_index :user_accounts, :slug, unique: true
    add_index :user_accounts, :account_name, unique: true
    # add_index :user_accounts, :email, unique: true
  end
end
