class CreateUserAccountTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :user_account_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      t.references :user_account, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :user_account_transitions, :user_accounts

    add_index(:user_account_transitions,
              [:user_account_id, :sort_key],
              unique: true,
              name: "index_user_account_transitions_parent_sort")
    add_index(:user_account_transitions,
              [:user_account_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_user_account_transitions_parent_most_recent")
  end
end
