class CreateUserCompanyOwnerships < ActiveRecord::Migration[5.2]
  def change
    create_table :user_company_ownerships do |t|
      t.belongs_to :user_company, foreign_key: true
      t.belongs_to :user_account, foreign_key: true

      t.timestamps
    end
  end
end
