class CreateUserContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :user_contacts do |t|
      t.references :user_company, foreign_key: true
      t.references :user_account, foreign_key: true
      t.integer :position
      t.string :company_name
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :mobile

      t.timestamps
    end
  end
end
