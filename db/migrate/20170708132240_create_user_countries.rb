class CreateUserCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :user_countries do |t|
      t.integer :position
      t.string :slug
      t.string :country

      t.timestamps
    end
    add_index :user_countries, :position
    add_index :user_countries, :slug, unique: true
  end
end
