class CreateUserStates < ActiveRecord::Migration[5.2]
  def change
    create_table :user_states do |t|
      t.integer :position
      t.belongs_to :user_country, foreign_key: true
      t.string :slug
      t.string :state

      t.timestamps
    end
    add_index :user_states, :position
    add_index :user_states, :slug, unique: true
  end
end
