class CreateUserAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :user_addresses do |t|
      t.belongs_to :user_account, foreign_key: true
      t.belongs_to :user_company, foreign_key: true
      t.integer :position
      t.string :firstname
      t.string :lastname
      t.string :address1
      t.string :address2
      t.string :city
      t.string :zipcode
      t.references :user_state, foreign_key: true
      t.references :user_country, foreign_key: true
      t.string :phone
      t.string :alternative_phone
      t.string :company



      t.timestamps
    end
    add_index :user_addresses, :position
  end
end
