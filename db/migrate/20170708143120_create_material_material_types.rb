class CreateMaterialMaterialTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :material_material_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :material_type, null: false

      t.timestamps
    end
    add_index :material_material_types, :position
    add_index :material_material_types, :slug, unique: true
    add_index :material_material_types, :material_type, unique: true
  end
end
