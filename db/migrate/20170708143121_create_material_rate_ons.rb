class CreateMaterialRateOns < ActiveRecord::Migration[5.2]
  def change
    create_table :material_rate_ons do |t|
      t.belongs_to :material_material_type, foreign_key: true
      t.integer :position
      t.string :slug
      t.string :rateon

      t.timestamps
    end
    add_index :material_rate_ons, :position
    add_index :material_rate_ons, :slug, unique: true
    add_index :material_rate_ons, :rateon, unique: true
  end
end
