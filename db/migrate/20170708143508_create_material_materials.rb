class CreateMaterialMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :material_materials do |t|
      t.belongs_to :material_material_type, foreign_key: true, null: false
      t.integer :position
      t.string :slug, null: false
      t.string :material_name, null: false

      t.timestamps
    end
    add_index :material_materials, :position
    add_index :material_materials, :slug, unique: true
    add_index :material_materials, :material_name, unique: true
  end
end
