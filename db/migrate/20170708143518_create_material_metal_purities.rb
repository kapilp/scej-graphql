class CreateMaterialMetalPurities < ActiveRecord::Migration[5.2]
  def change
    create_table :material_metal_purities do |t|
      t.belongs_to :material_material, foreign_key: true, null: false
      t.integer :position
      t.string :slug, null: false
      t.string :name, null: false
      t.decimal :purity, null: false, precision: 10, scale: 3
      t.decimal :specific_density, null: false, precision: 10, scale: 3
      t.decimal :price, precision: 10, scale: 2
      t.text :description

      t.timestamps
    end
    add_index :material_metal_purities, :position
    add_index :material_metal_purities, :slug, unique: true
  end
end
