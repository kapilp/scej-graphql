class CreateMaterialGemShapes < ActiveRecord::Migration[5.2]
  def change
    create_table :material_gem_shapes do |t|
      t.belongs_to :material_material, foreign_key: true, null: false
      t.integer :position
      t.string :slug, null: false
      t.string :shape, null: false

      t.timestamps
    end
    add_index :material_gem_shapes, :position
    add_index :material_gem_shapes, :slug, unique: true
  end
end
