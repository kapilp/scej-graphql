class CreateMaterialGemClarities < ActiveRecord::Migration[5.2]
  def change
    create_table :material_gem_clarities do |t|
      t.belongs_to :material_material, foreign_key: true, null: false
      t.integer :position
      t.string :slug, null: false
      t.string :clarity, null: false

      t.timestamps
    end
    add_index :material_gem_clarities, :position
    add_index :material_gem_clarities, :slug, unique: true
  end
end
