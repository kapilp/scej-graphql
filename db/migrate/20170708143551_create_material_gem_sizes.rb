class CreateMaterialGemSizes < ActiveRecord::Migration[5.2]
  def change
    create_table :material_gem_sizes do |t|
      t.belongs_to :material_material, foreign_key: true, null: false

      t.belongs_to :material_gem_shape, foreign_key: true, null: false
      t.integer :position
      t.string :slug, null: false
      t.string :mm_size
      t.decimal :carat_weight, null: false, precision: 10, scale: 3
      t.decimal :price, precision: 10, scale: 2

      t.timestamps
    end
    add_index :material_gem_sizes, :position
  end
end
