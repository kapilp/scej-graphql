class CreateMaterialGemClaritySizes < ActiveRecord::Migration[5.2]
  def change
    create_table :material_gem_clarity_sizes do |t|
      t.belongs_to :material_gem_clarity, foreign_key: true
      t.belongs_to :material_gem_size, foreign_key: true

      t.timestamps
    end
  end
end
