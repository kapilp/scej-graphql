class CreateMaterialColors < ActiveRecord::Migration[5.2]
  def change
    create_table :material_colors do |t|
      t.belongs_to :material_material, foreign_key: true, null: false
      t.integer :position
      t.string :slug, null: false
      t.string :color, null: false

      t.timestamps
    end
    add_index :material_colors, :position
    add_index :material_colors, :slug, unique: true
  end
end
