class CreateMaterialLockers < ActiveRecord::Migration[5.2]
  def change
    create_table :material_lockers do |t|
      t.belongs_to :user_company, foreign_key: true, null: false
      t.integer :position
      t.string :slug, null: false
      t.string :locker, null: false
      t.boolean :isdefault, null: false, default:false

      t.timestamps
    end
    add_index :material_lockers, :position
    add_index :material_lockers, :slug, unique: true
    add_index :material_lockers, :locker, unique: true
  end
end
