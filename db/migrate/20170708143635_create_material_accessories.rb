class CreateMaterialAccessories < ActiveRecord::Migration[5.2]
  def change
    create_table :material_accessories do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :accessory, null: false

      t.timestamps
    end
    add_index :material_accessories, :position
    add_index :material_accessories, :slug, unique: true
    add_index :material_accessories, :accessory, unique: true
  end
end
