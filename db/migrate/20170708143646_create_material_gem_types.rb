class CreateMaterialGemTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :material_gem_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :gem_type, null: false
      t.boolean :isdefault, null: false, default:false

      t.timestamps
    end
    add_index :material_gem_types, :position
    add_index :material_gem_types, :slug, unique: true
    add_index :material_gem_types, :gem_type, unique: true
  end
end
