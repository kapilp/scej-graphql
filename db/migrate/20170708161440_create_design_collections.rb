class CreateDesignCollections < ActiveRecord::Migration[5.2]
  def change
    create_table :design_collections do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :collection, null: false

      t.integer :parent_id, comment: 'self joining table'

      t.timestamps
    end
    add_index :design_collections, :position
    add_index :design_collections, :slug, unique: true
    add_index :design_collections, :collection, unique: true
  end
end
