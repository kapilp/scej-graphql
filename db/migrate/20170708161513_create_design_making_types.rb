class CreateDesignMakingTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :design_making_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :making_type, null: false
      t.boolean :isdefault, null: false, default: false

      t.timestamps
    end
    add_index :design_making_types, :position
    add_index :design_making_types, :slug, unique: true
    add_index :design_making_types, :making_type, unique: true
  end
end
