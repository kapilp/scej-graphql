class CreateDesignSettingTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :design_setting_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :setting_type, null: false
      t.boolean :isdefault, null: false, default: false

      t.timestamps
    end
    add_index :design_setting_types, :position
    add_index :design_setting_types, :slug, unique: true
    add_index :design_setting_types, :setting_type, unique: true
  end
end
