class CreateDesignImages < ActiveRecord::Migration[5.2]
  def change
    create_table :design_images do |t|
      t.references :imageable, polymorphic: true
      # t.string :image
      t.text :image_data
      
      t.timestamps
    end
  end
end
