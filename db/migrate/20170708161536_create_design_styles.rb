class CreateDesignStyles < ActiveRecord::Migration[5.2]
  def change
    create_table :design_styles do |t|
      t.integer :position
      t.string :slug, null: false
      t.references :design_collection, foreign_key: true
      t.references :design_making_type, foreign_key: true
      t.references :design_setting_type, foreign_key: true
      t.references :user_account, foreign_key: true
      t.references :material_material, foreign_key: true
      t.references :material_metal_purity, foreign_key: true
      t.references :material_color, foreign_key: true
      t.decimal :net_weight, precision: 10, scale: 3, null: false
      t.text :description
      t.boolean :active, null: false, default:false

      t.timestamps
    end
    add_index :design_styles, :position
    add_index :design_styles, :slug, unique: true
  end
end

# add option 1 pair, 2 pair , 3 pair
