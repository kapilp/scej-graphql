class CreateDesignStyleTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :design_style_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      t.references :design_style, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :design_style_transitions, :design_styles

    add_index(:design_style_transitions,
              [:design_style_id, :sort_key],
              unique: true,
              name: "index_design_style_transitions_parent_sort")
    add_index(:design_style_transitions,
              [:design_style_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_design_style_transitions_parent_most_recent")
  end
end
