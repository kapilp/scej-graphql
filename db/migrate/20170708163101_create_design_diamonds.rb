class CreateDesignDiamonds < ActiveRecord::Migration[5.2]
  def change
    create_table :design_diamonds do |t|
      t.integer :position
      t.references :material_material, foreign_key: true, null: false
      t.references :material_gem_shape, foreign_key: true, null: false
      t.references :material_gem_clarity, foreign_key: true, null: false
      t.references :material_color, foreign_key: true
      t.references :material_gem_size, foreign_key: true, null: false
      t.integer :pcs, null: false, :default => 0

      t.timestamps
    end
    add_index :design_diamonds, :position
  end
end
