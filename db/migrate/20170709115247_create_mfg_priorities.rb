class CreateMfgPriorities < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_priorities do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :priority, null: false
      t.boolean :isdefault, null: false, default:false

      t.timestamps
    end
    add_index :mfg_priorities, :position
    add_index :mfg_priorities, :slug, unique: true
    add_index :mfg_priorities, :priority, unique: true
  end
end
