class CreateMfgProductTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_product_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :product_type, null: false
      t.boolean :isdefault, null: false, default:false

      t.timestamps
    end
    add_index :mfg_product_types, :position
    add_index :mfg_product_types, :slug, unique: true
    add_index :mfg_product_types, :product_type, unique: true
  end
end
