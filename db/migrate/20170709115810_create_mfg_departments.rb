class CreateMfgDepartments < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_departments do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :department, null: false
      
      t.integer :parent_id

      t.timestamps
    end
    add_index :mfg_departments, :position
    add_index :mfg_departments, :slug, unique: true
    add_index :mfg_departments, :department, unique: true
  end
end
