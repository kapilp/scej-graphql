class CreateMfgDustResources < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_dust_resources do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :dust_resource, null: false

      t.timestamps
    end
    add_index :mfg_dust_resources, :position
    add_index :mfg_dust_resources, :slug, unique: true
    add_index :mfg_dust_resources, :dust_resource, unique: true
  end
end
