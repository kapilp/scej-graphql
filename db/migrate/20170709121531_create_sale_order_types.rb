class CreateSaleOrderTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_order_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :order_type, null: false

      t.timestamps
    end
    add_index :sale_order_types, :position
    add_index :sale_order_types, :slug, unique: true
    add_index :sale_order_types, :order_type, unique: true
  end
end
