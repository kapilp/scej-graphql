class CreateSaleOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_orders do |t|
      t.integer :position
      t.references :user_company, foreign_key: true, null: false
      t.references :customer, index: true, foreign_key: { to_table: :user_accounts }, null: false
      t.references :taken_by, index: true, foreign_key: { to_table: :user_accounts }, null: false
      t.datetime :order_date, null: false
      t.datetime :delivery_date, null: false
      t.references :mfg_product_type, foreign_key: true
      t.text :description

      t.timestamps
    end
    add_index :sale_orders, :position
  end
end
