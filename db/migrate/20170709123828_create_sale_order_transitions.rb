class CreateSaleOrderTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_order_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      # t.integer :sale_order_id, null: false
      t.references :sale_order, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :sale_order_transitions, :sale_orders

    add_index(:sale_order_transitions,
              [:sale_order_id, :sort_key],
              unique: true,
              name: "index_sale_order_transitions_parent_sort")
    add_index(:sale_order_transitions,
              [:sale_order_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_sale_order_transitions_parent_most_recent")
  end
end
