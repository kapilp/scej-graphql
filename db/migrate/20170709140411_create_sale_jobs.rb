class CreateSaleJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_jobs do |t|
      t.references :processable, polymorphic: true
      t.references :salable, polymorphic: true
      t.boolean :final, null: false, default:false
      t.integer :position
      t.references :design_style, foreign_key: true, null: false
      # t.references :mfg_product_type, foreign_key: true  #We not need it.
      t.references :user_company, foreign_key: true, null: false
      t.references :material_material, foreign_key: true, null: false
      t.references :metal_purity, foreign_key: { to_table: :material_metal_purities }, null: false
      t.references :metal_color, foreign_key: { to_table: :material_colors }, null: false
      t.integer :qty, :default => 1
      t.references :diamond_clarity, foreign_key: { to_table: :material_gem_clarities }
      t.references :diamond_color, foreign_key: { to_table: :material_colors }
      t.references :cs_clarity, foreign_key: { to_table: :material_gem_clarities }
      t.references :cs_color, foreign_key: { to_table: :material_colors }
      t.string :instruction
      t.string :item_size
      t.references :mfg_priority, foreign_key: true, null: false
      t.references :mfg_department, foreign_key: true

      t.timestamps
    end
    add_index :sale_jobs, :position
  end
end
      # t.decimal :net_weight, precision: 10, scale: 3, :default => 0.0
      # t.decimal :pure_weight, precision: 10, scale: 3, :default => 0.0
      # t.integer :diamond_pcs, :default => 0
      # t.decimal :diamond_weight, precision: 10, scale: 3, :default => 0.0
      # t.integer :cs_pcs, :default => 0
      # t.decimal :cs_weight, precision: 10, scale: 3, :default => 0.0
      # t.decimal :gross_weight, precision: 10, scale: 3, :default => 0.0