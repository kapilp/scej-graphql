class CreateSaleJobTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_job_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      # t.integer :sale_job_id, null: false
      t.references :sale_job, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :sale_job_transitions, :sale_jobs

    add_index(:sale_job_transitions,
              [:sale_job_id, :sort_key],
              unique: true,
              name: "index_sale_job_transitions_parent_sort")
    add_index(:sale_job_transitions,
              [:sale_job_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_sale_job_transitions_parent_most_recent")
  end
end
