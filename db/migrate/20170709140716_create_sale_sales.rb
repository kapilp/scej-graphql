class CreateSaleSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_sales do |t|
      t.integer :position
      t.references :customer, index: true, foreign_key: { to_table: :user_accounts }, null: false
      t.references :sold_by, index: true, foreign_key: { to_table: :user_accounts }, null: false
      t.datetime :date, null: false
      t.datetime :due_date, null: false
      t.text :description

      t.timestamps
    end
    add_index :sale_sales, :position
  end
end
      # t.decimal :net_weight, precision: 10, scale: 3, :default => 0.0
      # t.decimal :pure_weight, precision: 10, scale: 3, :default => 0.0
      # t.integer :diamond_pcs, :default => 0
      # t.decimal :diamond_weight, precision: 10, scale: 3, :default => 0.0
      # t.integer :cs_pcs, :default => 0
      # t.decimal :cs_weight, precision: 10, scale: 3, :default => 0.0
      # t.decimal :gross_weight, precision: 10, scale: 3, :default => 0.0
      # t.decimal :tax, precision: 10, scale: 3, :default => 0.0
      # t.decimal :discount, precision: 10, scale: 3, :default => 0.0
      # t.decimal :amount, precision: 10, scale: 2, :default => 0.0
