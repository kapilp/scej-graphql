class CreateSaleMemos < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_memos do |t|
      t.integer :position
      t.datetime :date, null: false
      t.text :description

      t.timestamps
    end
    add_index :sale_memos, :position
  end
end
