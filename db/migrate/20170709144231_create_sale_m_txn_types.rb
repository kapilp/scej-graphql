class CreateSaleMTxnTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_m_txn_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :txn_type, null: false

      t.timestamps
    end
    add_index :sale_m_txn_types, :position
    add_index :sale_m_txn_types, :slug, unique: true
  end
end
