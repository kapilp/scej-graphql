class CreateSaleMTxns < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_m_txns do |t|
      t.integer :position
      t.references :sale_m_txn_type, foreign_key: true, null: false
      t.references :user_account, index: true, foreign_key: { to_table: :user_accounts }, null: false
      t.references :taken_by, index: true, foreign_key: { to_table: :user_accounts }, null: false
      t.datetime :date, null: false
      t.datetime :due_date, null: false

      t.text :description

      t.timestamps
    end
    add_index :sale_m_txns, :position
  end
end
      # t.decimal :total_weight, precision: 10, scale: 3, null: false
      # t.decimal :amount, precision: 10, scale: 2, null: false