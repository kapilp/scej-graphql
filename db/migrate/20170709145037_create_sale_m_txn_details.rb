class CreateSaleMTxnDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_m_txn_details do |t|
      t.references :countable, polymorphic: true
      t.references :countable_r, polymorphic: true
      t.integer :position
      # t.references :user_account, foreign_key: true
      # t.references :sale_order, foreign_key: true
      # t.references :sale_job, foreign_key: true
      t.references :material_material, foreign_key: true
      t.references :material_metal_purity, foreign_key: true
      t.references :material_gem_shape, foreign_key: true
      t.references :material_gem_clarity, foreign_key: true
      t.references :material_color, foreign_key: true
      t.references :material_gem_size, foreign_key: true
      t.references :user_company, foreign_key: true
      t.references :material_locker, foreign_key: true
      t.references :material_accessory, foreign_key: true
      t.boolean :customer_stock, null: false, default:false
      t.boolean :isissue, null: false, default:true
      t.integer :pcs, :default => 0
      t.decimal :weight1, precision: 10, scale: 3, :default => 0.0
      t.string :note
      t.references :material_gem_type, foreign_key: true
      t.decimal :rate1, precision: 10, scale: 3, :default => 0.0
      t.boolean :rate_on_pc, null: false, default:false
      # t.decimal :amount, precision: 10, scale: 2, :default => 0.0

      t.timestamps
    end
    add_index :sale_m_txn_details, :position
  end
end