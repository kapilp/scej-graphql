class CreateMfgCastings < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_castings do |t|
      t.integer :position
      t.datetime :date, null: false
      t.string :slug, null: false
      t.references :employee, index: true, foreign_key: { to_table: :user_accounts }
      t.text :description


      t.timestamps
    end
    add_index :mfg_castings, :position
  end
end
      # t.decimal :t_issue, precision: 10, scale: 3, :default => 0.0
      # t.decimal :t_issue_pure, precision: 10, scale: 3, :default => 0.0
      # t.decimal :t_rcv, precision: 10, scale: 3, :default => 0.0
      # t.decimal :t_rcv_pure, precision: 10, scale: 3, :default => 0.0
      # t.decimal :loss, precision: 10, scale: 3, :default => 0.0
      # t.decimal :loss_pure, precision: 10, scale: 3, :default => 0.0