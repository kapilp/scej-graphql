class CreateMfgCastingTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_casting_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      # t.integer :mfg_casting_id, null: false
      t.references :mfg_casting, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :mfg_casting_transitions, :mfg_castings

    add_index(:mfg_casting_transitions,
              [:mfg_casting_id, :sort_key],
              unique: true,
              name: "index_mfg_casting_transitions_parent_sort")
    add_index(:mfg_casting_transitions,
              [:mfg_casting_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_mfg_casting_transitions_parent_most_recent")
  end
end
