class CreateMfgWaxSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_wax_settings do |t|
      t.integer :position
      t.datetime :date, null: false
      t.string :slug, null: false
      t.references :employee, index: true, foreign_key: { to_table: :user_accounts }
      t.text :description


      t.timestamps
    end
    add_index :mfg_wax_settings, :position
  end
end