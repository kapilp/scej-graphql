class CreateMfgMfgTxns < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_mfg_txns do |t|
      t.integer :position
      t.datetime :mfg_date, null: false
      t.references :mfg_casting, foreign_key: true
      t.references :mfg_wax_setting, foreign_key: true
      t.references :mfg_metal_issue, foreign_key: true
      t.references :user_company, foreign_key: true, null: false
      t.references :sale_job, foreign_key: true, null: false
      t.references :mfg_department, foreign_key: true, null: false
      # t.string :department
      t.references :employee, index: true, foreign_key: { to_table: :user_accounts }, null: false
      t.text :description
      # t.decimal :issue_weight, null: false, precision: 10, scale: 3, :default => 0.0
      # t.boolean :is_customer_stock, null: false, default:false

      t.decimal :receive_weight, null: false, precision: 10, scale: 3, :default => 0.0


      t.decimal :dust_weight, precision: 10, scale: 3, :default => 0.0
      t.boolean :received, null: false, default:false

      t.timestamps
    end
    add_index :mfg_mfg_txns, :position
  end
end
