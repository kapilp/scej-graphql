class CreateMfgRefinings < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_refinings do |t|
      t.integer :position
      t.references :user_account, foreign_key: true, null: false
      t.datetime :date_from, null: false
      t.datetime :date_to, null: false
      t.references :user_company, foreign_key: true, null: false
      t.references :mfg_department, foreign_key: true, null: false
      t.decimal :dust_weight, null: false, precision: 10, scale: 3, :default => 0.0
      t.text :description

      t.timestamps
    end
    add_index :mfg_refinings, :position
  end
end
