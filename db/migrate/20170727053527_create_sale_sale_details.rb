class CreateSaleSaleDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_sale_details do |t|
      t.references :saleable, polymorphic: true
      t.integer :position
      t.decimal :tounch, precision: 10, scale: 3, :default => 0.0
      t.references :m_rate_on
      t.decimal :m_rate, precision: 10, scale: 3, :default => 0.0
      
      t.references :d_rate_on
      t.decimal :d_rate, precision: 10, scale: 3, :default => 0.0
      
      t.references :cs_rate_on
      t.decimal :cs_rate, precision: 10, scale: 3, :default => 0.0
      
      

      t.timestamps
    end
    add_index :sale_sale_details, :position
  end
end
      # t.decimal :m_amount, precision: 10, scale: 3, :default => 0.0
      # t.decimal :d_amount, precision: 10, scale: 3, :default => 0.0
      # t.decimal :cs_amount, precision: 10, scale: 3, :default => 0.0
      # t.decimal :total_amount, precision: 10, scale: 3, :default => 0.0