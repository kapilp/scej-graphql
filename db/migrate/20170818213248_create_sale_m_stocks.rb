class CreateSaleMStocks < ActiveRecord::Migration[5.2]
  def up
    self.connection.execute %Q( CREATE OR REPLACE VIEW sale_m_stocks AS
      SELECT
        sale_m_txn_details.id,
        sale_m_txn_details.countable_type,
        sale_m_txn_details.countable_id,
        sale_m_txn_details.countable_r_type,
        sale_m_txn_details.countable_r_id,
        sale_m_txn_details."position",
        sale_m_txn_details.material_material_id,
        sale_m_txn_details.material_metal_purity_id,
        sale_m_txn_details.material_gem_shape_id,
        sale_m_txn_details.material_gem_clarity_id,
        sale_m_txn_details.material_color_id,
        sale_m_txn_details.material_gem_size_id,
        sale_m_txn_details.user_company_id,
        sale_m_txn_details.material_locker_id,
        sale_m_txn_details.material_accessory_id,
        sale_m_txn_details.isissue,
        sale_m_txn_details.pcs,
        CASE
          WHEN (sale_m_txn_details.isissue IS TRUE) THEN
            (- sale_m_txn_details.pcs)
          ELSE sale_m_txn_details.pcs
        END AS calc_pcs,
        sale_m_txn_details.weight1,
        CASE
          WHEN (sale_m_txn_details.isissue IS TRUE) THEN
            (- sale_m_txn_details.weight1)
          ELSE sale_m_txn_details.weight1
        END AS calc_weight,

        sale_m_txn_details.note,
        sale_m_txn_details.material_gem_type_id,
        sale_m_txn_details.rate_on_pc,
        sale_m_txn_details.rate1,

        sale_m_txn_details.created_at,
        sale_m_txn_details.updated_at
      FROM sale_m_txn_details;
          )
  end

  def down
    self.connection.execute "DROP VIEW IF EXISTS sale_m_stocks;"
  end
end


#  Add pure_weight calculated column
        # sale_m_txn_details.amount,
        #  add amount column