class CreateDesignCollectionHierarchies < ActiveRecord::Migration[5.2]
  def change
    create_table :design_collection_hierarchies, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :generations, null: false
    end

    add_index :design_collection_hierarchies, [:ancestor_id, :descendant_id, :generations],
      unique: true,
      name: "collection_anc_desc_idx"

    add_index :design_collection_hierarchies, [:descendant_id],
      name: "collection_desc_idx"
  end
end
