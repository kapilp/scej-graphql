class CreateUserTaxTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :user_tax_types do |t|
      t.integer :position
      t.string :slug, null: false
      t.string :tax_type, null: false

      t.timestamps
    end
    add_index :user_tax_types, :position
    add_index :user_tax_types, :slug, unique: true
  end
end
