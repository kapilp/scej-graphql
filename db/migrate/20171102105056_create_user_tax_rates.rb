class CreateUserTaxRates < ActiveRecord::Migration[5.2]
  def change
    create_table :user_tax_rates do |t|
      t.belongs_to :user_tax_type, foreign_key: true
      t.integer :position
      t.string :slug, null: false
      t.string :name, null: false
      t.decimal :rate, null: false, precision: 10, scale: 2

      t.timestamps
    end
    add_index :user_tax_rates, :position
    add_index :user_tax_rates, :slug, unique: true
  end
end
