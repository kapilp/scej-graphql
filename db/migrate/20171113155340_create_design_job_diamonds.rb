class CreateDesignJobDiamonds < ActiveRecord::Migration[5.2]
  def change
    create_table :design_job_diamonds do |t|
      t.belongs_to :sale_job, foreign_key: true
      t.belongs_to :design_diamond, foreign_key: true

      t.timestamps
    end
  end
end
