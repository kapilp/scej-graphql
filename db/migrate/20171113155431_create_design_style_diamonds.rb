class CreateDesignStyleDiamonds < ActiveRecord::Migration[5.2]
  def change
    create_table :design_style_diamonds do |t|
      t.belongs_to :design_style, foreign_key: true
      t.belongs_to :design_diamond, foreign_key: true

      t.timestamps
    end
  end
end
