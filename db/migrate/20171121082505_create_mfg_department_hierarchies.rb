class CreateMfgDepartmentHierarchies < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_department_hierarchies, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :generations, null: false
    end

    add_index :mfg_department_hierarchies, [:ancestor_id, :descendant_id, :generations],
      unique: true,
      name: "department_anc_desc_idx"

    add_index :mfg_department_hierarchies, [:descendant_id],
      name: "department_desc_idx"
  end
end
