class CreateMfgMfgTxnTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_mfg_txn_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      # t.integer :mfg_mfg_txn_id, null: false
      t.references :mfg_mfg_txn, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :mfg_mfg_txn_transitions, :mfg_mfg_txns

    add_index(:mfg_mfg_txn_transitions,
              [:mfg_mfg_txn_id, :sort_key],
              unique: true,
              name: "index_mfg_mfg_txn_transitions_parent_sort")
    add_index(:mfg_mfg_txn_transitions,
              [:mfg_mfg_txn_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_mfg_mfg_txn_transitions_parent_most_recent")
  end
end
