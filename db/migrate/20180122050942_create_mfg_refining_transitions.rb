class CreateMfgRefiningTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :mfg_refining_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      # t.integer :mfg_refining_id, null: false
      t.references :mfg_refining, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :mfg_refining_transitions, :mfg_refinings

    add_index(:mfg_refining_transitions,
              [:mfg_refining_id, :sort_key],
              unique: true,
              name: "index_mfg_refining_transitions_parent_sort")
    add_index(:mfg_refining_transitions,
              [:mfg_refining_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_mfg_refining_transitions_parent_most_recent")
  end
end
