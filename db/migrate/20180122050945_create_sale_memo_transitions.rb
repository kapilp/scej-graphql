class CreateSaleMemoTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_memo_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      # t.integer :sale_memo_id, null: false
       t.references :sale_memo, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :sale_memo_transitions, :sale_memos

    add_index(:sale_memo_transitions,
              [:sale_memo_id, :sort_key],
              unique: true,
              name: "index_sale_memo_transitions_parent_sort")
    add_index(:sale_memo_transitions,
              [:sale_memo_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_sale_memo_transitions_parent_most_recent")
  end
end
