class CreateSaleSaleTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_sale_transitions do |t|
      t.string :to_state, null: false
      t.json :metadata, default: {}
      t.integer :sort_key, null: false
      # t.integer :sale_sale_id, null: false
      t.references :sale_sale, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    # Foreign keys are optional, but highly recommended
    add_foreign_key :sale_sale_transitions, :sale_sales

    add_index(:sale_sale_transitions,
              [:sale_sale_id, :sort_key],
              unique: true,
              name: "index_sale_sale_transitions_parent_sort")
    add_index(:sale_sale_transitions,
              [:sale_sale_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_sale_sale_transitions_parent_most_recent")
  end
end
