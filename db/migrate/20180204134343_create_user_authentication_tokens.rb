class CreateUserAuthenticationTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :user_authentication_tokens do |t|
      # body, last_used_at, ip_address and user_agent fields are required.
      t.string :body
      t.references :user_account, foreign_key: true
      t.datetime :last_used_at
      t.integer :expires_in
      t.string :ip_address
      t.string :user_agent

      t.timestamps
    end
    add_index :user_authentication_tokens, :body
  end
end
