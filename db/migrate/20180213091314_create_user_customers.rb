class CreateUserCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_customers do |t|
      t.string :company_slug, null: false
      t.timestamps
    end
  end
end
