class CreateDesignStyleViews < ActiveRecord::Migration[5.2]
  def change
    create_view :design_style_views
  end
end
