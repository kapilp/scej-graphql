class CreateUserSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :user_settings do |t|
      t.references :user_account, null: false #
      t.json :config, default: {}


      # t.datetime :created_at, default: -> { 'CLOCK_TIMESTAMP' }
      t.timestamps null: false
    end
    add_index :user_settings, :created_at
  end
end

# only select for max created_time.
# whenever you change/update settings, only ever INSERT new rows
# grab the latest "global settings"(User::Account=NULL).