# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_02_20_134836) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.jsonb "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_id", "associated_type"], name: "associated_index"
    t.index ["auditable_id", "auditable_type"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "design_collection_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", null: false
    t.integer "descendant_id", null: false
    t.integer "generations", null: false
    t.index ["ancestor_id", "descendant_id", "generations"], name: "collection_anc_desc_idx", unique: true
    t.index ["descendant_id"], name: "collection_desc_idx"
  end

  create_table "design_collections", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "collection", null: false
    t.integer "parent_id", comment: "self joining table"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["collection"], name: "index_design_collections_on_collection", unique: true
    t.index ["position"], name: "index_design_collections_on_position"
    t.index ["slug"], name: "index_design_collections_on_slug", unique: true
  end

  create_table "design_diamonds", force: :cascade do |t|
    t.integer "position"
    t.bigint "material_material_id", null: false
    t.bigint "material_gem_shape_id", null: false
    t.bigint "material_gem_clarity_id", null: false
    t.bigint "material_color_id"
    t.bigint "material_gem_size_id", null: false
    t.integer "pcs", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_color_id"], name: "index_design_diamonds_on_material_color_id"
    t.index ["material_gem_clarity_id"], name: "index_design_diamonds_on_material_gem_clarity_id"
    t.index ["material_gem_shape_id"], name: "index_design_diamonds_on_material_gem_shape_id"
    t.index ["material_gem_size_id"], name: "index_design_diamonds_on_material_gem_size_id"
    t.index ["material_material_id"], name: "index_design_diamonds_on_material_material_id"
    t.index ["position"], name: "index_design_diamonds_on_position"
  end

  create_table "design_images", force: :cascade do |t|
    t.string "imageable_type"
    t.bigint "imageable_id"
    t.text "image_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["imageable_type", "imageable_id"], name: "index_design_images_on_imageable_type_and_imageable_id"
  end

  create_table "design_job_diamonds", force: :cascade do |t|
    t.bigint "sale_job_id"
    t.bigint "design_diamond_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["design_diamond_id"], name: "index_design_job_diamonds_on_design_diamond_id"
    t.index ["sale_job_id"], name: "index_design_job_diamonds_on_sale_job_id"
  end

  create_table "design_making_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "making_type", null: false
    t.boolean "isdefault", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["making_type"], name: "index_design_making_types_on_making_type", unique: true
    t.index ["position"], name: "index_design_making_types_on_position"
    t.index ["slug"], name: "index_design_making_types_on_slug", unique: true
  end

  create_table "design_setting_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "setting_type", null: false
    t.boolean "isdefault", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_design_setting_types_on_position"
    t.index ["setting_type"], name: "index_design_setting_types_on_setting_type", unique: true
    t.index ["slug"], name: "index_design_setting_types_on_slug", unique: true
  end

  create_table "design_style_diamonds", force: :cascade do |t|
    t.bigint "design_style_id"
    t.bigint "design_diamond_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["design_diamond_id"], name: "index_design_style_diamonds_on_design_diamond_id"
    t.index ["design_style_id"], name: "index_design_style_diamonds_on_design_style_id"
  end

  create_table "design_style_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "design_style_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["design_style_id", "most_recent"], name: "index_design_style_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["design_style_id", "sort_key"], name: "index_design_style_transitions_parent_sort", unique: true
    t.index ["design_style_id"], name: "index_design_style_transitions_on_design_style_id"
  end

  create_table "design_styles", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.bigint "design_collection_id"
    t.bigint "design_making_type_id"
    t.bigint "design_setting_type_id"
    t.bigint "user_account_id"
    t.bigint "material_material_id"
    t.bigint "material_metal_purity_id"
    t.bigint "material_color_id"
    t.decimal "net_weight", precision: 10, scale: 3, null: false
    t.text "description"
    t.boolean "active", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["design_collection_id"], name: "index_design_styles_on_design_collection_id"
    t.index ["design_making_type_id"], name: "index_design_styles_on_design_making_type_id"
    t.index ["design_setting_type_id"], name: "index_design_styles_on_design_setting_type_id"
    t.index ["material_color_id"], name: "index_design_styles_on_material_color_id"
    t.index ["material_material_id"], name: "index_design_styles_on_material_material_id"
    t.index ["material_metal_purity_id"], name: "index_design_styles_on_material_metal_purity_id"
    t.index ["position"], name: "index_design_styles_on_position"
    t.index ["slug"], name: "index_design_styles_on_slug", unique: true
    t.index ["user_account_id"], name: "index_design_styles_on_user_account_id"
  end

  create_table "material_accessories", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "accessory", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["accessory"], name: "index_material_accessories_on_accessory", unique: true
    t.index ["position"], name: "index_material_accessories_on_position"
    t.index ["slug"], name: "index_material_accessories_on_slug", unique: true
  end

  create_table "material_colors", force: :cascade do |t|
    t.bigint "material_material_id", null: false
    t.integer "position"
    t.string "slug", null: false
    t.string "color", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_material_id"], name: "index_material_colors_on_material_material_id"
    t.index ["position"], name: "index_material_colors_on_position"
    t.index ["slug"], name: "index_material_colors_on_slug", unique: true
  end

  create_table "material_gem_clarities", force: :cascade do |t|
    t.bigint "material_material_id", null: false
    t.integer "position"
    t.string "slug", null: false
    t.string "clarity", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_material_id"], name: "index_material_gem_clarities_on_material_material_id"
    t.index ["position"], name: "index_material_gem_clarities_on_position"
    t.index ["slug"], name: "index_material_gem_clarities_on_slug", unique: true
  end

  create_table "material_gem_clarity_sizes", force: :cascade do |t|
    t.bigint "material_gem_clarity_id"
    t.bigint "material_gem_size_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_gem_clarity_id"], name: "index_material_gem_clarity_sizes_on_material_gem_clarity_id"
    t.index ["material_gem_size_id"], name: "index_material_gem_clarity_sizes_on_material_gem_size_id"
  end

  create_table "material_gem_shapes", force: :cascade do |t|
    t.bigint "material_material_id", null: false
    t.integer "position"
    t.string "slug", null: false
    t.string "shape", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_material_id"], name: "index_material_gem_shapes_on_material_material_id"
    t.index ["position"], name: "index_material_gem_shapes_on_position"
    t.index ["slug"], name: "index_material_gem_shapes_on_slug", unique: true
  end

  create_table "material_gem_sizes", force: :cascade do |t|
    t.bigint "material_material_id", null: false
    t.bigint "material_gem_shape_id", null: false
    t.integer "position"
    t.string "slug", null: false
    t.string "mm_size"
    t.decimal "carat_weight", precision: 10, scale: 3, null: false
    t.decimal "price", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_gem_shape_id"], name: "index_material_gem_sizes_on_material_gem_shape_id"
    t.index ["material_material_id"], name: "index_material_gem_sizes_on_material_material_id"
    t.index ["position"], name: "index_material_gem_sizes_on_position"
  end

  create_table "material_gem_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "gem_type", null: false
    t.boolean "isdefault", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gem_type"], name: "index_material_gem_types_on_gem_type", unique: true
    t.index ["position"], name: "index_material_gem_types_on_position"
    t.index ["slug"], name: "index_material_gem_types_on_slug", unique: true
  end

  create_table "material_lockers", force: :cascade do |t|
    t.bigint "user_company_id", null: false
    t.integer "position"
    t.string "slug", null: false
    t.string "locker", null: false
    t.boolean "isdefault", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["locker"], name: "index_material_lockers_on_locker", unique: true
    t.index ["position"], name: "index_material_lockers_on_position"
    t.index ["slug"], name: "index_material_lockers_on_slug", unique: true
    t.index ["user_company_id"], name: "index_material_lockers_on_user_company_id"
  end

  create_table "material_material_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "material_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_type"], name: "index_material_material_types_on_material_type", unique: true
    t.index ["position"], name: "index_material_material_types_on_position"
    t.index ["slug"], name: "index_material_material_types_on_slug", unique: true
  end

  create_table "material_materials", force: :cascade do |t|
    t.bigint "material_material_type_id", null: false
    t.integer "position"
    t.string "slug", null: false
    t.string "material_name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_material_type_id"], name: "index_material_materials_on_material_material_type_id"
    t.index ["material_name"], name: "index_material_materials_on_material_name", unique: true
    t.index ["position"], name: "index_material_materials_on_position"
    t.index ["slug"], name: "index_material_materials_on_slug", unique: true
  end

  create_table "material_metal_purities", force: :cascade do |t|
    t.bigint "material_material_id", null: false
    t.integer "position"
    t.string "slug", null: false
    t.string "name", null: false
    t.decimal "purity", precision: 10, scale: 3, null: false
    t.decimal "specific_density", precision: 10, scale: 3, null: false
    t.decimal "price", precision: 10, scale: 2
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_material_id"], name: "index_material_metal_purities_on_material_material_id"
    t.index ["position"], name: "index_material_metal_purities_on_position"
    t.index ["slug"], name: "index_material_metal_purities_on_slug", unique: true
  end

  create_table "material_rate_ons", force: :cascade do |t|
    t.bigint "material_material_type_id"
    t.integer "position"
    t.string "slug"
    t.string "rateon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_material_type_id"], name: "index_material_rate_ons_on_material_material_type_id"
    t.index ["position"], name: "index_material_rate_ons_on_position"
    t.index ["rateon"], name: "index_material_rate_ons_on_rateon", unique: true
    t.index ["slug"], name: "index_material_rate_ons_on_slug", unique: true
  end

  create_table "mfg_casting_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "mfg_casting_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mfg_casting_id", "most_recent"], name: "index_mfg_casting_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["mfg_casting_id", "sort_key"], name: "index_mfg_casting_transitions_parent_sort", unique: true
    t.index ["mfg_casting_id"], name: "index_mfg_casting_transitions_on_mfg_casting_id"
  end

  create_table "mfg_castings", force: :cascade do |t|
    t.integer "position"
    t.datetime "date", null: false
    t.string "slug", null: false
    t.bigint "employee_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_mfg_castings_on_employee_id"
    t.index ["position"], name: "index_mfg_castings_on_position"
  end

  create_table "mfg_department_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", null: false
    t.integer "descendant_id", null: false
    t.integer "generations", null: false
    t.index ["ancestor_id", "descendant_id", "generations"], name: "department_anc_desc_idx", unique: true
    t.index ["descendant_id"], name: "department_desc_idx"
  end

  create_table "mfg_departments", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "department", null: false
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department"], name: "index_mfg_departments_on_department", unique: true
    t.index ["position"], name: "index_mfg_departments_on_position"
    t.index ["slug"], name: "index_mfg_departments_on_slug", unique: true
  end

  create_table "mfg_dust_resources", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "dust_resource", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dust_resource"], name: "index_mfg_dust_resources_on_dust_resource", unique: true
    t.index ["position"], name: "index_mfg_dust_resources_on_position"
    t.index ["slug"], name: "index_mfg_dust_resources_on_slug", unique: true
  end

  create_table "mfg_metal_issue_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "mfg_metal_issue_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mfg_metal_issue_id", "most_recent"], name: "index_mfg_metal_issue_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["mfg_metal_issue_id", "sort_key"], name: "index_mfg_metal_issue_transitions_parent_sort", unique: true
    t.index ["mfg_metal_issue_id"], name: "index_mfg_metal_issue_transitions_on_mfg_metal_issue_id"
  end

  create_table "mfg_metal_issues", force: :cascade do |t|
    t.integer "position"
    t.datetime "date", null: false
    t.string "slug", null: false
    t.bigint "employee_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_mfg_metal_issues_on_employee_id"
    t.index ["position"], name: "index_mfg_metal_issues_on_position"
  end

  create_table "mfg_mfg_txn_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "mfg_mfg_txn_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mfg_mfg_txn_id", "most_recent"], name: "index_mfg_mfg_txn_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["mfg_mfg_txn_id", "sort_key"], name: "index_mfg_mfg_txn_transitions_parent_sort", unique: true
    t.index ["mfg_mfg_txn_id"], name: "index_mfg_mfg_txn_transitions_on_mfg_mfg_txn_id"
  end

  create_table "mfg_mfg_txns", force: :cascade do |t|
    t.integer "position"
    t.datetime "mfg_date", null: false
    t.bigint "mfg_casting_id"
    t.bigint "mfg_wax_setting_id"
    t.bigint "mfg_metal_issue_id"
    t.bigint "user_company_id", null: false
    t.bigint "sale_job_id", null: false
    t.bigint "mfg_department_id", null: false
    t.bigint "employee_id", null: false
    t.text "description"
    t.decimal "receive_weight", precision: 10, scale: 3, default: "0.0", null: false
    t.decimal "dust_weight", precision: 10, scale: 3, default: "0.0"
    t.boolean "received", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_mfg_mfg_txns_on_employee_id"
    t.index ["mfg_casting_id"], name: "index_mfg_mfg_txns_on_mfg_casting_id"
    t.index ["mfg_department_id"], name: "index_mfg_mfg_txns_on_mfg_department_id"
    t.index ["mfg_metal_issue_id"], name: "index_mfg_mfg_txns_on_mfg_metal_issue_id"
    t.index ["mfg_wax_setting_id"], name: "index_mfg_mfg_txns_on_mfg_wax_setting_id"
    t.index ["position"], name: "index_mfg_mfg_txns_on_position"
    t.index ["sale_job_id"], name: "index_mfg_mfg_txns_on_sale_job_id"
    t.index ["user_company_id"], name: "index_mfg_mfg_txns_on_user_company_id"
  end

  create_table "mfg_priorities", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "priority", null: false
    t.boolean "isdefault", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_mfg_priorities_on_position"
    t.index ["priority"], name: "index_mfg_priorities_on_priority", unique: true
    t.index ["slug"], name: "index_mfg_priorities_on_slug", unique: true
  end

  create_table "mfg_product_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "product_type", null: false
    t.boolean "isdefault", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_mfg_product_types_on_position"
    t.index ["product_type"], name: "index_mfg_product_types_on_product_type", unique: true
    t.index ["slug"], name: "index_mfg_product_types_on_slug", unique: true
  end

  create_table "mfg_refining_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "mfg_refining_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mfg_refining_id", "most_recent"], name: "index_mfg_refining_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["mfg_refining_id", "sort_key"], name: "index_mfg_refining_transitions_parent_sort", unique: true
    t.index ["mfg_refining_id"], name: "index_mfg_refining_transitions_on_mfg_refining_id"
  end

  create_table "mfg_refinings", force: :cascade do |t|
    t.integer "position"
    t.bigint "user_account_id", null: false
    t.datetime "date_from", null: false
    t.datetime "date_to", null: false
    t.bigint "user_company_id", null: false
    t.bigint "mfg_department_id", null: false
    t.decimal "dust_weight", precision: 10, scale: 3, default: "0.0", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mfg_department_id"], name: "index_mfg_refinings_on_mfg_department_id"
    t.index ["position"], name: "index_mfg_refinings_on_position"
    t.index ["user_account_id"], name: "index_mfg_refinings_on_user_account_id"
    t.index ["user_company_id"], name: "index_mfg_refinings_on_user_company_id"
  end

  create_table "mfg_wax_setting_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "mfg_wax_setting_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mfg_wax_setting_id", "most_recent"], name: "index_mfg_wax_setting_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["mfg_wax_setting_id", "sort_key"], name: "index_mfg_wax_setting_transitions_parent_sort", unique: true
    t.index ["mfg_wax_setting_id"], name: "index_mfg_wax_setting_transitions_on_mfg_wax_setting_id"
  end

  create_table "mfg_wax_settings", force: :cascade do |t|
    t.integer "position"
    t.datetime "date", null: false
    t.string "slug", null: false
    t.bigint "employee_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_mfg_wax_settings_on_employee_id"
    t.index ["position"], name: "index_mfg_wax_settings_on_position"
  end

  create_table "sale_job_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "sale_job_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_job_id", "most_recent"], name: "index_sale_job_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["sale_job_id", "sort_key"], name: "index_sale_job_transitions_parent_sort", unique: true
    t.index ["sale_job_id"], name: "index_sale_job_transitions_on_sale_job_id"
  end

  create_table "sale_jobs", force: :cascade do |t|
    t.string "processable_type"
    t.bigint "processable_id"
    t.string "salable_type"
    t.bigint "salable_id"
    t.boolean "final", default: false, null: false
    t.integer "position"
    t.bigint "design_style_id", null: false
    t.bigint "user_company_id", null: false
    t.bigint "material_material_id", null: false
    t.bigint "metal_purity_id", null: false
    t.bigint "metal_color_id", null: false
    t.integer "qty", default: 1
    t.bigint "diamond_clarity_id"
    t.bigint "diamond_color_id"
    t.bigint "cs_clarity_id"
    t.bigint "cs_color_id"
    t.string "instruction"
    t.string "item_size"
    t.bigint "mfg_priority_id", null: false
    t.bigint "mfg_department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cs_clarity_id"], name: "index_sale_jobs_on_cs_clarity_id"
    t.index ["cs_color_id"], name: "index_sale_jobs_on_cs_color_id"
    t.index ["design_style_id"], name: "index_sale_jobs_on_design_style_id"
    t.index ["diamond_clarity_id"], name: "index_sale_jobs_on_diamond_clarity_id"
    t.index ["diamond_color_id"], name: "index_sale_jobs_on_diamond_color_id"
    t.index ["material_material_id"], name: "index_sale_jobs_on_material_material_id"
    t.index ["metal_color_id"], name: "index_sale_jobs_on_metal_color_id"
    t.index ["metal_purity_id"], name: "index_sale_jobs_on_metal_purity_id"
    t.index ["mfg_department_id"], name: "index_sale_jobs_on_mfg_department_id"
    t.index ["mfg_priority_id"], name: "index_sale_jobs_on_mfg_priority_id"
    t.index ["position"], name: "index_sale_jobs_on_position"
    t.index ["processable_type", "processable_id"], name: "index_sale_jobs_on_processable_type_and_processable_id"
    t.index ["salable_type", "salable_id"], name: "index_sale_jobs_on_salable_type_and_salable_id"
    t.index ["user_company_id"], name: "index_sale_jobs_on_user_company_id"
  end

  create_table "sale_m_txn_details", force: :cascade do |t|
    t.string "countable_type"
    t.bigint "countable_id"
    t.string "countable_r_type"
    t.bigint "countable_r_id"
    t.integer "position"
    t.bigint "material_material_id"
    t.bigint "material_metal_purity_id"
    t.bigint "material_gem_shape_id"
    t.bigint "material_gem_clarity_id"
    t.bigint "material_color_id"
    t.bigint "material_gem_size_id"
    t.bigint "user_company_id"
    t.bigint "material_locker_id"
    t.bigint "material_accessory_id"
    t.boolean "customer_stock", default: false, null: false
    t.boolean "isissue", default: true, null: false
    t.integer "pcs", default: 0
    t.decimal "weight1", precision: 10, scale: 3, default: "0.0"
    t.string "note"
    t.bigint "material_gem_type_id"
    t.decimal "rate1", precision: 10, scale: 3, default: "0.0"
    t.boolean "rate_on_pc", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["countable_r_type", "countable_r_id"], name: "index_sale_m_txn_details_on_countable_r_type_and_countable_r_id"
    t.index ["countable_type", "countable_id"], name: "index_sale_m_txn_details_on_countable_type_and_countable_id"
    t.index ["material_accessory_id"], name: "index_sale_m_txn_details_on_material_accessory_id"
    t.index ["material_color_id"], name: "index_sale_m_txn_details_on_material_color_id"
    t.index ["material_gem_clarity_id"], name: "index_sale_m_txn_details_on_material_gem_clarity_id"
    t.index ["material_gem_shape_id"], name: "index_sale_m_txn_details_on_material_gem_shape_id"
    t.index ["material_gem_size_id"], name: "index_sale_m_txn_details_on_material_gem_size_id"
    t.index ["material_gem_type_id"], name: "index_sale_m_txn_details_on_material_gem_type_id"
    t.index ["material_locker_id"], name: "index_sale_m_txn_details_on_material_locker_id"
    t.index ["material_material_id"], name: "index_sale_m_txn_details_on_material_material_id"
    t.index ["material_metal_purity_id"], name: "index_sale_m_txn_details_on_material_metal_purity_id"
    t.index ["position"], name: "index_sale_m_txn_details_on_position"
    t.index ["user_company_id"], name: "index_sale_m_txn_details_on_user_company_id"
  end

  create_table "sale_m_txn_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "sale_m_txn_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_m_txn_id", "most_recent"], name: "index_sale_m_txn_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["sale_m_txn_id", "sort_key"], name: "index_sale_m_txn_transitions_parent_sort", unique: true
    t.index ["sale_m_txn_id"], name: "index_sale_m_txn_transitions_on_sale_m_txn_id"
  end

  create_table "sale_m_txn_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "txn_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_sale_m_txn_types_on_position"
    t.index ["slug"], name: "index_sale_m_txn_types_on_slug", unique: true
  end

  create_table "sale_m_txns", force: :cascade do |t|
    t.integer "position"
    t.bigint "sale_m_txn_type_id", null: false
    t.bigint "user_account_id", null: false
    t.bigint "taken_by_id", null: false
    t.datetime "date", null: false
    t.datetime "due_date", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_sale_m_txns_on_position"
    t.index ["sale_m_txn_type_id"], name: "index_sale_m_txns_on_sale_m_txn_type_id"
    t.index ["taken_by_id"], name: "index_sale_m_txns_on_taken_by_id"
    t.index ["user_account_id"], name: "index_sale_m_txns_on_user_account_id"
  end

  create_table "sale_memo_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "sale_memo_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_memo_id", "most_recent"], name: "index_sale_memo_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["sale_memo_id", "sort_key"], name: "index_sale_memo_transitions_parent_sort", unique: true
    t.index ["sale_memo_id"], name: "index_sale_memo_transitions_on_sale_memo_id"
  end

  create_table "sale_memos", force: :cascade do |t|
    t.integer "position"
    t.datetime "date", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_sale_memos_on_position"
  end

  create_table "sale_order_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "sale_order_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_order_id", "most_recent"], name: "index_sale_order_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["sale_order_id", "sort_key"], name: "index_sale_order_transitions_parent_sort", unique: true
    t.index ["sale_order_id"], name: "index_sale_order_transitions_on_sale_order_id"
  end

  create_table "sale_order_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "order_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_type"], name: "index_sale_order_types_on_order_type", unique: true
    t.index ["position"], name: "index_sale_order_types_on_position"
    t.index ["slug"], name: "index_sale_order_types_on_slug", unique: true
  end

  create_table "sale_orders", force: :cascade do |t|
    t.integer "position"
    t.bigint "user_company_id", null: false
    t.bigint "customer_id", null: false
    t.bigint "taken_by_id", null: false
    t.datetime "order_date", null: false
    t.datetime "delivery_date", null: false
    t.bigint "mfg_product_type_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_sale_orders_on_customer_id"
    t.index ["mfg_product_type_id"], name: "index_sale_orders_on_mfg_product_type_id"
    t.index ["position"], name: "index_sale_orders_on_position"
    t.index ["taken_by_id"], name: "index_sale_orders_on_taken_by_id"
    t.index ["user_company_id"], name: "index_sale_orders_on_user_company_id"
  end

  create_table "sale_sale_details", force: :cascade do |t|
    t.string "saleable_type"
    t.bigint "saleable_id"
    t.integer "position"
    t.decimal "tounch", precision: 10, scale: 3, default: "0.0"
    t.bigint "m_rate_on_id"
    t.decimal "m_rate", precision: 10, scale: 3, default: "0.0"
    t.bigint "d_rate_on_id"
    t.decimal "d_rate", precision: 10, scale: 3, default: "0.0"
    t.bigint "cs_rate_on_id"
    t.decimal "cs_rate", precision: 10, scale: 3, default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cs_rate_on_id"], name: "index_sale_sale_details_on_cs_rate_on_id"
    t.index ["d_rate_on_id"], name: "index_sale_sale_details_on_d_rate_on_id"
    t.index ["m_rate_on_id"], name: "index_sale_sale_details_on_m_rate_on_id"
    t.index ["position"], name: "index_sale_sale_details_on_position"
    t.index ["saleable_type", "saleable_id"], name: "index_sale_sale_details_on_saleable_type_and_saleable_id"
  end

  create_table "sale_sale_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "sale_sale_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_sale_id", "most_recent"], name: "index_sale_sale_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["sale_sale_id", "sort_key"], name: "index_sale_sale_transitions_parent_sort", unique: true
    t.index ["sale_sale_id"], name: "index_sale_sale_transitions_on_sale_sale_id"
  end

  create_table "sale_sales", force: :cascade do |t|
    t.integer "position"
    t.bigint "customer_id", null: false
    t.bigint "sold_by_id", null: false
    t.datetime "date", null: false
    t.datetime "due_date", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_sale_sales_on_customer_id"
    t.index ["position"], name: "index_sale_sales_on_position"
    t.index ["sold_by_id"], name: "index_sale_sales_on_sold_by_id"
  end

  create_table "user_account_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", null: false
    t.integer "descendant_id", null: false
    t.integer "generations", null: false
    t.index ["ancestor_id", "descendant_id", "generations"], name: "account_anc_desc_idx", unique: true
    t.index ["descendant_id"], name: "account_desc_idx"
  end

  create_table "user_account_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.bigint "user_account_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_account_id", "most_recent"], name: "index_user_account_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["user_account_id", "sort_key"], name: "index_user_account_transitions_parent_sort", unique: true
    t.index ["user_account_id"], name: "index_user_account_transitions_on_user_account_id"
  end

  create_table "user_account_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug"
    t.string "account_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_type"], name: "index_user_account_types_on_account_type", unique: true
    t.index ["position"], name: "index_user_account_types_on_position"
    t.index ["slug"], name: "index_user_account_types_on_slug", unique: true
  end

  create_table "user_accounts", force: :cascade do |t|
    t.bigint "user_account_type_id", null: false
    t.integer "position"
    t.datetime "join_date"
    t.string "slug", null: false
    t.string "account_name", null: false
    t.integer "salary", default: 0
    t.bigint "user_role_id", null: false
    t.integer "parent_id", comment: "self joining table"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.index ["account_name"], name: "index_user_accounts_on_account_name", unique: true
    t.index ["email"], name: "index_user_accounts_on_email", unique: true
    t.index ["position"], name: "index_user_accounts_on_position"
    t.index ["reset_password_token"], name: "index_user_accounts_on_reset_password_token", unique: true
    t.index ["slug"], name: "index_user_accounts_on_slug", unique: true
    t.index ["user_account_type_id"], name: "index_user_accounts_on_user_account_type_id"
    t.index ["user_role_id"], name: "index_user_accounts_on_user_role_id"
  end

  create_table "user_addresses", force: :cascade do |t|
    t.bigint "user_account_id"
    t.bigint "user_company_id"
    t.integer "position"
    t.string "firstname"
    t.string "lastname"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "zipcode"
    t.bigint "user_state_id"
    t.bigint "user_country_id"
    t.string "phone"
    t.string "alternative_phone"
    t.string "company"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_user_addresses_on_position"
    t.index ["user_account_id"], name: "index_user_addresses_on_user_account_id"
    t.index ["user_company_id"], name: "index_user_addresses_on_user_company_id"
    t.index ["user_country_id"], name: "index_user_addresses_on_user_country_id"
    t.index ["user_state_id"], name: "index_user_addresses_on_user_state_id"
  end

  create_table "user_authentication_tokens", force: :cascade do |t|
    t.string "body"
    t.bigint "user_account_id"
    t.datetime "last_used_at"
    t.integer "expires_in"
    t.string "ip_address"
    t.string "user_agent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["body"], name: "index_user_authentication_tokens_on_body"
    t.index ["user_account_id"], name: "index_user_authentication_tokens_on_user_account_id"
  end

  create_table "user_companies", force: :cascade do |t|
    t.integer "position"
    t.string "slug"
    t.string "company"
    t.string "gst_no"
    t.integer "parent_id", comment: "self joining table"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company"], name: "index_user_companies_on_company", unique: true
    t.index ["position"], name: "index_user_companies_on_position"
    t.index ["slug"], name: "index_user_companies_on_slug", unique: true
  end

  create_table "user_company_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", null: false
    t.integer "descendant_id", null: false
    t.integer "generations", null: false
    t.index ["ancestor_id", "descendant_id", "generations"], name: "company_anc_desc_idx", unique: true
    t.index ["descendant_id"], name: "company_desc_idx"
  end

  create_table "user_company_ownerships", force: :cascade do |t|
    t.bigint "user_company_id"
    t.bigint "user_account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_account_id"], name: "index_user_company_ownerships_on_user_account_id"
    t.index ["user_company_id"], name: "index_user_company_ownerships_on_user_company_id"
  end

  create_table "user_contacts", force: :cascade do |t|
    t.bigint "user_company_id"
    t.bigint "user_account_id"
    t.integer "position"
    t.string "company_name"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone"
    t.string "mobile"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_account_id"], name: "index_user_contacts_on_user_account_id"
    t.index ["user_company_id"], name: "index_user_contacts_on_user_company_id"
  end

  create_table "user_countries", force: :cascade do |t|
    t.integer "position"
    t.string "slug"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_user_countries_on_position"
    t.index ["slug"], name: "index_user_countries_on_slug", unique: true
  end

  create_table "user_customers", force: :cascade do |t|
    t.string "company_slug", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.integer "position"
    t.string "slug"
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_user_roles_on_position"
    t.index ["role"], name: "index_user_roles_on_role", unique: true
    t.index ["slug"], name: "index_user_roles_on_slug", unique: true
  end

  create_table "user_settings", force: :cascade do |t|
    t.bigint "user_account_id", null: false
    t.json "config", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at"], name: "index_user_settings_on_created_at"
    t.index ["user_account_id"], name: "index_user_settings_on_user_account_id"
  end

  create_table "user_states", force: :cascade do |t|
    t.integer "position"
    t.bigint "user_country_id"
    t.string "slug"
    t.string "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_user_states_on_position"
    t.index ["slug"], name: "index_user_states_on_slug", unique: true
    t.index ["user_country_id"], name: "index_user_states_on_user_country_id"
  end

  create_table "user_tax_rates", force: :cascade do |t|
    t.bigint "user_tax_type_id"
    t.integer "position"
    t.string "slug", null: false
    t.string "name", null: false
    t.decimal "rate", precision: 10, scale: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_user_tax_rates_on_position"
    t.index ["slug"], name: "index_user_tax_rates_on_slug", unique: true
    t.index ["user_tax_type_id"], name: "index_user_tax_rates_on_user_tax_type_id"
  end

  create_table "user_tax_types", force: :cascade do |t|
    t.integer "position"
    t.string "slug", null: false
    t.string "tax_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_user_tax_types_on_position"
    t.index ["slug"], name: "index_user_tax_types_on_slug", unique: true
  end

  add_foreign_key "design_diamonds", "material_colors"
  add_foreign_key "design_diamonds", "material_gem_clarities"
  add_foreign_key "design_diamonds", "material_gem_shapes"
  add_foreign_key "design_diamonds", "material_gem_sizes"
  add_foreign_key "design_diamonds", "material_materials"
  add_foreign_key "design_job_diamonds", "design_diamonds"
  add_foreign_key "design_job_diamonds", "sale_jobs"
  add_foreign_key "design_style_diamonds", "design_diamonds"
  add_foreign_key "design_style_diamonds", "design_styles"
  add_foreign_key "design_style_transitions", "design_styles"
  add_foreign_key "design_styles", "design_collections"
  add_foreign_key "design_styles", "design_making_types"
  add_foreign_key "design_styles", "design_setting_types"
  add_foreign_key "design_styles", "material_colors"
  add_foreign_key "design_styles", "material_materials"
  add_foreign_key "design_styles", "material_metal_purities"
  add_foreign_key "design_styles", "user_accounts"
  add_foreign_key "material_colors", "material_materials"
  add_foreign_key "material_gem_clarities", "material_materials"
  add_foreign_key "material_gem_clarity_sizes", "material_gem_clarities"
  add_foreign_key "material_gem_clarity_sizes", "material_gem_sizes"
  add_foreign_key "material_gem_shapes", "material_materials"
  add_foreign_key "material_gem_sizes", "material_gem_shapes"
  add_foreign_key "material_gem_sizes", "material_materials"
  add_foreign_key "material_lockers", "user_companies"
  add_foreign_key "material_materials", "material_material_types"
  add_foreign_key "material_metal_purities", "material_materials"
  add_foreign_key "material_rate_ons", "material_material_types"
  add_foreign_key "mfg_casting_transitions", "mfg_castings"
  add_foreign_key "mfg_castings", "user_accounts", column: "employee_id"
  add_foreign_key "mfg_metal_issue_transitions", "mfg_metal_issues"
  add_foreign_key "mfg_metal_issues", "user_accounts", column: "employee_id"
  add_foreign_key "mfg_mfg_txn_transitions", "mfg_mfg_txns"
  add_foreign_key "mfg_mfg_txns", "mfg_castings"
  add_foreign_key "mfg_mfg_txns", "mfg_departments"
  add_foreign_key "mfg_mfg_txns", "mfg_metal_issues"
  add_foreign_key "mfg_mfg_txns", "mfg_wax_settings"
  add_foreign_key "mfg_mfg_txns", "sale_jobs"
  add_foreign_key "mfg_mfg_txns", "user_accounts", column: "employee_id"
  add_foreign_key "mfg_mfg_txns", "user_companies"
  add_foreign_key "mfg_refining_transitions", "mfg_refinings"
  add_foreign_key "mfg_refinings", "mfg_departments"
  add_foreign_key "mfg_refinings", "user_accounts"
  add_foreign_key "mfg_refinings", "user_companies"
  add_foreign_key "mfg_wax_setting_transitions", "mfg_wax_settings"
  add_foreign_key "mfg_wax_settings", "user_accounts", column: "employee_id"
  add_foreign_key "sale_job_transitions", "sale_jobs"
  add_foreign_key "sale_jobs", "design_styles"
  add_foreign_key "sale_jobs", "material_colors", column: "cs_color_id"
  add_foreign_key "sale_jobs", "material_colors", column: "diamond_color_id"
  add_foreign_key "sale_jobs", "material_colors", column: "metal_color_id"
  add_foreign_key "sale_jobs", "material_gem_clarities", column: "cs_clarity_id"
  add_foreign_key "sale_jobs", "material_gem_clarities", column: "diamond_clarity_id"
  add_foreign_key "sale_jobs", "material_materials"
  add_foreign_key "sale_jobs", "material_metal_purities", column: "metal_purity_id"
  add_foreign_key "sale_jobs", "mfg_departments"
  add_foreign_key "sale_jobs", "mfg_priorities"
  add_foreign_key "sale_jobs", "user_companies"
  add_foreign_key "sale_m_txn_details", "material_accessories"
  add_foreign_key "sale_m_txn_details", "material_colors"
  add_foreign_key "sale_m_txn_details", "material_gem_clarities"
  add_foreign_key "sale_m_txn_details", "material_gem_shapes"
  add_foreign_key "sale_m_txn_details", "material_gem_sizes"
  add_foreign_key "sale_m_txn_details", "material_gem_types"
  add_foreign_key "sale_m_txn_details", "material_lockers"
  add_foreign_key "sale_m_txn_details", "material_materials"
  add_foreign_key "sale_m_txn_details", "material_metal_purities"
  add_foreign_key "sale_m_txn_details", "user_companies"
  add_foreign_key "sale_m_txn_transitions", "sale_m_txns"
  add_foreign_key "sale_m_txns", "sale_m_txn_types"
  add_foreign_key "sale_m_txns", "user_accounts"
  add_foreign_key "sale_m_txns", "user_accounts", column: "taken_by_id"
  add_foreign_key "sale_memo_transitions", "sale_memos"
  add_foreign_key "sale_order_transitions", "sale_orders"
  add_foreign_key "sale_orders", "mfg_product_types"
  add_foreign_key "sale_orders", "user_accounts", column: "customer_id"
  add_foreign_key "sale_orders", "user_accounts", column: "taken_by_id"
  add_foreign_key "sale_orders", "user_companies"
  add_foreign_key "sale_sale_transitions", "sale_sales"
  add_foreign_key "sale_sales", "user_accounts", column: "customer_id"
  add_foreign_key "sale_sales", "user_accounts", column: "sold_by_id"
  add_foreign_key "user_account_transitions", "user_accounts"
  add_foreign_key "user_accounts", "user_account_types"
  add_foreign_key "user_accounts", "user_roles"
  add_foreign_key "user_addresses", "user_accounts"
  add_foreign_key "user_addresses", "user_companies"
  add_foreign_key "user_addresses", "user_countries"
  add_foreign_key "user_addresses", "user_states"
  add_foreign_key "user_authentication_tokens", "user_accounts"
  add_foreign_key "user_company_ownerships", "user_accounts"
  add_foreign_key "user_company_ownerships", "user_companies"
  add_foreign_key "user_contacts", "user_accounts"
  add_foreign_key "user_contacts", "user_companies"
  add_foreign_key "user_states", "user_countries"
  add_foreign_key "user_tax_rates", "user_tax_types"

  create_view "sale_m_stocks",  sql_definition: <<-SQL
      SELECT sale_m_txn_details.id,
      sale_m_txn_details.countable_type,
      sale_m_txn_details.countable_id,
      sale_m_txn_details.countable_r_type,
      sale_m_txn_details.countable_r_id,
      sale_m_txn_details."position",
      sale_m_txn_details.material_material_id,
      sale_m_txn_details.material_metal_purity_id,
      sale_m_txn_details.material_gem_shape_id,
      sale_m_txn_details.material_gem_clarity_id,
      sale_m_txn_details.material_color_id,
      sale_m_txn_details.material_gem_size_id,
      sale_m_txn_details.user_company_id,
      sale_m_txn_details.material_locker_id,
      sale_m_txn_details.material_accessory_id,
      sale_m_txn_details.isissue,
      sale_m_txn_details.pcs,
          CASE
              WHEN (sale_m_txn_details.isissue IS TRUE) THEN (- sale_m_txn_details.pcs)
              ELSE sale_m_txn_details.pcs
          END AS calc_pcs,
      sale_m_txn_details.weight1,
          CASE
              WHEN (sale_m_txn_details.isissue IS TRUE) THEN (- sale_m_txn_details.weight1)
              ELSE sale_m_txn_details.weight1
          END AS calc_weight,
      sale_m_txn_details.note,
      sale_m_txn_details.material_gem_type_id,
      sale_m_txn_details.rate_on_pc,
      sale_m_txn_details.rate1,
      sale_m_txn_details.created_at,
      sale_m_txn_details.updated_at
     FROM sale_m_txn_details;
  SQL

  create_view "design_diamond_views",  sql_definition: <<-SQL
      SELECT design_diamonds.id,
      design_diamonds."position",
      design_diamonds.material_material_id,
      design_diamonds.material_gem_shape_id,
      design_diamonds.material_gem_clarity_id,
      design_diamonds.material_color_id,
      design_diamonds.material_gem_size_id,
      design_diamonds.pcs,
      ((design_diamonds.pcs)::numeric * material_gem_sizes.carat_weight) AS weight,
      design_diamonds.created_at,
      design_diamonds.updated_at,
      material_gem_sizes.id AS id01,
      material_gem_sizes.material_material_id AS material_material_id01,
      material_gem_sizes.material_gem_shape_id AS material_gem_shape_id01,
      material_gem_sizes."position" AS position01,
      material_gem_sizes.slug,
      material_gem_sizes.mm_size,
      material_gem_sizes.carat_weight,
      material_gem_sizes.price,
      material_gem_sizes.created_at AS created_at01,
      material_gem_sizes.updated_at AS updated_at01
     FROM (design_diamonds
       LEFT JOIN material_gem_sizes ON ((design_diamonds.material_gem_size_id = material_gem_sizes.id)));
  SQL

  create_view "sale_m_txn_detail_views",  sql_definition: <<-SQL
      SELECT sale_m_txn_details.id,
          CASE
              WHEN (sale_m_txns.user_account_id IS NOT NULL) THEN sale_m_txns.user_account_id
              WHEN (sale_orders.customer_id IS NOT NULL) THEN sale_orders.customer_id
              ELSE (0)::bigint
          END AS account,
          CASE
              WHEN (sale_m_txn_details.isissue IS TRUE) THEN (- sale_m_txn_details.pcs)
              ELSE sale_m_txn_details.pcs
          END AS calc_pcs,
          CASE
              WHEN (sale_m_txn_details.isissue IS TRUE) THEN (- sale_m_txn_details.weight1)
              ELSE sale_m_txn_details.weight1
          END AS calc_weight,
      sale_m_txn_details.countable_type,
      sale_m_txn_details.countable_id,
      sale_m_txn_details.countable_r_type,
      sale_m_txn_details.countable_r_id,
      sale_m_txn_details."position",
      sale_m_txn_details.material_material_id,
      sale_m_txn_details.material_metal_purity_id,
      sale_m_txn_details.material_gem_shape_id,
      sale_m_txn_details.material_gem_clarity_id,
      sale_m_txn_details.material_color_id,
      sale_m_txn_details.material_gem_size_id,
      sale_m_txn_details.user_company_id,
      sale_m_txn_details.material_locker_id,
      sale_m_txn_details.material_accessory_id,
      sale_m_txn_details.isissue,
      sale_m_txn_details.customer_stock,
      sale_m_txn_details.pcs,
      sale_m_txn_details.weight1,
      sale_m_txn_details.note,
      sale_m_txn_details.material_gem_type_id,
      sale_m_txn_details.rate_on_pc,
      sale_m_txn_details.created_at,
      sale_m_txn_details.updated_at,
      sale_m_txns.id AS id01,
      sale_m_txns."position" AS position01,
      sale_m_txns.sale_m_txn_type_id,
      sale_m_txns.user_account_id,
      sale_m_txns.taken_by_id,
      sale_m_txns.date,
      sale_m_txns.due_date,
      sale_m_txns.description,
      sale_m_txns.created_at AS created_at01,
      sale_m_txns.updated_at AS updated_at01,
      mfg_castings.id AS id02,
      mfg_castings."position" AS position02,
      mfg_castings.date AS date01,
      mfg_castings.slug,
      mfg_castings.employee_id AS casting_employee_id,
      mfg_castings.created_at AS created_at02,
      mfg_castings.updated_at AS updated_at02,
      mfg_mfg_txns.id AS id03,
      mfg_mfg_txns."position" AS position03,
      mfg_mfg_txns.mfg_date,
      mfg_mfg_txns.mfg_casting_id,
      mfg_mfg_txns.sale_job_id,
      mfg_mfg_txns.user_company_id AS user_company_id01,
      mfg_mfg_txns.mfg_department_id,
      mfg_mfg_txns.employee_id AS mfg_employee_id,
      mfg_mfg_txns.receive_weight,
      mfg_mfg_txns.received,
      mfg_mfg_txns.dust_weight,
      mfg_mfg_txns.created_at AS created_at03,
      mfg_mfg_txns.updated_at AS updated_at03,
      sale_jobs.processable_type,
      sale_jobs."position" AS jobno,
      sale_jobs.processable_id AS sale_order_id,
      sale_orders.customer_id
     FROM (((((sale_m_txn_details
       LEFT JOIN mfg_castings ON (((sale_m_txn_details.countable_id = mfg_castings.id) AND ((sale_m_txn_details.countable_type)::text = 'Mfg::Casting'::text))))
       LEFT JOIN mfg_mfg_txns ON (((sale_m_txn_details.countable_id = mfg_mfg_txns.id) AND ((sale_m_txn_details.countable_type)::text = 'Mfg::MfgTxn'::text))))
       LEFT JOIN sale_m_txns ON (((sale_m_txn_details.countable_id = sale_m_txns.id) AND ((sale_m_txn_details.countable_type)::text = 'Sale::MTxn'::text))))
       LEFT JOIN sale_jobs ON ((mfg_mfg_txns.sale_job_id = sale_jobs.id)))
       LEFT JOIN sale_orders ON ((sale_jobs.processable_id = sale_orders.id)));
  SQL

  create_view "sale_order_views",  sql_definition: <<-SQL
      SELECT o.id,
      o."position",
      o.customer_id,
      o.taken_by_id,
      o.order_date,
      o.delivery_date,
      o.mfg_product_type_id,
      o.description,
      o.created_at,
      o.updated_at,
      COALESCE(s.count, (0)::bigint) AS num_jobs,
      s.processable_id,
      s.total_qty
     FROM (sale_orders o
       LEFT JOIN ( SELECT ol.processable_id,
              count(*) AS count,
              sum(ol.qty) AS total_qty
             FROM sale_jobs ol
            GROUP BY ol.processable_id) s ON ((s.processable_id = o.id)));
  SQL

  create_view "mfg_mfg_txn_views",  sql_definition: <<-SQL
      SELECT with_j_p.id,
      with_j_p."position",
      with_j_p.mfg_date,
      with_j_p.mfg_casting_id,
      with_j_p.mfg_wax_setting_id,
      with_j_p.mfg_metal_issue_id,
      with_j_p.user_company_id,
      with_j_p.sale_job_id,
      with_j_p.mfg_department_id,
      with_j_p.employee_id,
      with_j_p.description,
      with_j_p.issue_weight,
      with_j_p.diamond_pcs,
      with_j_p.cs_pcs,
      with_j_p.diamond_weight,
      with_j_p.cs_weight,
      with_j_p.d_cs_weight,
      with_j_p.total_d_cs_weight,
      with_j_p.receive_weight,
      with_j_p.dust_weight,
      with_j_p.received,
      with_j_p.purity,
      with_j_p.net_weight,
      ((with_j_p.net_weight * with_j_p.purity) / (100)::numeric) AS pure_weight,
      with_j_p.created_at,
      with_j_p.updated_at
     FROM ( SELECT sub2.id,
              sub2."position",
              sub2.mfg_date,
              sub2.mfg_casting_id,
              sub2.mfg_wax_setting_id,
              sub2.mfg_metal_issue_id,
              sub2.user_company_id,
              sub2.sale_job_id,
              sub2.mfg_department_id,
              sub2.employee_id,
              sub2.description,
              sub2.receive_weight,
              sub2.received,
              sub2.dust_weight,
              sub2.issue_weight,
              sub2.diamond_pcs,
              sub2.cs_pcs,
              sub2.diamond_weight,
              sub2.cs_weight,
              sub2.d_cs_weight,
              sub2.total_d_cs_weight,
              mp.purity,
              (sub2.receive_weight - (sub2.total_d_cs_weight / (5)::numeric)) AS net_weight,
              sub2.created_at,
              sub2.updated_at
             FROM ((( SELECT sub.id,
                      sub."position",
                      sub.mfg_date,
                      sub.mfg_casting_id,
                      sub.mfg_wax_setting_id,
                      sub.mfg_metal_issue_id,
                      sub.user_company_id,
                      sub.sale_job_id,
                      sub.mfg_department_id,
                      sub.employee_id,
                      sub.description,
                      sub.receive_weight,
                      sub.received,
                      sub.dust_weight,
                      sub.issue_weight,
                      sub.diamond_pcs,
                      sub.cs_pcs,
                      sub.diamond_weight,
                      sub.cs_weight,
                      sub.d_cs_weight,
                      sum(sub.d_cs_weight) OVER (PARTITION BY sub.sale_job_id ORDER BY sub.created_at DESC) AS total_d_cs_weight,
                      sub.created_at,
                      sub.updated_at
                     FROM ( SELECT m.id,
                              m."position",
                              m.mfg_date,
                              m.mfg_casting_id,
                              m.mfg_wax_setting_id,
                              m.mfg_metal_issue_id,
                              m.user_company_id,
                              m.sale_job_id,
                              m.mfg_department_id,
                              m.employee_id,
                              m.description,
                              m.receive_weight,
                              m.received,
                              m.dust_weight,
                              lag(m.receive_weight, 1) OVER (PARTITION BY m.sale_job_id ORDER BY m.id) AS issue_weight,
                              sum(md.calc_pcs) FILTER (WHERE (md.material_material_id = 4)) AS diamond_pcs,
                              sum(md.calc_pcs) FILTER (WHERE (md.material_material_id = 5)) AS cs_pcs,
                              sum(md.calc_weight) FILTER (WHERE (md.material_material_id = 4)) AS diamond_weight,
                              sum(md.calc_weight) FILTER (WHERE (md.material_material_id = 5)) AS cs_weight,
                              sum(md.calc_weight) FILTER (WHERE ((md.material_material_id = 4) OR (md.material_material_id = 5))) AS d_cs_weight,
                              m.created_at,
                              m.updated_at
                             FROM (mfg_mfg_txns m
                               LEFT JOIN sale_m_txn_detail_views md ON ((m.id = md.id03)))
                            GROUP BY m.id) sub) sub2
               LEFT JOIN sale_jobs j ON ((sub2.sale_job_id = j.id)))
               LEFT JOIN material_metal_purities mp ON ((j.metal_purity_id = mp.id)))) with_j_p;
  SQL

  create_view "sale_job_views",  sql_definition: <<-SQL
      SELECT sale_orders.id AS id01,
      ((sale_orders."position" || '/'::text) || sale_jobs."position") AS "position",
      sale_orders."position" AS position_order,
      sale_orders.customer_id,
      sale_orders.taken_by_id,
      sale_orders.order_date,
      sale_orders.delivery_date,
      sale_orders.mfg_product_type_id,
      sale_orders.description,
      sale_orders.created_at,
      sale_orders.updated_at,
      sale_jobs.id,
      sale_jobs.processable_type,
      sale_jobs.processable_id,
      sale_jobs.final,
      sale_jobs."position" AS position_job,
      sale_jobs.design_style_id,
      sale_jobs.user_company_id,
      sale_jobs.material_material_id,
      sale_jobs.metal_purity_id,
      sale_jobs.metal_color_id,
      sale_jobs.qty,
      sale_jobs.diamond_clarity_id,
      sale_jobs.diamond_color_id,
      sale_jobs.cs_clarity_id,
      sale_jobs.cs_color_id,
      sale_jobs.instruction,
      sale_jobs.item_size,
      sale_jobs.mfg_priority_id,
      sale_jobs.mfg_department_id,
      sale_jobs.created_at AS created_at01,
      sale_jobs.updated_at AS updated_at01,
      s.receive_weight AS gross_weight,
      s.net_weight,
      s.pure_weight
     FROM sale_orders,
      (sale_jobs
       LEFT JOIN LATERAL ( SELECT t.receive_weight,
              t.net_weight,
              t.pure_weight
             FROM mfg_mfg_txn_views t
            WHERE (t.sale_job_id = sale_jobs.id)
            ORDER BY t.created_at DESC
           LIMIT 1) s ON (true))
    WHERE (sale_orders.id = sale_jobs.processable_id);
  SQL

  create_view "design_style_views",  sql_definition: <<-SQL
      SELECT s2.id,
      s2.design_collection_id,
      s2."position",
      s2.slug,
      s2.design_making_type_id,
      s2.design_setting_type_id,
      s2.user_account_id,
      s2.material_material_id,
      s2.material_metal_purity_id,
      s2.material_color_id,
      s2.net_weight,
      (s2.net_weight / m.specific_density) AS volume,
      ((s2.net_weight * m.purity) / (100)::numeric) AS pure_weight,
      s2.description,
      s2.active,
      s2.diamond_pcs,
      s2.diamond_weight,
      s2.cs_pcs,
      s2.cs_weight,
      (s2.net_weight + ((s2.diamond_weight + s2.cs_weight) / (5)::numeric)) AS gross_weight,
      s2.created_at,
      s2.updated_at
     FROM (( SELECT s.id,
              s."position",
              s.slug,
              s.design_collection_id,
              s.design_making_type_id,
              s.design_setting_type_id,
              s.user_account_id,
              s.material_material_id,
              s.material_metal_purity_id,
              s.material_color_id,
              s.net_weight,
              s.description,
              s.active,
              s.created_at,
              s.updated_at,
              sum(d.pcs) FILTER (WHERE (d.material_material_id = 4)) AS diamond_pcs,
              sum(d.pcs) FILTER (WHERE (d.material_material_id = 5)) AS cs_pcs,
              sum(d.weight) FILTER (WHERE (d.material_material_id = 4)) AS diamond_weight,
              sum(d.weight) FILTER (WHERE (d.material_material_id = 5)) AS cs_weight
             FROM ((design_styles s
               LEFT JOIN design_style_diamonds sd ON ((sd.design_style_id = s.id)))
               LEFT JOIN design_diamond_views d ON ((sd.design_diamond_id = d.id)))
            GROUP BY s.id) s2
       LEFT JOIN material_metal_purities m ON ((s2.material_metal_purity_id = m.id)));
  SQL

  create_view "sale_sale_views",  sql_definition: <<-SQL
      SELECT s.id,
      s."position",
      s.customer_id,
      s.sold_by_id,
      s.date,
      s.due_date,
      s.description,
      s.created_at,
      s.updated_at
     FROM sale_sales s;
  SQL

  create_view "sale_m_txn_views",  sql_definition: <<-SQL
      SELECT m.id,
      m."position",
      m.sale_m_txn_type_id,
      m.user_account_id,
      m.taken_by_id,
      m.date,
      m.due_date,
      m.description,
      m.created_at,
      m.updated_at
     FROM sale_m_txns m;
  SQL

  create_view "mfg_wax_setting_views",  sql_definition: <<-SQL
      SELECT w.id,
      w."position",
      w.date,
      w.slug,
      w.employee_id,
      w.description,
      w.created_at,
      w.updated_at
     FROM mfg_wax_settings w;
  SQL

  create_view "mfg_metal_issue_views",  sql_definition: <<-SQL
      SELECT mi.id,
      mi."position",
      mi.date,
      mi.slug,
      mi.employee_id,
      mi.description,
      mi.created_at,
      mi.updated_at
     FROM mfg_metal_issues mi;
  SQL

  create_view "mfg_casting_views",  sql_definition: <<-SQL
      SELECT c.id,
      c."position",
      c.date,
      c.slug,
      c.employee_id,
      c.description,
      c.created_at,
      c.updated_at
     FROM mfg_castings c;
  SQL

  create_view "mfg_refining_views",  sql_definition: <<-SQL
      SELECT mr.id,
      mr."position",
      mr.user_account_id,
      mr.date_from,
      mr.date_to,
      mr.user_company_id,
      mr.mfg_department_id,
      mr.dust_weight,
      mr.description,
      mr.created_at,
      mr.updated_at
     FROM mfg_refinings mr;
  SQL

  create_view "sale_memo_views",  sql_definition: <<-SQL
      SELECT m.id,
      m."position",
      m.date,
      m.description,
      m.created_at,
      m.updated_at
     FROM sale_memos m;
  SQL

  create_view "user_account_views",  sql_definition: <<-SQL
      SELECT a.id,
      a.user_account_type_id,
      a."position",
      a.join_date,
      a.slug,
      a.account_name,
      a.email,
      a.salary,
      a.user_role_id,
      a.parent_id,
      a.created_at,
      a.updated_at,
      a.encrypted_password,
      a.reset_password_token,
      a.reset_password_sent_at,
      a.remember_created_at,
      a.sign_in_count,
      a.current_sign_in_at,
      a.last_sign_in_at,
      a.current_sign_in_ip,
      a.last_sign_in_ip
     FROM user_accounts a;
  SQL

end
