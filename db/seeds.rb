# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create!([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create!(name: 'Luke', movie: movies.first)
#----------------1------------------
User::AccountType.create!(slug: "O", account_type: "Office Staff")  # id:1,
User::AccountType.create!(slug: "C", account_type: "Customer")      # id:2,
User::AccountType.create!(slug: "S", account_type: "Supplier")      # id:3,
User::AccountType.create!(slug: "E", account_type: "Employee")      # id:4,
User::AccountType.create!(slug: "D", account_type: "Designer")      # id:5,


#----------User---
#----------
User::Role.create!(slug: "A", role: "Admin")
User::Role.create!(slug: "P", role: "Production")
User::Role.create!(slug: "D", role: "Diamond Department")
#--------
# Slug only allows letters and numbers
# User::Account.create!(user_account_type_id: 1, slug: 'OmkarJewels', join_date:'20-07-2017' ,account_name: 'SARANG JEWEL', salary: 15000, email: 'chetan@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 1, slug: 'SukhiJewels', join_date:'1-07-2017' ,account_name: 'Sukhi Jewels', salary: 15000, email: 'chetan1@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 2, slug: 'AmarTools', join_date:'2-07-2017' ,account_name: 'Amar Tools', salary: 15000, email: 'chetan2@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 2, slug: 'HappyTools', join_date:'3-07-2017' ,account_name: 'Happy Tools', salary: 15000, email: 'chetan3@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 3, slug: 'AsmaWorker', join_date:'4-07-2017' ,account_name: 'Asma Worker', salary: 15000, email: 'chetan4@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 3, slug: 'RhonDey', join_date:'5-07-2017' ,account_name: 'Rhon Dey', salary: 15000, email: 'chetan5@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 4, slug: 'Nikunj', join_date:'6-07-2017' ,account_name: 'Nikunj', salary: 15000, email: 'chetan6@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 4, slug: 'Manish', join_date:'7-07-2017' ,account_name: 'Manish', salary: 15000, email: 'chetan7@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 5, slug: 'Nayan', join_date:'8-07-2017' ,account_name: 'Nayan', salary: 15000, email: 'chetan8@gmail.com', password: '123456', user_role_id: 1)
# User::Account.create!(user_account_type_id: 5, slug: 'Ravi', join_date:'9-07-2017' ,account_name: 'Ravi', salary: 15000, email: 'chetan9@gmail.com', password: '123456', user_role_id: 1)

User::Country.create!(slug: "IN",country: "India")  # id: 1,
User::State.create!(user_country_id: 1, slug: "GJ",state: "Gujarat")  # id: 1,
User::State.create!(user_country_id: 1, slug: "DH",state: "DELHI")    # id: 2,
# User::Company.create!(slug: "SCE",company: "SCESOFTWARE",gst_no: 123)

# User::Address.create!(firstname: "Kapil",lastname: "Pipaliya",address1: "Mahidharpura",address2: "Lal Gate",city: "Surat",zipcode: "Gujarat",user_state_id: 1,user_country_id: 1,phone: "9898989898",alternative_phone: "9016677642",company: "SceSoftware")

# User::Contact.create!(user_account_id: 1, first_name: "Kapil", last_name: "Pipaliya", email: "kapil.pipaliya@yahoo.com", phone: '9016677642', mobile: '9016677642')
# User::Contact.create!(user_account_id: 1, first_name: "Manish", last_name: "Bhagwakar", email: "manishnagdewani@gmail.com", phone: '13214657897', mobile: '456465479')
# User::Contact.create!(user_account_id: 1, first_name: "Dipakar", last_name: "Gupta", email: "Dipakar@gmail.com", phone: '346545679', mobile: '56455464654')
# User::Contact.create!(user_account_id: 1, first_name: "Asma", last_name: "Malik", email: "Asma@gmail.com", phone: '465465456465', mobile: '456465456')
# User::Contact.create!(user_company_id: 1, first_name: "Rhon", last_name: "Chirinos", email: "Rhon@gmail.com", phone: '5465456456', mobile: '456456456456')
# User::Contact.create!(user_company_id: 1, first_name: "Dilkush", last_name: "Soni", email: "Dilkush@gmail.com", phone: '5465456456', mobile: '4564564654564')
# User::Contact.create!(user_company_id: 1, first_name: "Mahmud", last_name: "Hasan", email: "Mahmud@gmail.com", phone: '41654654564', mobile: '54564564564')
# User::Contact.create!(user_company_id: 1, first_name: "Amit", last_name: "Singh", email: "Amit@gmail.com", phone: '446545645654', mobile: '45456456465')

User::TaxType.create!(slug: 'CGST', tax_type: 'CGST')
User::TaxType.create!(slug: 'SGST', tax_type: 'SGST')
User::TaxType.create!(slug: 'IGST', tax_type: 'IGST')
User::TaxType.create!(slug: 'UTGST', tax_type: 'UTGST')
User::TaxType.create!(slug: 'Cess', tax_type: 'Cess')
User::TaxType.create!(slug: 'Other', tax_type: 'Other')

User::TaxRate.create!(user_tax_type_id: 1, slug: 'GST0', name: 'GST 0%', rate: 0.0)
User::TaxRate.create!(user_tax_type_id: 1, slug: 'GST5', name: 'GST 5%', rate: 5)
User::TaxRate.create!(user_tax_type_id: 1, slug: 'GST12', name: 'GST 12%', rate: 12)


#---------------2-------------------
Material::MaterialType.create!(slug: "M", material_type: "Metal") # id:1,
Material::Material.create!(material_material_type_id: 1, slug: "G", material_name: "Gold")
Material::MetalPurity.create!(material_material_id: 1, slug: "G999", name: "G999K", purity: 99.90, specific_density: 19.30, price: 2821, description: "Gold 24 karat")
Material::MetalPurity.create!(material_material_id: 1, slug: "G995", name: "G995K", purity: 99.50, specific_density: 19.22, price: 2809, description: "the minimum allowed in Good Delivery gold bars")
Material::MetalPurity.create!(material_material_id: 1, slug: "G23", name: "G23K", purity: 95.83, specific_density: 18.51, price: 2706, description: "Gold")
Material::MetalPurity.create!(material_material_id: 1, slug: "G22", name: "G22K", purity: 91.60, specific_density: 17.696, price: 2586, description: "Gold")
Material::MetalPurity.create!(material_material_id: 1, slug: "G20", name: "G20K", purity: 83.40, specific_density: 16.112, price: 3000, description: "Gold")
Material::MetalPurity.create!(material_material_id: 1, slug: "G18", name: "G18K", purity: 75.00, specific_density: 14.489, price: 2355, description: "Gold")
Material::MetalPurity.create!(material_material_id: 1, slug: "G15", name: "G15K", purity: 62.50, specific_density: 12.075, price: 1764, description: "Gold")
Material::MetalPurity.create!(material_material_id: 1, slug: "G14", name: "G14K", purity: 58.50, specific_density: 11.302, price: 1654, description: "Gold")
Material::Material.create!(material_material_type_id: 1, slug: "S", material_name: "Silver")
Material::MetalPurity.create!(material_material_id: 2, slug: "S999", name: "S999K", purity: 99.90, specific_density: 10.5, price: 37.565, description: "used in Good Delivery bullion bars")
Material::MetalPurity.create!(material_material_id: 2, slug: "S925", name: "S925K", purity: 92.50, specific_density: 10.3, price: 21.997, description: "Sterling silver")
Material::Material.create!(material_material_type_id: 1, slug: "A", material_name: "Alloy")
Material::MetalPurity.create!(material_material_id: 3, slug: "AG18", name: "AGOLD18", purity: 100.00, specific_density: 9.00, price: 10, description: "Gold Alloy")


Material::MaterialType.create!(slug: "G", material_type: "Gem")
Material::Material.create!(material_material_type_id: 2, slug: "D", material_name: "Diamond")
Material::GemClarity.create!( material_material_id: 4, slug: "PD", clarity: "PD")
Material::GemClarity.create!( material_material_id: 4, slug: "CZ", clarity: "CZ")
Material::GemShape.create!( material_material_id: 4, slug: "R", shape: "Round")
# k = Material::GemSize.create!( material_material_id: 4, material_gem_shape_id: 1, slug: "MM", sieve_size: "1MM", carat_weight: 0.100)
Material::GemShape.create!( material_material_id: 4, slug: "B", shape: "Bugget")
Material::GemShape.create!( material_material_id: 4, slug: "P", shape: "Princess")
Material::GemShape.create!( material_material_id: 4, slug: "C", shape: "Cushion")
Material::GemShape.create!( material_material_id: 4, slug: "M", shape: "Marquise")
Material::GemShape.create!( material_material_id: 4, slug: "E", shape: "Emerald")
Material::GemShape.create!( material_material_id: 4, slug: "PR", shape: "Pear")
Material::GemShape.create!( material_material_id: 4, slug: "O", shape: "Oval")

Material::Material.create!(material_material_type_id: 2, slug: "C", material_name: "Color Stone")
Material::GemShape.create!( material_material_id: 5, slug: "CPP", shape: "PEAR")
Material::GemShape.create!( material_material_id: 5, slug: "CPM", shape: "MARQUIES")
Material::GemShape.create!( material_material_id: 5, slug: "CPO", shape: "OVAL")
Material::GemShape.create!( material_material_id: 5, slug: "CPR", shape: "ROUND")
Material::GemShape.create!( material_material_id: 5, slug: "CPE", shape: "EMERALD")
Material::GemClarity.create!( material_material_id: 5, slug: "CSPD", clarity: "CSPD")
Material::GemClarity.create!( material_material_id: 5, slug: "CSCZ", clarity: "CSCZ")


Material::RateOn.create!( material_material_type_id: 1, slug: "N", rateon: "On Net Rate")
Material::RateOn.create!( material_material_type_id: 1, slug: "P", rateon: "On Pure Rate")
Material::RateOn.create!( material_material_type_id: 1, slug: "F", rateon: "Fixed Rate")
Material::RateOn.create!( material_material_type_id: 2, slug: "Kt", rateon: "On Karat")
Material::RateOn.create!( material_material_type_id: 2, slug: "Pcs", rateon: "On Pcs")
Material::RateOn.create!( material_material_type_id: 2, slug: "Fix", rateon: "Fixed DRate")
#----------------3-----------------
Material::Color.create!(material_material_id: 1, slug: "MY", color: "Yellow")
Material::Color.create!(material_material_id: 1, slug: "MP", color: "Pink")
Material::Color.create!(material_material_id: 1, slug: "MW", color: "White")
Material::Color.create!(material_material_id: 1, slug: "MG", color: "Green")
Material::Color.create!(material_material_id: 2, slug: "SW", color: "White")
Material::Color.create!(material_material_id: 3, slug: "AY", color: "Yellow")
Material::Color.create!(material_material_id: 3, slug: "AP", color: "Pink")
Material::Color.create!(material_material_id: 3, slug: "AW", color: "White")
Material::Color.create!(material_material_id: 3, slug: "AG", color: "Green")
Material::Color.create!(material_material_id: 4, slug: "DPDW", color: "PD White")
Material::Color.create!(material_material_id: 4, slug: "DCZW", color: "CZ White")
Material::Color.create!(material_material_id: 5, slug: "CSW", color: "White")
Material::Color.create!(material_material_id: 5, slug: "CSRED", color: "Red")

#----------
# Material::Locker.create!( user_company_id: 1, slug: "C", locker: "Center")
# Material::Locker.create!( user_company_id: 1, slug: "F", locker: "Filling")
#---------------------------
Material::Accessory.create!( slug: "C", accessory: "Chaki")
Material::Accessory.create!( slug: "W", accessory: "Wire")
Material::Accessory.create!( slug: "S", accessory: "Solder")
#----------
Material::GemType.create!( slug: "R", gem_type: "Regular")
Material::GemType.create!( slug: "B", gem_type: "Broekn")
Material::GemType.create!( slug: "M", gem_type: "Missing")




#-------Design-------
w = Design::Collection.create!( slug: "WOMEN", collection: "Women")
w.children.create!( slug: "WR", collection: "Women Ring")
w.children.create!( slug: "WPD", collection: "Women Pendendt")
w.children.create!( slug: "WPDE", collection: "Women Pendendt Earrings")
w.children.create!( slug: "NCK", collection: "Women Nacklece")
w.children.create!( slug: "NCKE", collection: "Women Nacklece Earrings")
w.children.create!( slug: "WBR", collection: "Women Bracelet")
w.children.create!( slug: "WBN", collection: "Women Bangle")
w.children.create!( slug: "ER", collection: "Women Earring")
w.children.create!( slug: "MR", collection: "Men Ring")
m = Design::Collection.create!( slug: "MEN", collection: "Men")
m.children.create!( slug: "MPD", collection: "Men Pendendt")
m.children.create!( slug: "MBR", collection: "Men Bracelet")
m.children.create!( slug: "MCHAIN", collection: "Men Chain")
1#-------
Design::MakingType.create!( slug: "C", making_type: "Casting")
#--------
Design::SettingType.create!( slug: "M", setting_type: "Micro")
Design::SettingType.create!( slug: "I", setting_type: "Invisible")


#---------MFG----------
Mfg::Priority.create!( slug: "U", priority: "Urgent")
Mfg::Priority.create!( slug: "N", priority: "Normal")
Mfg::Priority.create!( slug: "L", priority: "Low")
#---------
Mfg::ProductType.create!( slug: "S", product_type: "Sample Line")
Mfg::ProductType.create!( slug: "R", product_type: "Regular Jobs")
#---------
s = Mfg::Department.create!( slug: "S", department: "Sale")
m = Mfg::Department.create!( slug: "M", department: "Manufacturing")
#---------
s.children.create!( slug: "B", department: "Bag Preparation")
s.children.create!( slug: "SB", department: "Stock Book")
s.children.create!( slug: "SO", department: "Sold")
s.children.create!( slug: "Memo", department: "Memo")
m.children.create!( slug: "C", department: "CENTER")
m.children.create!( slug: "WX", department: "WAX")
m.children.create!( slug: "CST", department: "CASTING")
m.children.create!( slug: "FIL", department: "FILLING")
m.children.create!( slug: "PRE", department: "PRE-POLISH")
m.children.create!( slug: "SET", department: "SETTING")
m.children.create!( slug: "FINAL", department: "FINAL-POLISH")
m.children.create!( slug: "FG", department: "FG")

Mfg::DustResource.create!(position:1, slug:"FIL", dust_resource:"Filling")
Mfg::DustResource.create!(position:2, slug:"PPOL", dust_resource:"PrePolish")
Mfg::DustResource.create!(position:3, slug:"SET", dust_resource:"Setting")
Mfg::DustResource.create!(position:4, slug:"FPOL", dust_resource:"FinalPolish")

#-------Sale
Sale::OrderType.create!( slug: "REG", order_type: "Regular Order")
Sale::OrderType.create!( slug: "REP", order_type: "Repair Order")

Sale::MTxnType.create!( slug: "CRCV", txn_type: "Customer Receive")
Sale::MTxnType.create!( slug: "CRTN", txn_type: "Customer Return")
Sale::MTxnType.create!( slug: "MPUR", txn_type: "Material Purchase")
Sale::MTxnType.create!( slug: "MSALE", txn_type: "Material Sale")
Sale::MTxnType.create!( slug: "MTRFR", txn_type: "Material Transfer")

#==============================================================================
b = Material::GemClarity.find(1)
b1 = Material::GemClarity.find(2)
c = Material::GemClarity.find(3)
c1 = Material::GemClarity.find(4)

Material::GemSize.create!( id: 1, material_material_id: 4, material_gem_shape_id: 1, slug: "0000", mm_size: "0000", carat_weight: 0.003)
Material::GemSize.create!( id: 2, material_material_id: 4, material_gem_shape_id: 1, slug: "00 -000", mm_size: "00 -000", carat_weight: 0.004)
Material::GemSize.create!( id: 3, material_material_id: 4, material_gem_shape_id: 1, slug: "0-00", mm_size: "0-00", carat_weight: 0.005)
Material::GemSize.create!( id: 4, material_material_id: 4, material_gem_shape_id: 1, slug: "+0-1", mm_size: "+0-1", carat_weight: 0.006)
Material::GemSize.create!( id: 5, material_material_id: 4, material_gem_shape_id: 1, slug: "+1.0-1.5", mm_size: "+1.0-1.5", carat_weight: 0.007)
Material::GemSize.create!( id: 6, material_material_id: 4, material_gem_shape_id: 1, slug: "+1.5-2.0", mm_size: "+1.5-2.0", carat_weight: 0.008)
Material::GemSize.create!( id: 7, material_material_id: 4, material_gem_shape_id: 1, slug: "+2.0-2.5", mm_size: "+2.0-2.5", carat_weight: 0.009)
Material::GemSize.create!( id: 8, material_material_id: 4, material_gem_shape_id: 1, slug: "+2.5-3.0", mm_size: "+2.5-3.0", carat_weight: 0.01)
Material::GemSize.create!( id: 9, material_material_id: 4, material_gem_shape_id: 1, slug: "+3.0-3.5", mm_size: "+3.0-3.5", carat_weight: 0.011)
Material::GemSize.create!( id: 10, material_material_id: 4, material_gem_shape_id: 1, slug: "+3.5-4.0", mm_size: "+3.5-4.0", carat_weight: 0.012)
Material::GemSize.create!( id: 11, material_material_id: 4, material_gem_shape_id: 1, slug: "+4.0-4.5", mm_size: "+4.0-4.5", carat_weight: 0.014)
Material::GemSize.create!( id: 12, material_material_id: 4, material_gem_shape_id: 1, slug: "+4.5-5.0", mm_size: "+4.5-5.0", carat_weight: 0.015)
Material::GemSize.create!( id: 13, material_material_id: 4, material_gem_shape_id: 1, slug: "+5.0-5.5", mm_size: "+5.0-5.5", carat_weight: 0.016)
Material::GemSize.create!( id: 14, material_material_id: 4, material_gem_shape_id: 1, slug: "+5.5-6.0", mm_size: "+5.5-6.0", carat_weight: 0.018)
Material::GemSize.create!( id: 15, material_material_id: 4, material_gem_shape_id: 1, slug: "+6.0-6.5", mm_size: "+6.0-6.5", carat_weight: 0.02)
Material::GemSize.create!( id: 16, material_material_id: 4, material_gem_shape_id: 1, slug: "+6.5-7.0", mm_size: "+6.5-7.0", carat_weight: 0.025)
Material::GemSize.create!( id: 17, material_material_id: 4, material_gem_shape_id: 1, slug: "+7.0-7.5", mm_size: "+7.0-7.5", carat_weight: 0.03)
Material::GemSize.create!( id: 18, material_material_id: 4, material_gem_shape_id: 1, slug: "+7.5-8.0", mm_size: "+7.5-8.0", carat_weight: 0.035)
Material::GemSize.create!( id: 19, material_material_id: 4, material_gem_shape_id: 1, slug: "+8.0-8.5", mm_size: "+8.0-8.5", carat_weight: 0.04)
Material::GemSize.create!( id: 20, material_material_id: 4, material_gem_shape_id: 1, slug: "+8.5-9.0", mm_size: "+8.5-9.0", carat_weight: 0.045)
Material::GemSize.create!( id: 21, material_material_id: 4, material_gem_shape_id: 1, slug: "+9.0-9.5", mm_size: "+9.0-9.5", carat_weight: 0.05)
Material::GemSize.create!( id: 22, material_material_id: 4, material_gem_shape_id: 1, slug: "+9.5-10", mm_size: "+9.5-10", carat_weight: 0.06)
Material::GemSize.create!( id: 23, material_material_id: 4, material_gem_shape_id: 1, slug: "+10-10.5", mm_size: "+10-10.5", carat_weight: 0.065)
Material::GemSize.create!( id: 24, material_material_id: 4, material_gem_shape_id: 1, slug: "+10.5-11", mm_size: "+10.5-11", carat_weight: 0.07)
Material::GemSize.create!( id: 25, material_material_id: 4, material_gem_shape_id: 1, slug: "+11-11.5", mm_size: "+11-11.5", carat_weight: 0.08)
Material::GemSize.create!( id: 26, material_material_id: 4, material_gem_shape_id: 1, slug: "+11.5-12", mm_size: "+11.5-12", carat_weight: 0.09)
Material::GemSize.create!( id: 27, material_material_id: 4, material_gem_shape_id: 1, slug: "+12-12.5", mm_size: "+12-12.5", carat_weight: 0.1)
Material::GemSize.create!( id: 28, material_material_id: 4, material_gem_shape_id: 1, slug: "+12.5-13", mm_size: "+12.5-13", carat_weight: 0.11)
Material::GemSize.create!( id: 29, material_material_id: 4, material_gem_shape_id: 1, slug: "+13-13.5", mm_size: "+13-13.5", carat_weight: 0.12)
Material::GemSize.create!( id: 30, material_material_id: 4, material_gem_shape_id: 1, slug: "+13.5-14", mm_size: "+13.5-14", carat_weight: 0.13)
Material::GemSize.create!( id: 31, material_material_id: 4, material_gem_shape_id: 1, slug: "+14-14.5", mm_size: "+14-14.5", carat_weight: 0.146)
Material::GemSize.create!( id: 32, material_material_id: 4, material_gem_shape_id: 1, slug: "+14.5-15", mm_size: "+14.5-15", carat_weight: 0.153)
Material::GemSize.create!( id: 33, material_material_id: 4, material_gem_shape_id: 1, slug: "+15-15.5", mm_size: "+15-15.5", carat_weight: 0.165)
Material::GemSize.create!( id: 34, material_material_id: 4, material_gem_shape_id: 1, slug: "+15.5-16", mm_size: "+15.5-16", carat_weight: 0.195)
Material::GemSize.create!( id: 35, material_material_id: 4, material_gem_shape_id: 1, slug: "+16-16.5", mm_size: "+16-16.5", carat_weight: 0.21)
Material::GemSize.create!( id: 36, material_material_id: 4, material_gem_shape_id: 1, slug: "+16.5-17", mm_size: "+16.5-17", carat_weight: 0.239)
Material::GemSize.create!( id: 37, material_material_id: 4, material_gem_shape_id: 1, slug: "+17-17.5", mm_size: "+17-17.5", carat_weight: 0.251)
Material::GemSize.create!( id: 38, material_material_id: 4, material_gem_shape_id: 1, slug: "+17.5-18", mm_size: "+17.5-18", carat_weight: 0.264)
Material::GemSize.create!( id: 39, material_material_id: 4, material_gem_shape_id: 1, slug: "+18-18.5", mm_size: "+18-18.5", carat_weight: 0.3)
Material::GemSize.create!( id: 40, material_material_id: 4, material_gem_shape_id: 1, slug: "+18.5-19", mm_size: "+18.5-19", carat_weight: 0.31)
Material::GemSize.create!( id: 41, material_material_id: 4, material_gem_shape_id: 1, slug: "+19-19.5", mm_size: "+19-19.5", carat_weight: 0.33)
Material::GemSize.create!( id: 42, material_material_id: 4, material_gem_shape_id: 1, slug: "+19.5-20", mm_size: "+19.5-20", carat_weight: 0.38)
Material::GemSize.create!( id: 43, material_material_id: 4, material_gem_shape_id: 1, slug: "+20-20.5", mm_size: "+20-20.5", carat_weight: 0.39)
Material::GemSize.create!( id: 44, material_material_id: 4, material_gem_shape_id: 1, slug: "+20.5", mm_size: "+20.5", carat_weight: 0.4)

for i in 1..44
  k = Material::GemSize.find(i)
  k.material_gem_clarities << b
  k.material_gem_clarities << b1
end

Material::GemSize.create!( id: 45, material_material_id: 4, material_gem_shape_id: 2, slug: "1", mm_size: "1mm", carat_weight: 0.003)
Material::GemSize.create!( id: 46, material_material_id: 4, material_gem_shape_id: 2, slug: "1.1", mm_size: "1.1", carat_weight: 0.004)
Material::GemSize.create!( id: 47, material_material_id: 4, material_gem_shape_id: 2, slug: "1.2", mm_size: "1.2", carat_weight: 0.005)
Material::GemSize.create!( id: 48, material_material_id: 4, material_gem_shape_id: 2, slug: "1.3", mm_size: "1.3", carat_weight: 0.006)
Material::GemSize.create!( id: 49, material_material_id: 4, material_gem_shape_id: 2, slug: "1.4", mm_size: "1.4", carat_weight: 0.007)
Material::GemSize.create!( id: 50, material_material_id: 4, material_gem_shape_id: 2, slug: "1.5", mm_size: "1.5", carat_weight: 0.008)
Material::GemSize.create!( id: 51, material_material_id: 4, material_gem_shape_id: 2, slug: "1.6", mm_size: "1.6", carat_weight: 0.009)
Material::GemSize.create!( id: 52, material_material_id: 4, material_gem_shape_id: 2, slug: "1.7", mm_size: "1.7", carat_weight: 0.01)
Material::GemSize.create!( id: 53, material_material_id: 4, material_gem_shape_id: 2, slug: "1.8", mm_size: "1.8", carat_weight: 0.012)
Material::GemSize.create!( id: 54, material_material_id: 4, material_gem_shape_id: 2, slug: "1.9", mm_size: "1.9", carat_weight: 0.014)
Material::GemSize.create!( id: 55, material_material_id: 4, material_gem_shape_id: 2, slug: "2", mm_size: "2mm", carat_weight: 0.016)
Material::GemSize.create!( id: 56, material_material_id: 4, material_gem_shape_id: 2, slug: "2.1", mm_size: "2.1", carat_weight: 0.019)
Material::GemSize.create!( id: 57, material_material_id: 4, material_gem_shape_id: 2, slug: "2.2", mm_size: "2.2", carat_weight: 0.02)
Material::GemSize.create!( id: 58, material_material_id: 4, material_gem_shape_id: 2, slug: "2.3", mm_size: "2.3", carat_weight: 0.021)
Material::GemSize.create!( id: 59, material_material_id: 4, material_gem_shape_id: 2, slug: "2.4", mm_size: "2.4", carat_weight: 0.023)
Material::GemSize.create!( id: 60, material_material_id: 4, material_gem_shape_id: 2, slug: "2.5", mm_size: "2.5", carat_weight: 0.025)
Material::GemSize.create!( id: 61, material_material_id: 4, material_gem_shape_id: 2, slug: "2.6", mm_size: "2.6", carat_weight: 0.029)
Material::GemSize.create!( id: 62, material_material_id: 4, material_gem_shape_id: 2, slug: "2.7", mm_size: "2.7", carat_weight: 0.032)
Material::GemSize.create!( id: 63, material_material_id: 4, material_gem_shape_id: 2, slug: "2.8", mm_size: "2.8", carat_weight: 0.035)
Material::GemSize.create!( id: 64, material_material_id: 4, material_gem_shape_id: 2, slug: "2.9", mm_size: "2.9", carat_weight: 0.038)
Material::GemSize.create!( id: 65, material_material_id: 4, material_gem_shape_id: 2, slug: "3", mm_size: "3mm", carat_weight: 0.041)
Material::GemSize.create!( id: 66, material_material_id: 4, material_gem_shape_id: 2, slug: "3.1", mm_size: "3.1", carat_weight: 0.045)
Material::GemSize.create!( id: 67, material_material_id: 4, material_gem_shape_id: 2, slug: "3.2", mm_size: "3.2", carat_weight: 0.05)
Material::GemSize.create!( id: 68, material_material_id: 4, material_gem_shape_id: 2, slug: "3.3", mm_size: "3.3", carat_weight: 0.052)
Material::GemSize.create!( id: 69, material_material_id: 4, material_gem_shape_id: 2, slug: "3.4", mm_size: "3.4", carat_weight: 0.066)
Material::GemSize.create!( id: 70, material_material_id: 4, material_gem_shape_id: 2, slug: "3.5", mm_size: "3.5", carat_weight: 0.076)
Material::GemSize.create!( id: 71, material_material_id: 4, material_gem_shape_id: 2, slug: "3.6", mm_size: "3.6", carat_weight: 0.08)
Material::GemSize.create!( id: 72, material_material_id: 4, material_gem_shape_id: 2, slug: "3.7", mm_size: "3.7", carat_weight: 0.084)
Material::GemSize.create!( id: 73, material_material_id: 4, material_gem_shape_id: 2, slug: "3.8", mm_size: "3.8", carat_weight: 0.1)
Material::GemSize.create!( id: 74, material_material_id: 4, material_gem_shape_id: 2, slug: "3.9", mm_size: "3.9", carat_weight: 0.11)
Material::GemSize.create!( id: 75, material_material_id: 4, material_gem_shape_id: 2, slug: "4", mm_size: "4mm", carat_weight: 0.122)
Material::GemSize.create!( id: 76, material_material_id: 4, material_gem_shape_id: 2, slug: "4.1", mm_size: "4.1", carat_weight: 0.13)
Material::GemSize.create!( id: 77, material_material_id: 4, material_gem_shape_id: 2, slug: "4.2", mm_size: "4.2", carat_weight: 0.148)
Material::GemSize.create!( id: 78, material_material_id: 4, material_gem_shape_id: 2, slug: "4.3", mm_size: "4.3", carat_weight: 0.15)
Material::GemSize.create!( id: 79, material_material_id: 4, material_gem_shape_id: 2, slug: "4.4", mm_size: "4.4", carat_weight: 0.164)
Material::GemSize.create!( id: 80, material_material_id: 4, material_gem_shape_id: 2, slug: "4.5", mm_size: "4.5", carat_weight: 0.172)
Material::GemSize.create!( id: 81, material_material_id: 4, material_gem_shape_id: 2, slug: "4.6", mm_size: "4.6", carat_weight: 0.18)

for i in 45..81
  k = Material::GemSize.find(i)
  k.material_gem_clarities << b
  k.material_gem_clarities << b1
end

Material::GemSize.create!( id: 82, material_material_id: 4, material_gem_shape_id: 3, slug: "00-0", mm_size: "00-0", carat_weight: 0.0062)
Material::GemSize.create!( id: 83, material_material_id: 4, material_gem_shape_id: 3, slug: "0-1", mm_size: "0-1", carat_weight: 0.008)
Material::GemSize.create!( id: 84, material_material_id: 4, material_gem_shape_id: 3, slug: "1--1.5", mm_size: "1--1.5", carat_weight: 0.009)
Material::GemSize.create!( id: 85, material_material_id: 4, material_gem_shape_id: 3, slug: "1.5--2", mm_size: "1.5--2", carat_weight: 0.01)
Material::GemSize.create!( id: 86, material_material_id: 4, material_gem_shape_id: 3, slug: "2--2.5", mm_size: "2--2.5", carat_weight: 0.012)
Material::GemSize.create!( id: 87, material_material_id: 4, material_gem_shape_id: 3, slug: "2.5--3", mm_size: "2.5--3", carat_weight: 0.014)
Material::GemSize.create!( id: 88, material_material_id: 4, material_gem_shape_id: 3, slug: "3--3.5", mm_size: "3--3.5", carat_weight: 0.015)
Material::GemSize.create!( id: 89, material_material_id: 4, material_gem_shape_id: 3, slug: "3.5--4", mm_size: "3.5--4", carat_weight: 0.017)
Material::GemSize.create!( id: 90, material_material_id: 4, material_gem_shape_id: 3, slug: "4--4.5", mm_size: "4--4.5", carat_weight: 0.019)
Material::GemSize.create!( id: 91, material_material_id: 4, material_gem_shape_id: 3, slug: "4.5--5", mm_size: "4.5--5", carat_weight: 0.02)
Material::GemSize.create!( id: 92, material_material_id: 4, material_gem_shape_id: 3, slug: "5--5.5", mm_size: "5--5.5", carat_weight: 0.023)
Material::GemSize.create!( id: 93, material_material_id: 4, material_gem_shape_id: 3, slug: "5.5--6", mm_size: "5.5--6", carat_weight: 0.026)
Material::GemSize.create!( id: 94, material_material_id: 4, material_gem_shape_id: 3, slug: "6--6.5", mm_size: "6--6.5", carat_weight: 0.03)
Material::GemSize.create!( id: 95, material_material_id: 4, material_gem_shape_id: 3, slug: "6.5--7", mm_size: "6.5--7", carat_weight: 0.035)
Material::GemSize.create!( id: 96, material_material_id: 4, material_gem_shape_id: 3, slug: "7--7.5", mm_size: "7--7.5", carat_weight: 0.043)
Material::GemSize.create!( id: 97, material_material_id: 4, material_gem_shape_id: 3, slug: "7.5--8", mm_size: "7.5--8", carat_weight: 0.05)
Material::GemSize.create!( id: 98, material_material_id: 4, material_gem_shape_id: 3, slug: "8--8.5", mm_size: "8--8.5", carat_weight: 0.058)
Material::GemSize.create!( id: 99, material_material_id: 4, material_gem_shape_id: 3, slug: "8.5--9", mm_size: "8.5--9", carat_weight: 0.065)
Material::GemSize.create!( id: 100, material_material_id: 4, material_gem_shape_id: 3, slug: "9--9.5", mm_size: "9--9.5", carat_weight: 0.075)
Material::GemSize.create!( id: 101, material_material_id: 4, material_gem_shape_id: 3, slug: "9.5--10", mm_size: "9.5--10", carat_weight: 0.085)
Material::GemSize.create!( id: 102, material_material_id: 4, material_gem_shape_id: 3, slug: "10--10.5", mm_size: "10--10.5", carat_weight: 0.1)
Material::GemSize.create!( id: 103, material_material_id: 4, material_gem_shape_id: 3, slug: "10.5--11", mm_size: "10.5--11", carat_weight: 0.11)
Material::GemSize.create!( id: 104, material_material_id: 4, material_gem_shape_id: 3, slug: "11--11.5", mm_size: "11--11.5", carat_weight: 0.123)
Material::GemSize.create!( id: 105, material_material_id: 4, material_gem_shape_id: 3, slug: "11.5--12", mm_size: "11.5--12", carat_weight: 0.137)
Material::GemSize.create!( id: 106, material_material_id: 4, material_gem_shape_id: 3, slug: "12--12.5", mm_size: "12--12.5", carat_weight: 0.15)
Material::GemSize.create!( id: 107, material_material_id: 4, material_gem_shape_id: 3, slug: "12.5--13", mm_size: "12.5--13", carat_weight: 0.169)
Material::GemSize.create!( id: 108, material_material_id: 4, material_gem_shape_id: 3, slug: "13--13.5", mm_size: "13--13.5", carat_weight: 0.186)
Material::GemSize.create!( id: 109, material_material_id: 4, material_gem_shape_id: 3, slug: "13.5--14", mm_size: "13.5--14", carat_weight: 0.2)
Material::GemSize.create!( id: 110, material_material_id: 4, material_gem_shape_id: 3, slug: "14--14.5", mm_size: "14--14.5", carat_weight: 0.224)
Material::GemSize.create!( id: 111, material_material_id: 4, material_gem_shape_id: 3, slug: "14.5--15", mm_size: "14.5--15", carat_weight: 0.25)
Material::GemSize.create!( id: 112, material_material_id: 4, material_gem_shape_id: 3, slug: "15--15.5", mm_size: "15--15.5", carat_weight: 0.268)
Material::GemSize.create!( id: 113, material_material_id: 4, material_gem_shape_id: 3, slug: "15.5--16", mm_size: "15.5--16", carat_weight: 0.291)
Material::GemSize.create!( id: 114, material_material_id: 4, material_gem_shape_id: 3, slug: "16--16.5", mm_size: "16--16.5", carat_weight: 0.316)
Material::GemSize.create!( id: 115, material_material_id: 4, material_gem_shape_id: 3, slug: "16.5--17", mm_size: "16.5--17", carat_weight: 0.33)
Material::GemSize.create!( id: 116, material_material_id: 4, material_gem_shape_id: 3, slug: "17--17.5", mm_size: "17--17.5", carat_weight: 0.37)
Material::GemSize.create!( id: 117, material_material_id: 4, material_gem_shape_id: 3, slug: "17.5--18", mm_size: "17.5--18", carat_weight: 0.4)
Material::GemSize.create!( id: 118, material_material_id: 4, material_gem_shape_id: 3, slug: "18--18.5", mm_size: "18--18.5", carat_weight: 0.43)
Material::GemSize.create!( id: 119, material_material_id: 4, material_gem_shape_id: 3, slug: "18.5--19", mm_size: "18.5--19", carat_weight: 0.465)
Material::GemSize.create!( id: 120, material_material_id: 4, material_gem_shape_id: 3, slug: "19--19.5", mm_size: "19--19.5", carat_weight: 0.5)
Material::GemSize.create!( id: 121, material_material_id: 4, material_gem_shape_id: 3, slug: "19.5--20", mm_size: "19.5--20", carat_weight: 0.532)

for i in 82..121
  k = Material::GemSize.find(i)
  k.material_gem_clarities << b
  k.material_gem_clarities << b1
end

Material::GemSize.create!( id:122, material_material_id: 4, material_gem_shape_id: 6, slug: "4  X 3", mm_size: "4  X 3", carat_weight: 0.25)
Material::GemSize.create!( id:123, material_material_id: 4, material_gem_shape_id: 6, slug: "5  X 4 ", mm_size: "5  X 4 ", carat_weight: 0.33)
Material::GemSize.create!( id:124, material_material_id: 4, material_gem_shape_id: 6, slug: "6  X 4 ", mm_size: "6  X 4 ", carat_weight: 0.5)
Material::GemSize.create!( id:125, material_material_id: 4, material_gem_shape_id: 6, slug: "7 X 5", mm_size: "7 X 5", carat_weight: 1)
Material::GemSize.create!( id:126, material_material_id: 4, material_gem_shape_id: 6, slug: "8 X 6", mm_size: "8 X 6", carat_weight: 1.5)
Material::GemSize.create!( id:127, material_material_id: 4, material_gem_shape_id: 6, slug: "9 X 7", mm_size: "9 X 7", carat_weight: 2.5)
Material::GemSize.create!( id:128, material_material_id: 4, material_gem_shape_id: 6, slug: "10 X 8", mm_size: "10 X 8", carat_weight: 3)
Material::GemSize.create!( id:129, material_material_id: 4, material_gem_shape_id: 6, slug: "11 X 9", mm_size: "11 X 9", carat_weight: 4)
Material::GemSize.create!( id:130, material_material_id: 4, material_gem_shape_id: 6, slug: "12 X 10 ", mm_size: "12 X 10 ", carat_weight: 5)
Material::GemSize.create!( id:131, material_material_id: 4, material_gem_shape_id: 6, slug: "14 X 10", mm_size: "14 X 10", carat_weight: 6)
Material::GemSize.create!( id:132, material_material_id: 4, material_gem_shape_id: 6, slug: "14 X 12", mm_size: "14 X 12", carat_weight: 8)
Material::GemSize.create!( id:133, material_material_id: 4, material_gem_shape_id: 6, slug: "16 X 12", mm_size: "16 X 12", carat_weight: 10.5)
Material::GemSize.create!( id:134, material_material_id: 4, material_gem_shape_id: 6, slug: "18 X 13", mm_size: "18 X 13", carat_weight: 14.2)
Material::GemSize.create!( id:135, material_material_id: 4, material_gem_shape_id: 6, slug: "20 X 15", mm_size: "20 X 15", carat_weight: 20.3)
Material::GemSize.create!( id:136, material_material_id: 4, material_gem_shape_id: 6, slug: "25 X 18", mm_size: "25 X 18", carat_weight: 52.2)


for i in 122..136
  k = Material::GemSize.find(i)
  k.material_gem_clarities << b
  k.material_gem_clarities << b1
end

# PEAR:
# Material::GemSize.create!( id:137, material_material_id: 4, material_gem_shape_id: 7, slug: "+7", mm_size: "+7", carat_weight: 0.085)
Material::GemSize.create!( id:146, material_material_id: 4, material_gem_shape_id: 7, slug: "+7", mm_size: "+7", carat_weight: 0.09)
# Material::GemSize.create!( id:139, material_material_id: 4, material_gem_shape_id: 7, slug: "+13-18", mm_size: "+13-18", carat_weight: 0.14)
Material::GemSize.create!( id:147, material_material_id: 4, material_gem_shape_id: 7, slug: "+13-18", mm_size: "+13-18", carat_weight: 0.15)
# Material::GemSize.create!( id:141, material_material_id: 4, material_gem_shape_id: 7, slug: "+18-22.5", mm_size: "+18-22.5", carat_weight: 0.2)
# Material::GemSize.create!( id:142, material_material_id: 4, material_gem_shape_id: 7, slug: "+18-22.5", mm_size: "+18-22.5", carat_weight: 0.23)
Material::GemSize.create!( id:148, material_material_id: 4, material_gem_shape_id: 7, slug: "+18-22.5", mm_size: "+18-22.5", carat_weight: 0.25)
# Material::GemSize.create!( id:144, material_material_id: 4, material_gem_shape_id: 7, slug: "+22.5-30", mm_size: "+22.5-30", carat_weight: 0.3)
# Material::GemSize.create!( id:145, material_material_id: 4, material_gem_shape_id: 7, slug: "+22.5-30", mm_size: "+22.5-30", carat_weight: 0.33)
Material::GemSize.create!( id:149, material_material_id: 4, material_gem_shape_id: 7, slug: "+22.5-30", mm_size: "+22.5-30", carat_weight: 0.4)
# Material::GemSize.create!( id:147, material_material_id: 4, material_gem_shape_id: 7, slug: "+30-38", mm_size: "+30-38", carat_weight: 0.45)
Material::GemSize.create!( id:150, material_material_id: 4, material_gem_shape_id: 7, slug: "+30-38", mm_size: "+30-38", carat_weight: 0.5)
# Material::GemSize.create!( id:149, material_material_id: 4, material_gem_shape_id: 7, slug: "+38-45", mm_size: "+38-45", carat_weight: 0.6)
Material::GemSize.create!( id:151, material_material_id: 4, material_gem_shape_id: 7, slug: "+38-45", mm_size: "+38-45", carat_weight: 0.7)
Material::GemSize.create!( id:152, material_material_id: 4, material_gem_shape_id: 7, slug: "+45-70", mm_size: "+45-70", carat_weight: 0.75)
# Material::GemSize.create!( id:152, material_material_id: 4, material_gem_shape_id: 7, slug: "+70-90", mm_size: "+70-90", carat_weight: 0.78)
Material::GemSize.create!( id:153, material_material_id: 4, material_gem_shape_id: 7, slug: "+70-90", mm_size: "+70-90", carat_weight: 0.9)


for i in 146..153
  k = Material::GemSize.find(i)
  k.material_gem_clarities << b
  k.material_gem_clarities << b1
end

# MARQUIS
# Material::GemSize.create!( id:154, material_material_id: 4, material_gem_shape_id: 5, slug: "+7", mm_size: "+7", carat_weight: 0.07)
# Material::GemSize.create!( id:155, material_material_id: 4, material_gem_shape_id: 5, slug: "+7", mm_size: "+7", carat_weight: 0.09)
Material::GemSize.create!( id:157, material_material_id: 4, material_gem_shape_id: 5, slug: "+7", mm_size: "+7", carat_weight: 0.1)
# Material::GemSize.create!( id:157, material_material_id: 4, material_gem_shape_id: 5, slug: "+13-18", mm_size: "+13-18", carat_weight: 0.15)
Material::GemSize.create!( id:158, material_material_id: 4, material_gem_shape_id: 5, slug: "+13-18", mm_size: "+13-18", carat_weight: 0.15)
Material::GemSize.create!( id:159, material_material_id: 4, material_gem_shape_id: 5, slug: "+18-22.5", mm_size: "+18-22.5", carat_weight: 0.25)
Material::GemSize.create!( id:160, material_material_id: 4, material_gem_shape_id: 5, slug: "+22.5-30", mm_size: "+22.5-30", carat_weight: 0.33)
Material::GemSize.create!( id:161, material_material_id: 4, material_gem_shape_id: 5, slug: "+30-38", mm_size: "+30-38", carat_weight: 0.4)
Material::GemSize.create!( id:162, material_material_id: 4, material_gem_shape_id: 5, slug: "+38-45", mm_size: "+38-45", carat_weight: 0.5)
Material::GemSize.create!( id:163, material_material_id: 4, material_gem_shape_id: 5, slug: "+45-70", mm_size: "+45-70", carat_weight: 0.75)
Material::GemSize.create!( id:164, material_material_id: 4, material_gem_shape_id: 5, slug: "+70-90", mm_size: "+70-90", carat_weight: 0.85)


for i in 157..164
  k = Material::GemSize.find(i)
  k.material_gem_clarities << b
  k.material_gem_clarities << b1
end

# oval
Material::GemSize.create!( id:165, material_material_id: 4, material_gem_shape_id: 8, slug: "5 X 3 ", mm_size: "5 X 3 ", carat_weight: 0.25)
Material::GemSize.create!( id:166, material_material_id: 4, material_gem_shape_id: 8, slug: "6 X 4", mm_size: "6 X 4", carat_weight: 0.5)
Material::GemSize.create!( id:167, material_material_id: 4, material_gem_shape_id: 8, slug: "7 X 5", mm_size: "7 X 5", carat_weight: 1)
Material::GemSize.create!( id:168, material_material_id: 4, material_gem_shape_id: 8, slug: "8 X 6", mm_size: "8 X 6", carat_weight: 1.5)
Material::GemSize.create!( id:169, material_material_id: 4, material_gem_shape_id: 8, slug: "9 X 7", mm_size: "9 X 7", carat_weight: 2.5)
Material::GemSize.create!( id:170, material_material_id: 4, material_gem_shape_id: 8, slug: "10 X 8", mm_size: "10 X 8", carat_weight: 3)
Material::GemSize.create!( id:171, material_material_id: 4, material_gem_shape_id: 8, slug: "11 X 9", mm_size: "11 X 9", carat_weight: 4)
Material::GemSize.create!( id:172, material_material_id: 4, material_gem_shape_id: 8, slug: "12 X 10 ", mm_size: "12 X 10 ", carat_weight: 5)
Material::GemSize.create!( id:173, material_material_id: 4, material_gem_shape_id: 8, slug: "14 X 10", mm_size: "14 X 10", carat_weight: 6)
Material::GemSize.create!( id:174, material_material_id: 4, material_gem_shape_id: 8, slug: "14 X 12", mm_size: "14 X 12", carat_weight: 8)
Material::GemSize.create!( id:175, material_material_id: 4, material_gem_shape_id: 8, slug: "16 X 12", mm_size: "16 X 12", carat_weight: 10.5)
Material::GemSize.create!( id:176, material_material_id: 4, material_gem_shape_id: 8, slug: "18 X 13", mm_size: "18 X 13", carat_weight: 14.2)
Material::GemSize.create!( id:177, material_material_id: 4, material_gem_shape_id: 8, slug: "20 X 15", mm_size: "20 X 15", carat_weight: 20.3)
Material::GemSize.create!( id:178, material_material_id: 4, material_gem_shape_id: 8, slug: "22 X 15", mm_size: "22 X 15", carat_weight: 23.5)
Material::GemSize.create!( id:179, material_material_id: 4, material_gem_shape_id: 8, slug: "23 X 17", mm_size: "23 X 17", carat_weight: 32.2)
Material::GemSize.create!( id:180, material_material_id: 4, material_gem_shape_id: 8, slug: "25 X 18", mm_size: "25 X 18", carat_weight: 43)
Material::GemSize.create!( id:181, material_material_id: 4, material_gem_shape_id: 8, slug: "27 X 20", mm_size: "27 X 20", carat_weight: 58.5)
Material::GemSize.create!( id:182, material_material_id: 4, material_gem_shape_id: 8, slug: "30 X 22", mm_size: "30 X 22", carat_weight: 79.6)

for i in 165..182
  k = Material::GemSize.find(i)
  k.material_gem_clarities << b
  k.material_gem_clarities << b1
end




#----------------COLOR STONE ---------------------------
Material::GemSize.create!( id:183, material_material_id: 5, material_gem_shape_id: 9, slug: "4x2.5", mm_size: "4x2.5", carat_weight: 0.17)
Material::GemSize.create!( id:184, material_material_id: 5, material_gem_shape_id: 9, slug: "5x3", mm_size: "5x3", carat_weight: 0.32)
Material::GemSize.create!( id:185, material_material_id: 5, material_gem_shape_id: 9, slug: "6x4", mm_size: "6x4", carat_weight: 0.62)
Material::GemSize.create!( id:186, material_material_id: 5, material_gem_shape_id: 9, slug: "7x5", mm_size: "7x5", carat_weight: 1.06)
Material::GemSize.create!( id:187, material_material_id: 5, material_gem_shape_id: 9, slug: "8x5", mm_size: "8x5", carat_weight: 1.38)
Material::GemSize.create!( id:188, material_material_id: 5, material_gem_shape_id: 9, slug: "9x6", mm_size: "9x6", carat_weight: 2.1)
Material::GemSize.create!( id:189, material_material_id: 5, material_gem_shape_id: 9, slug: "10x7", mm_size: "10x7", carat_weight: 3.02)
Material::GemSize.create!( id:190, material_material_id: 5, material_gem_shape_id: 9, slug: "12x9", mm_size: "12x9", carat_weight: 4.7)




for i in 183..190
  k = Material::GemSize.find(i)
  k.material_gem_clarities << c
  k.material_gem_clarities << c1
end



Material::GemSize.create!( id:192, material_material_id: 5, material_gem_shape_id: 10, slug: "4x2", mm_size: "4x2", carat_weight: 0.12)
Material::GemSize.create!( id:193, material_material_id: 5, material_gem_shape_id: 10, slug: "4.5x2.5", mm_size: "4.5x2.5", carat_weight: 0.19)
Material::GemSize.create!( id:194, material_material_id: 5, material_gem_shape_id: 10, slug: "5x2.5", mm_size: "5x2.5", carat_weight: 0.24)
Material::GemSize.create!( id:195, material_material_id: 5, material_gem_shape_id: 10, slug: "5x3", mm_size: "5x3", carat_weight: 0.29)
Material::GemSize.create!( id:196, material_material_id: 5, material_gem_shape_id: 10, slug: "6x3", mm_size: "6x3", carat_weight: 0.41)
Material::GemSize.create!( id:197, material_material_id: 5, material_gem_shape_id: 10, slug: "7x3.5", mm_size: "7x3.5", carat_weight: 0.66)
Material::GemSize.create!( id:198, material_material_id: 5, material_gem_shape_id: 10, slug: "8x4", mm_size: "8x4", carat_weight: 0.98)
Material::GemSize.create!( id:199, material_material_id: 5, material_gem_shape_id: 10, slug: "9x4.5", mm_size: "9x4.5", carat_weight: 1.4)
Material::GemSize.create!( id:200, material_material_id: 5, material_gem_shape_id: 10, slug: "10x5", mm_size: "10x5", carat_weight: 1.92)
Material::GemSize.create!( id:201, material_material_id: 5, material_gem_shape_id: 10, slug: "12x6", mm_size: "12x6", carat_weight: 3.32)





for i in 192..201
  k = Material::GemSize.find(i)
  k.material_gem_clarities << c
  k.material_gem_clarities << c1
end

# 1delete
Material::GemSize.create!( id:202, material_material_id: 5, material_gem_shape_id: 11, slug: "5x3", mm_size: "5x3", carat_weight: 0.36)
Material::GemSize.create!( id:203, material_material_id: 5, material_gem_shape_id: 11, slug: "6x4", mm_size: "6x4", carat_weight: 0.69)
Material::GemSize.create!( id:204, material_material_id: 5, material_gem_shape_id: 11, slug: "7x5", mm_size: "7x5", carat_weight: 1.18)
Material::GemSize.create!( id:205, material_material_id: 5, material_gem_shape_id: 11, slug: "8x6", mm_size: "8x6", carat_weight: 1.84)
Material::GemSize.create!( id:206, material_material_id: 5, material_gem_shape_id: 11, slug: "9x7", mm_size: "9x7", carat_weight: 2.72)
Material::GemSize.create!( id:207, material_material_id: 5, material_gem_shape_id: 11, slug: "10x8", mm_size: "10x8", carat_weight: 3.84)
Material::GemSize.create!( id:208, material_material_id: 5, material_gem_shape_id: 11, slug: "11x9", mm_size: "11x9", carat_weight: 6.22)
Material::GemSize.create!( id:209, material_material_id: 5, material_gem_shape_id: 11, slug: "12x10", mm_size: "12x10", carat_weight: 6.91)





for i in 202..209
  k = Material::GemSize.find(i)
  k.material_gem_clarities << c
  k.material_gem_clarities << c1
end


Material::GemSize.create!( id:210, material_material_id: 5, material_gem_shape_id: 12, slug: "1.3", mm_size: "1.3", carat_weight: 0.01)
Material::GemSize.create!( id:211, material_material_id: 5, material_gem_shape_id: 12, slug: "1.4", mm_size: "1.4", carat_weight: 0.01)
Material::GemSize.create!( id:212, material_material_id: 5, material_gem_shape_id: 12, slug: "1.5", mm_size: "1.5", carat_weight: 0.01)
Material::GemSize.create!( id:213, material_material_id: 5, material_gem_shape_id: 12, slug: "1.7", mm_size: "1.7", carat_weight: 0.02)
Material::GemSize.create!( id:214, material_material_id: 5, material_gem_shape_id: 12, slug: "2mm", mm_size: "2mm", carat_weight: 0.03)
Material::GemSize.create!( id:215, material_material_id: 5, material_gem_shape_id: 12, slug: "2.5", mm_size: "2.5", carat_weight: 0.06)
Material::GemSize.create!( id:216, material_material_id: 5, material_gem_shape_id: 12, slug: "3mm", mm_size: "3mm", carat_weight: 0.1)
Material::GemSize.create!( id:217, material_material_id: 5, material_gem_shape_id: 12, slug: "3.5", mm_size: "3.5", carat_weight: 0.15)
Material::GemSize.create!( id:218, material_material_id: 5, material_gem_shape_id: 12, slug: "4mm", mm_size: "4mm", carat_weight: 0.23)
Material::GemSize.create!( id:219, material_material_id: 5, material_gem_shape_id: 12, slug: "4.5", mm_size: "4.5", carat_weight: 0.33)
Material::GemSize.create!( id:220, material_material_id: 5, material_gem_shape_id: 12, slug: "5mm", mm_size: "5mm", carat_weight: 0.45)
Material::GemSize.create!( id:221, material_material_id: 5, material_gem_shape_id: 12, slug: "5.5", mm_size: "5.5", carat_weight: 0.6)
Material::GemSize.create!( id:222, material_material_id: 5, material_gem_shape_id: 12, slug: "6mm", mm_size: "6mm", carat_weight: 0.78)
Material::GemSize.create!( id:223, material_material_id: 5, material_gem_shape_id: 12, slug: "6.5", mm_size: "6.5", carat_weight: 0.99)
Material::GemSize.create!( id:224, material_material_id: 5, material_gem_shape_id: 12, slug: "7mm", mm_size: "7mm", carat_weight: 1.23)
Material::GemSize.create!( id:225, material_material_id: 5, material_gem_shape_id: 12, slug: "7.5", mm_size: "7.5", carat_weight: 1.52)
Material::GemSize.create!( id:226, material_material_id: 5, material_gem_shape_id: 12, slug: "8mm", mm_size: "8mm", carat_weight: 1.84)





for i in 210..226
  k = Material::GemSize.find(i)
  k.material_gem_clarities << c
  k.material_gem_clarities << c1
end


Material::GemSize.create!( id:227, material_material_id: 5, material_gem_shape_id: 13, slug: "6  X 4 ", mm_size: "6  X 4 ", carat_weight: 0.86)
Material::GemSize.create!( id:228, material_material_id: 5, material_gem_shape_id: 13, slug: "7 X 5", mm_size: "7 X 5", carat_weight: 1.47)
Material::GemSize.create!( id:229, material_material_id: 5, material_gem_shape_id: 13, slug: "8 X 6", mm_size: "8 X 6", carat_weight: 2.3)
Material::GemSize.create!( id:230, material_material_id: 5, material_gem_shape_id: 13, slug: "9 X 7", mm_size: "9 X 7", carat_weight: 3.4)
Material::GemSize.create!( id:231, material_material_id: 5, material_gem_shape_id: 13, slug: "10 X 8", mm_size: "10 X 8", carat_weight: 4.8)
Material::GemSize.create!( id:232, material_material_id: 5, material_gem_shape_id: 13, slug: "11 X 9", mm_size: "11 X 9", carat_weight: 6.53)
Material::GemSize.create!( id:233, material_material_id: 5, material_gem_shape_id: 13, slug: "12 X 10 ", mm_size: "12 X 10 ", carat_weight: 8.64)
Material::GemSize.create!( id:234, material_material_id: 5, material_gem_shape_id: 13, slug: "14 X 10", mm_size: "14 X 10", carat_weight: 11.76)
Material::GemSize.create!( id:235, material_material_id: 5, material_gem_shape_id: 13, slug: "14 X 12", mm_size: "14 X 12", carat_weight: 14.11)
Material::GemSize.create!( id:236, material_material_id: 5, material_gem_shape_id: 13, slug: "16 X 12", mm_size: "16 X 12", carat_weight: 18.43)
Material::GemSize.create!( id:237, material_material_id: 5, material_gem_shape_id: 13, slug: "18 X 13", mm_size: "18 X 13", carat_weight: 25.27)
Material::GemSize.create!( id:238, material_material_id: 5, material_gem_shape_id: 13, slug: "20 X 15", mm_size: "20 X 15", carat_weight: 36)
Material::GemSize.create!( id:239, material_material_id: 5, material_gem_shape_id: 13, slug: "25 X 18", mm_size: "25 X 18", carat_weight: 67.5)




for i in 227..239
  k = Material::GemSize.find(i)
  k.material_gem_clarities << c
  k.material_gem_clarities << c1
end

ActiveRecord::Base.connection.reset_pk_sequence!('material_gem_sizes')
puts "Hello"
#====================================================================================
=begin
puts "Style"
w = Design::Style.create!(position: 1, slug: "wr001", design_collection_id: 2, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "10.0", description: "1")
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 1, pcs: 10)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 2, pcs: 11)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 4, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 6, pcs: 14)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 10, pcs: 10)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 11, pcs: 39)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 183, pcs: 1)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 189, pcs: 2)


w = Design::Style.create!(position: 1, slug: "WPD001", design_collection_id: 3, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 2, net_weight: "20.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 1, pcs: 100)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 4, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 11, pcs: 44)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 16, pcs: 52)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 14, pcs: 42)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 185, pcs: 45)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 183, pcs: 10)

w = Design::Style.create!(position: 1, slug: "WPDE001", design_collection_id: 4, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "14.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 1, pcs: 10)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 3, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 7, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 11, pcs: 60)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 190, pcs: 5)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 193, pcs: 6)


w = Design::Style.create!(position: 1, slug: "NCK001", design_collection_id: 5, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "15.0" , description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 3, pcs: 10)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 10, pcs: 16)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 13, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 15, pcs: 47)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 14, pcs: 34)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 17, pcs: 47)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 25, pcs: 56)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 2, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 58, pcs: 24)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 2, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 63, pcs: 41)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 2, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 76, pcs: 47)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 2, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 81, pcs: 64)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 186, pcs: 2)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 188, pcs: 16)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 195, pcs: 3)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 11, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 205, pcs: 12)



w = Design::Style.create!(position: 1, slug: "NCKE001", design_collection_id: 6, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "5.0",  description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 5, pcs: 7)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 9, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 12, pcs: 56)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 27, pcs: 18)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 18, pcs: 9)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 3, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 89, pcs: 1)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 186, pcs: 12)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 192, pcs: 2)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 11, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 204, pcs: 3)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 11, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 205, pcs: 3)


w = Design::Style.create!(position: 1, slug: "WBR001", design_collection_id: 7, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 2, net_weight: "45.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 5, pcs: 10)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 12, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 15, pcs: 18)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 18, pcs: 16)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 21, pcs: 78)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 23, pcs: 22)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 44, pcs: 12)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 190, pcs: 1)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 193, pcs: 1)


w = Design::Style.create!(position: 1, slug: "WBN001", design_collection_id: 8, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "56.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 9, pcs: 11)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 27, pcs: 24)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 24, pcs: 45)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 36, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 2, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 61, pcs: 45)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 2, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 60, pcs: 34)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 3, material_color_id: 13, material_gem_size_id: 185, pcs: 12)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 3, material_color_id: 13, material_gem_size_id: 188, pcs: 13)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 3, material_color_id: 13, material_gem_size_id: 197, pcs: 12)


w = Design::Style.create!(position: 1, slug: "ER001", design_collection_id: 9, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "4.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 9, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 17, pcs: 8)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 19, pcs: 5)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 8, pcs: 6)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 9, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 185, pcs: 6)



w = Design::Style.create!(position: 1, slug: "MR001", design_collection_id: 10, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "15.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 14, pcs: 16)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 21, pcs: 11)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 21, pcs: 10)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 24, pcs: 4)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 12, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 211, pcs: 4)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 12, material_gem_size_id: 192, pcs: 4)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 11, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 202, pcs: 45)


w = Design::Style.create!(position: 1, slug: "MPD001", design_collection_id: 12, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 2, net_weight: "15.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 4, pcs: 54)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 10, pcs: 10)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 9, pcs: 23)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 5, pcs: 65)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 2, material_gem_clarity_id: 2, material_color_id: 11, material_gem_size_id: 54, pcs: 14)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 194, pcs: 66)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 197, pcs: 15)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 199, pcs: 45)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 197, pcs: 4)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 196, pcs: 2)


w = Design::Style.create!(position: 1, slug: "MBR001", design_collection_id: 13, design_making_type_id: 1, design_setting_type_id: 1, user_account_id: 9, material_material_id: 1, material_metal_purity_id: 6, material_color_id: 1, net_weight: "19.0", description: nil)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 11, pcs: 16)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 12, pcs: 66)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 18, pcs: 54)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 4, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 23, pcs: 54)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 193, pcs: 4)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 13, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 233, pcs: 1)
w.design_diamonds <<  Design::Diamond.create!(material_material_id: 5, material_gem_shape_id: 10, material_gem_clarity_id: 4, material_color_id: 13, material_gem_size_id: 196, pcs: 2)

Sale::Order.create!([
                        {customer_id: 3, taken_by_id: 1, order_date: "2017-08-01", delivery_date: "2017-08-30", mfg_product_type_id: 1, description: "First Order"}
                    ])

Sale::Job.create!([
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 1, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "1", item_size: "10", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 2, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 2, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "2", item_size: "11", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 3, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "3", item_size: "12", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 4, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "4", item_size: "13", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 5, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "5", item_size: "14", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 6, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 2, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "6", item_size: "11", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 7, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "7", item_size: "10", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 8, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "8", item_size: "5", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 9, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "9", item_size: "15", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 10, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 2, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "10", item_size: "15", mfg_priority_id: 1, mfg_department_id: 1, },
                      {processable_type: "Sale::Order", processable_id: 1, final: false, design_style_id: 11, user_company_id: 1, material_material_id: 1, metal_purity_id: 6, metal_color_id: 1, qty: 1, diamond_clarity_id: 1, diamond_color_id: 10, cs_clarity_id: 4, cs_color_id: 12, instruction: "11", item_size: "15", mfg_priority_id: 1, mfg_department_id: 1, },
                  ])
puts "Order"

Sale::MTxn.create!([
                       {sale_m_txn_type_id: 1, user_account_id: 3, taken_by_id: 1, date: "2017-08-17", due_date: "2017-08-17", description: "GOLD"},
                       {sale_m_txn_type_id: 1, user_account_id: 3, taken_by_id: 1, date: "2017-08-18", due_date: "2017-08-22", description: "Gold 14K and 18K"},
                       {sale_m_txn_type_id: 1, user_account_id: 3, taken_by_id: 1, date: "2017-08-19", due_date: "2017-08-20", description: "Chaki"},
                       {sale_m_txn_type_id: 1, user_account_id: 4, taken_by_id: 2, date: "2017-08-21", due_date: "2017-08-29", description: "Silver"},
                       {sale_m_txn_type_id: 3, user_account_id: 5, taken_by_id: 1, date: "2017-08-17", due_date: "2017-08-21", description: "Alloy"},
                       {sale_m_txn_type_id: 1, user_account_id: 3, taken_by_id: 1, date: "2017-08-21", due_date: "2017-08-23", description: "DIAMOND"},
                       {sale_m_txn_type_id: 1, user_account_id: 3, taken_by_id: 1, date: "2017-08-21", due_date: "2017-08-22", description: "COLOR STONE"}
                   ])

Sale::MTxnDetail.create!([
                             {countable_type: "Sale::MTxn", countable_id: 1, countable_r_type: nil, countable_r_id: nil, material_material_id: 1, material_metal_purity_id: 1, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 1, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "100.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "2900.0"},
                             {countable_type: "Sale::MTxn", countable_id: 1, countable_r_type: nil, countable_r_id: nil, material_material_id: 1, material_metal_purity_id: 2, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 1, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "100.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "2500.0"},
                             {countable_type: "Sale::MTxn", countable_id: 2, countable_r_type: nil, countable_r_id: nil, material_material_id: 1, material_metal_purity_id: 6, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 1, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 2, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "50.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "2500.0"},
                             {countable_type: "Sale::MTxn", countable_id: 2, countable_r_type: nil, countable_r_id: nil, material_material_id: 1, material_metal_purity_id: 8, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 1, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "50.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "2200.0"},
                             {countable_type: "Sale::MTxn", countable_id: 3, countable_r_type: nil, countable_r_id: nil, material_material_id: 1, material_metal_purity_id: 6, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 1, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 2, material_accessory_id: 1, isissue: false, pcs: 0, weight1: "15.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "2600.0"},
                             {countable_type: "Sale::MTxn", countable_id: 3, countable_r_type: nil, countable_r_id: nil, material_material_id: 1, material_metal_purity_id: 8, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 1, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 2, material_accessory_id: 1, isissue: false, pcs: 0, weight1: "15.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "2500.0"},
                             {countable_type: "Sale::MTxn", countable_id: 4, countable_r_type: nil, countable_r_id: nil, material_material_id: 2, material_metal_purity_id: 9, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 5, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "500.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "45.0"},
                             {countable_type: "Sale::MTxn", countable_id: 5, countable_r_type: nil, countable_r_id: nil, material_material_id: 3, material_metal_purity_id: 11, material_gem_shape_id: nil, material_gem_clarity_id: nil, material_color_id: 6, material_gem_size_id: nil, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "100.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "24.0"},
                             {countable_type: "Sale::MTxn", countable_id: 6, countable_r_type: nil, countable_r_id: nil, material_material_id: 4, material_metal_purity_id: nil, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 1, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "15.5", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "52.0"},
                             {countable_type: "Sale::MTxn", countable_id: 6, countable_r_type: nil, countable_r_id: nil, material_material_id: 4, material_metal_purity_id: nil, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 4, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "10.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "10.0"},
                             {countable_type: "Sale::MTxn", countable_id: 6, countable_r_type: nil, countable_r_id: nil, material_material_id: 4, material_metal_purity_id: nil, material_gem_shape_id: 1, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 6, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "9.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "52.0"},
                             {countable_type: "Sale::MTxn", countable_id: 6, countable_r_type: nil, countable_r_id: nil, material_material_id: 4, material_metal_purity_id: nil, material_gem_shape_id: 2, material_gem_clarity_id: 1, material_color_id: 10, material_gem_size_id: 45, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "100.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "50.0"},
                             {countable_type: "Sale::MTxn", countable_id: 7, countable_r_type: nil, countable_r_id: nil, material_material_id: 5, material_metal_purity_id: nil, material_gem_shape_id: 9, material_gem_clarity_id: 3, material_color_id: 12, material_gem_size_id: 183, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "50.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "15.0"},
                             {countable_type: "Sale::MTxn", countable_id: 7, countable_r_type: nil, countable_r_id: nil, material_material_id: 5, material_metal_purity_id: nil, material_gem_shape_id: 9, material_gem_clarity_id: 3, material_color_id: 12, material_gem_size_id: 184, user_company_id: 1, material_locker_id: 1, material_accessory_id: nil, isissue: false, pcs: 0, weight1: "40.0", note: "", material_gem_type_id: nil, rate_on_pc: false, rate1: "10.0"}
                         ])
puts "Material Transaction"
=end