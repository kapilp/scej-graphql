SELECT
  design_diamonds.id,
  design_diamonds."position",
  design_diamonds.material_material_id,
  design_diamonds.material_gem_shape_id,
  design_diamonds.material_gem_clarity_id,
  design_diamonds.material_color_id,
  design_diamonds.material_gem_size_id,
  design_diamonds.pcs,
  ((design_diamonds.pcs)::numeric * material_gem_sizes.carat_weight) AS
  weight,
  design_diamonds.created_at,
  design_diamonds.updated_at,
  material_gem_sizes.id AS id01,
  material_gem_sizes.material_material_id AS material_material_id01,
  material_gem_sizes.material_gem_shape_id AS material_gem_shape_id01,
  material_gem_sizes."position" AS position01,
  material_gem_sizes.slug,
  material_gem_sizes.mm_size,
  material_gem_sizes.carat_weight,
  material_gem_sizes.price,
  material_gem_sizes.created_at AS created_at01,
  material_gem_sizes.updated_at AS updated_at01
FROM (design_diamonds
  LEFT JOIN material_gem_sizes
ON
  (
    (design_diamonds.material_gem_size_id = material_gem_sizes.id)
  ))
