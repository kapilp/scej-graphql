SELECT
  c.id,
  c."position",
  c.date,
  c.slug,
  c.employee_id,
  c.description,
  c.created_at,
  c.updated_at
FROM mfg_castings c;