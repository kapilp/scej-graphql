SELECT
  mi.id,
  mi."position",
  mi.date,
  mi.slug,
  mi.employee_id,
  mi.description,
  mi.created_at,
  mi.updated_at
FROM mfg_metal_issues mi;