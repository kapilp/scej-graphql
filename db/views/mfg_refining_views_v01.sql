SELECT
  mr.id,
  mr."position",
  mr.user_account_id,
  mr.date_from,
  mr.date_to,
  mr.user_company_id,
  mr.mfg_department_id,
  mr.dust_weight,
  mr.description,
  mr.created_at,
  mr.updated_at
FROM mfg_refinings mr;