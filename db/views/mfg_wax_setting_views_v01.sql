SELECT
  w.id,
  w."position",
  w.date,
  w.slug,
  w.employee_id,
  w.description,
  w.created_at,
  w.updated_at
FROM mfg_wax_settings w;