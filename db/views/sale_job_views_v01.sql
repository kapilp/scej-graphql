SELECT
  sale_orders.id AS id01,
  (
    (sale_orders."position" || '/'::text) || sale_jobs."position"
  ) AS "position",
  sale_orders."position" AS position_order,
  sale_orders.customer_id,
  sale_orders.taken_by_id,
  sale_orders.order_date,
  sale_orders.delivery_date,
  sale_orders.mfg_product_type_id,
  sale_orders.description,
  sale_orders.created_at,
  sale_orders.updated_at,
  sale_jobs.id,
  sale_jobs.processable_type,
  sale_jobs.processable_id,
  sale_jobs.final,
  sale_jobs."position" AS position_job,
  sale_jobs.design_style_id,
  sale_jobs.user_company_id,
  sale_jobs.material_material_id,
  sale_jobs.metal_purity_id,
  sale_jobs.metal_color_id,
  sale_jobs.qty,
  sale_jobs.diamond_clarity_id,
  sale_jobs.diamond_color_id,
  sale_jobs.cs_clarity_id,
  sale_jobs.cs_color_id,
  sale_jobs."instruction" AS instruction,
  sale_jobs."item_size" AS item_size,
  sale_jobs.mfg_priority_id,
  sale_jobs.mfg_department_id,

  sale_jobs.created_at AS created_at01,
  sale_jobs.updated_at AS updated_at01,
  s.receive_weight AS gross_weight,
  s.net_weight,
  s.pure_weight
FROM sale_orders,
(sale_jobs LEFT JOIN LATERAL ( SELECT t.receive_weight, t.net_weight, t.pure_weight FROM mfg_mfg_txn_views t WHERE (t.sale_job_id = sale_jobs.id) ORDER BY t.created_at DESC LIMIT 1) s ON (true))
WHERE
  (sale_orders.id = sale_jobs.processable_id);
