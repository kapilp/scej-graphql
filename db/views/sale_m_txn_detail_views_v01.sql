SELECT
  sale_m_txn_details.id,
  CASE
    WHEN (sale_m_txns.user_account_id IS NOT NULL) THEN sale_m_txns.user_account_id
    WHEN (sale_orders.customer_id IS NOT NULL) THEN sale_orders.customer_id
    ELSE
      (0)::bigint
  END AS account,
  CASE
    WHEN (sale_m_txn_details.isissue IS TRUE) THEN
      (- sale_m_txn_details.pcs)
    ELSE sale_m_txn_details.pcs
  END AS calc_pcs,
  CASE
    WHEN (sale_m_txn_details.isissue IS TRUE) THEN
      (- sale_m_txn_details.weight1)
    ELSE sale_m_txn_details.weight1
  END AS calc_weight,
  sale_m_txn_details.countable_type,
  sale_m_txn_details.countable_id,
  sale_m_txn_details.countable_r_type,
  sale_m_txn_details.countable_r_id,
  sale_m_txn_details."position",
  sale_m_txn_details.material_material_id,
  sale_m_txn_details.material_metal_purity_id,
  sale_m_txn_details.material_gem_shape_id,
  sale_m_txn_details.material_gem_clarity_id,
  sale_m_txn_details.material_color_id,
  sale_m_txn_details.material_gem_size_id,
  sale_m_txn_details.user_company_id,
  sale_m_txn_details.material_locker_id,
  sale_m_txn_details.material_accessory_id,
  sale_m_txn_details.isissue,
  sale_m_txn_details.customer_stock,
  sale_m_txn_details.pcs,
  sale_m_txn_details.weight1,

  sale_m_txn_details.note,
  sale_m_txn_details.material_gem_type_id,
  sale_m_txn_details.rate_on_pc,

  sale_m_txn_details.created_at,
  sale_m_txn_details.updated_at,
  sale_m_txns.id AS id01,
  sale_m_txns."position" AS position01,
  sale_m_txns.sale_m_txn_type_id,
  sale_m_txns.user_account_id,
  sale_m_txns.taken_by_id,
  sale_m_txns.date,
  sale_m_txns.due_date,


  sale_m_txns.description,
  sale_m_txns.created_at AS created_at01,
  sale_m_txns.updated_at AS updated_at01,
  mfg_castings.id AS id02,
  mfg_castings."position" AS position02,
  mfg_castings.date AS date01,
  mfg_castings.slug,
  mfg_castings.employee_id AS casting_employee_id,





  mfg_castings.created_at AS created_at02,
  mfg_castings.updated_at AS updated_at02,
  mfg_mfg_txns.id AS id03,
  mfg_mfg_txns."position" AS position03,
  mfg_mfg_txns.mfg_date,
  mfg_mfg_txns.mfg_casting_id,
  mfg_mfg_txns.sale_job_id,
  mfg_mfg_txns.user_company_id AS user_company_id01,
  mfg_mfg_txns.mfg_department_id,
  mfg_mfg_txns.employee_id AS mfg_employee_id,


  mfg_mfg_txns.receive_weight,

  mfg_mfg_txns.received,

  mfg_mfg_txns.dust_weight,

  mfg_mfg_txns.created_at AS created_at03,
  mfg_mfg_txns.updated_at AS updated_at03,
  sale_jobs.processable_type,
  sale_jobs."position" AS jobno,
  sale_jobs.processable_id AS sale_order_id,
  sale_orders.customer_id AS customer_id
FROM (((((sale_m_txn_details
  LEFT JOIN mfg_castings ON
    (
      (
        (sale_m_txn_details.countable_id = mfg_castings.id) AND
        (
          (sale_m_txn_details.countable_type)::text = 'Mfg::Casting'::text
        )
      )
    ))
  LEFT JOIN mfg_mfg_txns ON
    (
      (
        (sale_m_txn_details.countable_id = mfg_mfg_txns.id) AND
        (
          (sale_m_txn_details.countable_type)::text = 'Mfg::MfgTxn'::text
        )
      )
    ))
  LEFT JOIN sale_m_txns ON
    (
      (
        (sale_m_txn_details.countable_id = sale_m_txns.id) AND
        (
          (sale_m_txn_details.countable_type)::text = 'Sale::MTxn'::text
        )
      )
    ))
  LEFT JOIN sale_jobs ON
    (
      (mfg_mfg_txns.sale_job_id = sale_jobs.id)
    ))
  LEFT JOIN sale_orders ON
    (
      (sale_jobs.processable_id = sale_orders.id)
    ));