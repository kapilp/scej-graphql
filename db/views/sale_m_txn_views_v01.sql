SELECT
  m.id,
  m."position",
  m.sale_m_txn_type_id,
  m.user_account_id,
  m.taken_by_id,
  m.date,
  m.due_date,
  m.description,
  m.created_at,
  m.updated_at
FROM sale_m_txns m;