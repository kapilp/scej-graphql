SELECT
  m.id,
  m."position",
  m.date,
  m.description,
  m.created_at,
  m.updated_at
FROM sale_memos m;