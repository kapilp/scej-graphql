SELECT
  o.id,
  o."position",
  o.customer_id,
  o.taken_by_id,
  o.order_date,
  o.delivery_date,
  o.mfg_product_type_id,
  o.description,
  o.created_at,
  o.updated_at,
  COALESCE(s.count, (0)::bigint) AS num_jobs,
  s.processable_id,
  s.total_qty

FROM (sale_orders o
  LEFT JOIN ( SELECT
    ol.processable_id,
    count(*) AS count,
    sum(ol.qty) AS total_qty

  FROM sale_jobs ol
  GROUP BY
    ol.processable_id) s
  ON
    (
      (s.processable_id = o.id)
    ))
