SELECT
  s.id,
  s."position",
  s.customer_id,
  s.sold_by_id,
  s.date,
  s.due_date,
  s.description,
  s.created_at,
  s.updated_at
FROM sale_sales s;