OPTIONS=" -f --no-doc"
# OPTIONS=" --no-doc"
OPT2=" --all"
# OPT2=" --runSystemCommands"
# OPT2=" --cra"
# OPT2=" --generateArgumentsYaml"
# OPT2=" --generateServerMutation"
# OPT2=" --generateServerQuery"
# OPT2=" --generateDestroyMutation"
# OPT2=" --generateCreateMutation"
# OPT2=" --generateUpdateMutation"
# OPT2=" --generateForeditingquery"
# OPT2=" --generateClassquery"
# OPT2=" --generateClassesquery"
# OPT2=" --generateHeadlist"
# OPT2=" --generateList"
# OPT2=" --generateSearchForm"
# OPT2=" --generateForm"
# OPT2=" --generateFormMobx"
# OPT2=" --generateFormMobxHelper"
# OPT2=" --generateFormMobx2"
# OPT2=" --generateReduxFormFields"
# OPT2=" --generateClassPreview"
# OPT2=" --generateAll"
# OPT2=" --generateIndex"
# OPT2=" --generateNew"
# OPT2=" --generateEdit"
# OPT2=" --generateSearch2"
# OPT2=" --generateClass"
# OPT2=" --generateHoc"
# OPT2=" --runSystemCommandsLast"

#rails g destroymutations User::User $OPTIONS $OPT2
rails g destroymutations User::Contact $OPTIONS $OPT2
rails g destroymutations User::Company $OPTIONS $OPT2
rails g destroymutations User::Role $OPTIONS $OPT2
rails g destroymutations User::AccountType $OPTIONS $OPT2
rails g destroymutations User::Account $OPTIONS $OPT2
rails g destroymutations User::Address $OPTIONS $OPT2
rails g destroymutations User::Country $OPTIONS $OPT2
rails g destroymutations User::State $OPTIONS $OPT2
rails g destroymutations User::TaxType $OPTIONS $OPT2
rails g destroymutations User::TaxRate $OPTIONS $OPT2

rails g destroymutations Material::MaterialType $OPTIONS $OPT2
rails g destroymutations Material::RateOn $OPTIONS $OPT2
rails g destroymutations Material::Material $OPTIONS $OPT2
rails g destroymutations Material::MetalPurity $OPTIONS $OPT2
rails g destroymutations Material::GemShape $OPTIONS $OPT2
rails g destroymutations Material::GemClarity $OPTIONS $OPT2
rails g destroymutations Material::GemSize $OPTIONS $OPT2
#rails g destroymutations Material::GemClaritySize $OPTIONS $OPT2
rails g destroymutations Material::Color $OPTIONS $OPT2
rails g destroymutations Material::Location $OPTIONS $OPT2
rails g destroymutations Material::Locker $OPTIONS $OPT2
rails g destroymutations Material::Accessory $OPTIONS $OPT2
rails g destroymutations Material::GemType $OPTIONS $OPT2

rails g destroymutations Design::Collection $OPTIONS $OPT2
rails g destroymutations Design::MakingType $OPTIONS $OPT2
rails g destroymutations Design::SettingType $OPTIONS $OPT2
rails g destroymutations Design::Style $OPTIONS $OPT2
rails g destroymutations Design::StyleTransition $OPTIONS $OPT2

rails g destroymutations Design::StyleView $OPTIONS $OPT2
rails g destroymutations Design::Diamond $OPTIONS $OPT2
rails g destroymutations Design::DiamondView $OPTIONS $OPT2
#rails g destroymutations Design::Album $OPTIONS $OPT2
rails g destroymutations Design::Image $OPTIONS $OPT2

rails g destroymutations Mfg::Priority $OPTIONS $OPT2
rails g destroymutations Mfg::ProductType $OPTIONS $OPT2
rails g destroymutations Mfg::Department $OPTIONS $OPT2
rails g destroymutations Mfg::DustResource $OPTIONS $OPT2

rails g destroymutations Sale::OrderType $OPTIONS $OPT2
rails g destroymutations Sale::Order $OPTIONS $OPT2
rails g destroymutations Sale::OrderView $OPTIONS $OPT2
rails g destroymutations Sale::Job $OPTIONS $OPT2 --comp
rails g destroymutations Sale::JobView $OPTIONS $OPT2
#rails g destroymutations Sale::JobDiamond $OPTIONS $OPT2
rails g destroymutations Sale::Sale $OPTIONS $OPT2
#rails g destroymutations Sale::SaleDetail $OPTIONS $OPT2
rails g destroymutations Sale::Memo $OPTIONS $OPT2
rails g destroymutations Sale::MTxnType $OPTIONS $OPT2
rails g destroymutations Sale::MTxn $OPTIONS $OPT2
rails g destroymutations Sale::MTxnDetail $OPTIONS $OPT2
rails g destroymutations Sale::MTxnDetailView $OPTIONS $OPT2
rails g destroymutations Sale::MStock $OPTIONS $OPT2

rails g destroymutations Mfg::WaxSetting $OPTIONS $OPT2
rails g destroymutations Mfg::MetalIssue $OPTIONS $OPT2
rails g destroymutations Mfg::Casting $OPTIONS $OPT2
rails g destroymutations Mfg::MfgTxn $OPTIONS $OPT2
rails g destroymutations Mfg::Refining $OPTIONS $OPT2


rails g destroymutations Design::StyleTransition $OPTIONS $OPT2
rails g destroymutations Mfg::CastingTransition $OPTIONS $OPT2
rails g destroymutations Sale::JobTransition $OPTIONS $OPT2
rails g destroymutations Sale::MTxnTransition $OPTIONS $OPT2
rails g destroymutations Sale::OrderTransition $OPTIONS $OPT2
rails g destroymutations User::AccountTransition $OPTIONS $OPT2
rails g destroymutations Mfg::WaxSettingTransition $OPTIONS $OPT2
rails g destroymutations Mfg::MetalIssueTransition $OPTIONS $OPT2


 rails g allmodels k --force
