module Fnctions

  def pages_variables(object = @yamlConfig[@config])
    @general_client_page_no_create_generate = object.dig 'client', 'pages', 'no_create_generate'
    @general_client_page_no_update_generate = object.dig 'client', 'pages', 'no_update_generate'
    @general_client_page_no_delete_generate = object.dig 'client', 'pages', 'no_delete_generate'
    @general_client_page_no_list_generate = object.dig 'client', 'pages', 'no_list_generate'
    # binding.pry
    @general_client_page_no_preview_generate = object.dig 'client', 'pages', 'no_preview_generate'
    # @general_client_page_no_search_generate = object.dig 'client', 'pages', 'no_search_generate'
    @general_client_page_no_main_generate = object.dig 'client', 'pages', 'no_main_generate'
  end

  # class_name and name variable are same.
  def save_variabls(full_class_name = name)
    puts Time.now.strftime("%d/%m/%Y %H:%M%S")
    @noticeDoNotUseReactState =
"""// 1. Do not use React State in this File
// 2. Use Pure Function.
// 3. Use Recompose HOC for more control."""

    # Placed reactLiveDebugger in _Form _FormTable _ListClasses _SearchClass...
    @reactLiveDebugger = false
    @name = full_class_name
    @f = full_class_name.deconstantize.freeze
    @l = full_class_name.demodulize.freeze
    @fD = @f.downcase.freeze
    @fUD = @f.underscore.downcase.freeze
    @dP = @l.downcase.pluralize.freeze
    # @dS = @l.downcase.singularize.freeze
    @CP = @l.camelize.pluralize.freeze
    @cP = @l.camelize(:lower).pluralize.freeze
    @UP = @l.underscore.pluralize.freeze
    @UdP = @l.underscore.downcase.pluralize.freeze
    @UUS = @l.singularize.underscore.upcase.freeze
    @UDS = @l.singularize.underscore.downcase.freeze
    @CS = @l.singularize.camelize.freeze
    @cS = @l.singularize.camelize(:lower).freeze
    # @class = full_class_name.constantize
    @client = 'app/lerna/packages/scej-client'
    if options.path
      if options.path === 'tmp'
        @clientSrc = "#{@client}/tmp"
      else
        @clientSrc = options.path
      end
    else
      @clientSrc = "#{@client}/src"
    end
    @folder_path = @config ? @config.demodulize.underscore.downcase.pluralize : @UdP
    @container = "#{@clientSrc}/containers/generated/#{@fUD}/#{@folder_path}" # @UdP
  end

  def model_columns_for_attributes(class_string)
    h = class_string.constantize.columns.reject do |column|
      column.name.to_s =~ /^(id|user_id|created_at|updated_at)$/
    end
    editable_attributes_add_columns(h, class_string)
  end

  def editable_attributes(table_class=@table)
    attributes ||= model_columns_for_attributes(table_class).map do |column|
      Rails::Generators::GeneratedAttribute.new(column.name.to_s, column.type.to_s)
    end
  end

  def editable_attributes_add_columns(attributes, table_class=@name)
    # attributes << Rails::Generators::GeneratedAttribute.new("images", "file") if table_class=="Design::Style"
    attributes
  end

  def read_template(relative_path)
    ERB.new(File.read(self.class.source_root(relative_path)), nil, '-').result(binding)
  end

  # 2 Functions from : https://github.com/cucumber/cucumber-rails/blob/master/lib/generators/cucumber/install/install_generator.rb
  def embed_file(source, indent = '')
    # Same as File.read "...file"
    IO.read(File.join(self.class.source_root, source)).gsub(/^/, indent)
  end

  def embed_template(source, indent = '')
    template2 = File.join(self.class.source_root, source)
    ERB.new(IO.read(template2), nil, '-').result(binding).gsub(/^/, indent)
  end

  # def create_mutation_1
  #   create_mutation_1_string = "\n    can :manage, #{@name}, user_id: user.id"
  #   inject_into_file "#{Rails.root}/app/models/ability.rb", ability_string, after: /def initialize[a-z()]+/i
  # end

  # def create_mutation_2
  #   create_mutation_2_string = "\n    can :manage, #{@name}, user_id: user.id"
  #   inject_into_file "#{Rails.root}/app/models/ability.rb", ability_string, after: /def initialize[a-z()]+/i
  # end

# used in graphql both client and server side.
  def type_change(t)
    case t
    when 'integer' then
      'Int'
    when 'string' then
      'String'
    when 'decimal' then
      'Float'
    when 'text' then
      'String'
    when 'date' then
      'String'
    when 'datetime' then
      # '!DateTimeScalar'
      'String'
    when 'inet' then
      'String'
    when 'boolean' then
      'Boolean'
    when 'file' then
      'file'
    when 'json' then
      'String'
    end
  end

  def name_change(n)
    n = 'collection_id' if n == 'design_collection_id'
    n = 'making_type_id' if n == 'design_making_type_id'
    n = 'setting_type_id' if n == 'design_setting_type_id'
    n = 'style_id' if n == 'design_style_id'

    n = 'material_type_id' if n == 'material_material_type_id'
    n = 'material_id' if n == 'material_material_id'
    n = 'metal_purity_id' if n == 'material_metal_purity_id'
    n = 'color_id' if n == 'material_color_id'
    n = 'gem_shape_id' if n == 'material_gem_shape_id'
    n = 'gem_size_id' if n == 'material_gem_size_id'
    n = 'gem_clarity_id' if n == 'material_gem_clarity_id'
    n = 'company_id' if n == 'user_company_id'
    n = 'locker_id' if n == 'material_locker_id'
    n = 'accessory_id' if n == 'material_accessory_id'
    n = 'gem_type_id' if n == 'material_gem_type_id'

    n = 'department_id' if n == 'mfg_department_id'
    n = 'sub_department_id' if n == 'mfg_sub_department_id'
    n = 'casting_id' if n == 'mfg_casting_id'
    n = 'product_type_id' if n == 'mfg_product_type_id'
    n = 'priority_id' if n == 'mfg_priority_id'

    n = 'm_txn_type_id' if n == 'sale_m_txn_type_id'
    n = 'job_id' if n == 'sale_job_id'
    n = 'order_type_id' if n == 'sale_order_type_id'

    n = 'account_type_id' if n == 'user_account_type_id'
    n = 'account_id' if n == 'user_account_id'
    n = 'contact_id' if n == 'user_contact_id'
    n = 'state_id' if n == 'user_state_id'
    n = 'role_id' if n == 'user_role_id'
    n = 'country_id' if n == 'user_country_id'

    n
  end

  def column_change(t, c)
    c = 'entry_by_id' if c == 'account_id' && (t == 'Refining' || t == 'RefiningView')
    c = 'employee_id' if c == 'account_id' && (t == 'MfgTxn' || t == 'MfgTxnView')
    c = 'party_id' if c == 'account_id' && (t == 'MTxn' || t == 'MTxnView')
    c = 'designer_id' if c == 'account_id' && (t == 'Style' || t == 'StyleView')
    c
  end

  def is_nested_form
    table = false
    # 1.find table name keys
    objectKeys = @yamlConfig[@name]
    # 2. if key has object
    if objectKeys
    # 3. loop through it
      return table unless objectKeys.kind_of?(Hash)
      objectKeys.keys.each do |k|
    # 4. if each object has table return it.

        next unless objectKeys[k].is_a?(Hash) && objectKeys[k].key?('table') && !objectKeys[k]['noGenerateTable'] # If table key not exist we not process it.
        table = objectKeys[k]['table'].constantize
        break
      end
    end

    table
  end

  def nestedtables(object, prefix="")
    object.keys.each do |k| # Loop Through all keys
      next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
      table_CP = object[k]['table'].demodulize.camelize.pluralize
      relationship_name_CP =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'].camelize.pluralize : "#{table_CP}"
      prefix2 = "#{prefix}#{relationship_name_CP}"

      dest_file = "#{@container}/NewEdit/logic/_FormTable#{prefix2}.tsx"

      @is_nested_form_object = object[k]
      @yamlObject = object[k]
      # binding.pry
      template2 "client/container/NewEdit/logic/_FormTable.tsx.erb", dest_file

      nestedtables(object[k]['nested'], prefix2){|prefix2a| puts prefix2a } if object[k].key?('nested')
    end
  end

  # a = [ [ 1, 2, 3], [ 4, 5, 6] ];
  # b = [ 'A', 'B', 'C', 'D', 'E', 'F'];
  # b.zip(a.flatten) # [["A", 1], ["B", 2], ["C", 3], ["D", 4], ["E", 5], ["F", 6]]
  # [b.zip(a.flatten).reduce([]) { |memo, (k,v)| memo << { k => v } }] # mutating memo, which is bad form
  # [b.zip(a.flatten).reduce([]) { |memo, (k,v)| memo + [{ k => v }] }]
  # a.each.with_index { |ary, outer_idx| ary.each.with_index { i, inner_idx|
  def result_c(a, b)
    result = [];
    a.each { |ary|
      result << [];
      ary.each { |i| # iterate over each array # so the first time this runs, i will be 1, then 2, then 3
        # then we calculate how to get the b value
          b_idx = (result.length - 1) * ary.length + result.last.length;
          result.last << { b[b_idx] => i } # then we make a hash with a single key, i came from a
        }
    };
    result
  end
  # [[{"A"=>1}, {"B"=>2}, {"C"=>3}], [{"D"=>4}, {"E"=>5}, {"F"=>6}]]
  # c(a, b)




  def query_variables
    @v1 = '' # For Query Header
    @v2 = ''
    @v3 = ''

    # @v1  =  For each obj get argument_name and type and make @v1
    object = @yamlConfig[@config]
    arguments = object.is_a?(Hash) && object.key?('query_arguments') ? object['query_arguments'] : false

    if arguments
      r = []
      arguments.map do |o|
        r << "$#{o['argument_name']}: #{o['type']}"
      end
      @v1 = "(#{r.join(", ")})"
    end

    # @v2  =  For each obj get argument_name and type and make @v2
    if arguments
      r = []
      arguments.map do |o|
        r << "#{o['argument_name']}: $#{o['argument_name']}"
      end
      @v2 = "(#{r.join(", ")})"
    end

    # @v3  =  For each obj get argument_name and default value and make @v3
    if arguments
      r = []
      arguments.map do |o|
        r << "#{o['argument_name']}: #{o['client_default']}"
      end
      @v3 = " #{r.join(", ")} "
    end


    # @v3 = " f_material_id: 0 " if @l == 'Color'
    # @v3 = " f_material_id: 0 " if @l == 'GemClarity'
    # @v3 = " f_material_id: 0 " if @l == 'GemShape'
    # @v3 = " f_material_id: 0 " if @l == 'MetalPurity'
    # @v3 = " f_material_id: 0, f_gem_shape_id: 0, f_gem_clarity_id: 0 " if @l == 'GemSize'
    # @v3 = " f_m_type_id: 0 " if @l == 'Material'
    # @v3 = " f_company_id: 0 " if @l == 'Locker'
    # @v3 = " f_a_type_id: 0 " if @l == 'Account'
    # @v3 = " f_country_id: 0 " if @l == 'State'
  end

  def add_extra_fields(table, c)
    # add any field marked to add:
    if table && table.key?('calculated_fields_after')
      calculated_fields_after = table['calculated_fields_after']
      keys = calculated_fields_after.keys || []
      if keys.index(c)
        calculated_fields_after[c].each do |k|
          yield(k)
        end
      end
    end
  end

  def add_extra_fields_before(table, c)
    # add any field marked to add:
    if table && table.key?('calculated_fields_before')
      calculated_fields_before = table['calculated_fields_before']
      keys = calculated_fields_before.keys || []
      if keys.index(c)
        calculated_fields_before[c].each do |k|
          yield(k)
        end
      end
    end
  end

  def is_nested_form2
    table = false
    # 1.find table name keys
    objectKeys = @yamlConfig[@name]
    # 2. if key has object
    if objectKeys
    # 3. loop through it
      return table unless objectKeys.kind_of?(Hash)
      objectKeys.keys.each do |k|
    # 4. if each object has table return it.

        next unless objectKeys[k].is_a?(Hash) && objectKeys[k].key?('table') && !objectKeys[k]['noGenerateTable'] # If table key not exist we not process it.
        table = objectKeys[k]['table']
        yield("table: #{@name}, nested_table: #{table}\n")
      end
    end

    table
  end


  # ===========================all models functions=====================================

  def all_models
    modelNames = ApplicationRecord.descendants.map do |a| a.model_name.name end
    modelNames.sort
  end

  # Used to generate queries:
  def all_state_machines
    [
      {machine: 'Design::StyleStateMachine', model: 'Design::Style', transition: 'Design::StyleTransition'},
      {machine: 'Mfg::CastingStateMachine', model: 'Mfg::Casting', transition: 'Mfg::CastingTransition'},
      {machine: 'Sale::JobStateMachine', model: 'Sale::Job', transition: 'Sale::JobTransition'}, # not used in client side.
      {machine: 'Sale::MTxnStateMachine', model: 'Sale::MTxn', transition: 'Sale::MTxnTransition'},
      {machine: 'Sale::OrderStateMachine', model: 'Sale::Order', transition: 'Sale::OrderTransition'},
      {machine: 'User::AccountStateMachine', model: 'User::Account', transition: 'User::AccountTransition'},
      {machine: 'Mfg::IssueFinishStateMachine', model: 'Mfg::WaxSetting', transition: 'Mfg::WaxSettingTransition'},
      {machine: 'Mfg::IssueFinishStateMachine', model: 'Mfg::MetalIssue', transition: 'Mfg::MetalIssueTransition'},

      {machine: 'Mfg::MfgTxnStateMachine', model: 'Mfg::MfgTxn', transition: 'Mfg::MfgTxnTransition'},
      {machine: 'Mfg::RefiningStateMachine', model: 'Mfg::Refining', transition: 'Mfg::RefiningTransition'},
      {machine: 'Sale::MemoStateMachine', model: 'Sale::Memo', transition: 'Sale::MemoTransition'},
      {machine: 'Sale::SaleStateMachine', model: 'Sale::Sale', transition: 'Sale::SaleTransition'},



    ]
  end

  def ignore_join_tables(n)
    case n
    when "Design::JobDiamond" then
      true
    when "Design::StyleDiamond" then
      true
    when "Material::GemClaritySize" then
      true
    when "User::CompanyOwnership" then
      true

    when "Design::Diamond" then
      true
    when "Sale::SaleDetail" then
      true
    when "Sale::MTxnDetail" then
      true
    end
  end

  def ignore_route_tables(n)
    case n
    when "Design::Diamond" then
      true
    when "Design::Image" then
      true
      # // This should not be shown to customer...
    when "Sale::MTxnType" then
      true
    when "Sale::OrderType" then
      true
    when "User::Contact" then
      true
    when "User::AccountType" then
      true
    when "User::Address" then
      true
    when "Material::MaterialType" then
      true
    end
  end

  def ignore_view_tables(n)
    n.last(4) == 'View' ? true : false
  end

  # Generate Navigation TODO....NOT REQUIRED:
  def generator_navigation
    # navigation =
    {
      "User":[
        {table: User::Account, model: '', path: '../routes/user/account', name: "Account"},],
      "Material":[],
      "Design":[],
      "Sale":[],
      "Mfg":[],
    }
      # Make Required Hash...
      # h = Hash.new(0)
  end

  def query_arguments_server_strings
    object = @yamlConfig[@name] # we looping through all models directly.
    # object = @yamlConfig[@config]
    arguments = object.is_a?(Hash) && object.key?('query_arguments') ? object['query_arguments'] : false

    a = ""

    if arguments
      arguments.map do |o|
        next if o['argument_name'] == 'first'
        next if o['argument_name'] == 'after'
        next if o['argument_name'] == 'last'
        next if o['argument_name'] == 'before'
        if o['type'] == 'ID'
          a += "    argument :#{o['argument_name']}, types.#{o['type']}\n"
        else
          a += "    argument :#{o['argument_name']}, types.#{o['type']}, default_value: #{o['server_default']}\n"
        end
      end
    end

    # Make Required Hash...
    functionArguments = Hash.new()
    functionTypes = Hash.new()
    if arguments
      arguments.map do |o|
        next if o['argument_name'] == 'first'
        next if o['argument_name'] == 'after'
        next if o['argument_name'] == 'last'
        next if o['argument_name'] == 'before'
        if functionArguments.has_key?(o['function_name']) # If key exist in hash we add to it else we create new key.
          functionArguments[o['function_name']].push(o['argument_name'])
          functionTypes[o['function_name']]= o['type']
        else
          functionArguments[o['function_name']] = [o['argument_name']]
          functionTypes[o['function_name']] = o['type']
        end
      end
    end

    b = ''
    functionArguments.keys.map do |m|
      b += "      #{@cP} = #{@cP}.#{m}(#{functionArguments[m].map do |a1| "args[:#{a1}]" end.join(", ")}) if #{functionArguments[m].map do |a2|
        r = ''
        if functionTypes[m] == 'ID'
          r = "args[:#{a2}]"
        end
        if functionTypes[m] == 'Int'
          r = "args[:#{a2}] && args[:#{a2}] > 0"
        end
        if functionTypes[m] == 'String'
          r = "args[:#{a2}] && !args[:#{a2}].blank?"
        end
        r
      end.join(" && ")}\n"
    end

    r = Hash.new(0)
    r["arg1"] = a.chomp  # chomp = Returns a new String with the given record separator removed from the end of str
    r["arg2"] = b.chomp
    r
  end

  def all_mutations_generate(tables_array=loop_classes(true))
    tables_array.map { |table_data|
      n = table_data[:table]
      @config = table_data[:config] ? table_data[:config] : table_data[:table]
      object = @yamlConfig[@config]
      @general_disable_mutation_generate = object.dig 'general', 'disable_mutation_generate'

      next if ignore_join_tables(n)
      next if ignore_view_tables(n)
      next if n.last(10) == 'Transition'
      next if @general_disable_mutation_generate
      save_variabls(@config)

      @general_mutation_no_create_generate = @yamlConfig[@config].dig 'general', 'mutations', 'no_create_generate'
      @general_mutation_no_update_generate = @yamlConfig[@config].dig 'general', 'mutations', 'no_update_generate'
      @general_mutation_no_delete_generate = @yamlConfig[@config].dig 'general', 'mutations', 'no_delete_generate'

      o = []
      o << "  field :create#{@CS}, field: #{@CS}Mutations::Create.field" if !@general_mutation_no_create_generate
      o << "  field :update#{@CS}, field: #{@CS}Mutations::Update.field" if !@general_mutation_no_update_generate
      o << "  field :destroy#{@CS}, field: #{@CS}Mutations::Destroy.field" if !@general_mutation_no_delete_generate
      o << "\n"

      o.join("\n")
    }.join("\n")
  end



  def template2(source_file, dest_file)
    data_block_get(dest_file)
    template source_file, dest_file
  end

  def standard_imports(settings)
    # other way to check keys are:
    # %w[foo bar baz].any? { |key| settings.has_key?(key) }
    importData = []
    importData <<  {module: 'react', import_modules: 'React', defaultImp: true} if settings.has_key?('React')
    importData <<  {module: 'recompose', import_modules: settings.keys & %w(pure compose lifecycle withProps)}  if !(settings.keys & %w(pure compose lifecycle withProps)).empty?

    importData <<  {module: 'mobx', import_modules: settings.keys & %w(observable computed action extendObservable whyRun runInAction)}  if !(settings.keys & %w(observable computed action extendObservable whyRun runInAction)).empty?
    importData <<  {module: 'mobx-react', import_modules: settings.keys & %w(observer Observer)} if settings.has_key?('observer') if !(settings.keys & %w(observer Observer)).empty?
    importData <<  {module: 'mobx-react', import_modules: ['inject']} if settings.has_key?('inject')
    importData <<  {module: 'mobx-react-form-devtools', import_modules: 'MobxReactFormDevTools'} if settings.has_key?('mobx-react-form-devtools')
    importData <<  {module: 'mobx-little-router-react', import_modules: ['Link']} if settings.has_key?('Link')
    importData <<  {module: 'mobx-little-router-react', import_modules: ['withRouter']} if settings.has_key?('withRouter')

    importData <<  {module: 'react-apollo', import_modules: ['graphql']} if settings.has_key?('graphql')
    importData <<  {module: 'graphql-tag', import_modules: 'gql', defaultImp: true} if settings.has_key?('gql')
    importData <<  {module: 'react-apollo', import_modules: ['withApollo']} if settings.has_key?('withApollo')
    importData <<  {module: 'apollo-mutation-state', import_modules: 'withMutationState'} if settings.has_key?('apollo-mutation-state')

    importData <<  {module: 'validatorjs', import_modules: 'validatorjs', defaultImp: true} if settings.has_key?('validatorjs')
    importData <<  {module: 'numeral', import_modules: 'Numeral', defaultImp: true} if settings.has_key?('Numeral')
    importData <<  {module: 'moment', import_modules: 'moment', defaultImp: true} if settings.has_key?('moment')
    importData <<  {module: 'classnames', import_modules: 'classnames', defaultImp: true} if settings.has_key?('classnames') if settings.has_key?('classnames')

    importData <<  {module: 'material-ui/styles', import_modules: ['withStyles']} if settings.has_key?('withStyles')
    importData <<  {module: 'material-ui/Button', import_modules: 'Button'} if settings.has_key?('Button')
    importData <<  {module: 'material-ui/IconButton', import_modules: 'IconButton'} if settings.has_key?('IconButton')
    importData <<  {module: 'material-ui/Progress', import_modules: ['CircularProgress']} if settings.has_key?('CircularProgress')
    importData <<  {module: 'material-ui/Divider', import_modules: 'Divider'} if settings.has_key?('Divider')
    importData <<  {module: 'material-ui/TextField', import_modules: 'TextField'} if settings.has_key?('TextField')

    importData <<  {module: 'primereact/components/overlaypanel/OverlayPanel', import_modules: ['OverlayPanel']} if settings.has_key?('PrimeOverlayPanel')
    importData <<  {module: 'primereact/components/button/Button', import_modules: ['Button']} if settings.has_key?('PrimeButton')
    importData <<  {module: 'primereact/components/tabview/TabView', import_modules: ['TabView', 'TabPanel']} if settings.has_key?('PrimeTabPanel')
    importData <<  {module: 'primereact/components/picklist/PickList', import_modules: ['PickList']} if settings.has_key?('PrimePickList')
    importData <<  {module: 'primereact/components/breadcrumb/BreadCrumb', import_modules: ['BreadCrumb']} if settings.has_key?('PrimeBreadCrumb')

    importData <<  {module: 'material-ui-icons/AccessAlarm', import_modules: 'AccessAlarmIcon' } if settings.has_key?('AccessAlarmIcon')
    importData <<  {module: 'material-ui-icons/AccessAlarms', import_modules: 'AccessAlarmsIcon' } if settings.has_key?('AccessAlarmsIcon')
    importData <<  {module: 'material-ui-icons/Accessibility', import_modules: 'AccessibilityIcon' } if settings.has_key?('AccessibilityIcon')
    importData <<  {module: 'material-ui-icons/Accessible', import_modules: 'AccessibleIcon' } if settings.has_key?('AccessibleIcon')
    importData <<  {module: 'material-ui-icons/AccessTime', import_modules: 'AccessTimeIcon' } if settings.has_key?('AccessTimeIcon')
    importData <<  {module: 'material-ui-icons/AccountBalance', import_modules: 'AccountBalanceIcon' } if settings.has_key?('AccountBalanceIcon')
    importData <<  {module: 'material-ui-icons/AccountBalanceWallet', import_modules: 'AccountBalanceWalletIcon' } if settings.has_key?('AccountBalanceWalletIcon')
    importData <<  {module: 'material-ui-icons/AccountBox', import_modules: 'AccountBoxIcon' } if settings.has_key?('AccountBoxIcon')
    importData <<  {module: 'material-ui-icons/AccountCircle', import_modules: 'AccountCircleIcon' } if settings.has_key?('AccountCircleIcon')
    importData <<  {module: 'material-ui-icons/AcUnit', import_modules: 'AcUnitIcon' } if settings.has_key?('AcUnitIcon')
    importData <<  {module: 'material-ui-icons/Adb', import_modules: 'AdbIcon' } if settings.has_key?('AdbIcon')
    importData <<  {module: 'material-ui-icons/Add', import_modules: 'AddIcon' } if settings.has_key?('AddIcon')
    importData <<  {module: 'material-ui-icons/AddAlarm', import_modules: 'AddAlarmIcon' } if settings.has_key?('AddAlarmIcon')
    importData <<  {module: 'material-ui-icons/AddAlert', import_modules: 'AddAlertIcon' } if settings.has_key?('AddAlertIcon')
    importData <<  {module: 'material-ui-icons/AddAPhoto', import_modules: 'AddAPhotoIcon' } if settings.has_key?('AddAPhotoIcon')
    importData <<  {module: 'material-ui-icons/AddBox', import_modules: 'AddBoxIcon' } if settings.has_key?('AddBoxIcon')
    importData <<  {module: 'material-ui-icons/AddCircle', import_modules: 'AddCircleIcon' } if settings.has_key?('AddCircleIcon')
    importData <<  {module: 'material-ui-icons/AddCircleOutline', import_modules: 'AddCircleOutlineIcon' } if settings.has_key?('AddCircleOutlineIcon')
    importData <<  {module: 'material-ui-icons/AddLocation', import_modules: 'AddLocationIcon' } if settings.has_key?('AddLocationIcon')
    importData <<  {module: 'material-ui-icons/AddShoppingCart', import_modules: 'AddShoppingCartIcon' } if settings.has_key?('AddShoppingCartIcon')
    importData <<  {module: 'material-ui-icons/AddToPhotos', import_modules: 'AddToPhotosIcon' } if settings.has_key?('AddToPhotosIcon')
    importData <<  {module: 'material-ui-icons/AddToQueue', import_modules: 'AddToQueueIcon' } if settings.has_key?('AddToQueueIcon')
    importData <<  {module: 'material-ui-icons/Adjust', import_modules: 'AdjustIcon' } if settings.has_key?('AdjustIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatFlat', import_modules: 'AirlineSeatFlatIcon' } if settings.has_key?('AirlineSeatFlatIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatFlatAngled', import_modules: 'AirlineSeatFlatAngledIcon' } if settings.has_key?('AirlineSeatFlatAngledIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatIndividualSuite', import_modules: 'AirlineSeatIndividualSuiteIcon' } if settings.has_key?('AirlineSeatIndividualSuiteIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatLegroomExtra', import_modules: 'AirlineSeatLegroomExtraIcon' } if settings.has_key?('AirlineSeatLegroomExtraIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatLegroomNormal', import_modules: 'AirlineSeatLegroomNormalIcon' } if settings.has_key?('AirlineSeatLegroomNormalIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatLegroomReduced', import_modules: 'AirlineSeatLegroomReducedIcon' } if settings.has_key?('AirlineSeatLegroomReducedIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatReclineExtra', import_modules: 'AirlineSeatReclineExtraIcon' } if settings.has_key?('AirlineSeatReclineExtraIcon')
    importData <<  {module: 'material-ui-icons/AirlineSeatReclineNormal', import_modules: 'AirlineSeatReclineNormalIcon' } if settings.has_key?('AirlineSeatReclineNormalIcon')
    importData <<  {module: 'material-ui-icons/AirplanemodeActive', import_modules: 'AirplanemodeActiveIcon' } if settings.has_key?('AirplanemodeActiveIcon')
    importData <<  {module: 'material-ui-icons/AirplanemodeInactive', import_modules: 'AirplanemodeInactiveIcon' } if settings.has_key?('AirplanemodeInactiveIcon')
    importData <<  {module: 'material-ui-icons/Airplay', import_modules: 'AirplayIcon' } if settings.has_key?('AirplayIcon')
    importData <<  {module: 'material-ui-icons/AirportShuttle', import_modules: 'AirportShuttleIcon' } if settings.has_key?('AirportShuttleIcon')
    importData <<  {module: 'material-ui-icons/Alarm', import_modules: 'AlarmIcon' } if settings.has_key?('AlarmIcon')
    importData <<  {module: 'material-ui-icons/AlarmAdd', import_modules: 'AlarmAddIcon' } if settings.has_key?('AlarmAddIcon')
    importData <<  {module: 'material-ui-icons/AlarmOff', import_modules: 'AlarmOffIcon' } if settings.has_key?('AlarmOffIcon')
    importData <<  {module: 'material-ui-icons/AlarmOn', import_modules: 'AlarmOnIcon' } if settings.has_key?('AlarmOnIcon')
    importData <<  {module: 'material-ui-icons/Album', import_modules: 'AlbumIcon' } if settings.has_key?('AlbumIcon')
    importData <<  {module: 'material-ui-icons/AllInclusive', import_modules: 'AllInclusiveIcon' } if settings.has_key?('AllInclusiveIcon')
    importData <<  {module: 'material-ui-icons/AllOut', import_modules: 'AllOutIcon' } if settings.has_key?('AllOutIcon')
    importData <<  {module: 'material-ui-icons/Android', import_modules: 'AndroidIcon' } if settings.has_key?('AndroidIcon')
    importData <<  {module: 'material-ui-icons/Announcement', import_modules: 'AnnouncementIcon' } if settings.has_key?('AnnouncementIcon')
    importData <<  {module: 'material-ui-icons/Apps', import_modules: 'AppsIcon' } if settings.has_key?('AppsIcon')
    importData <<  {module: 'material-ui-icons/Archive', import_modules: 'ArchiveIcon' } if settings.has_key?('ArchiveIcon')
    importData <<  {module: 'material-ui-icons/ArrowBack', import_modules: 'ArrowBackIcon' } if settings.has_key?('ArrowBackIcon')
    importData <<  {module: 'material-ui-icons/ArrowDownward', import_modules: 'ArrowDownwardIcon' } if settings.has_key?('ArrowDownwardIcon')
    importData <<  {module: 'material-ui-icons/ArrowDropDown', import_modules: 'ArrowDropDownIcon' } if settings.has_key?('ArrowDropDownIcon')
    importData <<  {module: 'material-ui-icons/ArrowDropDownCircle', import_modules: 'ArrowDropDownCircleIcon' } if settings.has_key?('ArrowDropDownCircleIcon')
    importData <<  {module: 'material-ui-icons/ArrowDropUp', import_modules: 'ArrowDropUpIcon' } if settings.has_key?('ArrowDropUpIcon')
    importData <<  {module: 'material-ui-icons/ArrowForward', import_modules: 'ArrowForwardIcon' } if settings.has_key?('ArrowForwardIcon')
    importData <<  {module: 'material-ui-icons/ArrowUpward', import_modules: 'ArrowUpwardIcon' } if settings.has_key?('ArrowUpwardIcon')
    importData <<  {module: 'material-ui-icons/ArtTrack', import_modules: 'ArtTrackIcon' } if settings.has_key?('ArtTrackIcon')
    importData <<  {module: 'material-ui-icons/AspectRatio', import_modules: 'AspectRatioIcon' } if settings.has_key?('AspectRatioIcon')
    importData <<  {module: 'material-ui-icons/Assessment', import_modules: 'AssessmentIcon' } if settings.has_key?('AssessmentIcon')
    importData <<  {module: 'material-ui-icons/Assignment', import_modules: 'AssignmentIcon' } if settings.has_key?('AssignmentIcon')
    importData <<  {module: 'material-ui-icons/AssignmentInd', import_modules: 'AssignmentIndIcon' } if settings.has_key?('AssignmentIndIcon')
    importData <<  {module: 'material-ui-icons/AssignmentLate', import_modules: 'AssignmentLateIcon' } if settings.has_key?('AssignmentLateIcon')
    importData <<  {module: 'material-ui-icons/AssignmentReturn', import_modules: 'AssignmentReturnIcon' } if settings.has_key?('AssignmentReturnIcon')
    importData <<  {module: 'material-ui-icons/AssignmentReturned', import_modules: 'AssignmentReturnedIcon' } if settings.has_key?('AssignmentReturnedIcon')
    importData <<  {module: 'material-ui-icons/AssignmentTurnedIn', import_modules: 'AssignmentTurnedInIcon' } if settings.has_key?('AssignmentTurnedInIcon')
    importData <<  {module: 'material-ui-icons/Assistant', import_modules: 'AssistantIcon' } if settings.has_key?('AssistantIcon')
    importData <<  {module: 'material-ui-icons/AssistantPhoto', import_modules: 'AssistantPhotoIcon' } if settings.has_key?('AssistantPhotoIcon')
    importData <<  {module: 'material-ui-icons/AttachFile', import_modules: 'AttachFileIcon' } if settings.has_key?('AttachFileIcon')
    importData <<  {module: 'material-ui-icons/Attachment', import_modules: 'AttachmentIcon' } if settings.has_key?('AttachmentIcon')
    importData <<  {module: 'material-ui-icons/AttachMoney', import_modules: 'AttachMoneyIcon' } if settings.has_key?('AttachMoneyIcon')
    importData <<  {module: 'material-ui-icons/Audiotrack', import_modules: 'AudiotrackIcon' } if settings.has_key?('AudiotrackIcon')
    importData <<  {module: 'material-ui-icons/Autorenew', import_modules: 'AutorenewIcon' } if settings.has_key?('AutorenewIcon')
    importData <<  {module: 'material-ui-icons/AvTimer', import_modules: 'AvTimerIcon' } if settings.has_key?('AvTimerIcon')
    importData <<  {module: 'material-ui-icons/Backspace', import_modules: 'BackspaceIcon' } if settings.has_key?('BackspaceIcon')
    importData <<  {module: 'material-ui-icons/Backup', import_modules: 'BackupIcon' } if settings.has_key?('BackupIcon')
    importData <<  {module: 'material-ui-icons/Battery20', import_modules: 'Battery20Icon' } if settings.has_key?('Battery20Icon')
    importData <<  {module: 'material-ui-icons/Battery30', import_modules: 'Battery30Icon' } if settings.has_key?('Battery30Icon')
    importData <<  {module: 'material-ui-icons/Battery50', import_modules: 'Battery50Icon' } if settings.has_key?('Battery50Icon')
    importData <<  {module: 'material-ui-icons/Battery60', import_modules: 'Battery60Icon' } if settings.has_key?('Battery60Icon')
    importData <<  {module: 'material-ui-icons/Battery80', import_modules: 'Battery80Icon' } if settings.has_key?('Battery80Icon')
    importData <<  {module: 'material-ui-icons/Battery90', import_modules: 'Battery90Icon' } if settings.has_key?('Battery90Icon')
    importData <<  {module: 'material-ui-icons/BatteryAlert', import_modules: 'BatteryAlertIcon' } if settings.has_key?('BatteryAlertIcon')
    importData <<  {module: 'material-ui-icons/BatteryCharging20', import_modules: 'BatteryCharging20Icon' } if settings.has_key?('BatteryCharging20Icon')
    importData <<  {module: 'material-ui-icons/BatteryCharging30', import_modules: 'BatteryCharging30Icon' } if settings.has_key?('BatteryCharging30Icon')
    importData <<  {module: 'material-ui-icons/BatteryCharging50', import_modules: 'BatteryCharging50Icon' } if settings.has_key?('BatteryCharging50Icon')
    importData <<  {module: 'material-ui-icons/BatteryCharging60', import_modules: 'BatteryCharging60Icon' } if settings.has_key?('BatteryCharging60Icon')
    importData <<  {module: 'material-ui-icons/BatteryCharging80', import_modules: 'BatteryCharging80Icon' } if settings.has_key?('BatteryCharging80Icon')
    importData <<  {module: 'material-ui-icons/BatteryCharging90', import_modules: 'BatteryCharging90Icon' } if settings.has_key?('BatteryCharging90Icon')
    importData <<  {module: 'material-ui-icons/BatteryChargingFull', import_modules: 'BatteryChargingFullIcon' } if settings.has_key?('BatteryChargingFullIcon')
    importData <<  {module: 'material-ui-icons/BatteryFull', import_modules: 'BatteryFullIcon' } if settings.has_key?('BatteryFullIcon')
    importData <<  {module: 'material-ui-icons/BatteryStd', import_modules: 'BatteryStdIcon' } if settings.has_key?('BatteryStdIcon')
    importData <<  {module: 'material-ui-icons/BatteryUnknown', import_modules: 'BatteryUnknownIcon' } if settings.has_key?('BatteryUnknownIcon')
    importData <<  {module: 'material-ui-icons/BeachAccess', import_modules: 'BeachAccessIcon' } if settings.has_key?('BeachAccessIcon')
    importData <<  {module: 'material-ui-icons/Beenhere', import_modules: 'BeenhereIcon' } if settings.has_key?('BeenhereIcon')
    importData <<  {module: 'material-ui-icons/Block', import_modules: 'BlockIcon' } if settings.has_key?('BlockIcon')
    importData <<  {module: 'material-ui-icons/Bluetooth', import_modules: 'BluetoothIcon' } if settings.has_key?('BluetoothIcon')
    importData <<  {module: 'material-ui-icons/BluetoothAudio', import_modules: 'BluetoothAudioIcon' } if settings.has_key?('BluetoothAudioIcon')
    importData <<  {module: 'material-ui-icons/BluetoothConnected', import_modules: 'BluetoothConnectedIcon' } if settings.has_key?('BluetoothConnectedIcon')
    importData <<  {module: 'material-ui-icons/BluetoothDisabled', import_modules: 'BluetoothDisabledIcon' } if settings.has_key?('BluetoothDisabledIcon')
    importData <<  {module: 'material-ui-icons/BluetoothSearching', import_modules: 'BluetoothSearchingIcon' } if settings.has_key?('BluetoothSearchingIcon')
    importData <<  {module: 'material-ui-icons/BlurCircular', import_modules: 'BlurCircularIcon' } if settings.has_key?('BlurCircularIcon')
    importData <<  {module: 'material-ui-icons/BlurLinear', import_modules: 'BlurLinearIcon' } if settings.has_key?('BlurLinearIcon')
    importData <<  {module: 'material-ui-icons/BlurOff', import_modules: 'BlurOffIcon' } if settings.has_key?('BlurOffIcon')
    importData <<  {module: 'material-ui-icons/BlurOn', import_modules: 'BlurOnIcon' } if settings.has_key?('BlurOnIcon')
    importData <<  {module: 'material-ui-icons/Book', import_modules: 'BookIcon' } if settings.has_key?('BookIcon')
    importData <<  {module: 'material-ui-icons/Bookmark', import_modules: 'BookmarkIcon' } if settings.has_key?('BookmarkIcon')
    importData <<  {module: 'material-ui-icons/BookmarkBorder', import_modules: 'BookmarkBorderIcon' } if settings.has_key?('BookmarkBorderIcon')
    importData <<  {module: 'material-ui-icons/BorderAll', import_modules: 'BorderAllIcon' } if settings.has_key?('BorderAllIcon')
    importData <<  {module: 'material-ui-icons/BorderBottom', import_modules: 'BorderBottomIcon' } if settings.has_key?('BorderBottomIcon')
    importData <<  {module: 'material-ui-icons/BorderClear', import_modules: 'BorderClearIcon' } if settings.has_key?('BorderClearIcon')
    importData <<  {module: 'material-ui-icons/BorderColor', import_modules: 'BorderColorIcon' } if settings.has_key?('BorderColorIcon')
    importData <<  {module: 'material-ui-icons/BorderHorizontal', import_modules: 'BorderHorizontalIcon' } if settings.has_key?('BorderHorizontalIcon')
    importData <<  {module: 'material-ui-icons/BorderInner', import_modules: 'BorderInnerIcon' } if settings.has_key?('BorderInnerIcon')
    importData <<  {module: 'material-ui-icons/BorderLeft', import_modules: 'BorderLeftIcon' } if settings.has_key?('BorderLeftIcon')
    importData <<  {module: 'material-ui-icons/BorderOuter', import_modules: 'BorderOuterIcon' } if settings.has_key?('BorderOuterIcon')
    importData <<  {module: 'material-ui-icons/BorderRight', import_modules: 'BorderRightIcon' } if settings.has_key?('BorderRightIcon')
    importData <<  {module: 'material-ui-icons/BorderStyle', import_modules: 'BorderStyleIcon' } if settings.has_key?('BorderStyleIcon')
    importData <<  {module: 'material-ui-icons/BorderTop', import_modules: 'BorderTopIcon' } if settings.has_key?('BorderTopIcon')
    importData <<  {module: 'material-ui-icons/BorderVertical', import_modules: 'BorderVerticalIcon' } if settings.has_key?('BorderVerticalIcon')
    importData <<  {module: 'material-ui-icons/BrandingWatermark', import_modules: 'BrandingWatermarkIcon' } if settings.has_key?('BrandingWatermarkIcon')
    importData <<  {module: 'material-ui-icons/Brightness1', import_modules: 'Brightness1Icon' } if settings.has_key?('Brightness1Icon')
    importData <<  {module: 'material-ui-icons/Brightness2', import_modules: 'Brightness2Icon' } if settings.has_key?('Brightness2Icon')
    importData <<  {module: 'material-ui-icons/Brightness3', import_modules: 'Brightness3Icon' } if settings.has_key?('Brightness3Icon')
    importData <<  {module: 'material-ui-icons/Brightness4', import_modules: 'Brightness4Icon' } if settings.has_key?('Brightness4Icon')
    importData <<  {module: 'material-ui-icons/Brightness5', import_modules: 'Brightness5Icon' } if settings.has_key?('Brightness5Icon')
    importData <<  {module: 'material-ui-icons/Brightness6', import_modules: 'Brightness6Icon' } if settings.has_key?('Brightness6Icon')
    importData <<  {module: 'material-ui-icons/Brightness7', import_modules: 'Brightness7Icon' } if settings.has_key?('Brightness7Icon')
    importData <<  {module: 'material-ui-icons/BrightnessAuto', import_modules: 'BrightnessAutoIcon' } if settings.has_key?('BrightnessAutoIcon')
    importData <<  {module: 'material-ui-icons/BrightnessHigh', import_modules: 'BrightnessHighIcon' } if settings.has_key?('BrightnessHighIcon')
    importData <<  {module: 'material-ui-icons/BrightnessLow', import_modules: 'BrightnessLowIcon' } if settings.has_key?('BrightnessLowIcon')
    importData <<  {module: 'material-ui-icons/BrightnessMedium', import_modules: 'BrightnessMediumIcon' } if settings.has_key?('BrightnessMediumIcon')
    importData <<  {module: 'material-ui-icons/BrokenImage', import_modules: 'BrokenImageIcon' } if settings.has_key?('BrokenImageIcon')
    importData <<  {module: 'material-ui-icons/Brush', import_modules: 'BrushIcon' } if settings.has_key?('BrushIcon')
    importData <<  {module: 'material-ui-icons/BubbleChart', import_modules: 'BubbleChartIcon' } if settings.has_key?('BubbleChartIcon')
    importData <<  {module: 'material-ui-icons/BugReport', import_modules: 'BugReportIcon' } if settings.has_key?('BugReportIcon')
    importData <<  {module: 'material-ui-icons/Build', import_modules: 'BuildIcon' } if settings.has_key?('BuildIcon')
    importData <<  {module: 'material-ui-icons/BurstMode', import_modules: 'BurstModeIcon' } if settings.has_key?('BurstModeIcon')
    importData <<  {module: 'material-ui-icons/Business', import_modules: 'BusinessIcon' } if settings.has_key?('BusinessIcon')
    importData <<  {module: 'material-ui-icons/BusinessCenter', import_modules: 'BusinessCenterIcon' } if settings.has_key?('BusinessCenterIcon')
    importData <<  {module: 'material-ui-icons/Cached', import_modules: 'CachedIcon' } if settings.has_key?('CachedIcon')
    importData <<  {module: 'material-ui-icons/Cake', import_modules: 'CakeIcon' } if settings.has_key?('CakeIcon')
    importData <<  {module: 'material-ui-icons/Call', import_modules: 'CallIcon' } if settings.has_key?('CallIcon')
    importData <<  {module: 'material-ui-icons/CallEnd', import_modules: 'CallEndIcon' } if settings.has_key?('CallEndIcon')
    importData <<  {module: 'material-ui-icons/CallMade', import_modules: 'CallMadeIcon' } if settings.has_key?('CallMadeIcon')
    importData <<  {module: 'material-ui-icons/CallMerge', import_modules: 'CallMergeIcon' } if settings.has_key?('CallMergeIcon')
    importData <<  {module: 'material-ui-icons/CallMissed', import_modules: 'CallMissedIcon' } if settings.has_key?('CallMissedIcon')
    importData <<  {module: 'material-ui-icons/CallMissedOutgoing', import_modules: 'CallMissedOutgoingIcon' } if settings.has_key?('CallMissedOutgoingIcon')
    importData <<  {module: 'material-ui-icons/CallReceived', import_modules: 'CallReceivedIcon' } if settings.has_key?('CallReceivedIcon')
    importData <<  {module: 'material-ui-icons/CallSplit', import_modules: 'CallSplitIcon' } if settings.has_key?('CallSplitIcon')
    importData <<  {module: 'material-ui-icons/CallToAction', import_modules: 'CallToActionIcon' } if settings.has_key?('CallToActionIcon')
    importData <<  {module: 'material-ui-icons/Camera', import_modules: 'CameraIcon' } if settings.has_key?('CameraIcon')
    importData <<  {module: 'material-ui-icons/CameraAlt', import_modules: 'CameraAltIcon' } if settings.has_key?('CameraAltIcon')
    importData <<  {module: 'material-ui-icons/CameraEnhance', import_modules: 'CameraEnhanceIcon' } if settings.has_key?('CameraEnhanceIcon')
    importData <<  {module: 'material-ui-icons/CameraFront', import_modules: 'CameraFrontIcon' } if settings.has_key?('CameraFrontIcon')
    importData <<  {module: 'material-ui-icons/CameraRear', import_modules: 'CameraRearIcon' } if settings.has_key?('CameraRearIcon')
    importData <<  {module: 'material-ui-icons/CameraRoll', import_modules: 'CameraRollIcon' } if settings.has_key?('CameraRollIcon')
    importData <<  {module: 'material-ui-icons/Cancel', import_modules: 'CancelIcon' } if settings.has_key?('CancelIcon')
    importData <<  {module: 'material-ui-icons/CardGiftcard', import_modules: 'CardGiftcardIcon' } if settings.has_key?('CardGiftcardIcon')
    importData <<  {module: 'material-ui-icons/CardMembership', import_modules: 'CardMembershipIcon' } if settings.has_key?('CardMembershipIcon')
    importData <<  {module: 'material-ui-icons/CardTravel', import_modules: 'CardTravelIcon' } if settings.has_key?('CardTravelIcon')
    importData <<  {module: 'material-ui-icons/Casino', import_modules: 'CasinoIcon' } if settings.has_key?('CasinoIcon')
    importData <<  {module: 'material-ui-icons/Cast', import_modules: 'CastIcon' } if settings.has_key?('CastIcon')
    importData <<  {module: 'material-ui-icons/CastConnected', import_modules: 'CastConnectedIcon' } if settings.has_key?('CastConnectedIcon')
    importData <<  {module: 'material-ui-icons/CenterFocusStrong', import_modules: 'CenterFocusStrongIcon' } if settings.has_key?('CenterFocusStrongIcon')
    importData <<  {module: 'material-ui-icons/CenterFocusWeak', import_modules: 'CenterFocusWeakIcon' } if settings.has_key?('CenterFocusWeakIcon')
    importData <<  {module: 'material-ui-icons/ChangeHistory', import_modules: 'ChangeHistoryIcon' } if settings.has_key?('ChangeHistoryIcon')
    importData <<  {module: 'material-ui-icons/Chat', import_modules: 'ChatIcon' } if settings.has_key?('ChatIcon')
    importData <<  {module: 'material-ui-icons/ChatBubble', import_modules: 'ChatBubbleIcon' } if settings.has_key?('ChatBubbleIcon')
    importData <<  {module: 'material-ui-icons/ChatBubbleOutline', import_modules: 'ChatBubbleOutlineIcon' } if settings.has_key?('ChatBubbleOutlineIcon')
    importData <<  {module: 'material-ui-icons/Check', import_modules: 'CheckIcon' } if settings.has_key?('CheckIcon')
    importData <<  {module: 'material-ui-icons/CheckBox', import_modules: 'CheckBoxIcon' } if settings.has_key?('CheckBoxIcon')
    importData <<  {module: 'material-ui-icons/CheckBoxOutlineBlank', import_modules: 'CheckBoxOutlineBlankIcon' } if settings.has_key?('CheckBoxOutlineBlankIcon')
    importData <<  {module: 'material-ui-icons/CheckCircle', import_modules: 'CheckCircleIcon' } if settings.has_key?('CheckCircleIcon')
    importData <<  {module: 'material-ui-icons/ChevronLeft', import_modules: 'ChevronLeftIcon' } if settings.has_key?('ChevronLeftIcon')
    importData <<  {module: 'material-ui-icons/ChevronRight', import_modules: 'ChevronRightIcon' } if settings.has_key?('ChevronRightIcon')
    importData <<  {module: 'material-ui-icons/ChildCare', import_modules: 'ChildCareIcon' } if settings.has_key?('ChildCareIcon')
    importData <<  {module: 'material-ui-icons/ChildFriendly', import_modules: 'ChildFriendlyIcon' } if settings.has_key?('ChildFriendlyIcon')
    importData <<  {module: 'material-ui-icons/ChromeReaderMode', import_modules: 'ChromeReaderModeIcon' } if settings.has_key?('ChromeReaderModeIcon')
    importData <<  {module: 'material-ui-icons/Class', import_modules: 'ClassIcon' } if settings.has_key?('ClassIcon')
    importData <<  {module: 'material-ui-icons/Clear', import_modules: 'ClearIcon' } if settings.has_key?('ClearIcon')
    importData <<  {module: 'material-ui-icons/ClearAll', import_modules: 'ClearAllIcon' } if settings.has_key?('ClearAllIcon')
    importData <<  {module: 'material-ui-icons/Close', import_modules: 'CloseIcon' } if settings.has_key?('CloseIcon')
    importData <<  {module: 'material-ui-icons/ClosedCaption', import_modules: 'ClosedCaptionIcon' } if settings.has_key?('ClosedCaptionIcon')
    importData <<  {module: 'material-ui-icons/Cloud', import_modules: 'CloudIcon' } if settings.has_key?('CloudIcon')
    importData <<  {module: 'material-ui-icons/CloudCircle', import_modules: 'CloudCircleIcon' } if settings.has_key?('CloudCircleIcon')
    importData <<  {module: 'material-ui-icons/CloudDone', import_modules: 'CloudDoneIcon' } if settings.has_key?('CloudDoneIcon')
    importData <<  {module: 'material-ui-icons/CloudDownload', import_modules: 'CloudDownloadIcon' } if settings.has_key?('CloudDownloadIcon')
    importData <<  {module: 'material-ui-icons/CloudOff', import_modules: 'CloudOffIcon' } if settings.has_key?('CloudOffIcon')
    importData <<  {module: 'material-ui-icons/CloudQueue', import_modules: 'CloudQueueIcon' } if settings.has_key?('CloudQueueIcon')
    importData <<  {module: 'material-ui-icons/CloudUpload', import_modules: 'CloudUploadIcon' } if settings.has_key?('CloudUploadIcon')
    importData <<  {module: 'material-ui-icons/Code', import_modules: 'CodeIcon' } if settings.has_key?('CodeIcon')
    importData <<  {module: 'material-ui-icons/Collections', import_modules: 'CollectionsIcon' } if settings.has_key?('CollectionsIcon')
    importData <<  {module: 'material-ui-icons/CollectionsBookmark', import_modules: 'CollectionsBookmarkIcon' } if settings.has_key?('CollectionsBookmarkIcon')
    importData <<  {module: 'material-ui-icons/Colorize', import_modules: 'ColorizeIcon' } if settings.has_key?('ColorizeIcon')
    importData <<  {module: 'material-ui-icons/ColorLens', import_modules: 'ColorLensIcon' } if settings.has_key?('ColorLensIcon')
    importData <<  {module: 'material-ui-icons/Comment', import_modules: 'CommentIcon' } if settings.has_key?('CommentIcon')
    importData <<  {module: 'material-ui-icons/Compare', import_modules: 'CompareIcon' } if settings.has_key?('CompareIcon')
    importData <<  {module: 'material-ui-icons/CompareArrows', import_modules: 'CompareArrowsIcon' } if settings.has_key?('CompareArrowsIcon')
    importData <<  {module: 'material-ui-icons/Computer', import_modules: 'ComputerIcon' } if settings.has_key?('ComputerIcon')
    importData <<  {module: 'material-ui-icons/ConfirmationNumber', import_modules: 'ConfirmationNumberIcon' } if settings.has_key?('ConfirmationNumberIcon')
    importData <<  {module: 'material-ui-icons/ContactMail', import_modules: 'ContactMailIcon' } if settings.has_key?('ContactMailIcon')
    importData <<  {module: 'material-ui-icons/ContactPhone', import_modules: 'ContactPhoneIcon' } if settings.has_key?('ContactPhoneIcon')
    importData <<  {module: 'material-ui-icons/Contacts', import_modules: 'ContactsIcon' } if settings.has_key?('ContactsIcon')
    importData <<  {module: 'material-ui-icons/ContentCopy', import_modules: 'ContentCopyIcon' } if settings.has_key?('ContentCopyIcon')
    importData <<  {module: 'material-ui-icons/ContentCut', import_modules: 'ContentCutIcon' } if settings.has_key?('ContentCutIcon')
    importData <<  {module: 'material-ui-icons/ContentPaste', import_modules: 'ContentPasteIcon' } if settings.has_key?('ContentPasteIcon')
    importData <<  {module: 'material-ui-icons/ControlPoint', import_modules: 'ControlPointIcon' } if settings.has_key?('ControlPointIcon')
    importData <<  {module: 'material-ui-icons/ControlPointDuplicate', import_modules: 'ControlPointDuplicateIcon' } if settings.has_key?('ControlPointDuplicateIcon')
    importData <<  {module: 'material-ui-icons/Copyright', import_modules: 'CopyrightIcon' } if settings.has_key?('CopyrightIcon')
    importData <<  {module: 'material-ui-icons/Create', import_modules: 'CreateIcon' } if settings.has_key?('CreateIcon')
    importData <<  {module: 'material-ui-icons/CreateNewFolder', import_modules: 'CreateNewFolderIcon' } if settings.has_key?('CreateNewFolderIcon')
    importData <<  {module: 'material-ui-icons/CreditCard', import_modules: 'CreditCardIcon' } if settings.has_key?('CreditCardIcon')
    importData <<  {module: 'material-ui-icons/Crop', import_modules: 'CropIcon' } if settings.has_key?('CropIcon')
    importData <<  {module: 'material-ui-icons/Crop169', import_modules: 'Crop169Icon' } if settings.has_key?('Crop169Icon')
    importData <<  {module: 'material-ui-icons/Crop32', import_modules: 'Crop32Icon' } if settings.has_key?('Crop32Icon')
    importData <<  {module: 'material-ui-icons/Crop54', import_modules: 'Crop54Icon' } if settings.has_key?('Crop54Icon')
    importData <<  {module: 'material-ui-icons/Crop75', import_modules: 'Crop75Icon' } if settings.has_key?('Crop75Icon')
    importData <<  {module: 'material-ui-icons/CropDin', import_modules: 'CropDinIcon' } if settings.has_key?('CropDinIcon')
    importData <<  {module: 'material-ui-icons/CropFree', import_modules: 'CropFreeIcon' } if settings.has_key?('CropFreeIcon')
    importData <<  {module: 'material-ui-icons/CropLandscape', import_modules: 'CropLandscapeIcon' } if settings.has_key?('CropLandscapeIcon')
    importData <<  {module: 'material-ui-icons/CropOriginal', import_modules: 'CropOriginalIcon' } if settings.has_key?('CropOriginalIcon')
    importData <<  {module: 'material-ui-icons/CropPortrait', import_modules: 'CropPortraitIcon' } if settings.has_key?('CropPortraitIcon')
    importData <<  {module: 'material-ui-icons/CropRotate', import_modules: 'CropRotateIcon' } if settings.has_key?('CropRotateIcon')
    importData <<  {module: 'material-ui-icons/CropSquare', import_modules: 'CropSquareIcon' } if settings.has_key?('CropSquareIcon')
    importData <<  {module: 'material-ui-icons/Dashboard', import_modules: 'DashboardIcon' } if settings.has_key?('DashboardIcon')
    importData <<  {module: 'material-ui-icons/DataUsage', import_modules: 'DataUsageIcon' } if settings.has_key?('DataUsageIcon')
    importData <<  {module: 'material-ui-icons/DateRange', import_modules: 'DateRangeIcon' } if settings.has_key?('DateRangeIcon')
    importData <<  {module: 'material-ui-icons/Dehaze', import_modules: 'DehazeIcon' } if settings.has_key?('DehazeIcon')
    importData <<  {module: 'material-ui-icons/Delete', import_modules: 'DeleteIcon' } if settings.has_key?('DeleteIcon')
    importData <<  {module: 'material-ui-icons/DeleteForever', import_modules: 'DeleteForeverIcon' } if settings.has_key?('DeleteForeverIcon')
    importData <<  {module: 'material-ui-icons/DeleteSweep', import_modules: 'DeleteSweepIcon' } if settings.has_key?('DeleteSweepIcon')
    importData <<  {module: 'material-ui-icons/Description', import_modules: 'DescriptionIcon' } if settings.has_key?('DescriptionIcon')
    importData <<  {module: 'material-ui-icons/DesktopMac', import_modules: 'DesktopMacIcon' } if settings.has_key?('DesktopMacIcon')
    importData <<  {module: 'material-ui-icons/DesktopWindows', import_modules: 'DesktopWindowsIcon' } if settings.has_key?('DesktopWindowsIcon')
    importData <<  {module: 'material-ui-icons/Details', import_modules: 'DetailsIcon' } if settings.has_key?('DetailsIcon')
    importData <<  {module: 'material-ui-icons/DeveloperBoard', import_modules: 'DeveloperBoardIcon' } if settings.has_key?('DeveloperBoardIcon')
    importData <<  {module: 'material-ui-icons/DeveloperMode', import_modules: 'DeveloperModeIcon' } if settings.has_key?('DeveloperModeIcon')
    importData <<  {module: 'material-ui-icons/DeviceHub', import_modules: 'DeviceHubIcon' } if settings.has_key?('DeviceHubIcon')
    importData <<  {module: 'material-ui-icons/Devices', import_modules: 'DevicesIcon' } if settings.has_key?('DevicesIcon')
    importData <<  {module: 'material-ui-icons/DevicesOther', import_modules: 'DevicesOtherIcon' } if settings.has_key?('DevicesOtherIcon')
    importData <<  {module: 'material-ui-icons/DialerSip', import_modules: 'DialerSipIcon' } if settings.has_key?('DialerSipIcon')
    importData <<  {module: 'material-ui-icons/Dialpad', import_modules: 'DialpadIcon' } if settings.has_key?('DialpadIcon')
    importData <<  {module: 'material-ui-icons/Directions', import_modules: 'DirectionsIcon' } if settings.has_key?('DirectionsIcon')
    importData <<  {module: 'material-ui-icons/DirectionsBike', import_modules: 'DirectionsBikeIcon' } if settings.has_key?('DirectionsBikeIcon')
    importData <<  {module: 'material-ui-icons/DirectionsBoat', import_modules: 'DirectionsBoatIcon' } if settings.has_key?('DirectionsBoatIcon')
    importData <<  {module: 'material-ui-icons/DirectionsBus', import_modules: 'DirectionsBusIcon' } if settings.has_key?('DirectionsBusIcon')
    importData <<  {module: 'material-ui-icons/DirectionsCar', import_modules: 'DirectionsCarIcon' } if settings.has_key?('DirectionsCarIcon')
    importData <<  {module: 'material-ui-icons/DirectionsRailway', import_modules: 'DirectionsRailwayIcon' } if settings.has_key?('DirectionsRailwayIcon')
    importData <<  {module: 'material-ui-icons/DirectionsRun', import_modules: 'DirectionsRunIcon' } if settings.has_key?('DirectionsRunIcon')
    importData <<  {module: 'material-ui-icons/DirectionsSubway', import_modules: 'DirectionsSubwayIcon' } if settings.has_key?('DirectionsSubwayIcon')
    importData <<  {module: 'material-ui-icons/DirectionsTransit', import_modules: 'DirectionsTransitIcon' } if settings.has_key?('DirectionsTransitIcon')
    importData <<  {module: 'material-ui-icons/DirectionsWalk', import_modules: 'DirectionsWalkIcon' } if settings.has_key?('DirectionsWalkIcon')
    importData <<  {module: 'material-ui-icons/DiscFull', import_modules: 'DiscFullIcon' } if settings.has_key?('DiscFullIcon')
    importData <<  {module: 'material-ui-icons/Dns', import_modules: 'DnsIcon' } if settings.has_key?('DnsIcon')
    importData <<  {module: 'material-ui-icons/Dock', import_modules: 'DockIcon' } if settings.has_key?('DockIcon')
    importData <<  {module: 'material-ui-icons/Domain', import_modules: 'DomainIcon' } if settings.has_key?('DomainIcon')
    importData <<  {module: 'material-ui-icons/Done', import_modules: 'DoneIcon' } if settings.has_key?('DoneIcon')
    importData <<  {module: 'material-ui-icons/DoneAll', import_modules: 'DoneAllIcon' } if settings.has_key?('DoneAllIcon')
    importData <<  {module: 'material-ui-icons/DoNotDisturb', import_modules: 'DoNotDisturbIcon' } if settings.has_key?('DoNotDisturbIcon')
    importData <<  {module: 'material-ui-icons/DoNotDisturbAlt', import_modules: 'DoNotDisturbAltIcon' } if settings.has_key?('DoNotDisturbAltIcon')
    importData <<  {module: 'material-ui-icons/DoNotDisturbOff', import_modules: 'DoNotDisturbOffIcon' } if settings.has_key?('DoNotDisturbOffIcon')
    importData <<  {module: 'material-ui-icons/DoNotDisturbOn', import_modules: 'DoNotDisturbOnIcon' } if settings.has_key?('DoNotDisturbOnIcon')
    importData <<  {module: 'material-ui-icons/DonutLarge', import_modules: 'DonutLargeIcon' } if settings.has_key?('DonutLargeIcon')
    importData <<  {module: 'material-ui-icons/DonutSmall', import_modules: 'DonutSmallIcon' } if settings.has_key?('DonutSmallIcon')
    importData <<  {module: 'material-ui-icons/Drafts', import_modules: 'DraftsIcon' } if settings.has_key?('DraftsIcon')
    importData <<  {module: 'material-ui-icons/DragHandle', import_modules: 'DragHandleIcon' } if settings.has_key?('DragHandleIcon')
    importData <<  {module: 'material-ui-icons/DriveEta', import_modules: 'DriveEtaIcon' } if settings.has_key?('DriveEtaIcon')
    importData <<  {module: 'material-ui-icons/Dvr', import_modules: 'DvrIcon' } if settings.has_key?('DvrIcon')
    importData <<  {module: 'material-ui-icons/Edit', import_modules: 'EditIcon' } if settings.has_key?('EditIcon')
    importData <<  {module: 'material-ui-icons/EditLocation', import_modules: 'EditLocationIcon' } if settings.has_key?('EditLocationIcon')
    importData <<  {module: 'material-ui-icons/Eject', import_modules: 'EjectIcon' } if settings.has_key?('EjectIcon')
    importData <<  {module: 'material-ui-icons/Email', import_modules: 'EmailIcon' } if settings.has_key?('EmailIcon')
    importData <<  {module: 'material-ui-icons/EnhancedEncryption', import_modules: 'EnhancedEncryptionIcon' } if settings.has_key?('EnhancedEncryptionIcon')
    importData <<  {module: 'material-ui-icons/Equalizer', import_modules: 'EqualizerIcon' } if settings.has_key?('EqualizerIcon')
    importData <<  {module: 'material-ui-icons/Error', import_modules: 'ErrorIcon' } if settings.has_key?('ErrorIcon')
    importData <<  {module: 'material-ui-icons/ErrorOutline', import_modules: 'ErrorOutlineIcon' } if settings.has_key?('ErrorOutlineIcon')
    importData <<  {module: 'material-ui-icons/EuroSymbol', import_modules: 'EuroSymbolIcon' } if settings.has_key?('EuroSymbolIcon')
    importData <<  {module: 'material-ui-icons/Event', import_modules: 'EventIcon' } if settings.has_key?('EventIcon')
    importData <<  {module: 'material-ui-icons/EventAvailable', import_modules: 'EventAvailableIcon' } if settings.has_key?('EventAvailableIcon')
    importData <<  {module: 'material-ui-icons/EventBusy', import_modules: 'EventBusyIcon' } if settings.has_key?('EventBusyIcon')
    importData <<  {module: 'material-ui-icons/EventNote', import_modules: 'EventNoteIcon' } if settings.has_key?('EventNoteIcon')
    importData <<  {module: 'material-ui-icons/EventSeat', import_modules: 'EventSeatIcon' } if settings.has_key?('EventSeatIcon')
    importData <<  {module: 'material-ui-icons/EvStation', import_modules: 'EvStationIcon' } if settings.has_key?('EvStationIcon')
    importData <<  {module: 'material-ui-icons/ExitToApp', import_modules: 'ExitToAppIcon' } if settings.has_key?('ExitToAppIcon')
    importData <<  {module: 'material-ui-icons/ExpandLess', import_modules: 'ExpandLessIcon' } if settings.has_key?('ExpandLessIcon')
    importData <<  {module: 'material-ui-icons/ExpandMore', import_modules: 'ExpandMoreIcon' } if settings.has_key?('ExpandMoreIcon')
    importData <<  {module: 'material-ui-icons/Explicit', import_modules: 'ExplicitIcon' } if settings.has_key?('ExplicitIcon')
    importData <<  {module: 'material-ui-icons/Explore', import_modules: 'ExploreIcon' } if settings.has_key?('ExploreIcon')
    importData <<  {module: 'material-ui-icons/Exposure', import_modules: 'ExposureIcon' } if settings.has_key?('ExposureIcon')
    importData <<  {module: 'material-ui-icons/ExposureNeg1', import_modules: 'ExposureNeg1Icon' } if settings.has_key?('ExposureNeg1Icon')
    importData <<  {module: 'material-ui-icons/ExposureNeg2', import_modules: 'ExposureNeg2Icon' } if settings.has_key?('ExposureNeg2Icon')
    importData <<  {module: 'material-ui-icons/ExposurePlus1', import_modules: 'ExposurePlus1Icon' } if settings.has_key?('ExposurePlus1Icon')
    importData <<  {module: 'material-ui-icons/ExposurePlus2', import_modules: 'ExposurePlus2Icon' } if settings.has_key?('ExposurePlus2Icon')
    importData <<  {module: 'material-ui-icons/ExposureZero', import_modules: 'ExposureZeroIcon' } if settings.has_key?('ExposureZeroIcon')
    importData <<  {module: 'material-ui-icons/Extension', import_modules: 'ExtensionIcon' } if settings.has_key?('ExtensionIcon')
    importData <<  {module: 'material-ui-icons/Face', import_modules: 'FaceIcon' } if settings.has_key?('FaceIcon')
    importData <<  {module: 'material-ui-icons/FastForward', import_modules: 'FastForwardIcon' } if settings.has_key?('FastForwardIcon')
    importData <<  {module: 'material-ui-icons/FastRewind', import_modules: 'FastRewindIcon' } if settings.has_key?('FastRewindIcon')
    importData <<  {module: 'material-ui-icons/Favorite', import_modules: 'FavoriteIcon' } if settings.has_key?('FavoriteIcon')
    importData <<  {module: 'material-ui-icons/FavoriteBorder', import_modules: 'FavoriteBorderIcon' } if settings.has_key?('FavoriteBorderIcon')
    importData <<  {module: 'material-ui-icons/FeaturedPlayList', import_modules: 'FeaturedPlayListIcon' } if settings.has_key?('FeaturedPlayListIcon')
    importData <<  {module: 'material-ui-icons/FeaturedVideo', import_modules: 'FeaturedVideoIcon' } if settings.has_key?('FeaturedVideoIcon')
    importData <<  {module: 'material-ui-icons/Feedback', import_modules: 'FeedbackIcon' } if settings.has_key?('FeedbackIcon')
    importData <<  {module: 'material-ui-icons/FiberDvr', import_modules: 'FiberDvrIcon' } if settings.has_key?('FiberDvrIcon')
    importData <<  {module: 'material-ui-icons/FiberManualRecord', import_modules: 'FiberManualRecordIcon' } if settings.has_key?('FiberManualRecordIcon')
    importData <<  {module: 'material-ui-icons/FiberNew', import_modules: 'FiberNewIcon' } if settings.has_key?('FiberNewIcon')
    importData <<  {module: 'material-ui-icons/FiberPin', import_modules: 'FiberPinIcon' } if settings.has_key?('FiberPinIcon')
    importData <<  {module: 'material-ui-icons/FiberSmartRecord', import_modules: 'FiberSmartRecordIcon' } if settings.has_key?('FiberSmartRecordIcon')
    importData <<  {module: 'material-ui-icons/FileDownload', import_modules: 'FileDownloadIcon' } if settings.has_key?('FileDownloadIcon')
    importData <<  {module: 'material-ui-icons/FileUpload', import_modules: 'FileUploadIcon' } if settings.has_key?('FileUploadIcon')
    importData <<  {module: 'material-ui-icons/Filter', import_modules: 'FilterIcon' } if settings.has_key?('FilterIcon')
    importData <<  {module: 'material-ui-icons/Filter1', import_modules: 'Filter1Icon' } if settings.has_key?('Filter1Icon')
    importData <<  {module: 'material-ui-icons/Filter2', import_modules: 'Filter2Icon' } if settings.has_key?('Filter2Icon')
    importData <<  {module: 'material-ui-icons/Filter3', import_modules: 'Filter3Icon' } if settings.has_key?('Filter3Icon')
    importData <<  {module: 'material-ui-icons/Filter4', import_modules: 'Filter4Icon' } if settings.has_key?('Filter4Icon')
    importData <<  {module: 'material-ui-icons/Filter5', import_modules: 'Filter5Icon' } if settings.has_key?('Filter5Icon')
    importData <<  {module: 'material-ui-icons/Filter6', import_modules: 'Filter6Icon' } if settings.has_key?('Filter6Icon')
    importData <<  {module: 'material-ui-icons/Filter7', import_modules: 'Filter7Icon' } if settings.has_key?('Filter7Icon')
    importData <<  {module: 'material-ui-icons/Filter8', import_modules: 'Filter8Icon' } if settings.has_key?('Filter8Icon')
    importData <<  {module: 'material-ui-icons/Filter9', import_modules: 'Filter9Icon' } if settings.has_key?('Filter9Icon')
    importData <<  {module: 'material-ui-icons/Filter9Plus', import_modules: 'Filter9PlusIcon' } if settings.has_key?('Filter9PlusIcon')
    importData <<  {module: 'material-ui-icons/FilterBAndW', import_modules: 'FilterBAndWIcon' } if settings.has_key?('FilterBAndWIcon')
    importData <<  {module: 'material-ui-icons/FilterCenterFocus', import_modules: 'FilterCenterFocusIcon' } if settings.has_key?('FilterCenterFocusIcon')
    importData <<  {module: 'material-ui-icons/FilterDrama', import_modules: 'FilterDramaIcon' } if settings.has_key?('FilterDramaIcon')
    importData <<  {module: 'material-ui-icons/FilterFrames', import_modules: 'FilterFramesIcon' } if settings.has_key?('FilterFramesIcon')
    importData <<  {module: 'material-ui-icons/FilterHdr', import_modules: 'FilterHdrIcon' } if settings.has_key?('FilterHdrIcon')
    importData <<  {module: 'material-ui-icons/FilterList', import_modules: 'FilterListIcon' } if settings.has_key?('FilterListIcon')
    importData <<  {module: 'material-ui-icons/FilterNone', import_modules: 'FilterNoneIcon' } if settings.has_key?('FilterNoneIcon')
    importData <<  {module: 'material-ui-icons/FilterTiltShift', import_modules: 'FilterTiltShiftIcon' } if settings.has_key?('FilterTiltShiftIcon')
    importData <<  {module: 'material-ui-icons/FilterVintage', import_modules: 'FilterVintageIcon' } if settings.has_key?('FilterVintageIcon')
    importData <<  {module: 'material-ui-icons/FindInPage', import_modules: 'FindInPageIcon' } if settings.has_key?('FindInPageIcon')
    importData <<  {module: 'material-ui-icons/FindReplace', import_modules: 'FindReplaceIcon' } if settings.has_key?('FindReplaceIcon')
    importData <<  {module: 'material-ui-icons/Fingerprint', import_modules: 'FingerprintIcon' } if settings.has_key?('FingerprintIcon')
    importData <<  {module: 'material-ui-icons/FirstPage', import_modules: 'FirstPageIcon' } if settings.has_key?('FirstPageIcon')
    importData <<  {module: 'material-ui-icons/FitnessCenter', import_modules: 'FitnessCenterIcon' } if settings.has_key?('FitnessCenterIcon')
    importData <<  {module: 'material-ui-icons/Flag', import_modules: 'FlagIcon' } if settings.has_key?('FlagIcon')
    importData <<  {module: 'material-ui-icons/Flare', import_modules: 'FlareIcon' } if settings.has_key?('FlareIcon')
    importData <<  {module: 'material-ui-icons/FlashAuto', import_modules: 'FlashAutoIcon' } if settings.has_key?('FlashAutoIcon')
    importData <<  {module: 'material-ui-icons/FlashOff', import_modules: 'FlashOffIcon' } if settings.has_key?('FlashOffIcon')
    importData <<  {module: 'material-ui-icons/FlashOn', import_modules: 'FlashOnIcon' } if settings.has_key?('FlashOnIcon')
    importData <<  {module: 'material-ui-icons/Flight', import_modules: 'FlightIcon' } if settings.has_key?('FlightIcon')
    importData <<  {module: 'material-ui-icons/FlightLand', import_modules: 'FlightLandIcon' } if settings.has_key?('FlightLandIcon')
    importData <<  {module: 'material-ui-icons/FlightTakeoff', import_modules: 'FlightTakeoffIcon' } if settings.has_key?('FlightTakeoffIcon')
    importData <<  {module: 'material-ui-icons/Flip', import_modules: 'FlipIcon' } if settings.has_key?('FlipIcon')
    importData <<  {module: 'material-ui-icons/FlipToBack', import_modules: 'FlipToBackIcon' } if settings.has_key?('FlipToBackIcon')
    importData <<  {module: 'material-ui-icons/FlipToFront', import_modules: 'FlipToFrontIcon' } if settings.has_key?('FlipToFrontIcon')
    importData <<  {module: 'material-ui-icons/Folder', import_modules: 'FolderIcon' } if settings.has_key?('FolderIcon')
    importData <<  {module: 'material-ui-icons/FolderOpen', import_modules: 'FolderOpenIcon' } if settings.has_key?('FolderOpenIcon')
    importData <<  {module: 'material-ui-icons/FolderShared', import_modules: 'FolderSharedIcon' } if settings.has_key?('FolderSharedIcon')
    importData <<  {module: 'material-ui-icons/FolderSpecial', import_modules: 'FolderSpecialIcon' } if settings.has_key?('FolderSpecialIcon')
    importData <<  {module: 'material-ui-icons/FontDownload', import_modules: 'FontDownloadIcon' } if settings.has_key?('FontDownloadIcon')
    importData <<  {module: 'material-ui-icons/FormatAlignCenter', import_modules: 'FormatAlignCenterIcon' } if settings.has_key?('FormatAlignCenterIcon')
    importData <<  {module: 'material-ui-icons/FormatAlignJustify', import_modules: 'FormatAlignJustifyIcon' } if settings.has_key?('FormatAlignJustifyIcon')
    importData <<  {module: 'material-ui-icons/FormatAlignLeft', import_modules: 'FormatAlignLeftIcon' } if settings.has_key?('FormatAlignLeftIcon')
    importData <<  {module: 'material-ui-icons/FormatAlignRight', import_modules: 'FormatAlignRightIcon' } if settings.has_key?('FormatAlignRightIcon')
    importData <<  {module: 'material-ui-icons/FormatBold', import_modules: 'FormatBoldIcon' } if settings.has_key?('FormatBoldIcon')
    importData <<  {module: 'material-ui-icons/FormatClear', import_modules: 'FormatClearIcon' } if settings.has_key?('FormatClearIcon')
    importData <<  {module: 'material-ui-icons/FormatColorFill', import_modules: 'FormatColorFillIcon' } if settings.has_key?('FormatColorFillIcon')
    importData <<  {module: 'material-ui-icons/FormatColorReset', import_modules: 'FormatColorResetIcon' } if settings.has_key?('FormatColorResetIcon')
    importData <<  {module: 'material-ui-icons/FormatColorText', import_modules: 'FormatColorTextIcon' } if settings.has_key?('FormatColorTextIcon')
    importData <<  {module: 'material-ui-icons/FormatIndentDecrease', import_modules: 'FormatIndentDecreaseIcon' } if settings.has_key?('FormatIndentDecreaseIcon')
    importData <<  {module: 'material-ui-icons/FormatIndentIncrease', import_modules: 'FormatIndentIncreaseIcon' } if settings.has_key?('FormatIndentIncreaseIcon')
    importData <<  {module: 'material-ui-icons/FormatItalic', import_modules: 'FormatItalicIcon' } if settings.has_key?('FormatItalicIcon')
    importData <<  {module: 'material-ui-icons/FormatLineSpacing', import_modules: 'FormatLineSpacingIcon' } if settings.has_key?('FormatLineSpacingIcon')
    importData <<  {module: 'material-ui-icons/FormatListBulleted', import_modules: 'FormatListBulletedIcon' } if settings.has_key?('FormatListBulletedIcon')
    importData <<  {module: 'material-ui-icons/FormatListNumbered', import_modules: 'FormatListNumberedIcon' } if settings.has_key?('FormatListNumberedIcon')
    importData <<  {module: 'material-ui-icons/FormatPaint', import_modules: 'FormatPaintIcon' } if settings.has_key?('FormatPaintIcon')
    importData <<  {module: 'material-ui-icons/FormatQuote', import_modules: 'FormatQuoteIcon' } if settings.has_key?('FormatQuoteIcon')
    importData <<  {module: 'material-ui-icons/FormatShapes', import_modules: 'FormatShapesIcon' } if settings.has_key?('FormatShapesIcon')
    importData <<  {module: 'material-ui-icons/FormatSize', import_modules: 'FormatSizeIcon' } if settings.has_key?('FormatSizeIcon')
    importData <<  {module: 'material-ui-icons/FormatStrikethrough', import_modules: 'FormatStrikethroughIcon' } if settings.has_key?('FormatStrikethroughIcon')
    importData <<  {module: 'material-ui-icons/FormatTextdirectionLToR', import_modules: 'FormatTextdirectionLToRIcon' } if settings.has_key?('FormatTextdirectionLToRIcon')
    importData <<  {module: 'material-ui-icons/FormatTextdirectionRToL', import_modules: 'FormatTextdirectionRToLIcon' } if settings.has_key?('FormatTextdirectionRToLIcon')
    importData <<  {module: 'material-ui-icons/FormatUnderlined', import_modules: 'FormatUnderlinedIcon' } if settings.has_key?('FormatUnderlinedIcon')
    importData <<  {module: 'material-ui-icons/Forum', import_modules: 'ForumIcon' } if settings.has_key?('ForumIcon')
    importData <<  {module: 'material-ui-icons/Forward', import_modules: 'ForwardIcon' } if settings.has_key?('ForwardIcon')
    importData <<  {module: 'material-ui-icons/Forward10', import_modules: 'Forward10Icon' } if settings.has_key?('Forward10Icon')
    importData <<  {module: 'material-ui-icons/Forward30', import_modules: 'Forward30Icon' } if settings.has_key?('Forward30Icon')
    importData <<  {module: 'material-ui-icons/Forward5', import_modules: 'Forward5Icon' } if settings.has_key?('Forward5Icon')
    importData <<  {module: 'material-ui-icons/FreeBreakfast', import_modules: 'FreeBreakfastIcon' } if settings.has_key?('FreeBreakfastIcon')
    importData <<  {module: 'material-ui-icons/Fullscreen', import_modules: 'FullscreenIcon' } if settings.has_key?('FullscreenIcon')
    importData <<  {module: 'material-ui-icons/FullscreenExit', import_modules: 'FullscreenExitIcon' } if settings.has_key?('FullscreenExitIcon')
    importData <<  {module: 'material-ui-icons/Functions', import_modules: 'FunctionsIcon' } if settings.has_key?('FunctionsIcon')
    importData <<  {module: 'material-ui-icons/Gamepad', import_modules: 'GamepadIcon' } if settings.has_key?('GamepadIcon')
    importData <<  {module: 'material-ui-icons/Games', import_modules: 'GamesIcon' } if settings.has_key?('GamesIcon')
    importData <<  {module: 'material-ui-icons/Gavel', import_modules: 'GavelIcon' } if settings.has_key?('GavelIcon')
    importData <<  {module: 'material-ui-icons/Gesture', import_modules: 'GestureIcon' } if settings.has_key?('GestureIcon')
    importData <<  {module: 'material-ui-icons/GetApp', import_modules: 'GetAppIcon' } if settings.has_key?('GetAppIcon')
    importData <<  {module: 'material-ui-icons/Gif', import_modules: 'GifIcon' } if settings.has_key?('GifIcon')
    importData <<  {module: 'material-ui-icons/GolfCourse', import_modules: 'GolfCourseIcon' } if settings.has_key?('GolfCourseIcon')
    importData <<  {module: 'material-ui-icons/GpsFixed', import_modules: 'GpsFixedIcon' } if settings.has_key?('GpsFixedIcon')
    importData <<  {module: 'material-ui-icons/GpsNotFixed', import_modules: 'GpsNotFixedIcon' } if settings.has_key?('GpsNotFixedIcon')
    importData <<  {module: 'material-ui-icons/GpsOff', import_modules: 'GpsOffIcon' } if settings.has_key?('GpsOffIcon')
    importData <<  {module: 'material-ui-icons/Grade', import_modules: 'GradeIcon' } if settings.has_key?('GradeIcon')
    importData <<  {module: 'material-ui-icons/Gradient', import_modules: 'GradientIcon' } if settings.has_key?('GradientIcon')
    importData <<  {module: 'material-ui-icons/Grain', import_modules: 'GrainIcon' } if settings.has_key?('GrainIcon')
    importData <<  {module: 'material-ui-icons/GraphicEq', import_modules: 'GraphicEqIcon' } if settings.has_key?('GraphicEqIcon')
    importData <<  {module: 'material-ui-icons/GridOff', import_modules: 'GridOffIcon' } if settings.has_key?('GridOffIcon')
    importData <<  {module: 'material-ui-icons/GridOn', import_modules: 'GridOnIcon' } if settings.has_key?('GridOnIcon')
    importData <<  {module: 'material-ui-icons/Group', import_modules: 'GroupIcon' } if settings.has_key?('GroupIcon')
    importData <<  {module: 'material-ui-icons/GroupAdd', import_modules: 'GroupAddIcon' } if settings.has_key?('GroupAddIcon')
    importData <<  {module: 'material-ui-icons/GroupWork', import_modules: 'GroupWorkIcon' } if settings.has_key?('GroupWorkIcon')
    importData <<  {module: 'material-ui-icons/GTranslate', import_modules: 'GTranslateIcon' } if settings.has_key?('GTranslateIcon')
    importData <<  {module: 'material-ui-icons/Hd', import_modules: 'HdIcon' } if settings.has_key?('HdIcon')
    importData <<  {module: 'material-ui-icons/HdrOff', import_modules: 'HdrOffIcon' } if settings.has_key?('HdrOffIcon')
    importData <<  {module: 'material-ui-icons/HdrOn', import_modules: 'HdrOnIcon' } if settings.has_key?('HdrOnIcon')
    importData <<  {module: 'material-ui-icons/HdrStrong', import_modules: 'HdrStrongIcon' } if settings.has_key?('HdrStrongIcon')
    importData <<  {module: 'material-ui-icons/HdrWeak', import_modules: 'HdrWeakIcon' } if settings.has_key?('HdrWeakIcon')
    importData <<  {module: 'material-ui-icons/Headset', import_modules: 'HeadsetIcon' } if settings.has_key?('HeadsetIcon')
    importData <<  {module: 'material-ui-icons/HeadsetMic', import_modules: 'HeadsetMicIcon' } if settings.has_key?('HeadsetMicIcon')
    importData <<  {module: 'material-ui-icons/Healing', import_modules: 'HealingIcon' } if settings.has_key?('HealingIcon')
    importData <<  {module: 'material-ui-icons/Hearing', import_modules: 'HearingIcon' } if settings.has_key?('HearingIcon')
    importData <<  {module: 'material-ui-icons/Help', import_modules: 'HelpIcon' } if settings.has_key?('HelpIcon')
    importData <<  {module: 'material-ui-icons/HelpOutline', import_modules: 'HelpOutlineIcon' } if settings.has_key?('HelpOutlineIcon')
    importData <<  {module: 'material-ui-icons/Highlight', import_modules: 'HighlightIcon' } if settings.has_key?('HighlightIcon')
    importData <<  {module: 'material-ui-icons/HighlightOff', import_modules: 'HighlightOffIcon' } if settings.has_key?('HighlightOffIcon')
    importData <<  {module: 'material-ui-icons/HighQuality', import_modules: 'HighQualityIcon' } if settings.has_key?('HighQualityIcon')
    importData <<  {module: 'material-ui-icons/History', import_modules: 'HistoryIcon' } if settings.has_key?('HistoryIcon')
    importData <<  {module: 'material-ui-icons/Home', import_modules: 'HomeIcon' } if settings.has_key?('HomeIcon')
    importData <<  {module: 'material-ui-icons/Hotel', import_modules: 'HotelIcon' } if settings.has_key?('HotelIcon')
    importData <<  {module: 'material-ui-icons/HotTub', import_modules: 'HotTubIcon' } if settings.has_key?('HotTubIcon')
    importData <<  {module: 'material-ui-icons/HourglassEmpty', import_modules: 'HourglassEmptyIcon' } if settings.has_key?('HourglassEmptyIcon')
    importData <<  {module: 'material-ui-icons/HourglassFull', import_modules: 'HourglassFullIcon' } if settings.has_key?('HourglassFullIcon')
    importData <<  {module: 'material-ui-icons/Http', import_modules: 'HttpIcon' } if settings.has_key?('HttpIcon')
    importData <<  {module: 'material-ui-icons/Https', import_modules: 'HttpsIcon' } if settings.has_key?('HttpsIcon')
    importData <<  {module: 'material-ui-icons/Image', import_modules: 'ImageIcon' } if settings.has_key?('ImageIcon')
    importData <<  {module: 'material-ui-icons/ImageAspectRatio', import_modules: 'ImageAspectRatioIcon' } if settings.has_key?('ImageAspectRatioIcon')
    importData <<  {module: 'material-ui-icons/ImportantDevices', import_modules: 'ImportantDevicesIcon' } if settings.has_key?('ImportantDevicesIcon')
    importData <<  {module: 'material-ui-icons/ImportContacts', import_modules: 'ImportContactsIcon' } if settings.has_key?('ImportContactsIcon')
    importData <<  {module: 'material-ui-icons/ImportExport', import_modules: 'ImportExportIcon' } if settings.has_key?('ImportExportIcon')
    importData <<  {module: 'material-ui-icons/Inbox', import_modules: 'InboxIcon' } if settings.has_key?('InboxIcon')
    importData <<  {module: 'material-ui-icons/IndeterminateCheckBox', import_modules: 'IndeterminateCheckBoxIcon' } if settings.has_key?('IndeterminateCheckBoxIcon')
    importData <<  {module: 'material-ui-icons/Info', import_modules: 'InfoIcon' } if settings.has_key?('InfoIcon')
    importData <<  {module: 'material-ui-icons/InfoOutline', import_modules: 'InfoOutlineIcon' } if settings.has_key?('InfoOutlineIcon')
    importData <<  {module: 'material-ui-icons/Input', import_modules: 'InputIcon' } if settings.has_key?('InputIcon')
    importData <<  {module: 'material-ui-icons/InsertChart', import_modules: 'InsertChartIcon' } if settings.has_key?('InsertChartIcon')
    importData <<  {module: 'material-ui-icons/InsertComment', import_modules: 'InsertCommentIcon' } if settings.has_key?('InsertCommentIcon')
    importData <<  {module: 'material-ui-icons/InsertDriveFile', import_modules: 'InsertDriveFileIcon' } if settings.has_key?('InsertDriveFileIcon')
    importData <<  {module: 'material-ui-icons/InsertEmoticon', import_modules: 'InsertEmoticonIcon' } if settings.has_key?('InsertEmoticonIcon')
    importData <<  {module: 'material-ui-icons/InsertInvitation', import_modules: 'InsertInvitationIcon' } if settings.has_key?('InsertInvitationIcon')
    importData <<  {module: 'material-ui-icons/InsertLink', import_modules: 'InsertLinkIcon' } if settings.has_key?('InsertLinkIcon')
    importData <<  {module: 'material-ui-icons/InsertPhoto', import_modules: 'InsertPhotoIcon' } if settings.has_key?('InsertPhotoIcon')
    importData <<  {module: 'material-ui-icons/InvertColors', import_modules: 'InvertColorsIcon' } if settings.has_key?('InvertColorsIcon')
    importData <<  {module: 'material-ui-icons/InvertColorsOff', import_modules: 'InvertColorsOffIcon' } if settings.has_key?('InvertColorsOffIcon')
    importData <<  {module: 'material-ui-icons/Iso', import_modules: 'IsoIcon' } if settings.has_key?('IsoIcon')
    importData <<  {module: 'material-ui-icons/Keyboard', import_modules: 'KeyboardIcon' } if settings.has_key?('KeyboardIcon')
    importData <<  {module: 'material-ui-icons/KeyboardArrowDown', import_modules: 'KeyboardArrowDownIcon' } if settings.has_key?('KeyboardArrowDownIcon')
    importData <<  {module: 'material-ui-icons/KeyboardArrowLeft', import_modules: 'KeyboardArrowLeftIcon' } if settings.has_key?('KeyboardArrowLeftIcon')
    importData <<  {module: 'material-ui-icons/KeyboardArrowRight', import_modules: 'KeyboardArrowRightIcon' } if settings.has_key?('KeyboardArrowRightIcon')
    importData <<  {module: 'material-ui-icons/KeyboardArrowUp', import_modules: 'KeyboardArrowUpIcon' } if settings.has_key?('KeyboardArrowUpIcon')
    importData <<  {module: 'material-ui-icons/KeyboardBackspace', import_modules: 'KeyboardBackspaceIcon' } if settings.has_key?('KeyboardBackspaceIcon')
    importData <<  {module: 'material-ui-icons/KeyboardCapslock', import_modules: 'KeyboardCapslockIcon' } if settings.has_key?('KeyboardCapslockIcon')
    importData <<  {module: 'material-ui-icons/KeyboardHide', import_modules: 'KeyboardHideIcon' } if settings.has_key?('KeyboardHideIcon')
    importData <<  {module: 'material-ui-icons/KeyboardReturn', import_modules: 'KeyboardReturnIcon' } if settings.has_key?('KeyboardReturnIcon')
    importData <<  {module: 'material-ui-icons/KeyboardTab', import_modules: 'KeyboardTabIcon' } if settings.has_key?('KeyboardTabIcon')
    importData <<  {module: 'material-ui-icons/KeyboardVoice', import_modules: 'KeyboardVoiceIcon' } if settings.has_key?('KeyboardVoiceIcon')
    importData <<  {module: 'material-ui-icons/Kitchen', import_modules: 'KitchenIcon' } if settings.has_key?('KitchenIcon')
    importData <<  {module: 'material-ui-icons/Label', import_modules: 'LabelIcon' } if settings.has_key?('LabelIcon')
    importData <<  {module: 'material-ui-icons/LabelOutline', import_modules: 'LabelOutlineIcon' } if settings.has_key?('LabelOutlineIcon')
    importData <<  {module: 'material-ui-icons/Landscape', import_modules: 'LandscapeIcon' } if settings.has_key?('LandscapeIcon')
    importData <<  {module: 'material-ui-icons/Language', import_modules: 'LanguageIcon' } if settings.has_key?('LanguageIcon')
    importData <<  {module: 'material-ui-icons/Laptop', import_modules: 'LaptopIcon' } if settings.has_key?('LaptopIcon')
    importData <<  {module: 'material-ui-icons/LaptopChromebook', import_modules: 'LaptopChromebookIcon' } if settings.has_key?('LaptopChromebookIcon')
    importData <<  {module: 'material-ui-icons/LaptopMac', import_modules: 'LaptopMacIcon' } if settings.has_key?('LaptopMacIcon')
    importData <<  {module: 'material-ui-icons/LaptopWindows', import_modules: 'LaptopWindowsIcon' } if settings.has_key?('LaptopWindowsIcon')
    importData <<  {module: 'material-ui-icons/LastPage', import_modules: 'LastPageIcon' } if settings.has_key?('LastPageIcon')
    importData <<  {module: 'material-ui-icons/Launch', import_modules: 'LaunchIcon' } if settings.has_key?('LaunchIcon')
    importData <<  {module: 'material-ui-icons/Layers', import_modules: 'LayersIcon' } if settings.has_key?('LayersIcon')
    importData <<  {module: 'material-ui-icons/LayersClear', import_modules: 'LayersClearIcon' } if settings.has_key?('LayersClearIcon')
    importData <<  {module: 'material-ui-icons/LeakAdd', import_modules: 'LeakAddIcon' } if settings.has_key?('LeakAddIcon')
    importData <<  {module: 'material-ui-icons/LeakRemove', import_modules: 'LeakRemoveIcon' } if settings.has_key?('LeakRemoveIcon')
    importData <<  {module: 'material-ui-icons/Lens', import_modules: 'LensIcon' } if settings.has_key?('LensIcon')
    importData <<  {module: 'material-ui-icons/LibraryAdd', import_modules: 'LibraryAddIcon' } if settings.has_key?('LibraryAddIcon')
    importData <<  {module: 'material-ui-icons/LibraryBooks', import_modules: 'LibraryBooksIcon' } if settings.has_key?('LibraryBooksIcon')
    importData <<  {module: 'material-ui-icons/LibraryMusic', import_modules: 'LibraryMusicIcon' } if settings.has_key?('LibraryMusicIcon')
    importData <<  {module: 'material-ui-icons/LightbulbOutline', import_modules: 'LightbulbOutlineIcon' } if settings.has_key?('LightbulbOutlineIcon')
    importData <<  {module: 'material-ui-icons/LinearScale', import_modules: 'LinearScaleIcon' } if settings.has_key?('LinearScaleIcon')
    importData <<  {module: 'material-ui-icons/LineStyle', import_modules: 'LineStyleIcon' } if settings.has_key?('LineStyleIcon')
    importData <<  {module: 'material-ui-icons/LineWeight', import_modules: 'LineWeightIcon' } if settings.has_key?('LineWeightIcon')
    importData <<  {module: 'material-ui-icons/Link', import_modules: 'LinkIcon' } if settings.has_key?('LinkIcon')
    importData <<  {module: 'material-ui-icons/LinkedCamera', import_modules: 'LinkedCameraIcon' } if settings.has_key?('LinkedCameraIcon')
    importData <<  {module: 'material-ui-icons/List', import_modules: 'ListIcon' } if settings.has_key?('ListIcon')
    importData <<  {module: 'material-ui-icons/LiveHelp', import_modules: 'LiveHelpIcon' } if settings.has_key?('LiveHelpIcon')
    importData <<  {module: 'material-ui-icons/LiveTv', import_modules: 'LiveTvIcon' } if settings.has_key?('LiveTvIcon')
    importData <<  {module: 'material-ui-icons/LocalActivity', import_modules: 'LocalActivityIcon' } if settings.has_key?('LocalActivityIcon')
    importData <<  {module: 'material-ui-icons/LocalAirport', import_modules: 'LocalAirportIcon' } if settings.has_key?('LocalAirportIcon')
    importData <<  {module: 'material-ui-icons/LocalAtm', import_modules: 'LocalAtmIcon' } if settings.has_key?('LocalAtmIcon')
    importData <<  {module: 'material-ui-icons/LocalBar', import_modules: 'LocalBarIcon' } if settings.has_key?('LocalBarIcon')
    importData <<  {module: 'material-ui-icons/LocalCafe', import_modules: 'LocalCafeIcon' } if settings.has_key?('LocalCafeIcon')
    importData <<  {module: 'material-ui-icons/LocalCarWash', import_modules: 'LocalCarWashIcon' } if settings.has_key?('LocalCarWashIcon')
    importData <<  {module: 'material-ui-icons/LocalConvenienceStore', import_modules: 'LocalConvenienceStoreIcon' } if settings.has_key?('LocalConvenienceStoreIcon')
    importData <<  {module: 'material-ui-icons/LocalDining', import_modules: 'LocalDiningIcon' } if settings.has_key?('LocalDiningIcon')
    importData <<  {module: 'material-ui-icons/LocalDrink', import_modules: 'LocalDrinkIcon' } if settings.has_key?('LocalDrinkIcon')
    importData <<  {module: 'material-ui-icons/LocalFlorist', import_modules: 'LocalFloristIcon' } if settings.has_key?('LocalFloristIcon')
    importData <<  {module: 'material-ui-icons/LocalGasStation', import_modules: 'LocalGasStationIcon' } if settings.has_key?('LocalGasStationIcon')
    importData <<  {module: 'material-ui-icons/LocalGroceryStore', import_modules: 'LocalGroceryStoreIcon' } if settings.has_key?('LocalGroceryStoreIcon')
    importData <<  {module: 'material-ui-icons/LocalHospital', import_modules: 'LocalHospitalIcon' } if settings.has_key?('LocalHospitalIcon')
    importData <<  {module: 'material-ui-icons/LocalHotel', import_modules: 'LocalHotelIcon' } if settings.has_key?('LocalHotelIcon')
    importData <<  {module: 'material-ui-icons/LocalLaundryService', import_modules: 'LocalLaundryServiceIcon' } if settings.has_key?('LocalLaundryServiceIcon')
    importData <<  {module: 'material-ui-icons/LocalLibrary', import_modules: 'LocalLibraryIcon' } if settings.has_key?('LocalLibraryIcon')
    importData <<  {module: 'material-ui-icons/LocalMall', import_modules: 'LocalMallIcon' } if settings.has_key?('LocalMallIcon')
    importData <<  {module: 'material-ui-icons/LocalMovies', import_modules: 'LocalMoviesIcon' } if settings.has_key?('LocalMoviesIcon')
    importData <<  {module: 'material-ui-icons/LocalOffer', import_modules: 'LocalOfferIcon' } if settings.has_key?('LocalOfferIcon')
    importData <<  {module: 'material-ui-icons/LocalParking', import_modules: 'LocalParkingIcon' } if settings.has_key?('LocalParkingIcon')
    importData <<  {module: 'material-ui-icons/LocalPharmacy', import_modules: 'LocalPharmacyIcon' } if settings.has_key?('LocalPharmacyIcon')
    importData <<  {module: 'material-ui-icons/LocalPhone', import_modules: 'LocalPhoneIcon' } if settings.has_key?('LocalPhoneIcon')
    importData <<  {module: 'material-ui-icons/LocalPizza', import_modules: 'LocalPizzaIcon' } if settings.has_key?('LocalPizzaIcon')
    importData <<  {module: 'material-ui-icons/LocalPlay', import_modules: 'LocalPlayIcon' } if settings.has_key?('LocalPlayIcon')
    importData <<  {module: 'material-ui-icons/LocalPostOffice', import_modules: 'LocalPostOfficeIcon' } if settings.has_key?('LocalPostOfficeIcon')
    importData <<  {module: 'material-ui-icons/LocalPrintshop', import_modules: 'LocalPrintshopIcon' } if settings.has_key?('LocalPrintshopIcon')
    importData <<  {module: 'material-ui-icons/LocalSee', import_modules: 'LocalSeeIcon' } if settings.has_key?('LocalSeeIcon')
    importData <<  {module: 'material-ui-icons/LocalShipping', import_modules: 'LocalShippingIcon' } if settings.has_key?('LocalShippingIcon')
    importData <<  {module: 'material-ui-icons/LocalTaxi', import_modules: 'LocalTaxiIcon' } if settings.has_key?('LocalTaxiIcon')
    importData <<  {module: 'material-ui-icons/LocationCity', import_modules: 'LocationCityIcon' } if settings.has_key?('LocationCityIcon')
    importData <<  {module: 'material-ui-icons/LocationDisabled', import_modules: 'LocationDisabledIcon' } if settings.has_key?('LocationDisabledIcon')
    importData <<  {module: 'material-ui-icons/LocationOff', import_modules: 'LocationOffIcon' } if settings.has_key?('LocationOffIcon')
    importData <<  {module: 'material-ui-icons/LocationOn', import_modules: 'LocationOnIcon' } if settings.has_key?('LocationOnIcon')
    importData <<  {module: 'material-ui-icons/LocationSearching', import_modules: 'LocationSearchingIcon' } if settings.has_key?('LocationSearchingIcon')
    importData <<  {module: 'material-ui-icons/Lock', import_modules: 'LockIcon' } if settings.has_key?('LockIcon')
    importData <<  {module: 'material-ui-icons/LockOpen', import_modules: 'LockOpenIcon' } if settings.has_key?('LockOpenIcon')
    importData <<  {module: 'material-ui-icons/LockOutline', import_modules: 'LockOutlineIcon' } if settings.has_key?('LockOutlineIcon')
    importData <<  {module: 'material-ui-icons/Looks', import_modules: 'LooksIcon' } if settings.has_key?('LooksIcon')
    importData <<  {module: 'material-ui-icons/Looks3', import_modules: 'Looks3Icon' } if settings.has_key?('Looks3Icon')
    importData <<  {module: 'material-ui-icons/Looks4', import_modules: 'Looks4Icon' } if settings.has_key?('Looks4Icon')
    importData <<  {module: 'material-ui-icons/Looks5', import_modules: 'Looks5Icon' } if settings.has_key?('Looks5Icon')
    importData <<  {module: 'material-ui-icons/Looks6', import_modules: 'Looks6Icon' } if settings.has_key?('Looks6Icon')
    importData <<  {module: 'material-ui-icons/LooksOne', import_modules: 'LooksOneIcon' } if settings.has_key?('LooksOneIcon')
    importData <<  {module: 'material-ui-icons/LooksTwo', import_modules: 'LooksTwoIcon' } if settings.has_key?('LooksTwoIcon')
    importData <<  {module: 'material-ui-icons/Loop', import_modules: 'LoopIcon' } if settings.has_key?('LoopIcon')
    importData <<  {module: 'material-ui-icons/Loupe', import_modules: 'LoupeIcon' } if settings.has_key?('LoupeIcon')
    importData <<  {module: 'material-ui-icons/LowPriority', import_modules: 'LowPriorityIcon' } if settings.has_key?('LowPriorityIcon')
    importData <<  {module: 'material-ui-icons/Loyalty', import_modules: 'LoyaltyIcon' } if settings.has_key?('LoyaltyIcon')
    importData <<  {module: 'material-ui-icons/Mail', import_modules: 'MailIcon' } if settings.has_key?('MailIcon')
    importData <<  {module: 'material-ui-icons/MailOutline', import_modules: 'MailOutlineIcon' } if settings.has_key?('MailOutlineIcon')
    importData <<  {module: 'material-ui-icons/Map', import_modules: 'MapIcon' } if settings.has_key?('MapIcon')
    importData <<  {module: 'material-ui-icons/Markunread', import_modules: 'MarkunreadIcon' } if settings.has_key?('MarkunreadIcon')
    importData <<  {module: 'material-ui-icons/MarkunreadMailbox', import_modules: 'MarkunreadMailboxIcon' } if settings.has_key?('MarkunreadMailboxIcon')
    importData <<  {module: 'material-ui-icons/Memory', import_modules: 'MemoryIcon' } if settings.has_key?('MemoryIcon')
    importData <<  {module: 'material-ui-icons/Menu', import_modules: 'MenuIcon' } if settings.has_key?('MenuIcon')
    importData <<  {module: 'material-ui-icons/MergeType', import_modules: 'MergeTypeIcon' } if settings.has_key?('MergeTypeIcon')
    importData <<  {module: 'material-ui-icons/Message', import_modules: 'MessageIcon' } if settings.has_key?('MessageIcon')
    importData <<  {module: 'material-ui-icons/Mic', import_modules: 'MicIcon' } if settings.has_key?('MicIcon')
    importData <<  {module: 'material-ui-icons/MicNone', import_modules: 'MicNoneIcon' } if settings.has_key?('MicNoneIcon')
    importData <<  {module: 'material-ui-icons/MicOff', import_modules: 'MicOffIcon' } if settings.has_key?('MicOffIcon')
    importData <<  {module: 'material-ui-icons/Mms', import_modules: 'MmsIcon' } if settings.has_key?('MmsIcon')
    importData <<  {module: 'material-ui-icons/ModeComment', import_modules: 'ModeCommentIcon' } if settings.has_key?('ModeCommentIcon')
    importData <<  {module: 'material-ui-icons/ModeEdit', import_modules: 'ModeEditIcon' } if settings.has_key?('ModeEditIcon')
    importData <<  {module: 'material-ui-icons/MonetizationOn', import_modules: 'MonetizationOnIcon' } if settings.has_key?('MonetizationOnIcon')
    importData <<  {module: 'material-ui-icons/MoneyOff', import_modules: 'MoneyOffIcon' } if settings.has_key?('MoneyOffIcon')
    importData <<  {module: 'material-ui-icons/MonochromePhotos', import_modules: 'MonochromePhotosIcon' } if settings.has_key?('MonochromePhotosIcon')
    importData <<  {module: 'material-ui-icons/Mood', import_modules: 'MoodIcon' } if settings.has_key?('MoodIcon')
    importData <<  {module: 'material-ui-icons/MoodBad', import_modules: 'MoodBadIcon' } if settings.has_key?('MoodBadIcon')
    importData <<  {module: 'material-ui-icons/More', import_modules: 'MoreIcon' } if settings.has_key?('MoreIcon')
    importData <<  {module: 'material-ui-icons/MoreHoriz', import_modules: 'MoreHorizIcon' } if settings.has_key?('MoreHorizIcon')
    importData <<  {module: 'material-ui-icons/MoreVert', import_modules: 'MoreVertIcon' } if settings.has_key?('MoreVertIcon')
    importData <<  {module: 'material-ui-icons/Motorcycle', import_modules: 'MotorcycleIcon' } if settings.has_key?('MotorcycleIcon')
    importData <<  {module: 'material-ui-icons/Mouse', import_modules: 'MouseIcon' } if settings.has_key?('MouseIcon')
    importData <<  {module: 'material-ui-icons/MoveToInbox', import_modules: 'MoveToInboxIcon' } if settings.has_key?('MoveToInboxIcon')
    importData <<  {module: 'material-ui-icons/Movie', import_modules: 'MovieIcon' } if settings.has_key?('MovieIcon')
    importData <<  {module: 'material-ui-icons/MovieCreation', import_modules: 'MovieCreationIcon' } if settings.has_key?('MovieCreationIcon')
    importData <<  {module: 'material-ui-icons/MovieFilter', import_modules: 'MovieFilterIcon' } if settings.has_key?('MovieFilterIcon')
    importData <<  {module: 'material-ui-icons/MultilineChart', import_modules: 'MultilineChartIcon' } if settings.has_key?('MultilineChartIcon')
    importData <<  {module: 'material-ui-icons/MusicNote', import_modules: 'MusicNoteIcon' } if settings.has_key?('MusicNoteIcon')
    importData <<  {module: 'material-ui-icons/MusicVideo', import_modules: 'MusicVideoIcon' } if settings.has_key?('MusicVideoIcon')
    importData <<  {module: 'material-ui-icons/MyLocation', import_modules: 'MyLocationIcon' } if settings.has_key?('MyLocationIcon')
    importData <<  {module: 'material-ui-icons/Nature', import_modules: 'NatureIcon' } if settings.has_key?('NatureIcon')
    importData <<  {module: 'material-ui-icons/NaturePeople', import_modules: 'NaturePeopleIcon' } if settings.has_key?('NaturePeopleIcon')
    importData <<  {module: 'material-ui-icons/NavigateBefore', import_modules: 'NavigateBeforeIcon' } if settings.has_key?('NavigateBeforeIcon')
    importData <<  {module: 'material-ui-icons/NavigateNext', import_modules: 'NavigateNextIcon' } if settings.has_key?('NavigateNextIcon')
    importData <<  {module: 'material-ui-icons/Navigation', import_modules: 'NavigationIcon' } if settings.has_key?('NavigationIcon')
    importData <<  {module: 'material-ui-icons/NearMe', import_modules: 'NearMeIcon' } if settings.has_key?('NearMeIcon')
    importData <<  {module: 'material-ui-icons/NetworkCell', import_modules: 'NetworkCellIcon' } if settings.has_key?('NetworkCellIcon')
    importData <<  {module: 'material-ui-icons/NetworkCheck', import_modules: 'NetworkCheckIcon' } if settings.has_key?('NetworkCheckIcon')
    importData <<  {module: 'material-ui-icons/NetworkLocked', import_modules: 'NetworkLockedIcon' } if settings.has_key?('NetworkLockedIcon')
    importData <<  {module: 'material-ui-icons/NetworkWifi', import_modules: 'NetworkWifiIcon' } if settings.has_key?('NetworkWifiIcon')
    importData <<  {module: 'material-ui-icons/NewReleases', import_modules: 'NewReleasesIcon' } if settings.has_key?('NewReleasesIcon')
    importData <<  {module: 'material-ui-icons/NextWeek', import_modules: 'NextWeekIcon' } if settings.has_key?('NextWeekIcon')
    importData <<  {module: 'material-ui-icons/Nfc', import_modules: 'NfcIcon' } if settings.has_key?('NfcIcon')
    importData <<  {module: 'material-ui-icons/NoEncryption', import_modules: 'NoEncryptionIcon' } if settings.has_key?('NoEncryptionIcon')
    importData <<  {module: 'material-ui-icons/NoSim', import_modules: 'NoSimIcon' } if settings.has_key?('NoSimIcon')
    importData <<  {module: 'material-ui-icons/Note', import_modules: 'NoteIcon' } if settings.has_key?('NoteIcon')
    importData <<  {module: 'material-ui-icons/NoteAdd', import_modules: 'NoteAddIcon' } if settings.has_key?('NoteAddIcon')
    importData <<  {module: 'material-ui-icons/Notifications', import_modules: 'NotificationsIcon' } if settings.has_key?('NotificationsIcon')
    importData <<  {module: 'material-ui-icons/NotificationsActive', import_modules: 'NotificationsActiveIcon' } if settings.has_key?('NotificationsActiveIcon')
    importData <<  {module: 'material-ui-icons/NotificationsNone', import_modules: 'NotificationsNoneIcon' } if settings.has_key?('NotificationsNoneIcon')
    importData <<  {module: 'material-ui-icons/NotificationsOff', import_modules: 'NotificationsOffIcon' } if settings.has_key?('NotificationsOffIcon')
    importData <<  {module: 'material-ui-icons/NotificationsPaused', import_modules: 'NotificationsPausedIcon' } if settings.has_key?('NotificationsPausedIcon')
    importData <<  {module: 'material-ui-icons/NotInterested', import_modules: 'NotInterestedIcon' } if settings.has_key?('NotInterestedIcon')
    importData <<  {module: 'material-ui-icons/OfflinePin', import_modules: 'OfflinePinIcon' } if settings.has_key?('OfflinePinIcon')
    importData <<  {module: 'material-ui-icons/OndemandVideo', import_modules: 'OndemandVideoIcon' } if settings.has_key?('OndemandVideoIcon')
    importData <<  {module: 'material-ui-icons/Opacity', import_modules: 'OpacityIcon' } if settings.has_key?('OpacityIcon')
    importData <<  {module: 'material-ui-icons/OpenInBrowser', import_modules: 'OpenInBrowserIcon' } if settings.has_key?('OpenInBrowserIcon')
    importData <<  {module: 'material-ui-icons/OpenInNew', import_modules: 'OpenInNewIcon' } if settings.has_key?('OpenInNewIcon')
    importData <<  {module: 'material-ui-icons/OpenWith', import_modules: 'OpenWithIcon' } if settings.has_key?('OpenWithIcon')
    importData <<  {module: 'material-ui-icons/Pages', import_modules: 'PagesIcon' } if settings.has_key?('PagesIcon')
    importData <<  {module: 'material-ui-icons/Pageview', import_modules: 'PageviewIcon' } if settings.has_key?('PageviewIcon')
    importData <<  {module: 'material-ui-icons/Palette', import_modules: 'PaletteIcon' } if settings.has_key?('PaletteIcon')
    importData <<  {module: 'material-ui-icons/Panorama', import_modules: 'PanoramaIcon' } if settings.has_key?('PanoramaIcon')
    importData <<  {module: 'material-ui-icons/PanoramaFishEye', import_modules: 'PanoramaFishEyeIcon' } if settings.has_key?('PanoramaFishEyeIcon')
    importData <<  {module: 'material-ui-icons/PanoramaHorizontal', import_modules: 'PanoramaHorizontalIcon' } if settings.has_key?('PanoramaHorizontalIcon')
    importData <<  {module: 'material-ui-icons/PanoramaVertical', import_modules: 'PanoramaVerticalIcon' } if settings.has_key?('PanoramaVerticalIcon')
    importData <<  {module: 'material-ui-icons/PanoramaWideAngle', import_modules: 'PanoramaWideAngleIcon' } if settings.has_key?('PanoramaWideAngleIcon')
    importData <<  {module: 'material-ui-icons/PanTool', import_modules: 'PanToolIcon' } if settings.has_key?('PanToolIcon')
    importData <<  {module: 'material-ui-icons/PartyMode', import_modules: 'PartyModeIcon' } if settings.has_key?('PartyModeIcon')
    importData <<  {module: 'material-ui-icons/Pause', import_modules: 'PauseIcon' } if settings.has_key?('PauseIcon')
    importData <<  {module: 'material-ui-icons/PauseCircleFilled', import_modules: 'PauseCircleFilledIcon' } if settings.has_key?('PauseCircleFilledIcon')
    importData <<  {module: 'material-ui-icons/PauseCircleOutline', import_modules: 'PauseCircleOutlineIcon' } if settings.has_key?('PauseCircleOutlineIcon')
    importData <<  {module: 'material-ui-icons/Payment', import_modules: 'PaymentIcon' } if settings.has_key?('PaymentIcon')
    importData <<  {module: 'material-ui-icons/People', import_modules: 'PeopleIcon' } if settings.has_key?('PeopleIcon')
    importData <<  {module: 'material-ui-icons/PeopleOutline', import_modules: 'PeopleOutlineIcon' } if settings.has_key?('PeopleOutlineIcon')
    importData <<  {module: 'material-ui-icons/PermCameraMic', import_modules: 'PermCameraMicIcon' } if settings.has_key?('PermCameraMicIcon')
    importData <<  {module: 'material-ui-icons/PermContactCalendar', import_modules: 'PermContactCalendarIcon' } if settings.has_key?('PermContactCalendarIcon')
    importData <<  {module: 'material-ui-icons/PermDataSetting', import_modules: 'PermDataSettingIcon' } if settings.has_key?('PermDataSettingIcon')
    importData <<  {module: 'material-ui-icons/PermDeviceInformation', import_modules: 'PermDeviceInformationIcon' } if settings.has_key?('PermDeviceInformationIcon')
    importData <<  {module: 'material-ui-icons/PermIdentity', import_modules: 'PermIdentityIcon' } if settings.has_key?('PermIdentityIcon')
    importData <<  {module: 'material-ui-icons/PermMedia', import_modules: 'PermMediaIcon' } if settings.has_key?('PermMediaIcon')
    importData <<  {module: 'material-ui-icons/PermPhoneMsg', import_modules: 'PermPhoneMsgIcon' } if settings.has_key?('PermPhoneMsgIcon')
    importData <<  {module: 'material-ui-icons/PermScanWifi', import_modules: 'PermScanWifiIcon' } if settings.has_key?('PermScanWifiIcon')
    importData <<  {module: 'material-ui-icons/Person', import_modules: 'PersonIcon' } if settings.has_key?('PersonIcon')
    importData <<  {module: 'material-ui-icons/PersonAdd', import_modules: 'PersonAddIcon' } if settings.has_key?('PersonAddIcon')
    importData <<  {module: 'material-ui-icons/PersonalVideo', import_modules: 'PersonalVideoIcon' } if settings.has_key?('PersonalVideoIcon')
    importData <<  {module: 'material-ui-icons/PersonOutline', import_modules: 'PersonOutlineIcon' } if settings.has_key?('PersonOutlineIcon')
    importData <<  {module: 'material-ui-icons/PersonPin', import_modules: 'PersonPinIcon' } if settings.has_key?('PersonPinIcon')
    importData <<  {module: 'material-ui-icons/PersonPinCircle', import_modules: 'PersonPinCircleIcon' } if settings.has_key?('PersonPinCircleIcon')
    importData <<  {module: 'material-ui-icons/Pets', import_modules: 'PetsIcon' } if settings.has_key?('PetsIcon')
    importData <<  {module: 'material-ui-icons/Phone', import_modules: 'PhoneIcon' } if settings.has_key?('PhoneIcon')
    importData <<  {module: 'material-ui-icons/PhoneAndroid', import_modules: 'PhoneAndroidIcon' } if settings.has_key?('PhoneAndroidIcon')
    importData <<  {module: 'material-ui-icons/PhoneBluetoothSpeaker', import_modules: 'PhoneBluetoothSpeakerIcon' } if settings.has_key?('PhoneBluetoothSpeakerIcon')
    importData <<  {module: 'material-ui-icons/PhoneForwarded', import_modules: 'PhoneForwardedIcon' } if settings.has_key?('PhoneForwardedIcon')
    importData <<  {module: 'material-ui-icons/PhoneInTalk', import_modules: 'PhoneInTalkIcon' } if settings.has_key?('PhoneInTalkIcon')
    importData <<  {module: 'material-ui-icons/PhoneIphone', import_modules: 'PhoneIphoneIcon' } if settings.has_key?('PhoneIphoneIcon')
    importData <<  {module: 'material-ui-icons/Phonelink', import_modules: 'PhonelinkIcon' } if settings.has_key?('PhonelinkIcon')
    importData <<  {module: 'material-ui-icons/PhonelinkErase', import_modules: 'PhonelinkEraseIcon' } if settings.has_key?('PhonelinkEraseIcon')
    importData <<  {module: 'material-ui-icons/PhonelinkLock', import_modules: 'PhonelinkLockIcon' } if settings.has_key?('PhonelinkLockIcon')
    importData <<  {module: 'material-ui-icons/PhonelinkOff', import_modules: 'PhonelinkOffIcon' } if settings.has_key?('PhonelinkOffIcon')
    importData <<  {module: 'material-ui-icons/PhonelinkRing', import_modules: 'PhonelinkRingIcon' } if settings.has_key?('PhonelinkRingIcon')
    importData <<  {module: 'material-ui-icons/PhonelinkSetup', import_modules: 'PhonelinkSetupIcon' } if settings.has_key?('PhonelinkSetupIcon')
    importData <<  {module: 'material-ui-icons/PhoneLocked', import_modules: 'PhoneLockedIcon' } if settings.has_key?('PhoneLockedIcon')
    importData <<  {module: 'material-ui-icons/PhoneMissed', import_modules: 'PhoneMissedIcon' } if settings.has_key?('PhoneMissedIcon')
    importData <<  {module: 'material-ui-icons/PhonePaused', import_modules: 'PhonePausedIcon' } if settings.has_key?('PhonePausedIcon')
    importData <<  {module: 'material-ui-icons/Photo', import_modules: 'PhotoIcon' } if settings.has_key?('PhotoIcon')
    importData <<  {module: 'material-ui-icons/PhotoAlbum', import_modules: 'PhotoAlbumIcon' } if settings.has_key?('PhotoAlbumIcon')
    importData <<  {module: 'material-ui-icons/PhotoCamera', import_modules: 'PhotoCameraIcon' } if settings.has_key?('PhotoCameraIcon')
    importData <<  {module: 'material-ui-icons/PhotoFilter', import_modules: 'PhotoFilterIcon' } if settings.has_key?('PhotoFilterIcon')
    importData <<  {module: 'material-ui-icons/PhotoLibrary', import_modules: 'PhotoLibraryIcon' } if settings.has_key?('PhotoLibraryIcon')
    importData <<  {module: 'material-ui-icons/PhotoSizeSelectActual', import_modules: 'PhotoSizeSelectActualIcon' } if settings.has_key?('PhotoSizeSelectActualIcon')
    importData <<  {module: 'material-ui-icons/PhotoSizeSelectLarge', import_modules: 'PhotoSizeSelectLargeIcon' } if settings.has_key?('PhotoSizeSelectLargeIcon')
    importData <<  {module: 'material-ui-icons/PhotoSizeSelectSmall', import_modules: 'PhotoSizeSelectSmallIcon' } if settings.has_key?('PhotoSizeSelectSmallIcon')
    importData <<  {module: 'material-ui-icons/PictureAsPdf', import_modules: 'PictureAsPdfIcon' } if settings.has_key?('PictureAsPdfIcon')
    importData <<  {module: 'material-ui-icons/PictureInPicture', import_modules: 'PictureInPictureIcon' } if settings.has_key?('PictureInPictureIcon')
    importData <<  {module: 'material-ui-icons/PictureInPictureAlt', import_modules: 'PictureInPictureAltIcon' } if settings.has_key?('PictureInPictureAltIcon')
    importData <<  {module: 'material-ui-icons/PieChart', import_modules: 'PieChartIcon' } if settings.has_key?('PieChartIcon')
    importData <<  {module: 'material-ui-icons/PieChartOutlined', import_modules: 'PieChartOutlinedIcon' } if settings.has_key?('PieChartOutlinedIcon')
    importData <<  {module: 'material-ui-icons/PinDrop', import_modules: 'PinDropIcon' } if settings.has_key?('PinDropIcon')
    importData <<  {module: 'material-ui-icons/Place', import_modules: 'PlaceIcon' } if settings.has_key?('PlaceIcon')
    importData <<  {module: 'material-ui-icons/PlayArrow', import_modules: 'PlayArrowIcon' } if settings.has_key?('PlayArrowIcon')
    importData <<  {module: 'material-ui-icons/PlayCircleFilled', import_modules: 'PlayCircleFilledIcon' } if settings.has_key?('PlayCircleFilledIcon')
    importData <<  {module: 'material-ui-icons/PlayCircleOutline', import_modules: 'PlayCircleOutlineIcon' } if settings.has_key?('PlayCircleOutlineIcon')
    importData <<  {module: 'material-ui-icons/PlayForWork', import_modules: 'PlayForWorkIcon' } if settings.has_key?('PlayForWorkIcon')
    importData <<  {module: 'material-ui-icons/PlaylistAdd', import_modules: 'PlaylistAddIcon' } if settings.has_key?('PlaylistAddIcon')
    importData <<  {module: 'material-ui-icons/PlaylistAddCheck', import_modules: 'PlaylistAddCheckIcon' } if settings.has_key?('PlaylistAddCheckIcon')
    importData <<  {module: 'material-ui-icons/PlaylistPlay', import_modules: 'PlaylistPlayIcon' } if settings.has_key?('PlaylistPlayIcon')
    importData <<  {module: 'material-ui-icons/PlusOne', import_modules: 'PlusOneIcon' } if settings.has_key?('PlusOneIcon')
    importData <<  {module: 'material-ui-icons/Poll', import_modules: 'PollIcon' } if settings.has_key?('PollIcon')
    importData <<  {module: 'material-ui-icons/Polymer', import_modules: 'PolymerIcon' } if settings.has_key?('PolymerIcon')
    importData <<  {module: 'material-ui-icons/Pool', import_modules: 'PoolIcon' } if settings.has_key?('PoolIcon')
    importData <<  {module: 'material-ui-icons/PortableWifiOff', import_modules: 'PortableWifiOffIcon' } if settings.has_key?('PortableWifiOffIcon')
    importData <<  {module: 'material-ui-icons/Portrait', import_modules: 'PortraitIcon' } if settings.has_key?('PortraitIcon')
    importData <<  {module: 'material-ui-icons/Power', import_modules: 'PowerIcon' } if settings.has_key?('PowerIcon')
    importData <<  {module: 'material-ui-icons/PowerInput', import_modules: 'PowerInputIcon' } if settings.has_key?('PowerInputIcon')
    importData <<  {module: 'material-ui-icons/PowerSettingsNew', import_modules: 'PowerSettingsNewIcon' } if settings.has_key?('PowerSettingsNewIcon')
    importData <<  {module: 'material-ui-icons/PregnantWoman', import_modules: 'PregnantWomanIcon' } if settings.has_key?('PregnantWomanIcon')
    importData <<  {module: 'material-ui-icons/PresentToAll', import_modules: 'PresentToAllIcon' } if settings.has_key?('PresentToAllIcon')
    importData <<  {module: 'material-ui-icons/Print', import_modules: 'PrintIcon' } if settings.has_key?('PrintIcon')
    importData <<  {module: 'material-ui-icons/PriorityHigh', import_modules: 'PriorityHighIcon' } if settings.has_key?('PriorityHighIcon')
    importData <<  {module: 'material-ui-icons/Public', import_modules: 'PublicIcon' } if settings.has_key?('PublicIcon')
    importData <<  {module: 'material-ui-icons/Publish', import_modules: 'PublishIcon' } if settings.has_key?('PublishIcon')
    importData <<  {module: 'material-ui-icons/QueryBuilder', import_modules: 'QueryBuilderIcon' } if settings.has_key?('QueryBuilderIcon')
    importData <<  {module: 'material-ui-icons/QuestionAnswer', import_modules: 'QuestionAnswerIcon' } if settings.has_key?('QuestionAnswerIcon')
    importData <<  {module: 'material-ui-icons/Queue', import_modules: 'QueueIcon' } if settings.has_key?('QueueIcon')
    importData <<  {module: 'material-ui-icons/QueueMusic', import_modules: 'QueueMusicIcon' } if settings.has_key?('QueueMusicIcon')
    importData <<  {module: 'material-ui-icons/QueuePlayNext', import_modules: 'QueuePlayNextIcon' } if settings.has_key?('QueuePlayNextIcon')
    importData <<  {module: 'material-ui-icons/Radio', import_modules: 'RadioIcon' } if settings.has_key?('RadioIcon')
    importData <<  {module: 'material-ui-icons/RadioButtonChecked', import_modules: 'RadioButtonCheckedIcon' } if settings.has_key?('RadioButtonCheckedIcon')
    importData <<  {module: 'material-ui-icons/RadioButtonUnchecked', import_modules: 'RadioButtonUncheckedIcon' } if settings.has_key?('RadioButtonUncheckedIcon')
    importData <<  {module: 'material-ui-icons/RateReview', import_modules: 'RateReviewIcon' } if settings.has_key?('RateReviewIcon')
    importData <<  {module: 'material-ui-icons/Receipt', import_modules: 'ReceiptIcon' } if settings.has_key?('ReceiptIcon')
    importData <<  {module: 'material-ui-icons/RecentActors', import_modules: 'RecentActorsIcon' } if settings.has_key?('RecentActorsIcon')
    importData <<  {module: 'material-ui-icons/RecordVoiceOver', import_modules: 'RecordVoiceOverIcon' } if settings.has_key?('RecordVoiceOverIcon')
    importData <<  {module: 'material-ui-icons/Redeem', import_modules: 'RedeemIcon' } if settings.has_key?('RedeemIcon')
    importData <<  {module: 'material-ui-icons/Redo', import_modules: 'RedoIcon' } if settings.has_key?('RedoIcon')
    importData <<  {module: 'material-ui-icons/Refresh', import_modules: 'RefreshIcon' } if settings.has_key?('RefreshIcon')
    importData <<  {module: 'material-ui-icons/Remove', import_modules: 'RemoveIcon' } if settings.has_key?('RemoveIcon')
    importData <<  {module: 'material-ui-icons/RemoveCircle', import_modules: 'RemoveCircleIcon' } if settings.has_key?('RemoveCircleIcon')
    importData <<  {module: 'material-ui-icons/RemoveCircleOutline', import_modules: 'RemoveCircleOutlineIcon' } if settings.has_key?('RemoveCircleOutlineIcon')
    importData <<  {module: 'material-ui-icons/RemoveFromQueue', import_modules: 'RemoveFromQueueIcon' } if settings.has_key?('RemoveFromQueueIcon')
    importData <<  {module: 'material-ui-icons/RemoveRedEye', import_modules: 'RemoveRedEyeIcon' } if settings.has_key?('RemoveRedEyeIcon')
    importData <<  {module: 'material-ui-icons/RemoveShoppingCart', import_modules: 'RemoveShoppingCartIcon' } if settings.has_key?('RemoveShoppingCartIcon')
    importData <<  {module: 'material-ui-icons/Reorder', import_modules: 'ReorderIcon' } if settings.has_key?('ReorderIcon')
    importData <<  {module: 'material-ui-icons/Repeat', import_modules: 'RepeatIcon' } if settings.has_key?('RepeatIcon')
    importData <<  {module: 'material-ui-icons/RepeatOne', import_modules: 'RepeatOneIcon' } if settings.has_key?('RepeatOneIcon')
    importData <<  {module: 'material-ui-icons/Replay', import_modules: 'ReplayIcon' } if settings.has_key?('ReplayIcon')
    importData <<  {module: 'material-ui-icons/Replay10', import_modules: 'Replay10Icon' } if settings.has_key?('Replay10Icon')
    importData <<  {module: 'material-ui-icons/Replay30', import_modules: 'Replay30Icon' } if settings.has_key?('Replay30Icon')
    importData <<  {module: 'material-ui-icons/Replay5', import_modules: 'Replay5Icon' } if settings.has_key?('Replay5Icon')
    importData <<  {module: 'material-ui-icons/Reply', import_modules: 'ReplyIcon' } if settings.has_key?('ReplyIcon')
    importData <<  {module: 'material-ui-icons/ReplyAll', import_modules: 'ReplyAllIcon' } if settings.has_key?('ReplyAllIcon')
    importData <<  {module: 'material-ui-icons/Report', import_modules: 'ReportIcon' } if settings.has_key?('ReportIcon')
    importData <<  {module: 'material-ui-icons/ReportProblem', import_modules: 'ReportProblemIcon' } if settings.has_key?('ReportProblemIcon')
    importData <<  {module: 'material-ui-icons/Restaurant', import_modules: 'RestaurantIcon' } if settings.has_key?('RestaurantIcon')
    importData <<  {module: 'material-ui-icons/RestaurantMenu', import_modules: 'RestaurantMenuIcon' } if settings.has_key?('RestaurantMenuIcon')
    importData <<  {module: 'material-ui-icons/Restore', import_modules: 'RestoreIcon' } if settings.has_key?('RestoreIcon')
    importData <<  {module: 'material-ui-icons/RestorePage', import_modules: 'RestorePageIcon' } if settings.has_key?('RestorePageIcon')
    importData <<  {module: 'material-ui-icons/RingVolume', import_modules: 'RingVolumeIcon' } if settings.has_key?('RingVolumeIcon')
    importData <<  {module: 'material-ui-icons/Room', import_modules: 'RoomIcon' } if settings.has_key?('RoomIcon')
    importData <<  {module: 'material-ui-icons/RoomService', import_modules: 'RoomServiceIcon' } if settings.has_key?('RoomServiceIcon')
    importData <<  {module: 'material-ui-icons/Rotate90DegreesCcw', import_modules: 'Rotate90DegreesCcwIcon' } if settings.has_key?('Rotate90DegreesCcwIcon')
    importData <<  {module: 'material-ui-icons/RotateLeft', import_modules: 'RotateLeftIcon' } if settings.has_key?('RotateLeftIcon')
    importData <<  {module: 'material-ui-icons/RotateRight', import_modules: 'RotateRightIcon' } if settings.has_key?('RotateRightIcon')
    importData <<  {module: 'material-ui-icons/RoundedCorner', import_modules: 'RoundedCornerIcon' } if settings.has_key?('RoundedCornerIcon')
    importData <<  {module: 'material-ui-icons/Router', import_modules: 'RouterIcon' } if settings.has_key?('RouterIcon')
    importData <<  {module: 'material-ui-icons/Rowing', import_modules: 'RowingIcon' } if settings.has_key?('RowingIcon')
    importData <<  {module: 'material-ui-icons/RssFeed', import_modules: 'RssFeedIcon' } if settings.has_key?('RssFeedIcon')
    importData <<  {module: 'material-ui-icons/RvHookup', import_modules: 'RvHookupIcon' } if settings.has_key?('RvHookupIcon')
    importData <<  {module: 'material-ui-icons/Satellite', import_modules: 'SatelliteIcon' } if settings.has_key?('SatelliteIcon')
    importData <<  {module: 'material-ui-icons/Save', import_modules: 'SaveIcon' } if settings.has_key?('SaveIcon')
    importData <<  {module: 'material-ui-icons/Scanner', import_modules: 'ScannerIcon' } if settings.has_key?('ScannerIcon')
    importData <<  {module: 'material-ui-icons/Schedule', import_modules: 'ScheduleIcon' } if settings.has_key?('ScheduleIcon')
    importData <<  {module: 'material-ui-icons/School', import_modules: 'SchoolIcon' } if settings.has_key?('SchoolIcon')
    importData <<  {module: 'material-ui-icons/ScreenLockLandscape', import_modules: 'ScreenLockLandscapeIcon' } if settings.has_key?('ScreenLockLandscapeIcon')
    importData <<  {module: 'material-ui-icons/ScreenLockPortrait', import_modules: 'ScreenLockPortraitIcon' } if settings.has_key?('ScreenLockPortraitIcon')
    importData <<  {module: 'material-ui-icons/ScreenLockRotation', import_modules: 'ScreenLockRotationIcon' } if settings.has_key?('ScreenLockRotationIcon')
    importData <<  {module: 'material-ui-icons/ScreenRotation', import_modules: 'ScreenRotationIcon' } if settings.has_key?('ScreenRotationIcon')
    importData <<  {module: 'material-ui-icons/ScreenShare', import_modules: 'ScreenShareIcon' } if settings.has_key?('ScreenShareIcon')
    importData <<  {module: 'material-ui-icons/SdCard', import_modules: 'SdCardIcon' } if settings.has_key?('SdCardIcon')
    importData <<  {module: 'material-ui-icons/SdStorage', import_modules: 'SdStorageIcon' } if settings.has_key?('SdStorageIcon')
    importData <<  {module: 'material-ui-icons/Search', import_modules: 'SearchIcon' } if settings.has_key?('SearchIcon')
    importData <<  {module: 'material-ui-icons/Security', import_modules: 'SecurityIcon' } if settings.has_key?('SecurityIcon')
    importData <<  {module: 'material-ui-icons/SelectAll', import_modules: 'SelectAllIcon' } if settings.has_key?('SelectAllIcon')
    importData <<  {module: 'material-ui-icons/Send', import_modules: 'SendIcon' } if settings.has_key?('SendIcon')
    importData <<  {module: 'material-ui-icons/SentimentDissatisfied', import_modules: 'SentimentDissatisfiedIcon' } if settings.has_key?('SentimentDissatisfiedIcon')
    importData <<  {module: 'material-ui-icons/SentimentNeutral', import_modules: 'SentimentNeutralIcon' } if settings.has_key?('SentimentNeutralIcon')
    importData <<  {module: 'material-ui-icons/SentimentSatisfied', import_modules: 'SentimentSatisfiedIcon' } if settings.has_key?('SentimentSatisfiedIcon')
    importData <<  {module: 'material-ui-icons/SentimentVeryDissatisfied', import_modules: 'SentimentVeryDissatisfiedIcon' } if settings.has_key?('SentimentVeryDissatisfiedIcon')
    importData <<  {module: 'material-ui-icons/SentimentVerySatisfied', import_modules: 'SentimentVerySatisfiedIcon' } if settings.has_key?('SentimentVerySatisfiedIcon')
    importData <<  {module: 'material-ui-icons/Settings', import_modules: 'SettingsIcon' } if settings.has_key?('SettingsIcon')
    importData <<  {module: 'material-ui-icons/SettingsApplications', import_modules: 'SettingsApplicationsIcon' } if settings.has_key?('SettingsApplicationsIcon')
    importData <<  {module: 'material-ui-icons/SettingsBackupRestore', import_modules: 'SettingsBackupRestoreIcon' } if settings.has_key?('SettingsBackupRestoreIcon')
    importData <<  {module: 'material-ui-icons/SettingsBluetooth', import_modules: 'SettingsBluetoothIcon' } if settings.has_key?('SettingsBluetoothIcon')
    importData <<  {module: 'material-ui-icons/SettingsBrightness', import_modules: 'SettingsBrightnessIcon' } if settings.has_key?('SettingsBrightnessIcon')
    importData <<  {module: 'material-ui-icons/SettingsCell', import_modules: 'SettingsCellIcon' } if settings.has_key?('SettingsCellIcon')
    importData <<  {module: 'material-ui-icons/SettingsEthernet', import_modules: 'SettingsEthernetIcon' } if settings.has_key?('SettingsEthernetIcon')
    importData <<  {module: 'material-ui-icons/SettingsInputAntenna', import_modules: 'SettingsInputAntennaIcon' } if settings.has_key?('SettingsInputAntennaIcon')
    importData <<  {module: 'material-ui-icons/SettingsInputComponent', import_modules: 'SettingsInputComponentIcon' } if settings.has_key?('SettingsInputComponentIcon')
    importData <<  {module: 'material-ui-icons/SettingsInputComposite', import_modules: 'SettingsInputCompositeIcon' } if settings.has_key?('SettingsInputCompositeIcon')
    importData <<  {module: 'material-ui-icons/SettingsInputHdmi', import_modules: 'SettingsInputHdmiIcon' } if settings.has_key?('SettingsInputHdmiIcon')
    importData <<  {module: 'material-ui-icons/SettingsInputSvideo', import_modules: 'SettingsInputSvideoIcon' } if settings.has_key?('SettingsInputSvideoIcon')
    importData <<  {module: 'material-ui-icons/SettingsOverscan', import_modules: 'SettingsOverscanIcon' } if settings.has_key?('SettingsOverscanIcon')
    importData <<  {module: 'material-ui-icons/SettingsPhone', import_modules: 'SettingsPhoneIcon' } if settings.has_key?('SettingsPhoneIcon')
    importData <<  {module: 'material-ui-icons/SettingsPower', import_modules: 'SettingsPowerIcon' } if settings.has_key?('SettingsPowerIcon')
    importData <<  {module: 'material-ui-icons/SettingsRemote', import_modules: 'SettingsRemoteIcon' } if settings.has_key?('SettingsRemoteIcon')
    importData <<  {module: 'material-ui-icons/SettingsSystemDaydream', import_modules: 'SettingsSystemDaydreamIcon' } if settings.has_key?('SettingsSystemDaydreamIcon')
    importData <<  {module: 'material-ui-icons/SettingsVoice', import_modules: 'SettingsVoiceIcon' } if settings.has_key?('SettingsVoiceIcon')
    importData <<  {module: 'material-ui-icons/Share', import_modules: 'ShareIcon' } if settings.has_key?('ShareIcon')
    importData <<  {module: 'material-ui-icons/Shop', import_modules: 'ShopIcon' } if settings.has_key?('ShopIcon')
    importData <<  {module: 'material-ui-icons/ShoppingBasket', import_modules: 'ShoppingBasketIcon' } if settings.has_key?('ShoppingBasketIcon')
    importData <<  {module: 'material-ui-icons/ShoppingCart', import_modules: 'ShoppingCartIcon' } if settings.has_key?('ShoppingCartIcon')
    importData <<  {module: 'material-ui-icons/ShopTwo', import_modules: 'ShopTwoIcon' } if settings.has_key?('ShopTwoIcon')
    importData <<  {module: 'material-ui-icons/ShortText', import_modules: 'ShortTextIcon' } if settings.has_key?('ShortTextIcon')
    importData <<  {module: 'material-ui-icons/ShowChart', import_modules: 'ShowChartIcon' } if settings.has_key?('ShowChartIcon')
    importData <<  {module: 'material-ui-icons/Shuffle', import_modules: 'ShuffleIcon' } if settings.has_key?('ShuffleIcon')
    importData <<  {module: 'material-ui-icons/SignalCellular0Bar', import_modules: 'SignalCellular0BarIcon' } if settings.has_key?('SignalCellular0BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellular1Bar', import_modules: 'SignalCellular1BarIcon' } if settings.has_key?('SignalCellular1BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellular2Bar', import_modules: 'SignalCellular2BarIcon' } if settings.has_key?('SignalCellular2BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellular3Bar', import_modules: 'SignalCellular3BarIcon' } if settings.has_key?('SignalCellular3BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellular4Bar', import_modules: 'SignalCellular4BarIcon' } if settings.has_key?('SignalCellular4BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularConnectedNoInternet0Bar', import_modules: 'SignalCellularConnectedNoInternet0BarIcon' } if settings.has_key?('SignalCellularConnectedNoInternet0BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularConnectedNoInternet1Bar', import_modules: 'SignalCellularConnectedNoInternet1BarIcon' } if settings.has_key?('SignalCellularConnectedNoInternet1BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularConnectedNoInternet2Bar', import_modules: 'SignalCellularConnectedNoInternet2BarIcon' } if settings.has_key?('SignalCellularConnectedNoInternet2BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularConnectedNoInternet3Bar', import_modules: 'SignalCellularConnectedNoInternet3BarIcon' } if settings.has_key?('SignalCellularConnectedNoInternet3BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularConnectedNoInternet4Bar', import_modules: 'SignalCellularConnectedNoInternet4BarIcon' } if settings.has_key?('SignalCellularConnectedNoInternet4BarIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularNoSim', import_modules: 'SignalCellularNoSimIcon' } if settings.has_key?('SignalCellularNoSimIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularNull', import_modules: 'SignalCellularNullIcon' } if settings.has_key?('SignalCellularNullIcon')
    importData <<  {module: 'material-ui-icons/SignalCellularOff', import_modules: 'SignalCellularOffIcon' } if settings.has_key?('SignalCellularOffIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi0Bar', import_modules: 'SignalWifi0BarIcon' } if settings.has_key?('SignalWifi0BarIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi1Bar', import_modules: 'SignalWifi1BarIcon' } if settings.has_key?('SignalWifi1BarIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi1BarLock', import_modules: 'SignalWifi1BarLockIcon' } if settings.has_key?('SignalWifi1BarLockIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi2Bar', import_modules: 'SignalWifi2BarIcon' } if settings.has_key?('SignalWifi2BarIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi2BarLock', import_modules: 'SignalWifi2BarLockIcon' } if settings.has_key?('SignalWifi2BarLockIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi3Bar', import_modules: 'SignalWifi3BarIcon' } if settings.has_key?('SignalWifi3BarIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi3BarLock', import_modules: 'SignalWifi3BarLockIcon' } if settings.has_key?('SignalWifi3BarLockIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi4Bar', import_modules: 'SignalWifi4BarIcon' } if settings.has_key?('SignalWifi4BarIcon')
    importData <<  {module: 'material-ui-icons/SignalWifi4BarLock', import_modules: 'SignalWifi4BarLockIcon' } if settings.has_key?('SignalWifi4BarLockIcon')
    importData <<  {module: 'material-ui-icons/SignalWifiOff', import_modules: 'SignalWifiOffIcon' } if settings.has_key?('SignalWifiOffIcon')
    importData <<  {module: 'material-ui-icons/SimCard', import_modules: 'SimCardIcon' } if settings.has_key?('SimCardIcon')
    importData <<  {module: 'material-ui-icons/SimCardAlert', import_modules: 'SimCardAlertIcon' } if settings.has_key?('SimCardAlertIcon')
    importData <<  {module: 'material-ui-icons/SkipNext', import_modules: 'SkipNextIcon' } if settings.has_key?('SkipNextIcon')
    importData <<  {module: 'material-ui-icons/SkipPrevious', import_modules: 'SkipPreviousIcon' } if settings.has_key?('SkipPreviousIcon')
    importData <<  {module: 'material-ui-icons/Slideshow', import_modules: 'SlideshowIcon' } if settings.has_key?('SlideshowIcon')
    importData <<  {module: 'material-ui-icons/SlowMotionVideo', import_modules: 'SlowMotionVideoIcon' } if settings.has_key?('SlowMotionVideoIcon')
    importData <<  {module: 'material-ui-icons/Smartphone', import_modules: 'SmartphoneIcon' } if settings.has_key?('SmartphoneIcon')
    importData <<  {module: 'material-ui-icons/SmokeFree', import_modules: 'SmokeFreeIcon' } if settings.has_key?('SmokeFreeIcon')
    importData <<  {module: 'material-ui-icons/SmokingRooms', import_modules: 'SmokingRoomsIcon' } if settings.has_key?('SmokingRoomsIcon')
    importData <<  {module: 'material-ui-icons/Sms', import_modules: 'SmsIcon' } if settings.has_key?('SmsIcon')
    importData <<  {module: 'material-ui-icons/SmsFailed', import_modules: 'SmsFailedIcon' } if settings.has_key?('SmsFailedIcon')
    importData <<  {module: 'material-ui-icons/Snooze', import_modules: 'SnoozeIcon' } if settings.has_key?('SnoozeIcon')
    importData <<  {module: 'material-ui-icons/Sort', import_modules: 'SortIcon' } if settings.has_key?('SortIcon')
    importData <<  {module: 'material-ui-icons/SortByAlpha', import_modules: 'SortByAlphaIcon' } if settings.has_key?('SortByAlphaIcon')
    importData <<  {module: 'material-ui-icons/Spa', import_modules: 'SpaIcon' } if settings.has_key?('SpaIcon')
    importData <<  {module: 'material-ui-icons/SpaceBar', import_modules: 'SpaceBarIcon' } if settings.has_key?('SpaceBarIcon')
    importData <<  {module: 'material-ui-icons/Speaker', import_modules: 'SpeakerIcon' } if settings.has_key?('SpeakerIcon')
    importData <<  {module: 'material-ui-icons/SpeakerGroup', import_modules: 'SpeakerGroupIcon' } if settings.has_key?('SpeakerGroupIcon')
    importData <<  {module: 'material-ui-icons/SpeakerNotes', import_modules: 'SpeakerNotesIcon' } if settings.has_key?('SpeakerNotesIcon')
    importData <<  {module: 'material-ui-icons/SpeakerNotesOff', import_modules: 'SpeakerNotesOffIcon' } if settings.has_key?('SpeakerNotesOffIcon')
    importData <<  {module: 'material-ui-icons/SpeakerPhone', import_modules: 'SpeakerPhoneIcon' } if settings.has_key?('SpeakerPhoneIcon')
    importData <<  {module: 'material-ui-icons/Spellcheck', import_modules: 'SpellcheckIcon' } if settings.has_key?('SpellcheckIcon')
    importData <<  {module: 'material-ui-icons/Star', import_modules: 'StarIcon' } if settings.has_key?('StarIcon')
    importData <<  {module: 'material-ui-icons/StarBorder', import_modules: 'StarBorderIcon' } if settings.has_key?('StarBorderIcon')
    importData <<  {module: 'material-ui-icons/StarHalf', import_modules: 'StarHalfIcon' } if settings.has_key?('StarHalfIcon')
    importData <<  {module: 'material-ui-icons/Stars', import_modules: 'StarsIcon' } if settings.has_key?('StarsIcon')
    importData <<  {module: 'material-ui-icons/StayCurrentLandscape', import_modules: 'StayCurrentLandscapeIcon' } if settings.has_key?('StayCurrentLandscapeIcon')
    importData <<  {module: 'material-ui-icons/StayCurrentPortrait', import_modules: 'StayCurrentPortraitIcon' } if settings.has_key?('StayCurrentPortraitIcon')
    importData <<  {module: 'material-ui-icons/StayPrimaryLandscape', import_modules: 'StayPrimaryLandscapeIcon' } if settings.has_key?('StayPrimaryLandscapeIcon')
    importData <<  {module: 'material-ui-icons/StayPrimaryPortrait', import_modules: 'StayPrimaryPortraitIcon' } if settings.has_key?('StayPrimaryPortraitIcon')
    importData <<  {module: 'material-ui-icons/Stop', import_modules: 'StopIcon' } if settings.has_key?('StopIcon')
    importData <<  {module: 'material-ui-icons/StopScreenShare', import_modules: 'StopScreenShareIcon' } if settings.has_key?('StopScreenShareIcon')
    importData <<  {module: 'material-ui-icons/Storage', import_modules: 'StorageIcon' } if settings.has_key?('StorageIcon')
    importData <<  {module: 'material-ui-icons/Store', import_modules: 'StoreIcon' } if settings.has_key?('StoreIcon')
    importData <<  {module: 'material-ui-icons/StoreMallDirectory', import_modules: 'StoreMallDirectoryIcon' } if settings.has_key?('StoreMallDirectoryIcon')
    importData <<  {module: 'material-ui-icons/Straighten', import_modules: 'StraightenIcon' } if settings.has_key?('StraightenIcon')
    importData <<  {module: 'material-ui-icons/Streetview', import_modules: 'StreetviewIcon' } if settings.has_key?('StreetviewIcon')
    importData <<  {module: 'material-ui-icons/StrikethroughS', import_modules: 'StrikethroughSIcon' } if settings.has_key?('StrikethroughSIcon')
    importData <<  {module: 'material-ui-icons/Style', import_modules: 'StyleIcon' } if settings.has_key?('StyleIcon')
    importData <<  {module: 'material-ui-icons/SubdirectoryArrowLeft', import_modules: 'SubdirectoryArrowLeftIcon' } if settings.has_key?('SubdirectoryArrowLeftIcon')
    importData <<  {module: 'material-ui-icons/SubdirectoryArrowRight', import_modules: 'SubdirectoryArrowRightIcon' } if settings.has_key?('SubdirectoryArrowRightIcon')
    importData <<  {module: 'material-ui-icons/Subject', import_modules: 'SubjectIcon' } if settings.has_key?('SubjectIcon')
    importData <<  {module: 'material-ui-icons/Subscriptions', import_modules: 'SubscriptionsIcon' } if settings.has_key?('SubscriptionsIcon')
    importData <<  {module: 'material-ui-icons/Subtitles', import_modules: 'SubtitlesIcon' } if settings.has_key?('SubtitlesIcon')
    importData <<  {module: 'material-ui-icons/Subway', import_modules: 'SubwayIcon' } if settings.has_key?('SubwayIcon')
    importData <<  {module: 'material-ui-icons/SupervisorAccount', import_modules: 'SupervisorAccountIcon' } if settings.has_key?('SupervisorAccountIcon')
    importData <<  {module: 'material-ui-icons/SurroundSound', import_modules: 'SurroundSoundIcon' } if settings.has_key?('SurroundSoundIcon')
    importData <<  {module: 'material-ui-icons/SwapCalls', import_modules: 'SwapCallsIcon' } if settings.has_key?('SwapCallsIcon')
    importData <<  {module: 'material-ui-icons/SwapHoriz', import_modules: 'SwapHorizIcon' } if settings.has_key?('SwapHorizIcon')
    importData <<  {module: 'material-ui-icons/SwapVert', import_modules: 'SwapVertIcon' } if settings.has_key?('SwapVertIcon')
    importData <<  {module: 'material-ui-icons/SwapVerticalCircle', import_modules: 'SwapVerticalCircleIcon' } if settings.has_key?('SwapVerticalCircleIcon')
    importData <<  {module: 'material-ui-icons/SwitchCamera', import_modules: 'SwitchCameraIcon' } if settings.has_key?('SwitchCameraIcon')
    importData <<  {module: 'material-ui-icons/SwitchVideo', import_modules: 'SwitchVideoIcon' } if settings.has_key?('SwitchVideoIcon')
    importData <<  {module: 'material-ui-icons/Sync', import_modules: 'SyncIcon' } if settings.has_key?('SyncIcon')
    importData <<  {module: 'material-ui-icons/SyncDisabled', import_modules: 'SyncDisabledIcon' } if settings.has_key?('SyncDisabledIcon')
    importData <<  {module: 'material-ui-icons/SyncProblem', import_modules: 'SyncProblemIcon' } if settings.has_key?('SyncProblemIcon')
    importData <<  {module: 'material-ui-icons/SystemUpdate', import_modules: 'SystemUpdateIcon' } if settings.has_key?('SystemUpdateIcon')
    importData <<  {module: 'material-ui-icons/SystemUpdateAlt', import_modules: 'SystemUpdateAltIcon' } if settings.has_key?('SystemUpdateAltIcon')
    importData <<  {module: 'material-ui-icons/Tab', import_modules: 'TabIcon' } if settings.has_key?('TabIcon')
    importData <<  {module: 'material-ui-icons/Tablet', import_modules: 'TabletIcon' } if settings.has_key?('TabletIcon')
    importData <<  {module: 'material-ui-icons/TabletAndroid', import_modules: 'TabletAndroidIcon' } if settings.has_key?('TabletAndroidIcon')
    importData <<  {module: 'material-ui-icons/TabletMac', import_modules: 'TabletMacIcon' } if settings.has_key?('TabletMacIcon')
    importData <<  {module: 'material-ui-icons/TabUnselected', import_modules: 'TabUnselectedIcon' } if settings.has_key?('TabUnselectedIcon')
    importData <<  {module: 'material-ui-icons/TagFaces', import_modules: 'TagFacesIcon' } if settings.has_key?('TagFacesIcon')
    importData <<  {module: 'material-ui-icons/TapAndPlay', import_modules: 'TapAndPlayIcon' } if settings.has_key?('TapAndPlayIcon')
    importData <<  {module: 'material-ui-icons/Terrain', import_modules: 'TerrainIcon' } if settings.has_key?('TerrainIcon')
    importData <<  {module: 'material-ui-icons/TextFields', import_modules: 'TextFieldsIcon' } if settings.has_key?('TextFieldsIcon')
    importData <<  {module: 'material-ui-icons/TextFormat', import_modules: 'TextFormatIcon' } if settings.has_key?('TextFormatIcon')
    importData <<  {module: 'material-ui-icons/Textsms', import_modules: 'TextsmsIcon' } if settings.has_key?('TextsmsIcon')
    importData <<  {module: 'material-ui-icons/Texture', import_modules: 'TextureIcon' } if settings.has_key?('TextureIcon')
    importData <<  {module: 'material-ui-icons/Theaters', import_modules: 'TheatersIcon' } if settings.has_key?('TheatersIcon')
    importData <<  {module: 'material-ui-icons/ThreeDRotation', import_modules: 'ThreeDRotationIcon' } if settings.has_key?('ThreeDRotationIcon')
    importData <<  {module: 'material-ui-icons/ThumbDown', import_modules: 'ThumbDownIcon' } if settings.has_key?('ThumbDownIcon')
    importData <<  {module: 'material-ui-icons/ThumbsUpDown', import_modules: 'ThumbsUpDownIcon' } if settings.has_key?('ThumbsUpDownIcon')
    importData <<  {module: 'material-ui-icons/ThumbUp', import_modules: 'ThumbUpIcon' } if settings.has_key?('ThumbUpIcon')
    importData <<  {module: 'material-ui-icons/Timelapse', import_modules: 'TimelapseIcon' } if settings.has_key?('TimelapseIcon')
    importData <<  {module: 'material-ui-icons/Timeline', import_modules: 'TimelineIcon' } if settings.has_key?('TimelineIcon')
    importData <<  {module: 'material-ui-icons/Timer', import_modules: 'TimerIcon' } if settings.has_key?('TimerIcon')
    importData <<  {module: 'material-ui-icons/Timer10', import_modules: 'Timer10Icon' } if settings.has_key?('Timer10Icon')
    importData <<  {module: 'material-ui-icons/Timer3', import_modules: 'Timer3Icon' } if settings.has_key?('Timer3Icon')
    importData <<  {module: 'material-ui-icons/TimerOff', import_modules: 'TimerOffIcon' } if settings.has_key?('TimerOffIcon')
    importData <<  {module: 'material-ui-icons/TimeToLeave', import_modules: 'TimeToLeaveIcon' } if settings.has_key?('TimeToLeaveIcon')
    importData <<  {module: 'material-ui-icons/Title', import_modules: 'TitleIcon' } if settings.has_key?('TitleIcon')
    importData <<  {module: 'material-ui-icons/Toc', import_modules: 'TocIcon' } if settings.has_key?('TocIcon')
    importData <<  {module: 'material-ui-icons/Today', import_modules: 'TodayIcon' } if settings.has_key?('TodayIcon')
    importData <<  {module: 'material-ui-icons/Toll', import_modules: 'TollIcon' } if settings.has_key?('TollIcon')
    importData <<  {module: 'material-ui-icons/Tonality', import_modules: 'TonalityIcon' } if settings.has_key?('TonalityIcon')
    importData <<  {module: 'material-ui-icons/TouchApp', import_modules: 'TouchAppIcon' } if settings.has_key?('TouchAppIcon')
    importData <<  {module: 'material-ui-icons/Toys', import_modules: 'ToysIcon' } if settings.has_key?('ToysIcon')
    importData <<  {module: 'material-ui-icons/TrackChanges', import_modules: 'TrackChangesIcon' } if settings.has_key?('TrackChangesIcon')
    importData <<  {module: 'material-ui-icons/Traffic', import_modules: 'TrafficIcon' } if settings.has_key?('TrafficIcon')
    importData <<  {module: 'material-ui-icons/Train', import_modules: 'TrainIcon' } if settings.has_key?('TrainIcon')
    importData <<  {module: 'material-ui-icons/Tram', import_modules: 'TramIcon' } if settings.has_key?('TramIcon')
    importData <<  {module: 'material-ui-icons/TransferWithinAStation', import_modules: 'TransferWithinAStationIcon' } if settings.has_key?('TransferWithinAStationIcon')
    importData <<  {module: 'material-ui-icons/Transform', import_modules: 'TransformIcon' } if settings.has_key?('TransformIcon')
    importData <<  {module: 'material-ui-icons/Translate', import_modules: 'TranslateIcon' } if settings.has_key?('TranslateIcon')
    importData <<  {module: 'material-ui-icons/TrendingDown', import_modules: 'TrendingDownIcon' } if settings.has_key?('TrendingDownIcon')
    importData <<  {module: 'material-ui-icons/TrendingFlat', import_modules: 'TrendingFlatIcon' } if settings.has_key?('TrendingFlatIcon')
    importData <<  {module: 'material-ui-icons/TrendingUp', import_modules: 'TrendingUpIcon' } if settings.has_key?('TrendingUpIcon')
    importData <<  {module: 'material-ui-icons/Tune', import_modules: 'TuneIcon' } if settings.has_key?('TuneIcon')
    importData <<  {module: 'material-ui-icons/TurnedIn', import_modules: 'TurnedInIcon' } if settings.has_key?('TurnedInIcon')
    importData <<  {module: 'material-ui-icons/TurnedInNot', import_modules: 'TurnedInNotIcon' } if settings.has_key?('TurnedInNotIcon')
    importData <<  {module: 'material-ui-icons/Tv', import_modules: 'TvIcon' } if settings.has_key?('TvIcon')
    importData <<  {module: 'material-ui-icons/Unarchive', import_modules: 'UnarchiveIcon' } if settings.has_key?('UnarchiveIcon')
    importData <<  {module: 'material-ui-icons/Undo', import_modules: 'UndoIcon' } if settings.has_key?('UndoIcon')
    importData <<  {module: 'material-ui-icons/UnfoldLess', import_modules: 'UnfoldLessIcon' } if settings.has_key?('UnfoldLessIcon')
    importData <<  {module: 'material-ui-icons/UnfoldMore', import_modules: 'UnfoldMoreIcon' } if settings.has_key?('UnfoldMoreIcon')
    importData <<  {module: 'material-ui-icons/Update', import_modules: 'UpdateIcon' } if settings.has_key?('UpdateIcon')
    importData <<  {module: 'material-ui-icons/Usb', import_modules: 'UsbIcon' } if settings.has_key?('UsbIcon')
    importData <<  {module: 'material-ui-icons/VerifiedUser', import_modules: 'VerifiedUserIcon' } if settings.has_key?('VerifiedUserIcon')
    importData <<  {module: 'material-ui-icons/VerticalAlignBottom', import_modules: 'VerticalAlignBottomIcon' } if settings.has_key?('VerticalAlignBottomIcon')
    importData <<  {module: 'material-ui-icons/VerticalAlignCenter', import_modules: 'VerticalAlignCenterIcon' } if settings.has_key?('VerticalAlignCenterIcon')
    importData <<  {module: 'material-ui-icons/VerticalAlignTop', import_modules: 'VerticalAlignTopIcon' } if settings.has_key?('VerticalAlignTopIcon')
    importData <<  {module: 'material-ui-icons/Vibration', import_modules: 'VibrationIcon' } if settings.has_key?('VibrationIcon')
    importData <<  {module: 'material-ui-icons/VideoCall', import_modules: 'VideoCallIcon' } if settings.has_key?('VideoCallIcon')
    importData <<  {module: 'material-ui-icons/Videocam', import_modules: 'VideocamIcon' } if settings.has_key?('VideocamIcon')
    importData <<  {module: 'material-ui-icons/VideocamOff', import_modules: 'VideocamOffIcon' } if settings.has_key?('VideocamOffIcon')
    importData <<  {module: 'material-ui-icons/VideogameAsset', import_modules: 'VideogameAssetIcon' } if settings.has_key?('VideogameAssetIcon')
    importData <<  {module: 'material-ui-icons/VideoLabel', import_modules: 'VideoLabelIcon' } if settings.has_key?('VideoLabelIcon')
    importData <<  {module: 'material-ui-icons/VideoLibrary', import_modules: 'VideoLibraryIcon' } if settings.has_key?('VideoLibraryIcon')
    importData <<  {module: 'material-ui-icons/ViewAgenda', import_modules: 'ViewAgendaIcon' } if settings.has_key?('ViewAgendaIcon')
    importData <<  {module: 'material-ui-icons/ViewArray', import_modules: 'ViewArrayIcon' } if settings.has_key?('ViewArrayIcon')
    importData <<  {module: 'material-ui-icons/ViewCarousel', import_modules: 'ViewCarouselIcon' } if settings.has_key?('ViewCarouselIcon')
    importData <<  {module: 'material-ui-icons/ViewColumn', import_modules: 'ViewColumnIcon' } if settings.has_key?('ViewColumnIcon')
    importData <<  {module: 'material-ui-icons/ViewComfy', import_modules: 'ViewComfyIcon' } if settings.has_key?('ViewComfyIcon')
    importData <<  {module: 'material-ui-icons/ViewCompact', import_modules: 'ViewCompactIcon' } if settings.has_key?('ViewCompactIcon')
    importData <<  {module: 'material-ui-icons/ViewDay', import_modules: 'ViewDayIcon' } if settings.has_key?('ViewDayIcon')
    importData <<  {module: 'material-ui-icons/ViewHeadline', import_modules: 'ViewHeadlineIcon' } if settings.has_key?('ViewHeadlineIcon')
    importData <<  {module: 'material-ui-icons/ViewList', import_modules: 'ViewListIcon' } if settings.has_key?('ViewListIcon')
    importData <<  {module: 'material-ui-icons/ViewModule', import_modules: 'ViewModuleIcon' } if settings.has_key?('ViewModuleIcon')
    importData <<  {module: 'material-ui-icons/ViewQuilt', import_modules: 'ViewQuiltIcon' } if settings.has_key?('ViewQuiltIcon')
    importData <<  {module: 'material-ui-icons/ViewStream', import_modules: 'ViewStreamIcon' } if settings.has_key?('ViewStreamIcon')
    importData <<  {module: 'material-ui-icons/ViewWeek', import_modules: 'ViewWeekIcon' } if settings.has_key?('ViewWeekIcon')
    importData <<  {module: 'material-ui-icons/Vignette', import_modules: 'VignetteIcon' } if settings.has_key?('VignetteIcon')
    importData <<  {module: 'material-ui-icons/Visibility', import_modules: 'VisibilityIcon' } if settings.has_key?('VisibilityIcon')
    importData <<  {module: 'material-ui-icons/VisibilityOff', import_modules: 'VisibilityOffIcon' } if settings.has_key?('VisibilityOffIcon')
    importData <<  {module: 'material-ui-icons/VoiceChat', import_modules: 'VoiceChatIcon' } if settings.has_key?('VoiceChatIcon')
    importData <<  {module: 'material-ui-icons/Voicemail', import_modules: 'VoicemailIcon' } if settings.has_key?('VoicemailIcon')
    importData <<  {module: 'material-ui-icons/VolumeDown', import_modules: 'VolumeDownIcon' } if settings.has_key?('VolumeDownIcon')
    importData <<  {module: 'material-ui-icons/VolumeMute', import_modules: 'VolumeMuteIcon' } if settings.has_key?('VolumeMuteIcon')
    importData <<  {module: 'material-ui-icons/VolumeOff', import_modules: 'VolumeOffIcon' } if settings.has_key?('VolumeOffIcon')
    importData <<  {module: 'material-ui-icons/VolumeUp', import_modules: 'VolumeUpIcon' } if settings.has_key?('VolumeUpIcon')
    importData <<  {module: 'material-ui-icons/VpnKey', import_modules: 'VpnKeyIcon' } if settings.has_key?('VpnKeyIcon')
    importData <<  {module: 'material-ui-icons/VpnLock', import_modules: 'VpnLockIcon' } if settings.has_key?('VpnLockIcon')
    importData <<  {module: 'material-ui-icons/Wallpaper', import_modules: 'WallpaperIcon' } if settings.has_key?('WallpaperIcon')
    importData <<  {module: 'material-ui-icons/Warning', import_modules: 'WarningIcon' } if settings.has_key?('WarningIcon')
    importData <<  {module: 'material-ui-icons/Watch', import_modules: 'WatchIcon' } if settings.has_key?('WatchIcon')
    importData <<  {module: 'material-ui-icons/WatchLater', import_modules: 'WatchLaterIcon' } if settings.has_key?('WatchLaterIcon')
    importData <<  {module: 'material-ui-icons/WbAuto', import_modules: 'WbAutoIcon' } if settings.has_key?('WbAutoIcon')
    importData <<  {module: 'material-ui-icons/WbCloudy', import_modules: 'WbCloudyIcon' } if settings.has_key?('WbCloudyIcon')
    importData <<  {module: 'material-ui-icons/WbIncandescent', import_modules: 'WbIncandescentIcon' } if settings.has_key?('WbIncandescentIcon')
    importData <<  {module: 'material-ui-icons/WbIridescent', import_modules: 'WbIridescentIcon' } if settings.has_key?('WbIridescentIcon')
    importData <<  {module: 'material-ui-icons/WbSunny', import_modules: 'WbSunnyIcon' } if settings.has_key?('WbSunnyIcon')
    importData <<  {module: 'material-ui-icons/Wc', import_modules: 'WcIcon' } if settings.has_key?('WcIcon')
    importData <<  {module: 'material-ui-icons/Web', import_modules: 'WebIcon' } if settings.has_key?('WebIcon')
    importData <<  {module: 'material-ui-icons/WebAsset', import_modules: 'WebAssetIcon' } if settings.has_key?('WebAssetIcon')
    importData <<  {module: 'material-ui-icons/Weekend', import_modules: 'WeekendIcon' } if settings.has_key?('WeekendIcon')
    importData <<  {module: 'material-ui-icons/Whatshot', import_modules: 'WhatshotIcon' } if settings.has_key?('WhatshotIcon')
    importData <<  {module: 'material-ui-icons/Widgets', import_modules: 'WidgetsIcon' } if settings.has_key?('WidgetsIcon')
    importData <<  {module: 'material-ui-icons/Wifi', import_modules: 'WifiIcon' } if settings.has_key?('WifiIcon')
    importData <<  {module: 'material-ui-icons/WifiLock', import_modules: 'WifiLockIcon' } if settings.has_key?('WifiLockIcon')
    importData <<  {module: 'material-ui-icons/WifiTethering', import_modules: 'WifiTetheringIcon' } if settings.has_key?('WifiTetheringIcon')
    importData <<  {module: 'material-ui-icons/Work', import_modules: 'WorkIcon' } if settings.has_key?('WorkIcon')
    importData <<  {module: 'material-ui-icons/WrapText', import_modules: 'WrapTextIcon' } if settings.has_key?('WrapTextIcon')
    importData <<  {module: 'material-ui-icons/YoutubeSearchedFor', import_modules: 'YoutubeSearchedForIcon' } if settings.has_key?('YoutubeSearchedForIcon')
    importData <<  {module: 'material-ui-icons/ZoomIn', import_modules: 'ZoomInIcon' } if settings.has_key?('ZoomInIcon')
    importData <<  {module: 'material-ui-icons/ZoomOut', import_modules: 'ZoomOutIcon' } if settings.has_key?('ZoomOutIcon')
    importData <<  {module: 'material-ui-icons/ZoomOutMap', import_modules: 'ZoomOutMapIcon' } if settings.has_key?('ZoomOutMapIcon')




    import_modules_function(importData).join("\n")
  end

  # Not added do_process_column logic to server mutations..
  def do_process_column(yamlSection, c)
    return false if yamlSection.key?('columns_only_process_everywhere') && !yamlSection['columns_only_process_everywhere'].include?(c)
    return false if yamlSection.key?('columns_hide_everywhere') && yamlSection['columns_hide_everywhere'].include?(c)
    true
  end

  def pretty_json(object, indent='  ')
    # indent = '      '
    # JSON.pretty_generate((object), {indent: indent})
    JSON.neat_generate(object, wrap: 40, aligned: true, around_colon: 1, indent: indent, indent_last: true)
  end

end