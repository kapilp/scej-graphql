
def field_count

end

def field_all

end

def field_one_preview

end

def all_queries_generate(tables_array=loop_classes(true))
  # Loop Through All Models and Crate Three Queres:
  # 1.
  tables_array.map { |table_data|
  # return r unless object.is_a?(Hash) && object.key?(table_key) # If table key not exist we not process it.
    n = table_data[:table]
    @config = table_data[:config] ? table_data[:config] : table_data[:table]
    object = @yamlConfig[@config]
    @general_disable_query_generate = object.dig 'general', 'disable_query_generate'
    next if ignore_join_tables(n)
    next if ignore_view_tables(n)
    next if @general_disable_query_generate

    table_key='table_view'
    table = object.is_a?(Hash) && object.key?(table_key) ? object[table_key] : n

    # Add Agruments...
    save_variabls(@config)
    arg_hash = query_arguments_server_strings
    @v2arg1 = "    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}\n"
    @v2arg1 += "#{arg_hash["arg1"]}\n" unless  arg_hash["arg1"].empty?

    @v2arg2 = "      # #{@cP} = #{table}.limit(args[:limit].to_i)\n"
    @v2arg2 += "      #{@cP} = #{table}.all\n"
    @v2arg2 += "#{arg_hash["arg2"]}\n" unless arg_hash["arg2"].empty?
    @v2arg2 += "      #{@cP}"

    # If custom logic provided...
    @v2arg2 = custom_query_logic(n) if custom_query_logic(n)

    # Generate String=============================
    # Simple Connection:
    # field :all#{@f + @CP}, !types[Types::#{@f + @CS}Type] do
    o = """
  connection :all#{@f + @CP}, Types::#{@f + @CS}Type.connection_type do
#{@v2arg1}
    resolve ->(_obj, args, ctx) {
#{@v2arg2}
    }
  end
  field :#{@cP}Count do
    type types.Int
    description 'Number of #{@CP}'
    resolve ->(_object, args, ctx) {
      #{table}.count
    }
  end
  field :#{@cS}, Types::#{@f + @CS}Type do
    argument :id, types.ID
    description 'fetch a #{@CS} by id'
    resolve ->(object, args, ctx) {
      #{table}.find(args[:id])
    }
  end"""

  }.join("\n")
end

def all_fields_generate(tables_array=loop_classes(true))
  # Loop Through All Models and Crate Three Queres:
  # 1.
  tables_array.each { |table_data|
  # return r unless object.is_a?(Hash) && object.key?(table_key) # If table key not exist we not process it.
    n = table_data[:table]
    @config = table_data[:config] ? table_data[:config] : table_data[:table]
    object = @yamlConfig[@config]
    @general_disable_query_generate = object.dig 'general', 'disable_query_generate'
    next if ignore_join_tables(n)
    next if ignore_view_tables(n)
    next if @general_disable_query_generate

    table_key='table_view'
    table = object.is_a?(Hash) && object.key?(table_key) ? object[table_key] : n

    # Add Agruments...
    save_variabls(@config)
    arg_hash = query_arguments_server_strings
    @v2arg1 = "  # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}\n"
    @v2arg1 += "#{arg_hash["arg1"]}\n" unless  arg_hash["arg1"].empty?

    @v2arg2 = "     # #{@cP} = #{table}.limit(args[:limit].to_i)\n"
    @v2arg2 += "    #{@cP} = #{table}.all\n"
    @v2arg2 += "#{arg_hash["arg2"]}\n" unless arg_hash["arg2"].empty?
    @v2arg2 += "    #{@cP}"

    # If custom logic provided...
    @v2arg2 = custom_query_logic(n) if custom_query_logic(n)

    # Generate String=============================
    # Simple Connection:
    # field :all#{@f + @CP}, !types[Types::#{@f + @CS}Type] do
    field_all = """#{@f + @CP}Field = GraphQL::Field.define do
  name('#{@fD + @CP}')
  type(Types::#{@f + @CS}Type.connection_type)
  description '#{@CS} connection to fetch paginated #{@CP} collection.'
#{@v2arg1}
  resolve ->(_obj, args, ctx) {
#{@v2arg2}
  }
end"""
    field_count = """#{@f + @CP}CountField = GraphQL::Field.define do
  name('#{@fD + @CP}Count')
  type types.Int
  description 'Number of #{@CP}'
  resolve ->(_object, args, ctx) {
    #{table}.count
  }
end"""
    field_one_preview = """#{@f + @CS}Field = GraphQL::Field.define do
  name('#{@fD + @CS}')
  type(Types::#{@f + @CS}Type)
  argument :id, types.ID
  description 'fetch a #{@CS} by id'
  resolve ->(object, args, ctx) {
    #{table}.find(args[:id])
  }
end"""

  # append_to_file dest_file, embed_template("client/container/routes01.tsx.erb")
  create_file "app/graphql/fields/#{@f.underscore}_#{@CP.underscore}_field.rb", field_all
  create_file "app/graphql/fields/#{@f.underscore}_#{@cP.underscore}_count_field.rb", field_count
  create_file "app/graphql/fields/#{@f.underscore}_#{@cS.underscore}_field.rb", field_one_preview
  }
end

# It may be wrong way of making query but lets do it..
def all_state_machines_queries_generate(tables_array_hash=all_state_machines)
  # Loop Through All Models and Crate Three Queres:
  tables_array_hash.map { |n|
    model = n[:model]
    n = n[:machine]
    next if ignore_join_tables(n)

    # Add Agruments...
    save_variabls(@config)
    arg_hash = query_arguments_server_strings
    @v2arg1 = "    \n"
    @v2arg1 += "#{arg_hash["arg1"]}\n" unless  arg_hash["arg1"].empty?

    @v2arg2 = "      #{@cP} = #{n}.states\n"
    @v2arg2 += "#{arg_hash["arg2"]}\n" unless arg_hash["arg2"].empty?
    @v2arg2 += "      #{@cP}"

    # If custom logic provided...
    @v2arg2 = custom_query_logic(n) if custom_query_logic(n)

    # Generate String=============================
    o = """
  field :all#{@f + @CP} do
    type types[types.String]
#{@v2arg1}
    resolve ->(_obj, args, ctx) {
#{@v2arg2}
    }
  end
  field :#{@cS}, types.String do
    argument :id, types.ID
    description 'fetch status by id'
    resolve ->(object, args, ctx) {
      #{model}.find(args[:id]).state_machine.current_state
    }
  end"""

  }.join("\n")
end
# It may be wrong way of making query but lets do it..
def all_state_machines_fields_generate(tables_array_hash=all_state_machines)
  # Loop Through All Models and Crate Three Queres:
  tables_array_hash.map { |n|
    model = n[:model]
    n = n[:machine]
    next if ignore_join_tables(n)

    # Add Agruments...
    save_variabls(model)
    arg_hash = query_arguments_server_strings
    @v2arg1 = "    \n"
    @v2arg1 += "#{arg_hash["arg1"]}\n" unless  arg_hash["arg1"].empty?

    @v2arg2 = "      #{@cP} = #{n}.states\n"
    @v2arg2 += "#{arg_hash["arg2"]}\n" unless arg_hash["arg2"].empty?
    @v2arg2 += "      #{@cP}"

    # If custom logic provided...
    @v2arg2 = custom_query_logic(n) if custom_query_logic(n)

    # Generate String=============================
    field_all = """#{@f + @CP}StatesField = GraphQL::Field.define do
  name('#{@fD + @CP}States')
  type types[types.String]
  description 'All #{@CP} States.'
#{@v2arg1}
    resolve ->(_obj, args, ctx) {
#{@v2arg2}
  }
end"""
    field_one_preview = """#{@f + @CS}StateField = GraphQL::Field.define do
  name('#{@fD + @CP}State')
  type types[types.String]
  argument :id, types.ID
  description 'fetch status by id'
  resolve ->(object, args, ctx) {
    #{model}.find(args[:id]).state_machine.current_state
  }
end"""
puts n
puts @f.underscore
puts @CP.underscore
  # append_to_file dest_file, embed_template("client/container/routes01.tsx.erb")
  create_file "app/graphql/fields/#{@f.underscore}_#{@CP.underscore}_states_field.rb", field_all
  # create_file "app/graphql/fields/#{@f.underscore}_#{@cP.underscore}_count_field.rb", field_count
  create_file "app/graphql/fields/#{@f.underscore}_#{@cS.underscore}_state_field.rb", field_one_preview

  }
end

def custom_query_logic(n)
  # if( n == "Design::Collection" )
  #     """      unless args[:all].blank?
  #         Design::Collection.all
  #       else
  #         Design::Collection.roots
  #       end"""
  # else
  #   false
  # end
  false
end

def all_viewer_fields_generate(tables_array=loop_classes(true), suffix='')
  # Loop Through All Models and Crate Three Queres:
  # 1.
  tables_array.map { |table_data|
  # return r unless object.is_a?(Hash) && object.key?(table_key) # If table key not exist we not process it.
    n = table_data[:table]
    @config = table_data[:config] ? table_data[:config] : table_data[:table]
    object = @yamlConfig[@config]
    @general_disable_query_generate = object.dig 'general', 'disable_query_generate'
    next if ignore_join_tables(n)
    next if ignore_view_tables(n)
    next if @general_disable_query_generate

    table_key='table_view'
    table = object.is_a?(Hash) && object.key?(table_key) ? object[table_key] : n

    # Add Agruments...
    save_variabls(@config)
    arg_hash = query_arguments_server_strings
    @v2arg1 = "    # argument :limit, types.Int, default_value: 50, prepare: ->(limit, ctx) {[limit, 100].min}\n"
    @v2arg1 += "#{arg_hash["arg1"]}\n" unless  arg_hash["arg1"].empty?

    @v2arg2 = "      # #{@cP} = #{table}.limit(args[:limit].to_i)\n"
    @v2arg2 += "      #{@cP} = #{table}.all\n"
    @v2arg2 += "#{arg_hash["arg2"]}\n" unless arg_hash["arg2"].empty?
    @v2arg2 += "      #{@cP}"

    # If custom logic provided...
    @v2arg2 = custom_query_logic(n) if custom_query_logic(n)

    # Generate String=============================
    # Simple Connection:
    # field :all#{@f + @CP}, !types[Types::#{@f + @CS}Type] do
    o = []
  o << "  field :#{@fD + @CP}#{suffix.pluralize}Count, #{@f + @CP}#{suffix.pluralize}CountField" if suffix.blank?
  o << "  #{suffix.blank? ? 'connection' : 'field'} :#{@fD + @CP}#{suffix.pluralize}, #{@f + @CP}#{suffix.pluralize}Field"
  o << "  field :#{@fD + @CS}#{suffix.singularize}, #{@f + @CS}#{suffix.singularize}Field"

  }.join("\n")
end

# ============================Routes.tsx file genenrator helper:===========================
  def routes_import(tables_array=loop_classes, settings={imports: true} )

    result = []
    tables_array.each { |table_data|
      n = table_data[:table]
      @config = table_data[:config] ? table_data[:config] : table_data[:table]
      object = @yamlConfig[@config]
      next if ignore_route_tables(n)
      next if ignore_join_tables(n)
      next if ignore_view_tables(n)
      next if n.last(10) == 'Transition'

      save_variabls(@config)
      pages_variables(object)
      # o = "import {  " + ",  + ", Edit" + @CS + " }" + "from 'containers/" + @fD + "/" + @folder_path + "';"
      # o = ""
      page_settings = object.is_a?(Hash) ? object.dig('client', 'pages', 'page_settings') : true
      # pages_variables(object) # Must Pass a Hash
      importData = []
      if settings && settings.has_key?(:imports)
        # importData <<  {module: "containers/#{ @fD}/#{@folder_path}/view", import_modules: ["All#{@CP}", "Search#{@CP}", "#{@CS}", "New#{@CS}", "Edit#{@CS}"]}
        if !@general_client_page_no_main_generate
          importData <<  {module: "containers/generated/#{ @fD}/#{@folder_path}/All/Search#{@CP}", import_modules: "Search#{@CP}"} if trueOrKey(page_settings, 'search')
        end
        if !@general_client_page_no_create_generate
          importData <<  {module: "containers/generated/#{ @fD}/#{@folder_path}/NewEdit/New#{@CS}", import_modules: "New#{@CS}"} if trueOrKey(page_settings, 'create')
        end
        if !@general_client_page_no_update_generate
          importData <<  {module: "containers/generated/#{ @fD}/#{@folder_path}/NewEdit/Edit#{@CS}", import_modules: "Edit#{@CS}"} if trueOrKey(page_settings, 'update')
        end
        if !@general_client_page_no_preview_generate
          importData <<  {module: "containers/generated/#{ @fD}/#{@folder_path}/Preview/#{@CS}", import_modules: "#{@CS}"}  if trueOrKey(page_settings, 'preview')
        end
        if !@general_client_page_no_main_generate
          importData <<  {module: "containers/generated/#{ @fD}/#{@folder_path}/All/All#{@CP}", import_modules: "All#{@CP}"} if trueOrKey(page_settings, 'read')
        end
        # importData <<  {module: "queries/#{@fD}/#{@folder_path}/#{@cP}Query", import_modules: "{ All#{@l.camelize.pluralize}Query }"}
        # importData <<  {module: "containers/#{@fUD}/#{@folder_path}/logic/_List#{@CP}", import_modules: "List#{@CP}"}
        # importData <<  {module: "containers/#{@fUD}/#{@folder_path}/logic/_HeadList#{@CP}", import_modules: "HeadList#{@CP}"}
        result.push(import_modules_function(importData).join("\n"))
        result.push("\n")
      elsif settings && settings.has_key?(:links)
        result.push("const #{@dP} = new PageLink('#{@dP}');")
      end
    }

    result.join("\n")
  end

  def routes_fetch_data_functions(tables_array=loop_classes)

    tables_array.map { |table_data|
      n = table_data[:table]
      @config = table_data[:config] ? table_data[:config] : table_data[:table]
      object = @yamlConfig[@config]
      next if ignore_route_tables(n)
      next if ignore_join_tables(n)
      next if ignore_view_tables(n)
      next if n.last(10) == 'Transition'

      save_variabls(@config)
      # we can write hash but how I write componet without bracket....
      o = []

      # if key exist
      lCp = @l.camelize.pluralize
      o << "export async function fetch#{@CP}(apolloClient ) {
  try {
    const #{@cS} = await apolloClient.query({
      query: All#{lCp}Query,
      variables: {},
    });
    const data = await  #{@cS}.data.all#{@f}#{lCp}
    return data
  }
  catch(e) {
    console.log('there was an error on :fetch#{@CP}');
    console.log(e);
  }
}"
      # o << "      {show: false, icon: 'ade.jpg', path: '/#{@dP}/search/:keywords', display_name: 'Search #{@CP}', component: Search#{@CP},}, {show: false, icon: 'ade.jpg', path: '/#{@dP}/new', display_name: 'New #{@CS}', component: New#{@CS},}, {show: false, icon: 'ade.jpg', path: '/#{@dP}/:id/edit', display_name: 'Edit #{@CS}', component: Edit#{@CS},}, {show: false, icon: 'ade.jpg', path: '/#{@dP}/:id', display_name: '#{@CS} ID', component: #{@CS},}, {show: true, icon: 'ade.jpg', path: '/#{@dP}', display_name: 'All #{@CP}', component: All#{@CP},},"
      o.join("\n")
    }.join("\n")
  end

  def trueOrKey(page_settings, key)
    page_settings = page_settings.is_a?(Hash) ? page_settings.dig(key) : true
  end

  def include_has_value(hash)
    # str[1...-1]
    hash.delete 'label' if hash.is_a?(Hash)
    hash.is_a?(Hash) ? pretty_json(hash).gsub(/\"/,"'").gsub(/\{/,"").gsub(/\}/,"") : ''
  end

  def hashValueLabel(hash)
    hash.is_a?(Hash) && hash.key?('label') ? "'#{hash['label']}'" : false
  end

  def routes_hash(tables_array=loop_classes)
    h = {}

    result = []
    tables_array.map { |table_data|
      n = table_data[:table]
      @config = table_data[:config] ? table_data[:config] : table_data[:table]
      object = @yamlConfig[@config]
      next if ignore_route_tables(n)
      next if ignore_join_tables(n)
      next if ignore_view_tables(n)
      next if n.last(10) == 'Transition'

      save_variabls(@config)
      pages_variables(object)
      # we can write hash but how I write componet without bracket....
      o = []

      key = @f
      if h.key?(key)
        # nothing
      else
        h[key] = ''
        o << "]}," if h.keys.size != 1
        o << "
  #{key}: {
    show: true,
    icon: 'abe.jpg',
    color: 'green',
    array: ["
      end

      # if key exist
      # show = false,
      # icon = 'abe.jpg'
      # path = "#{@dp}"
      # display_name= ""
      # component= ""
      # canActivate: ""
      # canActivate: (route, navigation) => { const { GridLayoutStore } = route.context.stores; const params = route.params; GridLayoutStore.onAddItem({component: NewState, params}); return false }
      hide_from_navigation = object.is_a?(Hash) && object.key?('hide_from_navigation') ? object['hide_from_navigation'] : 'true'
      table_page = options.one ? "''" : "#{@dP}.mainLink()"
      table_page = "#{@dP}.mainLink()"
      page_settings = object.is_a?(Hash) ? object.dig('client', 'pages', 'page_settings') : true
      userIsAuthenticated = object.is_a?(Hash) ? object.dig('client', 'pages', 'no_redirect_if_user_is_not_authenticated') : {}
      canActivate="canActivate: (route, navigation) => { const { GridLayoutStore } = route.context.stores; const params = route.params; GridLayoutStore.onAddItem({component: UserIsAuthenticated(Search#{@CP}), params}); return false }"

      if !@general_client_page_no_main_generate
        search_settings = trueOrKey(page_settings, 'search')
        search_auth = userIsAuthenticated ? !userIsAuthenticated.include?('search') : true
        o << "      {show: false, path: #{@dP}.searchLink(), display_name: #{hashValueLabel(search_settings) ? hashValueLabel(search_settings) : "'Search #{@CP}'"}, component: #{search_auth ? 'UserIsAuthenticated(' : ''}Search#{@CP}#{search_auth ? ')' : ''}, }," if search_settings
      end
      if !@general_client_page_no_create_generate
        create_settings = trueOrKey(page_settings, 'create')
        create_auth = userIsAuthenticated ? !userIsAuthenticated.include?('create') : true
        o << "      {show: false, path: #{@dP}.newLink(), display_name: #{hashValueLabel(create_settings) ? hashValueLabel(create_settings) : "'New #{@CS}'"}, component: #{create_auth ? 'UserIsAuthenticated(' : ''}New#{@CS}#{create_auth ? ')' : ''}, #{ include_has_value(create_settings) }}," if create_settings
      end
      if !@general_client_page_no_update_generate
        update_settings = trueOrKey(page_settings, 'update')
        update_auth = userIsAuthenticated ? !userIsAuthenticated.include?('update') : true
        o << "      {show: false, path: #{@dP}.editLink(), display_name: #{hashValueLabel(update_settings) ? hashValueLabel(update_settings) : "'Edit #{@CS}'"}, component: #{update_auth ? 'UserIsAuthenticated(' : ''}Edit#{@CS}#{update_auth ? ')' : ''},}," if trueOrKey(page_settings, 'update') if update_settings
      end
      if !@general_client_page_no_preview_generate
        preview_settings = trueOrKey(page_settings, 'preview')
        preview_auth = userIsAuthenticated ? !userIsAuthenticated.include?('preview') : true
        o << "      {show: false, path: #{@dP}.previewLink(), display_name: #{hashValueLabel(preview_settings) ? hashValueLabel(preview_settings) : "'#{@CS} ID'"}, component: #{preview_auth ? 'UserIsAuthenticated(' : ''}#{@CS}#{preview_auth ? ')' : ''},}," if trueOrKey(page_settings, 'preview') if preview_settings
      end
      if !@general_client_page_no_main_generate
        read_settings = trueOrKey(page_settings, 'read')
        read_auth = userIsAuthenticated ? !userIsAuthenticated.include?('read') : true
        o << "      {show: #{hide_from_navigation}, icon: 'ade.jpg', path: #{table_page}, display_name: #{hashValueLabel(read_settings) ? hashValueLabel(read_settings) : "'#{@CP}'"}, component: #{read_auth ? 'UserIsAuthenticated(' : ''}All#{@CP}#{read_auth ? ')' : ''}, info: pageInfo['#{@cS}'] }," if trueOrKey(page_settings, 'read') if read_settings
      end
      #   willResolve: async (route) => {
      #     const { SessionStore } = route.context.stores
      #     const { client } = SessionStore
      #     SessionStore.#{@cS} = {}
      #     SessionStore.loading = true
      #     SessionStore.#{@cS}.data = await fetch#{@CP}(client)
      #     SessionStore.loading = false
      #     SessionStore.#{@cS}.table = List#{@CP}
      #     SessionStore.#{@cS}.headList = HeadList#{@CP}
      #     SessionStore.#{@cS}.headerName = '#{@CS.humanize}'
      #     SessionStore.currentPage = '#{@cS}'
      #   }
      # },"
      # o << "      {show: false, icon: 'ade.jpg', path: '/#{@dP}/search/:keywords', display_name: 'Search #{@CP}', component: Search#{@CP},}, {show: false, icon: 'ade.jpg', path: '/#{@dP}/new', display_name: 'New #{@CS}', component: New#{@CS},}, {show: false, icon: 'ade.jpg', path: '/#{@dP}/:id/edit', display_name: 'Edit #{@CS}', component: Edit#{@CS},}, {show: false, icon: 'ade.jpg', path: '/#{@dP}/:id', display_name: '#{@CS} ID', component: #{@CS},}, {show: true, icon: 'ade.jpg', path: '/#{@dP}', display_name: 'All #{@CP}', component: All#{@CP},},"
      result.push(o.join("\n"))
    }


    result.join("\n\n")
  end