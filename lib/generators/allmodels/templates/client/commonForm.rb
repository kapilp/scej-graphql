# You can't access local variables defined in the included file. You can use instant vars:
# https://stackoverflow.com/questions/2699324/how-can-i-access-a-variable-defined-in-a-ruby-file-i-required-in-irb
# https://stackoverflow.com/questions/8334684/how-to-share-variables-across-my-rb-files
# https://stackoverflow.com/questions/4048093/ruby-how-to-load-rb-file-in-the-local-context
# Table accessor :  accessor: '#{c}',

=begin
TODO
form generate table and main form input should be same..

=end
require 'neatjson'
@i=0
@string=0
@text=0
@decimal=0
@date=0
@datetime=0
@inet=0
@bool=0
@select=0
@radio=0
@file=0
@functionComment = ''

def add_form_extra_props(columnsArray, k)
      # Add Column Properties.
      # Column properties..........................................
  if k
    columnsArray.last["width"] = k["width"]
    columnsArray.last["isNextRow"] = k["isNextRow"]
    columnsArray.last["function"] = k["function"]
    columnsArray.last["name"] = k["name"]
    columnsArray.last["functionType"] = k["functionType"]
    columnsArray.last["isYield"] = k["isYield"]
    columnsArray.last["footerType"] = k["footerType"]
    columnsArray.last["footer"] = k["footer"]
    columnsArray.last["labelKey"] = k["labelKey"]
    columnsArray.last["infotip"] = k["infotip"]
  end
end

def add_extra_fields_form(function2, columnsArray, yamlSection, c)
  function2.call(yamlSection, c) do |k|
    next if yamlSection.key?('columns_hide_view_only') && yamlSection['columns_hide_view_only'].include?(k["name"])
    add_extra_fields_form(method(:add_extra_fields_before), columnsArray, yamlSection, k["name"])
    columnsArray << {name: k["name"], type: k["type"]}
    if yamlSection.key?('observer') && yamlSection['observer'].key?(k["name"])
      columnsArray.last["observer"] = true
      columnsArray.last["observerVisibleFunctions"] = yamlSection['observer'][k["name"]]['visibleFunctions']
    end
    columnsArray.last["label"] = yamlSection['change_labels'][k["name"]] if yamlSection.key?('change_labels') && yamlSection['change_labels'].key?(k["name"])
    add_form_extra_props(columnsArray, k )
    add_extra_fields_form(method(:add_extra_fields), columnsArray, yamlSection, k["name"])
  end
end

# =============================Presentational Form:==========================
def add_form_field(table, prefix, yamlSection, indent="", isTable=false, array_field = "form", settings='')
  # defaultWidth = 140
  # totalRow = false
  # totalState="form"

  # 1. Hide Columns.
  # 2. Add Column Properties.
  # 3. Add Calculated Columns...
  columnsArray=Array.new(0);
  # table_UdP = table.name.demodulize.underscore.downcase.pluralize
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    # 1. Hide Fields
    next unless do_process_column(yamlSection, c.name)
    add_extra_fields_form(method(:add_extra_fields_before), columnsArray, yamlSection, c.name)

  # binding.pry
    columnsArray << {name: c.name.to_s, type: c.type.to_s}
    if yamlSection.key?('observer') && yamlSection['observer'].key?(c.name)
      columnsArray.last["observer"] = true
      columnsArray.last["observerVisibleFunctions"] = yamlSection['observer'][c.name]['visibleFunctions']
    end
    columnsArray.last["label"] = yamlSection['change_labels'][c.name] if yamlSection.key?('change_labels') && yamlSection['change_labels'].key?(c.name)

    column_properties = yamlSection['column_properties']
    k = false
    k = column_properties[c.name] if column_properties
    add_form_extra_props(columnsArray, k )

    # 2. Add Fields(Columns)
    # Go in calculated_fields_after hash and add columns.
    add_extra_fields_form(method(:add_extra_fields), columnsArray, yamlSection, c.name)

  end


  finalResultsArray=[]
  fieldsArray = []
  key=1
  fieldsArray << {}
  dataIndex = 1
  @maximumColumns1 = 1
  @maximumColumns2 = 1
  @table_added = false
  @yield_done = false
  myarray = columnsArray.map do |a|

    # name and type are symbols other keys are strings..
    #  cell = a[1]
    #  a = a[0]
    s = ''
    # width = a["width"] ? a["width"] : defaultWidth
    isNextRow = a["isNextRow"] ? a["isNextRow"] : false
    array_field = 'form' unless array_field

    c = column_change(@l, name_change(a[:name]))
    next "'#{c}': 100" if settings == 'widthRow'
    #  grid = "<Grid item xs={" + cell.to_s + "}>"
    #  grid_end = '</Grid>'
    # grid = "<div style={{width: #{width}, border : 'solid 1px', background: 'skyblue', marginLeft : '-1px'}}>"
    array_field = 'form' unless array_field  #  I don't know why array_field not default set.
    fieldSelect = "this.#{array_field}.$('#{c}')"

    grid = ""
    # grid = "<Div width='#{width} #{headerStyle}'>" if totalRow
    # grid_end = "</div>"

    grid_end = ""
    # totalVariable = "{#{totalState}.total#{c.camelize}}"
    wrapperStart = "<td>"
    wrapperEnd = "</td>"
    observerStart = ''
    observerEnd = ''
    visible = ''

    if a["observer"]
      observerStart = "<Observer>{() => "
      observerEnd = "}</Observer>"
      visible1 = a['observerVisibleFunctions'].map{|i| "form.#{i}(#{array_field}.$(props.original)).get()"}.join( " && ")
      visible = "visible={#{visible1}}"
    end

    infotip = a["infotip"] ? "<InfoTip>#{a["infotip"]}</InfoTip>" : ""
    label = a["label"] ? "<label>{#{a["label"]}}#{infotip}</label>" : "<label>{#{fieldSelect}.label}#{infotip}</label>"
    header = a["label"] ? "Header: '#{a["label"]}', " : "Header: '#{c.humanize}', "
    tableWidth = "width: formCoumnWidths['#{c}'], "
    # headerStyle = "headerStyle: {background: 'aquamarine', fontSize: 'large'}, "
    headerStyle=''
    tableId = "id: '#{c}', "
    tableIdHeaderWidth = "#{indent}{ #{tableId}#{tableWidth}#{header}#{headerStyle}"
    # next if yamlSection.key?('columns_hide_everywhere') && yamlSection['columns_hide_everywhere'].include?(c.name)
    a[:type] = 'string' if c == 'position'
    # tableIdHeaderWidth = "#{indent}{ id: '#{c}', width: formCoumnWidths['#{c}'], Header: '#{c.humanize}', width: #{width}#{headerStyle}, "
    # icon = "<div class='input-group'><span class='input-group-addon'><i class=''></i>{#{fieldSelect}.label}</span></div>"

    footer = ''
    if a["footerType"]
      case a["footerType"]
      when 'jsx'
        puts "1, 2, or 3"
      when 'string'
        footer = " Footer: '#{a['footer']}', "
      when 'computed'
        footer = ", Footer: form.#{a['footer']} "
      when 'computedFunction'
        footer = ", Footer: (props) => #{a['footer']}#{isTable ? '(props.original)' : '()'} "
      else
        # footer = " Footer: '#{array_field}.#{a['footer']}, "
      end
    end
    functionCode = ''
    functionLabel= ''
    if a["functionType"]
      functionLabel= a['name']

      case a["functionType"]
      when 'jsx'
        puts "1, 2, or 3"
      when 'string'
        functionCode = " Cell: '#{a['function']}', "
      when 'computed'
        functionCode = isTable ? ", Cell: (props) => form.#{a['function']} " : "form.#{a['function']}"
      when 'computedFunction'
        if isTable
          functionCode = "Cell: (props) => #{a["function"]}#{isTable ? '(props.original)' : '()'}"
        else
          functionCode = "this.#{a["function"]}()"
        end
      else
        # footer = " Footer: '#{array_field}.#{a['footer']}, "
      end

    end

    # Real Section =================================================================

    mobxField = isTable ? "#{array_field}.$(props.original).$('#{c}')" : fieldSelect
    tableCell = ''
    inputField = ''

    # Common Inputs=====================
    if a[:type] == 'integer'
      if c.last(3) == '_id' || c.last(5) == 'ids[]' || c.last(3) == 'ids'
        @select = 1
        labelKey = a["labelKey"] ? "labelKey='#{a['labelKey']}'" : ''
        inputField  = "<Select size='large' field={#{mobxField}} isLabelShow={false} #{labelKey} #{visible}#{'multi' if c.last(3) == 'ids'}/>"
      else
        @i = 1
        inputField = "<NumberPicker field={#{mobxField}} min={0} max={1000000} precision={0} #{visible}/>"
      end
    end

    if a[:type] == 'decimal'
      @decimal = 1
      inputField = "<NumberPicker field={#{mobxField}} min={0} max={1000000} precision={3} step={0.001} format='#.000' #{visible}/>"
    end
    # puts a[:type]
    if a[:type] == 'date' || a[:type] == 'datetime'
      @date = 1
      inputField = "<DateTimePicker field={#{mobxField}} #{visible}/>"
    end

    if a[:type] == 'boolean'
      @bool = 1
      inputField = "<Checkbox field={#{mobxField}} #{visible}/>"
    end

    if a[:type] == 'string'
      if c == 'status_id'
        @select = 1
        labelKey = a["labelKey"] ? "labelKey='#{a['labelKey']}'" : ''
        inputField  = "<Select size='large' field={#{mobxField}} isLabelShow={false} #{labelKey} #{visible}#{'multi' if c.last(3) == 'ids'}/>"
      else
        @string = 1
        type = c == 'position' ? "type='number'" : ''
        inputField = "<Input field={#{mobxField}} #{type} #{visible}/>"
      end
    end

    if a[:type] == 'file'
      @file = 1
      inputField = "<DropZone field={#{mobxField}} #{visible}/>"
    end

    if a[:type] == 'text'
      @text = 1
      inputField = "<div className='ui form'><TextArea rows={1} field={#{mobxField}} #{visible}/></div>"
    end

    if a[:name] == 'image_data'
      @file = 1
      inputField = "<DropZone field={#{mobxField}} #{visible}/>"
    end

    if a[:type] == 'json'
      @text = 1
      inputField = "<div className='ui form'><TextArea rows={1} field={#{mobxField}} #{visible}/></div>"
    end

    tableCell = "Cell: (props) => (#{observerStart}#{inputField}#{observerEnd})"

    # Common Inputs End=====================

    # OverWrite Some Variables....
    if a[:type] == 'calc'
      inputField = functionCode
      tableCell = functionCode
      label = "'#{functionLabel}'"
    end



    s = "#{indent}#{grid}#{label}#{wrapperStart}#{inputField}#{wrapperEnd}#{grid_end}"
    s = "#{tableIdHeaderWidth}#{tableCell} #{footer} }," if isTable

    # ------------------
    fieldsArray.last["label#{key}"] = label
    fieldsArray.last["value#{key}"] = inputField
    @maximumColumns1 = key if @maximumColumns1 < key && dataIndex == 1
    @maximumColumns2 = key if @maximumColumns2 < key && dataIndex == 2
    key = key + 1
    key = 1 if isNextRow
    fieldsArray << {} if isNextRow

    if a["isYield"] && yield
      finalResultsArray << "@observable data#{dataIndex} = " + pretty_json(fieldsArray).gsub(/\"/,"")
      # finalResultsArray << tableeeee

      s << yield.join("\n")  # this yield is required in Mfg::MfgTxn
      # finalResultsArray << yield  # yield block is not used here.
      # @table_added = true
      @yield_done = true
      fieldsArray = []
      key = 1
      fieldsArray << {}
      dataIndex +=1

    end
    # ----------------------

    # insertFlexRow =  """
    #     </tr>
    #     <tr>"""
    # s << insertFlexRow if isNextRow

    @functionComment << "// const #{a["function"]} = () => { return(<span>___</span>) }\n"  if a["function"]

    # if a["isYield"] && yield
    #   s << """
    #       </tr>
    #     </tbody>
    #   </table>
    #   </fieldset>"""
    # s << yield
    # s << """
    # <fieldset>
    #     <table className='main-form-table'>
    #       <colgroup><col/><col/><col/><col/><col/><col/><col/><col/></colgroup>
    #       <tbody>
    #       <tr>"""
    # @table_added = true
    # end
     s
  end
  finalResultsArray << "@observable data#{dataIndex} = " + pretty_json(fieldsArray).gsub(/\"/,"")
  # finalResultsArray << "table"
  isTable ? myarray : finalResultsArray
end

# You can also use this syntex to pass a block
# def b(&block)
#   a(&block)
# end
# or
# def b
#   a(&Proc.new)
# end
def do_loop_all_keys_return_forms(object, indent="                ", addMain=true, r=[], isTable=false, array_field='form', settings='', &block)
  return r unless object;
  return r unless object.kind_of?(Hash)
  # Add fields of itself
  return r  unless object.is_a?(Hash) && object.key?('table') && !object['noGenerateTable']
  r.concat(add_form_field(object['table'].constantize, "", object, indent, isTable, array_field, settings, &block)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    # table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    # relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    # prefix2 = "#{prefix}#{relationship_name}[]."
#+++ r.concat(add_form_field(object[k]['table'].constantize, "", object[k]))
#+++ do_loop_all_keys_return_fields(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end

# ===========================================================
def import_list_table(object, importDataArray, table_type='taco_table')
  @i=0
  @string=0
  @text=0
  @decimal=0
  @date=0
  @datetime=0
  @inet=0
  @bool=0
  @select=0
  @radio=0
  @file=0

  @includeMoment = 0
  # Set Some Variables.
  main_view_list_generate(object, settings='',settings2='target')
  main_view_list_generate(object, settings='',settings2='source')

  if table_type == 'react_table'
    importDataArray <<  {module: 'components/views/table/ReactTable/ReactTable', import_modules: 'Table'}
  elsif table_type == 'taco_table'
    importDataArray <<  {module: 'components/views/table/TacoTable/TacoTable', import_modules: 'Table'}
    importDataArray <<  {module: 'react-taco-table', import_modules: ['DataType', 'Formatters']}
  end
  importDataArray <<  {module: 'moment', import_modules: 'moment', defaultImp: true} if @includeMoment == 1
end

# table_type = 'react_table' 'taco_table' 'fixed_data_table_2' 'react_pivotized'
def add_list_line_generate(object, column_name, column_type, settings='',settings2='target', table_type='taco_table')
  n = column_name
    return '' if object.key?('view_columns_hide') && object['view_columns_hide'].include?(n)
    return '' if object.key?('viewAvailableColumns') && object['viewAvailableColumns'].include?(n) && settings2 == 'target'
    return '' if object.key?('viewAvailableColumns') && !object['viewAvailableColumns'].include?(n) && settings2 == 'source'
    return '' if !object.key?('viewAvailableColumns') && settings2 == 'source'
    return "'#{column_name}': 100" if settings == 'widthRow'
    # width = columnWidthsObjects.key?(a.name) ? ", width: #{columnWidthsObjects[a.name]}" : '' if columnWidthsObjects

    n = name_change(n)
    n = column_change(@l, n)
    h = n.humanize
    h = 'Code' if h == 'Slug'
    h = 'SN#' if h == 'Position'
    h = 'Batch Name' if h == 'Code' && @l == 'Casting'
    cell = ''
    style=''
    cellStyle = "tdStyle: tacoTdStyle,"
    datatype = ''
    if n == 'status_id'
      cell = ''
    elsif n.last(3) == '_id'
      column_properties = object['view_column_properties']
      t =  column_properties ? column_properties[column_name] : false
      if t && t.key?('labelKey')
        value = t['labelKey'] if t['labelKey']
      else
        value = 'slug'
      end
      if table_type == 'react_table'
        cell += "Cell: (props) => (props.original.#{n} ? props.original.#{n}.#{value} : undefined),"
      elsif table_type == 'taco_table'
        datatype = 'type: DataType.String,'
        cell += "renderer: tacoIdRenderer('#{n}'),"
      end
    elsif column_type == 'integer'
      if table_type == 'react_table'
        cell += "Cell: ({ value }) => Numeral(value).format('0,0'),"
        style = "style: { textAlign: 'right' },"
      elsif table_type == 'taco_table'
        datatype = 'type: DataType.Number,'
        cell += "renderer: tacoIntegerRenderer,"
      end
    elsif column_type == 'decimal'
      if table_type == 'react_table'
        cell += "Cell: ({ value }) => Numeral(value).format('0.000'),"
        style = "style: { textAlign: 'right' },"  if table_type == 'react_table'
      elsif table_type == 'taco_table'
        datatype = 'type: DataType.Number,'
        cell += "renderer: tacoDecimalRenderer,"
      end
    elsif column_type == 'boolean'
      if table_type == 'taco_table'
        cell += "renderer: tacoBooleanRenderer,"
      end
    elsif column_type == 'datetime'
      @includeMoment = 1
      if table_type == 'taco_table'
        cell += "renderer: tacoDateRenderer,"
      end
    end
    h = object['change_labels_view'][column_name] if object.key?('change_labels_view') && object['change_labels_view'].key?(column_name)
    # "    { accessor: '#{n}', width: viewCoumnWidths['#{n}'], Header: '#{h}', #{cell} #{style} },"
    "    { id: '#{n}', #{datatype} header: '#{h}', #{cell} #{cellStyle} },"
end

def react_table_cell_render

end

def taco_table_cell_render(settings)
  id = "id = #{settings[:id]}, " if settings.has_key?(:id)
  bottomDataRender = "bottomDataRender = #{settings[:bottomDataRender]}, " if settings.has_key?(:bottomDataRender)
  className = "className = #{settings[:className]}, " if settings.has_key?(:className)
  firstSortDirection = "firstSortDirection = #{settings[:firstSortDirection]}, " if settings.has_key?(:firstSortDirection)
  header = "header = #{settings[:header]}, " if settings.has_key?(:header)
  renderer = "renderer = #{settings[:renderer]}, " if settings.has_key?(:renderer)
  rendererOptions = "rendererOptions = #{settings[:rendererOptions]}, " if settings.has_key?(:rendererOptions)
  renderOnNull = "renderOnNull = #{settings[:renderOnNull]}, " if settings.has_key?(:renderOnNull)
  simpleRenderer = "simpleRenderer = #{settings[:simpleRenderer]}, " if settings.has_key?(:simpleRenderer)
  sortType = "sortType = #{settings[:sortType]}, " if settings.has_key?(:sortType)
  sortValue = "sortValue = #{settings[:sortValue]}, " if settings.has_key?(:sortValue)
  summarize = "summarize = #{settings[:summarize]}, " if settings.has_key?(:summarize)
  tdClassName = "tdClassName = #{settings[:tdClassName]}, " if settings.has_key?(:tdClassName)
  tdStyle = "tdStyle = #{settings[:tdStyle]}, " if settings.has_key?(:tdStyle)
  thClassName = "thClassName = #{settings[:thClassName]}, " if settings.has_key?(:thClassName)
  type = "type = #{settings[:type]}, " if settings.has_key?(:type)
  value = "value = #{settings[:value]}, " if settings.has_key?(:value)
  width = "width = #{settings[:width]}, " if settings.has_key?(:width)
  "#{id} #{bottomDataRender} #{className} #{firstSortDirection} #{header} #{renderer} #{rendererOptions} #{renderOnNull} #{simpleRenderer} #{sortType} #{sortValue} #{summarize} #{tdClassName} #{tdStyle} #{thClassName} #{type} #{value} #{width}"
end

def main_view_list_generate(object, settings='',settings2='target', table_type='taco_table')
  columnWidthsObjects = object['column_widths']

  table = false
  table = object.is_a?(Hash) && object.key?('table_view') ? object['table_view'].constantize : object['table'].constantize

  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end

  result = []
  # edit_button = "<Link to={`/#{@dP}/${#{@cS}Row.id}/edit`} key='edit'><IconButton className={classes.badge} title='Edit'><EditIcon /></IconButton></Link>"
  # delete_button = "<div><IconButton  color='secondary' className={classes.badge} title='Delete' onClick={this.destroy} key='delete'><DeleteIcon /></IconButton>"

  if settings2 != 'source'
    if table_type == 'react_table'
      result << """    { id: '#{@CS}Preview', Header: '-', width: 130, Cell: (props) => <#{@CS}Preview #{@cS}Row={props.original} currentUser={this.props.currentUser} destroyRow={this.props.destroyRow} variables />,
        style: { backgroundColor: '#DDD', textAlign: 'right', fontWeight: 'bold' }, },"""
    elsif table_type == 'taco_table'
      # result << taco_table_cell_render({id: 'id', renderer: "(cellData, { column, rowData }) => #{edit_button}"})
      result << """    { id: 'id', header: '-', renderer: tacoActionsRenderer,
        tdStyle: cellData => { return { backgroundColor: '#DDD', textAlign: 'right', fontWeight: 'bold' } }, },"""
      result << """    { id: 'id2', header: '-', renderer: tacoActionsRenderer2, renderOnNull: true,
        tdStyle: cellData => { return { backgroundColor: '#DDD', textAlign: 'right', fontWeight: 'bold' } }, },"""
    end
  end
  table_columns.each do |c|
    line = add_list_line_generate(object, c.name, c.type.to_s, settings, settings2, table_type)
    result << line if line != ''

    # 2. Add Fields if k['addGraphFields']
    add_extra_fields(object, c.name) do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields'] # Comment If required.
      line = add_list_line_generate(object, k["name"], k["type"], settings, settings2, table_type)
      result << line if line != '' if k['addGraphFields']
    end

  end
    result
end

# ==================================Presentational Form end==================

def get_import_component
  importData = []
  if INPUT_NUMBER_LIBRARY == 'material-ui'
    dest_folder = 'components/structures/inputs'
    importData << "import SimpleInput from '#{dest_folder}/SimpleInput';" if @i == 1 || @string == 1 || @text == 1
    importData << "import Checkbox from '#{dest_folder}/SimpleCheckbox';" if @bool == 1
    importData << "import NumberInput from '#{dest_folder}/NumberInput';" if @decimal == 1
    importData << "import SimpleSelectMaterial from '#{dest_folder}/SimpleSelectMaterial';" if @select == 1
  else
    if INPUT_NUMBER_LIBRARY == 'antd'
      componentFolder = 'components/structures/inputs'
      importData << {module: "#{componentFolder}/DropZone/DropZone", import_modules: 'DropZone'} if @file == 1
      importData << {module: "#{componentFolder}/ReactWidgets/NumberPicker", import_modules: 'NumberPicker'} if @i == 1 || @decimal == 1
      importData << {module: "#{componentFolder}/SemanticUI/Input", import_modules: 'Input'} if @string == 1
      importData << {module: "#{componentFolder}/SemanticUI/TextArea", import_modules: 'TextArea'} if @text == 1
      importData << {module: "#{componentFolder}/ReactWidgets/WidgetDatePicker", import_modules: 'DateTimePicker'} if @date == 1 || @datetime == 1
      importData << {module: "#{componentFolder}/MaterialUI/MaterialSwitch", import_modules: 'Checkbox'} if @bool == 1  # SemanticUI/Checkbox
      importData << {module: "#{componentFolder}/ReactSelect/ReactMultiSelect", import_modules: 'Select'} if @select == 1
    end
  end
  importData
end

def get_import_queries_form_fetch_functions(table)
  checkIncluded = []
  importData = []
  return importData unless @yamlConfig[table].key?('query_variables')
  @yamlConfig[table]['query_variables'].each{|m|
    if m['table'].last(12) == 'StateMachine'
      importData <<  {module: 'react-apollo', import_modules: ['graphql']}
      importData <<  {module: 'graphql-tag', import_modules: 'gql', defaultImp: true}
    else
      f1 = m['table'].deconstantize
      l1 = m['table'].demodulize
      importCheck = "import { #{f1.camelize + l1.camelize.pluralize}Query } from 'queries/#{f1.downcase}/#{l1.underscore.downcase.pluralize}/#{l1.camelize(:lower).pluralize}Query';"
      importDataIn = {module: "queries/#{f1.downcase}/#{l1.underscore.downcase.pluralize}/#{l1.camelize(:lower).pluralize}Query", import_modules: ["#{f1.camelize + l1.camelize.pluralize}Query"]}
      importData << importDataIn unless checkIncluded.index(importCheck)
    end
  }
  importData
end

# can accept nested tables too.
# NOTE: this Fetch Helper Function Taking arguments from Main Table section. So it create extra functions even if you hidden columns.
def fetch_functions(table, prefix="", isImport=false)
  # @fetch = table_variables[table.demodulize].map{|m|
  return [] unless @yamlConfig[table].key?('query_variables')
  @fetch = @yamlConfig[table]['query_variables'].map{|m|

    f1 = m['table'].deconstantize
    l1 = m['table'].demodulize
    states = false
    if l1.last(12) == 'StateMachine'
      states = true
      # l1.gsub!('StateMachine', '')
    end
    lCp = l1.camelize.pluralize
    lcp = l1.camelize(:lower).pluralize
    arguments = m['function']['variables'].map{|v| v['name']}.join(", ")
    arguments.prepend(', ') if(!arguments.empty?)
    # variables = m['function']['variables'].map{|v| "#{v['name']}: parseInt(#{v['name']})"}.join(", ")
    variables = m['function']['variables'].map{|v| "#{v['name']}: #{v['name']}"}.join(", ")
    unless isImport
      s = []
      state_field_name = "#{f1.downcase + lCp.gsub('StateMachines', '').pluralize}States"
      if states
        s << "const #{f1.camelize + l1.camelize.pluralize}Query = gql`"
        s << "  query #{f1.camelize + l1.camelize.pluralize}#{states ? 'States' : ''}Query {"
        s << "    root {"
        s << "      #{state_field_name}"
        s << "    }"
        s << "  }"
        s << "`;"
      end
      s << ""
      s << "export async function fetch#{prefix}#{lCp}(field #{arguments}) {"
      s << "  try {"
      s << "    const #{lcp} = await this.apolloClient.query({"
      s << "      query: #{f1.camelize + l1.camelize.pluralize}Query,"
      s << "      variables: {#{variables}},"
      s << "      fetchPolicy: 'network-only',"
      s << "      errorPolicy: 'all'"
      s << "    });"
      # s << "  field.set('extra',  _.mapKeys(#{lcp}.data.all#{f1}#{lCp}, (value) => value.id))"
      # s << "  const data = #{lcp}.data.all#{f1}#{lCp}.map(x => {let copy = Object.assign({}, x); copy.label= x.slug; copy.value= x.id; return copy} )"
      if states
        s << "    const data = #{lcp}.data.root.#{state_field_name}"
      else
        s << "    const data = #{lcp}.data.root.#{f1.downcase}#{lCp} ? #{lcp}.data.root.#{f1.downcase}#{lCp}.edges.map(nodeObjs => nodeObjs.node) : this.props.data"
      end
      s << "    field.set('extra', data);"
      s << "    return data;"
      s << "  }"
      s << "  catch(e) {"
      s << "    console.log('there was an error on :fetch#{prefix}#{lCp}#{states ? 'States' : ''}');"
      s << "    console.log(e); "
      s << "  }"
      s << "}"
      s.join("\n")
    else
      "fetch#{prefix}#{lCp}"
    end
  }
end

def import_all_fetch_functions(object, prefix = '', addMain=true, write = true, r=[], isImport=false)
  r << fetch_functions(@name, @l, isImport) if addMain
   object.keys.each do |k| # Loop Through all keys
      next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
      # table_CP = object[k]['table'].demodulize.camelize.pluralize
      # prefix2 = "#{prefix}#{table_CP}"

      if write
        table_CP = object[k]['table'].demodulize.camelize.pluralize
        relationship_name_CP =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'].camelize.pluralize : "#{table_CP}"
        prefix2 = "#{prefix}#{relationship_name_CP}"
        r << fetch_functions(object[k]['table'], prefix2, isImport)
        # @nestedTablesComponent << "FormTable#{prefix2}"
        # @r << "import FormTable#{prefix2} from './_FormTable#{prefix2}';"

        if is_nested_form.name == object[k]['table']
          import_all_fetch_functions(object[k]['nested'], prefix2, false, true, r, isImport) if object[k].key?('nested')
        end

      end
    end
    r.flatten
end

def fetch_calls(table, form="form")
  # @fetchcalls = table_variables[table].map{|m|
  return [] unless @yamlConfig[table].key?('query_variables')
  @fetchcalls = @yamlConfig[table]['query_variables'].map{|m|
    # f1 = m['table'].deconstantize
    l1 = m['table'].demodulize
    lCp = l1.camelize.pluralize
    # lcp = l1.camelize(:lower).pluralize
    call = m['function']['fields'].map{|m|
      args_string = m['args']
    args_string = m['args'].to_s.prepend(", ").concat("") if m['args'].kind_of?(Integer)
    args_string = m['args'].prepend(", ").concat("") if m['args'].kind_of?(String) && !m['args'].empty?
    args_string = m['args'].map{|a| "#{a}"}.join(", ").prepend(", ") if m['args'].kind_of?(Array)
    "      fetch#{table.demodulize}#{lCp}.call(this,#{form}.$('#{m['name']}')#{args_string}).then(d=>filterIDSet(#{form}.$('#{m['name']}'), d, #{form}.$('#{m['name']}').get('initial')));"
    }
    call
  }
end

# Nested.tsx.erb
def nested_observe(columnsArray)
  columnsArray.map{|a|
    c = column_change(@l, name_change(a['name']))
    s = []
    s << ""
    s << "    #{@table_last_name3}.$('#{c}').observe({"
    s << "      key: 'value', // can be any field property"
    s << "      call: ({ form, field, change }) =>{"
    s << "        #{c.camelize(:lower)}Change(form, field, change)"
    s << "      }, // set new value here"
    s << "    });"
    s.join("\n")
  }
end

def nested_observe_comment(columnsArray)
  columnsArray.map{|a|
    c = column_change(@l, name_change(a['name']))
    s = []
    s << "// const #{c.camelize(:lower)}Change = (form, field, change) => {}"
    s.join("\n")
  }
end

def nested_total_comment(columnsArray)
  columnsArray.map{|a|
    c = column_change(@l, name_change(a['name']))
    totalVariable = "total#{c.camelize}"
    s = []
    s << "//  @computed get #{totalVariable}() {return;}"
    s.join("\n")
  }
end

def rails_types_to_mobx_types(railsType)
  value = case railsType
    when 'integer' then
      'number'
    when 'decimal' then
      'float'
    when 'string' then
      'text'
    when 'text' then
      'text'
    when 'date' then
      'date'
    when 'datetime' then
      'datetime-local'
    when 'inet' then
      'text'
    when 'boolean' then
      'checkbox'
  end
    value = 'email' if railsType == 'email'
    value
end

# module Form
#   @test_var="Hello, World"

#   def self.test_var
#     return @test_var
#   end

#   def self.test_var=(val)
#     @test_val=val;
#   end

# end




# =============================form fields==========================
def add_field(table, prefix, yamlSection)
  fieldsArray2=Array.new(0);
  # table_UdP = table.name.demodulize.underscore.downcase.pluralize
  table_columns = table.column_names.reject do |column|
    column =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      fieldsArray2 << prefix + column_change(@l, name_change(k["name"]) )
    end
    # 1. Hide Fields
    add_extra_fields_before(yamlSection, c) { |k| new_field_proc.call(k) }
    next unless do_process_column(yamlSection, c)
    fieldsArray2 << prefix + column_change(@l, name_change(c) )
    # 2. Add Fields
    add_extra_fields(yamlSection, c) { |k| new_field_proc.call(k) }

  end
  fieldsArray2
end

def do_loop_all_keys_return_fields(object, prefix="", addMain=true, r=[])
  return r unless object;
  return r unless object.kind_of?(Hash)
  # Add fields of itself
  r.concat(add_field(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    r << "#{prefix}#{relationship_name}"
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.concat(add_field(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_fields(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end

# ==================================form fields end==================
# =============================form types==========================
def add_type(table, prefix, yamlSection)
  typesObject = Hash.new(0)
  # table_UdP = table.name.demodulize.underscore.downcase.pluralize
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    next unless do_process_column(yamlSection, c.name)
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      value = rails_types_to_mobx_types(k["type"].to_s)
      value = 'text' if k["name"] == 'position'
      value = 'password' if k["name"] == 'password' || k["name"] == 'password_confirmation'
      next if k["name"].last(3) == '_id' # its number or text?
      key = prefix + column_change(@l, name_change(k["name"]))
      typesObject[key] = value
    end
    add_extra_fields_before(yamlSection, c.name) { |k| new_field_proc.call(k) }
    value = rails_types_to_mobx_types(c.type.to_s)
    value = 'text' if c.name == 'position'
    value = 'password' if c.name == 'password' || c.name == 'password_confirmation'
    next if c.name.last(3) == '_id' # its number or text?
    key = prefix + column_change(@l, name_change(c.name))
    typesObject[key] = value

    add_extra_fields(yamlSection, c.name) { |k| new_field_proc.call(k) }
  end
  typesObject
end

def do_loop_all_keys_return_types(object, prefix="", addMain=true, r={})
  # Add fields of itself
  r.merge!(add_type(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.merge!(add_type(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_types(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end
# ==================================form types end==================

# =============================form disabled==========================
def add_disabled(table, prefix, yamlSection)
  typesObject = Hash.new(0)
  # table_UdP = table.name.demodulize.underscore.downcase.pluralize
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    next unless do_process_column(yamlSection, c.name)
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      key = prefix + column_change(@l, name_change(k["name"]))
      value=false
      column_properties = yamlSection['column_properties']
      t =  column_properties ? column_properties[k["name"]] : false
      if t && t.key?('disabled')
        value = t['disabled'] if t['disabled']
      end

      typesObject[key] = value if value
    end
    add_extra_fields_before(yamlSection, c.name)  { |k| new_field_proc.call(k) }
    key = prefix + column_change(@l, name_change(c.name))
    value=false
    column_properties = yamlSection['column_properties']
    t =  column_properties ? column_properties[c.name] : false
    if t && t.key?('disabled')
      # binding.pry if c.name == 'isissue'
      value = t['disabled'] if t['disabled']
    end

    typesObject[key] = value if value

    add_extra_fields(yamlSection, c.name)  { |k| new_field_proc.call(k) }
  end
  typesObject
end

def do_loop_all_keys_return_disabled(object, prefix="", addMain=true, r={})
  # Add fields of itself
  r.merge!(add_disabled(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.merge!(add_disabled(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_disabled(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end
# ==================================form disabled end==================

# =============================form initial values==========================
def add_initial(table, prefix, yamlSection)
  initialsObject = Hash.new(0)
  # table_UdP = table.name.demodulize.underscore.downcase.pluralize
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    next unless do_process_column(yamlSection, c.name)
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      key = prefix + column_change(@l, name_change(k["name"]))
      value = false
      @momentimport = 1 if k["type"].to_s == 'date' || k["type"].to_s == 'datetime'
      value = "moment().format('YYYY-MM-DD')" if k["type"].to_s == 'date'
      value = "moment().format('YYYY-MM-DDTHH:mm')" if k["type"].to_s == 'datetime'
      column_properties = yamlSection['column_properties']
      t =  column_properties ? column_properties[k["name"]] : false
      if t && t.key?('initial')
        value = t['initial'] if t['initial']
      end

      initialsObject[key] = value if value
    end

    add_extra_fields_before(yamlSection, c.name) { |k| new_field_proc.call(k) }
    key = prefix + column_change(@l, name_change(c.name))
    value = false
    @momentimport = 1 if c.type.to_s == 'date' || c.type.to_s == 'datetime'
    value = "moment().format('YYYY-MM-DD')" if c.type.to_s == 'date'
    value = "moment().format('YYYY-MM-DDTHH:mm')" if c.type.to_s == 'datetime'
    column_properties = yamlSection['column_properties']
    t =  column_properties ? column_properties[c.name] : false
    if t && t.key?('initial')
      value = t['initial'] if t['initial']
    end

    initialsObject[key] = value if value

    add_extra_fields(yamlSection, c.name) { |k| new_field_proc.call(k) }

  end
  initialsObject
end

def do_loop_all_keys_return_initials(object, prefix="", addMain=true, r={})
  # Add fields of itself
  r.merge!(add_initial(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.merge!(add_initial(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_initials(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end

# ==================================form initial values end==================
# =============================form labels==========================
def add_label(table, prefix, yamlSection, emptyLable= true)
  labelsObject = Hash.new(0)
  # table_UdP = table.name.demodulize.underscore.downcase.pluralize
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    next unless do_process_column(yamlSection, c.name)
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      key = prefix + column_change(@l, name_change(k["name"]))
      l = column_change(@l, name_change(k["name"])).humanize
      l = 'Code' if l == 'Slug'
      l = 'SN#' if l == 'Position'
      l = '' if  emptyLable.blank?
      labelsObject[key] = l
    end
    add_extra_fields_before(yamlSection, c.name) { |k| new_field_proc.call(k) }

    key = prefix + column_change(@l, name_change(c.name))
    l = column_change(@l, name_change(c.name)).humanize
    l = 'Code' if l == 'Slug'
    l = 'SN#' if l == 'Position'
    l = '' if  emptyLable.blank?
    labelsObject[key] = l

    add_extra_fields(yamlSection, c.name) { |k| new_field_proc.call(k) }
  end
  labelsObject
end

def do_loop_all_keys_return_labels(object, prefix="", addMain=true, r={}, labelNested='')
  # Add fields of itself
  r.merge!(add_label(@table.constantize, "", object)) if addMain
  if labelNested
    object.keys.each do |k| # Loop Through all keys
      next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
      table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
      relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
      r.merge!({"#{prefix}#{relationship_name}": "#{object[k]['label']}"})
      prefix2 = "#{prefix}#{relationship_name}[]."
      r.merge!(add_label(object[k]['table'].constantize, prefix2, object[k], labelNested))
      do_loop_all_keys_return_labels(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
    end
  end
  r
end

# ==================================form labels end==================
# =============================form Rules==========================
def add_rule(table, prefix, yamlSection)
  rulesObject = Hash.new(0)
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    next unless do_process_column(yamlSection, c.name)
    next if c.name == 'position'
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      # next unless k['optionalField']
      next if c.name == 'position'
      key = prefix + column_change(@l, name_change(k["name"]))
      if k && k.key?('optionalField')
        next if k['optionalField']
      elsif k && k.key?('dvrRules')
        rulesObject[key] = k['dvrRules'] if k['dvrRules']
      else
        rulesObject[key] = 'required'
      end
    end

    add_extra_fields_before(yamlSection, c.name) { |k| new_field_proc.call(k) }

    key = prefix + column_change(@l, name_change(c.name))
    column_properties = yamlSection['column_properties']
    t =  column_properties ? column_properties[c.name] : false
    if t && t.key?('optionalField')
      next if t['optionalField']
    elsif t && t.key?('dvrRules')
      rulesObject[key] = t['dvrRules'] if t['dvrRules']
    else
      rulesObject[key] = 'required'
    end

    add_extra_fields(yamlSection, c.name) { |k| new_field_proc.call(k) }
  end
  rulesObject
end

def do_loop_all_keys_return_rules(object, prefix="", addMain=true, r={})
  # Add fields of itself
  r.merge!(add_rule(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.merge!(add_rule(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_rules(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end

# ==================================form Rules end==================
# =============================form Input==========================
def add_input(table, prefix, yamlSection)
  inputObject = Hash.new([])
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    inputFunction = ''
    inputFunction = '' unless do_process_column(yamlSection, c.name)

    # if we not set this to '_id', the value will be 0 instead of null.
    inputFunction = "value => value == '' ? new Date() : new Date(value)," if c.type.to_s == 'datetime' # (from input to store)
    inputFunction = "value => Number(value)," if (c.name.to_s != 'position' && c.name.to_s.last(3) != '_id') && (c.type.to_s == 'integer' || c.type.to_s == 'decimal') # (from input to store)
    # inputFunction = 'value => value.toString(),' if c.name.last(3) == '_id' # (from input to store)
    next if inputFunction == ''
    next unless do_process_column(yamlSection, c.name)
    key = prefix + column_change(@l, name_change(c.name))
    inputObject[key] = "#{inputFunction}" # (from input to store)

    # TODO: (Not time) : add extra column yield function...
  end
  inputObject
end

def do_loop_all_keys_return_inputs(object, prefix="", addMain=true, r={})
  # Add fields of itself
  r.merge!(add_input(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.merge!(add_input(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_inputs(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end

# ==================================form Input end==================

# If we use 'next' it blocks below add extra keys section:
# =============================form Output==========================
def add_output(table, prefix, yamlSection)
  outputObject = Hash.new([])
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    outputFunction = ''
    outputFunction = '' unless do_process_column(yamlSection, c.name)

    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      outputFunction = ''
      outputFunction = '' unless do_process_column(yamlSection, k["name"])
      # outputFunction = 'value => Number(value),' if m.type == 'datetime' # (from store to output)
      outputFunction = "value => value ? Number(value) : undefined," if k["name"] == 'position'# For React-selct
      outputFunction = "valueOption => valueOption ? Number(valueOption.id) : valueOption," if k["name"].last(3) == '_id'# For React-selct
      outputFunction = "valueOption => valueOption ? valueOption.id : valueOption," if k["name"] == 'status_id'# For React-selct #Not convert to number
      outputFunction = "valueOption => { const result = []; if(valueOption){ valueOption.forEach(function(item, index, array) { result.push(item.id) }); return result; } else { return valueOption; } }," if k["name"].last(3) == 'ids'# For React-selct
      next if outputFunction == ''
      next unless do_process_column(yamlSection, k["name"])
      key = prefix + column_change(@l, name_change(k["name"]))
      outputObject[key] = "#{outputFunction}" if outputFunction != '' # (from input to store)
    end
    add_extra_fields_before(yamlSection, c.name) { |k| new_field_proc.call(k) }
    # outputFunction = 'value => Number(value),' if m.type == 'datetime' # (from store to output)
    outputFunction = "value => value ? Number(value) : undefined," if c.name == 'position'# For React-selct
    outputFunction = "valueOption => valueOption ? Number(valueOption.id) : valueOption," if c.name.last(3) == '_id'# For React-selct
    outputFunction = "valueOption => valueOption ? valueOption.id : valueOption," if c.name == 'status_id'# For React-selct #Not convert to number
    outputFunction = "valueOption => { const result = []; if(valueOption){ valueOption.forEach(function(item, index, array) { result.push(item.id) }); return result; } else { return valueOption; } }," if c.name.last(3) == 'ids'# For React-selct
    # next if outputFunction == ''
    next unless do_process_column(yamlSection, c.name)

    key = prefix + column_change(@l, name_change(c.name))
    outputObject[key] = "#{outputFunction}" if outputFunction != '' # (from input to store)

    add_extra_fields(yamlSection, c.name) { |k| new_field_proc.call(k) }

  end
  outputObject
end

def do_loop_all_keys_return_outputs(object, prefix="", addMain=true, r={})
  # Add fields of itself
  r.merge!(add_output(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.merge!(add_output(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_outputs(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end
# ==================================form Output end==================

# =============================form Heler functions==========================
def add_form_helper(table, prefix, yamlSection)
  formHelperObject = Hash.new(0)
  table_columns = table.columns.reject do |column|
    column.name =~ /^(id|user_id|created_at|updated_at)$/
  end
  table_columns.each do |c|
    next unless do_process_column(yamlSection, c.name)
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      next unless do_process_column(yamlSection, k["name"])
      column_name = column_change(@l, name_change(k["name"]))
      key = prefix + column_name
      functionPrefix = prefix.gsub '[].', '_'

      column_properties = yamlSection['column_properties']
      t =  column_properties ? column_properties[k["name"]] : false
      if t && t.key?('onChange')
        formHelperObject[key] = {onInit: "#{functionPrefix.camelize(:lower)}#{table.name.demodulize.singularize.camelize(:lower)}#{column_name.camelize}"}
      else
        # Nothing...
      end
    end
    add_extra_fields_before(yamlSection, c.name) { |k| new_field_proc.call(k) }
    column_name = column_change(@l, name_change(c.name))
    key = prefix + column_name
    functionPrefix = prefix.gsub '[].', '_'

    column_properties = yamlSection['column_properties']
    t =  column_properties ? column_properties[c.name] : false
    if t && t.key?('onChange')
      formHelperObject[key] = {onInit: "#{functionPrefix.camelize(:lower)}#{table.name.demodulize.singularize.camelize(:lower)}#{column_name.camelize}"}
    else
      # Nothing...
    end

    add_extra_fields(yamlSection, c.name) { |k| new_field_proc.call(k) }



  end






  formHelperObject
end

def do_loop_all_keys_return_form_helper(object, prefix="", addMain=true, r={})
  # Add fields of itself
  r.merge!(add_form_helper(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    r.merge!({"#{relationship_name}": {onInit: "#{object[k]['table'].demodulize.pluralize.camelize(:lower)}"}})
    prefix2 = "#{prefix}#{relationship_name}[]."
    r.merge!(add_form_helper(object[k]['table'].constantize, prefix2, object[k]))
    do_loop_all_keys_return_form_helper(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r
end

# ==================================form Heler functions end==================
def allQueriesFunctionFromFetchFunctions(object, addMain=true, r=[])
  # Add fields of itself
  r.concat(get_import_queries_form_fetch_functions(name)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    r.concat(get_import_queries_form_fetch_functions(object[k]['table']))
    allQueriesFunctionFromFetchFunctions(object[k]['nested'], false, r) if object[k].key?('nested')
  end
  r
end

# ==========Other Function=========
# Form Helper Functions:
# r = true not working because inner loop result overwriten by outer loop.
def do_loop_all_keys_find_sale_m_txn_detail(object, r='')
  # No Add fields of itself
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    r = 'Sale::MTxnDetail' if object[k]['table'] == 'Sale::MTxnDetail'
    do_loop_all_keys_find_sale_m_txn_detail(object[k]['nested'], r) if object[k].key?('nested')
  end
  r
end

def form_table_columns_array(columnsCount, returnWidth=false)
  i = 1
  array = []
  totalWidth = 0
  until i > columnsCount  do
    array << {accessor: "label#{i}", Header: "", width: 100, style: {textAlign: 'right', backgroundColor: 'lightgray'}}
     array << {accessor: "value#{i}", Header: "", width: 200}
     totalWidth += 300
     i +=1;
  end
  returnWidth ? totalWidth + 2 : array
end

def form_control(indent='          ')
  """#{indent}<FormControls
 #{indent}form={form}
 #{indent}controls={{
 #{indent}  onSubmit: true, onReset: true
 #{indent}}}/>"""
end

# Form Functions===============================================
# its mobx form fields like.
def do_loop_all_import_nested_table_level1(object, prefix="", addMain=true, r=[])
  # loop all object
  nestedTablesComponent = []
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_CP = object[k]['table'].demodulize.camelize.pluralize
    relationship_name_CP =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'].camelize.pluralize : "#{table_CP}"
    prefix2 = "#{prefix}#{relationship_name_CP}"
    nestedTablesComponent << "FormTable#{prefix2}"
    r << "import FormTable#{prefix2} from './_FormTable#{prefix2}';"
    # We Not need this to loop over all levels. otherwise we have to make function and call itself here.. if object[k].key?('nested')

    # # If We Need Nested Table Imports...........
    # # Other Task for nested table import for columns..
    # columnProperties = object[k]['column_properties']
    # addColumns = object[k]['calculated_fields_after']
    # # @nested_column_grid = (editable_attributes(is_nested_form.name)).zip( @yaml['nested_form']).map(&:compact)
    # nested_column_with_extra = mergeColumnsProperties(is_nested_form.name, @name, columnProperties, addColumns)
    # array_field = relationship_name
    # # Must Run This Function to get Imports
    # # For Second Table it will erase first Import fix it..
    # fieldsNested = getFields(nested_column_with_extra, "        ", array_field, 130, false, "form", true){ }

  end
  # importComponentNested = get_import_component()
  r

end

# this also mobx form fields like.
def do_loop_all_nested_form_level1_output(object, prefix="", addMain=true, r=[])

  # Add fields of itself
  # r.concat(add_field(@table.constantize, "", object)) if addMain
  object.keys.each do |k| # Loop Through all keys
    next unless object[k].is_a?(Hash) && object[k].key?('table') && !object[k]['noGenerateTable']  # If table key not exist we not process it.
    table_UdP = object[k]['table'].demodulize.underscore.downcase.pluralize
    table_CP = object[k]['table'].demodulize.camelize.pluralize
    relationship_name_CP =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'].camelize.pluralize : "#{table_CP}"
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    prefix2 = "#{prefix}#{relationship_name_CP}"
    r <<   """
              {form.has('#{relationship_name}') &&
                <div style={{width: '100%', background: 'bisque'}}>
                  <FormTable#{prefix2}
                    form={form} parentField={form}
                  />
                </div>}"""
    # prefix2 = "#{prefix}#{relationship_name}[]."
    # r.concat(add_field(object[k]['table'].constantize, prefix2, object[k]))
    # do_loop_all_keys_return_fields(object[k]['nested'], prefix2, false, r) if object[k].key?('nested')
  end
  r


end


