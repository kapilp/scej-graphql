# very useful function:
def deep_set(hash, value, *keys)
  keys[0...-1].inject(hash) do |acc, h|
    acc.public_send(:[], h)
  end.public_send(:[]=, keys.last, value)
end

def add_sub_field_keys(c, object)
  r = ''
  column_properties = object['view_column_properties']
  t =  column_properties ? column_properties[c] : false
  if t && t.key?('view_fetch_field_keys')
    keys_string = t['view_fetch_field_keys'].join(", ")
    r = " {#{keys_string}}" if keys_string != ""
  elsif c.last(3) == '_id'
    r = " { id slug }"
  end
  r
end

# =============================preview query==========================
def add_fields_object(table, prefix, yamlConfig, columns_hide = /^(user_id|updated_at)$/)
  typesObject = Hash.new(0)
  table_columns = table.columns.reject do |column|
    column.name =~ columns_hide
  end
  table_columns.each do |c|
    # binding.pry if  yamlConfig.key?('columns_only_process_everywhere')
    new_field_proc = Proc.new do |k|
      next if k["type"]=='calc'
      next unless k['addMobxFields']
      next unless do_process_column(yamlConfig, k["name"])
      key = prefix + column_change(@l, name_change(k["name"]))
      key = key + add_sub_field_keys(k["name"], @yamlConfig[table.name])
      typesObject[key] = '' if k['addGraphFields']
    end

    next unless do_process_column(yamlConfig, c.name)
    add_extra_fields_before(yamlConfig, c.name) { |k| new_field_proc.call(k) }

    key = prefix + column_change(@l, name_change(c.name))
    key = key + add_sub_field_keys(c.name, @yamlConfig[table.name])
    typesObject[key] = ''
    # 2. Add Fields
    add_extra_fields(yamlConfig, c.name) { |k| new_field_proc.call(k) }



  end
  typesObject
end

# This is very useful example
def do_loop_all_keys_query(object, prefix=[:edges, :node], addMain=true, r={}, key=[], addNested=true, columns_hide = /^(user_id|updated_at)$/, table_key='table_view')


  # Add fields of itself
  table = object.is_a?(Hash) && object.key?(table_key) ? object[table_key].constantize : @table.constantize
  node = {edges: {node: {}}}
  if addMain
    fields = add_fields_object(table, "", object, columns_hide)
    if prefix==[:edges, :node]
      r.merge!(node)
      deep_set(r, fields, *prefix)
    else
      r.merge!(fields)
    end
  end


  object.keys.each do |k| # Loop Through all keys
    next unless addNested
    next unless object[k].is_a?(Hash) && object[k].key?(table_key) # If table key not exist we not process it.
    table_UdP = object[k][table_key].demodulize.underscore.downcase.pluralize
    relationship_name =  object[k].is_a?(Hash) && object[k].key?('table_relationship_name') ? object[k]['table_relationship_name'] : "#{table_UdP}"
    # r << "#{prefix}#{relationship_name}"
    prefix2 = [*prefix, relationship_name]
    value = add_fields_object(object[k][table_key].constantize, '', object[k], columns_hide)
    deep_set(r, value, *prefix2)
    do_loop_all_keys_query(object[k]['nested'], prefix2, false, r, key, addNested, columns_hide, table_key) if object[k].key?('nested')
  end


  r
end
# ==================================preview query end==================

def query_standard_import
  standard_imports({
  'graphql'=>'',
  'gql'=>'',
  })
end