def loop_classes(return_all=false)
  r = [
    # User::User
    { :table => 'User::Contact' },
    { :table => 'User::Company' },
    { :table => 'User::Company', :config => 'User::CompanySignUp' },
    { :table => 'User::CompanyOwnership' },
    { :table => 'User::AccountType' },
    { :table => 'User::Account' },
    { :table => 'User::Account', :config => 'User::AccountSignIn' },
    { :table => 'User::AccountView' },
    { :table => 'User::AuthenticationToken' },
    { :table => 'User::Role' },
    { :table => 'User::Address' },
    { :table => 'User::Country' },
    { :table => 'User::State' },
    { :table => 'User::TaxType' },
    { :table => 'User::TaxRate' },

    { :table => 'Material::MaterialType' },
    { :table => 'Material::Material' },
    { :table => 'Material::MetalPurity' },
    { :table => 'Material::GemShape' },
    { :table => 'Material::GemClarity' },
    { :table => 'Material::Color' },
    { :table => 'Material::GemSize' },
    { :table => 'Material::Accessory' },
    { :table => 'Material::GemClaritySize' },
    { :table => 'Material::Locker' },
    { :table => 'Material::RateOn' },
    { :table => 'Material::GemType' },

    { :table => 'Design::Collection' },
    { :table => 'Design::MakingType' },
    { :table => 'Design::SettingType' },
    { :table => 'Design::Style' },
    { :table => 'Design::StyleView' },
    { :table => 'Design::StyleDiamond' },

    { :table => 'Design::Diamond' },
    { :table => 'Design::DiamondView' },
    { :table => 'Design::JobDiamond' },
    # { :table => 'Design::Album' },
    { :table => 'Design::Image' },

    { :table => 'Sale::MTxnType' },
    { :table => 'Sale::MTxn' },
    { :table => 'Sale::OrderType' },
    { :table => 'Sale::Order' },
    { :table => 'Sale::OrderView' },
    { :table => 'Sale::Job' },
    { :table => 'Sale::JobView' },
    { :table => 'Sale::Sale' },
    { :table => 'Sale::SaleView' },
    { :table => 'Sale::SaleDetail' },
    { :table => 'Sale::Memo' },
    { :table => 'Sale::MemoView' },
    { :table => 'Sale::MTxnView' },
    { :table => 'Sale::MTxnDetail' },
    { :table => 'Sale::MTxnDetailView' },
    { :table => 'Sale::MStock' },

    { :table => 'Mfg::Priority' },
    { :table => 'Mfg::ProductType' },
    { :table => 'Mfg::Department' },
    { :table => 'Mfg::DustResource' },

    { :table => 'Mfg::WaxSetting' },
    { :table => 'Mfg::WaxSettingView' },
    { :table => 'Mfg::Casting' },
    { :table => 'Mfg::CastingView' },
    { :table => 'Mfg::MetalIssue' },
    { :table => 'Mfg::MetalIssueView' },
    { :table => 'Mfg::MfgTxn' },
    { :table => 'Mfg::MfgTxnView' },
    { :table => 'Mfg::Refining' },
    { :table => 'Mfg::RefiningView' },


    { :table => 'Design::StyleTransition' },
    { :table => 'Mfg::CastingTransition' },
    { :table => 'Sale::JobTransition' },
    { :table => 'Sale::MTxnTransition' },
    { :table => 'Sale::OrderTransition' },
    { :table => 'User::AccountTransition' },
    { :table => 'Mfg::WaxSettingTransition' },
    { :table => 'Mfg::MetalIssueTransition' },

    { :table => 'Mfg::MfgTxnTransition' },
    { :table => 'Mfg::RefiningTransition' },
    { :table => 'Sale::MemoTransition' },
    { :table => 'Sale::SaleTransition' }
  ]
  if return_all
    r
  elsif options.one
    # %w(@oneModel).map {|t| {:table => t } }
    # r = [ {:table => @oneModel } ]
    r = [

    { :table => 'User::Company' },
    { :table => 'User::Company', :config => 'User::CompanySignUp' },
    { :table => 'User::Company', :config => 'User::CompanyChange' },
    { :table => 'User::Account' },
    { :table => 'User::Account', :config => 'User::AccountSignIn' },
    # { :table => 'Design::Collection' },
    # { :table => 'Design::Style' },

    # { :table => 'Sale::MTxn' },
    # { :table => 'Sale::Order' },
    # { :table => 'Sale::Job' },
    # { :table => 'Sale::Sale' },
    # { :table => 'Sale::Memo' },
    # { :table => 'Sale::MStock' },

    # { :table => 'Mfg::WaxSetting' },
    # { :table => 'Mfg::Casting' },
    # { :table => 'Mfg::CastingView' },
    # { :table => 'Mfg::MetalIssue' },
    # { :table => 'Mfg::MetalIssueView' },
    # { :table => 'Mfg::MfgTxn' },
    # { :table => 'Mfg::MfgTxnView' },
    # { :table => 'Mfg::Refining' },
    # { :table => 'Mfg::RefiningView' },


    # { :table => 'User::Company' },
    # { :table => 'User::Company', :config => 'User::CompanySignUp' },
    # { :table => 'User::Account' },
    # { :table => 'User::Account', :config => 'User::AccountSignIn' },
    # { :table => 'User::Account', :config => 'User::AccountProfileEdit' },
    # { :table => 'User::Role' },
    # { :table => 'Design::Style' },
    # { :table => 'Sale::Order' },
    ]
  else
    r
  end
end

def data_block_get(dest_file)
  @dataBlock0A = get_part0_a(dest_file)
    @dataBlock1A = get_part1_a(dest_file)
    @dataBlock1B = get_part1_b(dest_file)
    @dataBlock1C = get_part1_c(dest_file)
    @dataBlockServer1A = get_part_server1_a(dest_file)
    @dataBlockServer1B = get_part_server1_b(dest_file)
    @dataBlockServer1C = get_part_server1_c(dest_file)
    @dataBlockServer2A = get_part_server2_a(dest_file)
    @dataBlockServer2B = get_part_server2_b(dest_file)
    @dataBlockServer3A = get_part_server3_a(dest_file)
    @dataBlockServer3B = get_part_server3_b(dest_file)
    @dataBlockServer4A = get_part_server4_a(dest_file)
    @dataBlockServer4B = get_part_server4_b(dest_file)
    @dataBlockServer5A = get_part_server5_a(dest_file)
    @dataBlockServer5B = get_part_server5_b(dest_file)
    @dataBlockServer6A = get_part_server6_a(dest_file)
    @dataBlockServer6B = get_part_server6_b(dest_file)
    @dataBlock2A = get_part2_a(dest_file)
    @dataBlock2B = get_part2_b(dest_file)
    @dataBlock3A = get_part3_a(dest_file)
    @dataBlock4A = get_part4_a(dest_file)
    @dataBlock5A = get_part5_a(dest_file)
    @dataBlock5B = get_part5_b(dest_file)
    @dataBlock6A = get_part6_a(dest_file)
    @dataBlock7A = get_part7_a(dest_file)
    @dataBlockJSX1A = get_jsx_part1_a(dest_file)
    @todoBlockA =  get_todo_block(dest_file)
    @importsBlockA =  get_imports_block(dest_file)
    @importsBlockB =  get_imports_block_b(dest_file)
    @settingsBlockA =  get_settings_block(dest_file)
end

def get_part0_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part0A(.|\n)*\/\/ Part0A)/).last # If No data is found it returns nil.
  end
    s = ["""// Part0A
// Part0A"""] if !s
  s
end

def get_part1_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part1A(.|\n)*\/\/ Part1A)/).last
  end
    s = ["""// Part1A
// Part1A"""] if !s
  s
end

def get_part1_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part1B(.|\n)*\/\/ Part1B)/).last
  end
    s = ["""// Part1B
// Part1B"""] if !s
  s
end

def get_part1_c(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part1C(.|\n)*\/\/ Part1C)/).last
  end
    s = ["""// Part1C
// Part1C"""] if !s
  s
end

def get_part_server1_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part1A(.|\n)*\# Part1A)/).last
  end
    s = ["""# Part1A
# Part1A"""] if !s
  s
end

def get_part_server1_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part1B(.|\n)*\# Part1B)/).last
  end
    s = ["""# Part1B
# Part1B"""] if !s
  s
end
def get_part_server1_c(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part1C(.|\n)*\# Part1C)/).last
  end
    s = ["""# Part1C
# Part1C"""] if !s
  s
end

def get_part_server2_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part2A(.|\n)*\# Part2A)/).last
  end
    s = ["""# Part2A
# Part2A"""] if !s
  s
end

def get_part_server2_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part2B(.|\n)*\# Part2B)/).last
  end
    s = ["""# Part2B
# Part2B"""] if !s
  s
end

def get_part_server3_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part3A(.|\n)*\# Part3A)/).last
  end
    s = ["""# Part3A
# Part3A"""] if !s
  s
end

def get_part_server3_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part3B(.|\n)*\# Part3B)/).last
  end
    s = ["""# Part3B
# Part3B"""] if !s
  s
end

def get_part_server4_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part4A(.|\n)*\# Part4A)/).last
  end
    s = ["""# Part4A
# Part4A"""] if !s
  s
end

def get_part_server4_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part4B(.|\n)*\# Part4B)/).last
  end
    s = ["""# Part4B
# Part4B"""] if !s
  s
end
def get_part_server5_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part5A(.|\n)*\# Part5A)/).last
  end
    s = ["""# Part5A
# Part5A"""] if !s
  s
end

def get_part_server5_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part5B(.|\n)*\# Part5B)/).last
  end
    s = ["""# Part5B
# Part5B"""] if !s
  s
end
def get_part_server6_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part6A(.|\n)*\# Part6A)/).last
  end
    s = ["""# Part6A
# Part6A"""] if !s
  s
end

def get_part_server6_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\# Part6B(.|\n)*\# Part6B)/).last
  end
    s = ["""# Part6B
# Part6B"""] if !s
  s
end

def get_part2_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part2A(.|\n)*\/\/ Part2A)/).last
  end


  s = ["""// Part2A
// Part2A"""] if !s
  s
end

def get_part2_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part2B(.|\n)*\/\/ Part2B)/).last
  end

  s = ["""// Part2B
// Part2B"""] if !s
  s
end

def get_part3_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part3A(.|\n)*\/\/ Part3A)/).last
  end

  s = ["""// Part3A
// Part3A"""] if !s
  s
end

def get_part4_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part4A(.|\n)*\/\/ Part4A)/).last
  end

  s = ["""// Part4A
// Part4A"""] if !s
  s
end

def get_part5_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part5A(.|\n)*\/\/ Part5A)/).last
  end

  s = ["""// Part5A
// Part5A"""] if !s
  s
end

def get_part5_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\{\/\* Part5B\*\/(.|\n)*\{\/\* Part5B\*\/\})/).last
  end

  s = ["""{/* Part5B*/}
{/* Part5B*/}"""] if !s
  s
end

def get_part6_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part6A(.|\n)*\/\/ Part6A)/).last
  end

  s = ["""// Part6A
// Part6A"""] if !s
  s
end

def get_part7_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ Part7A(.|\n)*\/\/ Part7A)/).last
  end

  s = ["""// Part7A
// Part7A"""] if !s
  s
end

def get_jsx_part1_a(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\{\/\* JSX Part1A\*\/(.|\n)*\{\/\* JSX Part1A\*\/\})/).last
  end

  s = ["""{/* JSX Part1A*/}
{/* JSX Part1A*/}"""] if !s
  s
end

def get_todo_block(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ TODO(.|\n)*\/\/ TODO)/).last
  end

  s = ["""// TODO
// TODO"""] if !s
  s
end

def get_imports_block(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ IMPORTSA(.|\n)*\/\/ IMPORTSA)/).last
  end

  s = ["""// IMPORTSA
// IMPORTSA"""] if !s
  s
end

def get_imports_block_b(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ IMPORTSB(.|\n)*\/\/ IMPORTSB)/).last
  end

  s = ["""// IMPORTSB
// IMPORTSB"""] if !s
  s
end

def get_settings_block(dest_file_name)
  s = false
  if Pathname(dest_file_name).exist?
    datastream = File.read(dest_file_name)
    s = datastream.scan(/(\/\/ SETTINGS(.|\n)*\/\/ SETTINGS)/).last
  end

  s = ["""// SETTINGS
// SETTINGS"""] if !s
  s
end

def import_modules_function(data)
  result=[]
  @namespaces = []
  data.map do |o|
    if o[:import_modules].kind_of?(String)
      import2 = o[:defaultImp] ? "* as #{o[:import_modules]}" : "#{o[:import_modules]}"
      @namespaces << o[:import_modules]
      result << "import #{import2} from '#{o[:module]}';"
    else
      result << "import { #{o[:import_modules].join(", ")} } from '#{o[:module]}';"
      @namespaces.concat(o[:import_modules])
    end
  end
  result
end

