# == Schema Information
#
# Table name: user_accounts
#
#  id                     :integer          not null, primary key
#  user_account_type_id   :integer          not null
#  position               :integer
#  join_date              :datetime
#  slug                   :string           not null
#  account_name           :string           not null
#  salary                 :integer          default(0)
#  user_role_id           :integer          not null
#  parent_id              :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

require 'rails_helper'

RSpec.describe User::Account, type: :model do

  describe 'associations' do
    it 'has many address' do
      ar = described_class.reflect_on_association(:user_addresses)
      expect(ar.macro) == :has_many
    end
  end
  describe 'associations' do
    it 'has many user_companies' do
      ar = described_class.reflect_on_association(:user_companies)
      expect(ar.macro) == :has_many
    end
  end
  describe 'associations' do
    it 'has many user_companies' do
      ar = described_class.reflect_on_association(:user_company_ownerships)
      expect(ar.macro) == :has_many
    end
  end
  
  # Belongs_To Associations: _____
  describe 'associations' do
    it 'belongs_to user_account_type' do
      ar = described_class.reflect_on_association(:user_account_type)
      expect(ar.macro) == :belongs_to
    end
  end
  describe 'associations' do
    it 'belongs_to user_role' do
      ar = described_class.reflect_on_association(:user_role)
      expect(ar.macro) == :belongs_to
    end
  end

  
end
