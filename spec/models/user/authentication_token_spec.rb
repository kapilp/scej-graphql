# == Schema Information
#
# Table name: user_authentication_tokens
#
#  id              :integer          not null, primary key
#  body            :string
#  user_account_id :integer
#  last_used_at    :datetime
#  expires_in      :integer
#  ip_address      :string
#  user_agent      :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe User::AuthenticationToken, type: :model do
  # Belongs_To Associations: _____
  describe 'associations' do
    it 'belongs_to user_account_type' do
      ar = described_class.reflect_on_association(:user_account)
      expect(ar.macro) == :belongs_to
    end
  end
  
  
  
end
